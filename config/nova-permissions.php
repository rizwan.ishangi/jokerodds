<?php

return [
    /*
    |--------------------------------------------------------------------------
    | User model class
    |--------------------------------------------------------------------------
    */

    'user_model' => 'App\Models\Admin',

    /*
    |--------------------------------------------------------------------------
    | Nova User resource tool class
    |--------------------------------------------------------------------------
    */

    'user_resource' => 'App\Nova\Admin',

    /*
    |--------------------------------------------------------------------------
    | The group associated with the resource
    |--------------------------------------------------------------------------
    */

    'role_resource_group' => 'CMS',

    /*
    |--------------------------------------------------------------------------
    | Database table names
    |--------------------------------------------------------------------------
    | When using the "HasRoles" trait from this package, we need to know which
    | table should be used to retrieve your roles. We have chosen a basic
    | default value but you may easily change it to any table you like.
    */

    'table_names' => [
        'roles' => 'roles',

        'role_permission' => 'role_permission',

        'role_user' => 'admin_role',

        'users' => 'admins',
    ],

    /*
    |--------------------------------------------------------------------------
    | Application Permissions
    |--------------------------------------------------------------------------
    */

    'permissions' => [
        'view banners' => [
            'display_name' => 'View banners',
            'description'  => 'Can view banners',
            'group'        => 'Banner',
        ],

        'create banners' => [
            'display_name' => 'Create banners',
            'description'  => 'Can create banners',
            'group'        => 'Banner',
        ],

        'edit banners' => [
            'display_name' => 'Edit banners',
            'description'  => 'Can edit banners',
            'group'        => 'Banner',
        ],

        'delete banners' => [
            'display_name' => 'Delete banners',
            'description'  => 'Can delete banners',
            'group'        => 'Banner',
        ],

        'restore banners' => [
            'display_name' => 'Restore banners',
            'description'  => 'Can restore banners',
            'group'        => 'Banner',
        ],

        'force delete banners' => [
            'display_name' => 'Force delete banners',
            'description'  => 'Can force delete banners',
            'group'        => 'Banner',
        ],



        'view bookmakers' => [
            'display_name' => 'View bookmakers',
            'description'  => 'Can view bookmakers',
            'group'        => 'Bookmaker',
        ],

        'create bookmakers' => [
            'display_name' => 'Create bookmakers',
            'description'  => 'Can create bookmakers',
            'group'        => 'Bookmaker',
        ],

        'edit bookmakers' => [
            'display_name' => 'Edit bookmakers',
            'description'  => 'Can edit bookmakers',
            'group'        => 'Bookmaker',
        ],

        'delete bookmakers' => [
            'display_name' => 'Delete bookmakers',
            'description'  => 'Can delete bookmakers',
            'group'        => 'Bookmaker',
        ],

        'restore bookmakers' => [
            'display_name' => 'Restore bookmakers',
            'description'  => 'Can restore bookmakers',
            'group'        => 'Bookmaker',
        ],

        'force delete bookmakers' => [
            'display_name' => 'Force delete bookmakers',
            'description'  => 'Can force delete bookmakers',
            'group'        => 'Bookmaker',
        ],

        'view leagues' => [
            'display_name' => 'View leagues',
            'description'  => 'Can view leagues',
            'group'        => 'League',
        ],

        'create leagues' => [
            'display_name' => 'Create leagues',
            'description'  => 'Can create leagues',
            'group'        => 'League',
        ],

        'edit leagues' => [
            'display_name' => 'Edit leagues',
            'description'  => 'Can edit leagues',
            'group'        => 'League',
        ],

        'delete leagues' => [
            'display_name' => 'Delete leagues',
            'description'  => 'Can delete leagues',
            'group'        => 'League',
        ],

        'restore leagues' => [
            'display_name' => 'Restore leagues',
            'description'  => 'Can restore leagues',
            'group'        => 'League',
        ],

        'force delete leagues' => [
            'display_name' => 'Force delete leagues',
            'description'  => 'Can force delete leagues',
            'group'        => 'League',
        ],

        'add top leagues' => [
            'display_name' => 'Add top leagues',
            'description'  => 'Can add leagues',
            'group'        => 'Top Leagues',
        ],

        'remove top leagues' => [
            'display_name' => 'Remove top leagues',
            'description'  => 'Can remove top leagues',
            'group'        => 'Top Leagues',
        ],



        'view sports' => [
            'display_name' => 'View sports',
            'description'  => 'Can view sports',
            'group'        => 'Sport',
        ],

        'create sports' => [
            'display_name' => 'Create sports',
            'description'  => 'Can create sports',
            'group'        => 'Sport',
        ],

        'edit sports' => [
            'display_name' => 'Edit sports',
            'description'  => 'Can edit sports',
            'group'        => 'Sport',
        ],

        'delete sports' => [
            'display_name' => 'Delete sports',
            'description'  => 'Can delete sports',
            'group'        => 'Sport',
        ],

        'restore sports' => [
            'display_name' => 'Restore sports',
            'description'  => 'Can restore sports',
            'group'        => 'Sport',
        ],

        'force delete sports' => [
            'display_name' => 'Force delete sports',
            'description'  => 'Can force delete sports',
            'group'        => 'Sport',
        ],



        'view text blocks' => [
            'display_name' => 'View text blocks',
            'description'  => 'Can view text blocks',
            'group'        => 'Text Blocks',
        ],

        'create text blocks' => [
            'display_name' => 'Create text blocks',
            'description'  => 'Can create text blocks',
            'group'        => 'Text Blocks',
        ],

        'edit text blocks' => [
            'display_name' => 'Edit text blocks',
            'description'  => 'Can edit text blocks',
            'group'        => 'Text Blocks',
        ],

        'delete text blocks' => [
            'display_name' => 'Delete text blocks',
            'description'  => 'Can delete text blocks',
            'group'        => 'Text Blocks',
        ],

        'restore text blocks' => [
            'display_name' => 'Restore text blocks',
            'description'  => 'Can restore text blocks',
            'group'        => 'Text Blocks',
        ],

        'force delete text blocks' => [
            'display_name' => 'Force delete text blocks',
            'description'  => 'Can force delete text blocks',
            'group'        => 'Text Blocks',
        ],


        'view users' => [
            'display_name' => 'View users',
            'description'  => 'Can view users',
            'group'        => 'User',
        ],

        'create users' => [
            'display_name' => 'Create users',
            'description'  => 'Can create users',
            'group'        => 'User',
        ],

        'edit users' => [
            'display_name' => 'Edit users',
            'description'  => 'Can edit users',
            'group'        => 'User',
        ],

        'delete users' => [
            'display_name' => 'Delete users',
            'description'  => 'Can delete users',
            'group'        => 'User',
        ],

        'restore users' => [
            'display_name' => 'Restore users',
            'description'  => 'Can restore users',
            'group'        => 'User',
        ],

        'force delete users' => [
            'display_name' => 'Force delete users',
            'description'  => 'Can force delete users',
            'group'        => 'User',
        ],


        'view pages' => [
            'display_name' => 'View pages',
            'description'  => 'Can view pages',
            'group'        => 'Pages',
        ],

        'create pages' => [
            'display_name' => 'Create pages',
            'description'  => 'Can create pages',
            'group'        => 'Pages',
        ],

        'edit pages' => [
            'display_name' => 'Edit pages',
            'description'  => 'Can edit pages',
            'group'        => 'Pages',
        ],

        'delete pages' => [
            'display_name' => 'Delete pages',
            'description'  => 'Can delete pages',
            'group'        => 'Pages',
        ],

        'restore pages' => [
            'display_name' => 'Restore pages',
            'description'  => 'Can restore pages',
            'group'        => 'Pages',
        ],

        'force delete pages' => [
            'display_name' => 'Force delete pages',
            'description'  => 'Can force delete pages',
            'group'        => 'Pages',
        ],


        'view locations' => [
            'display_name' => 'View locations',
            'description'  => 'Can view locations',
            'group'        => 'Locations',
        ],

        'create locations' => [
            'display_name' => 'Create locations',
            'description'  => 'Can create locations',
            'group'        => 'Locations',
        ],

        'edit locations' => [
            'display_name' => 'Edit locations',
            'description'  => 'Can edit locations',
            'group'        => 'Locations',
        ],

        'delete locations' => [
            'display_name' => 'Delete locations',
            'description'  => 'Can delete locations',
            'group'        => 'Locations',
        ],

        'restore locations' => [
            'display_name' => 'Restore locations',
            'description'  => 'Can restore locations',
            'group'        => 'Locations',
        ],

        'force delete locations' => [
            'display_name' => 'Force delete locations',
            'description'  => 'Can force delete locations',
            'group'        => 'Locations',
        ],


        'view events' => [
            'display_name' => 'View events',
            'description'  => 'Can view events',
            'group'        => 'Events',
        ],

        'create events' => [
            'display_name' => 'Create events',
            'description'  => 'Can create events',
            'group'        => 'Events',
        ],

        'edit events' => [
            'display_name' => 'Edit events',
            'description'  => 'Can edit events',
            'group'        => 'Events',
        ],

        'delete events' => [
            'display_name' => 'Delete events',
            'description'  => 'Can delete events',
            'group'        => 'Events',
        ],

        'restore events' => [
            'display_name' => 'Restore events',
            'description'  => 'Can restore events',
            'group'        => 'Events',
        ],

        'force delete events' => [
            'display_name' => 'Force delete events',
            'description'  => 'Can force delete events',
            'group'        => 'Events',
        ],

        'add top events' => [
            'display_name' => 'Add top events',
            'description'  => 'Can add events',
            'group'        => 'Top Events',
        ],

        'remove top events' => [
            'display_name' => 'Remove top events',
            'description'  => 'Can remove top events',
            'group'        => 'Top Events',
        ],


        'view teams' => [
            'display_name' => 'View teams',
            'description'  => 'Can view teams',
            'group'        => 'Teams',
        ],

        'create teams' => [
            'display_name' => 'Create teams',
            'description'  => 'Can create teams',
            'group'        => 'Teams',
        ],

        'edit teams' => [
            'display_name' => 'Edit teams',
            'description'  => 'Can edit teams',
            'group'        => 'Teams',
        ],

        'delete teams' => [
            'display_name' => 'Delete teams',
            'description'  => 'Can delete teams',
            'group'        => 'Teams',
        ],

        'restore teams' => [
            'display_name' => 'Restore teams',
            'description'  => 'Can restore teams',
            'group'        => 'Teams',
        ],

        'force delete teams' => [
            'display_name' => 'Force delete teams',
            'description'  => 'Can force delete teams',
            'group'        => 'Teams',
        ],


        'view admins' => [
            'display_name' => 'View admins',
            'description'  => 'Can view admins',
            'group'        => 'Admins',
        ],

        'create admins' => [
            'display_name' => 'Create admins',
            'description'  => 'Can create admins',
            'group'        => 'Admins',
        ],

        'edit admins' => [
            'display_name' => 'Edit admins',
            'description'  => 'Can edit admins',
            'group'        => 'Admins',
        ],

        'delete admins' => [
            'display_name' => 'Delete admins',
            'description'  => 'Can delete admins',
            'group'        => 'Admins',
        ],


        'view roles' => [
            'display_name' => 'View roles',
            'description'  => 'Can view roles',
            'group'        => 'Role',
        ],

        'create roles' => [
            'display_name' => 'Create roles',
            'description'  => 'Can create roles',
            'group'        => 'Role',
        ],

        'edit roles' => [
            'display_name' => 'Edit roles',
            'description'  => 'Can edit roles',
            'group'        => 'Role',
        ],

        'delete roles' => [
            'display_name' => 'Delete roles',
            'description'  => 'Can delete roles',
            'group'        => 'Role',
        ],


        'view menus' => [
            'display_name' => 'View menu',
            'description'  => 'Can view menu',
            'group'        => 'Menu',
        ],

        'create menus' => [
            'display_name' => 'Create menus',
            'description'  => 'Can create menus',
            'group'        => 'Menu',
        ],

        'edit menus' => [
            'display_name' => 'Edit menus',
            'description'  => 'Can edit menus',
            'group'        => 'Menu',
        ],

        'delete menus' => [
            'display_name' => 'Delete menus',
            'description'  => 'Can delete menus',
            'group'        => 'Menu',
        ],
    ],
];
