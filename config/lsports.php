<?php

return [
    /**
     * Rms:
     */
    'rmsHosts' => [
        'prematch' => 'prematch-rmq.lsports.eu',
        'inplay' => 'inplay-rmq.lsports.eu',
    ],
    'rmsPort' => 5672,

    /**
     * Common creds
     */
    'username' => env('LSPORTS_USERNAME', 'username'),
    'password' => env('LSPORTS_PASSWORD', 'password'),

    /**
     * Packages
     */
    'prematchGuid' => 'd5898161-c33d-4cf0-996c-11ec7ed97280',
    'inplayGuid' => '13b227bd-3a25-4a35-ae7a-e1baa7b61aa7',

    'prematchPackageId' => '2491',
    'inplayPackageId' => '2492',

    /**
     * Whitelist
     */
    'onlyLeagues' => json_decode(file_get_contents(__DIR__ . '/lsports/leagues.json'), true),

    'onlyCountries' => json_decode(file_get_contents(__DIR__ . '/lsports/locations.json'), true),

    'onlyBookmakers' => json_decode(file_get_contents(__DIR__ . '/lsports/bookmakers.json'), true),
];
