<?php
return [
    /*auth*/
    'token' => env('BET_THREE_SIX_FIVE_TOKEN', 'token'),

    /* url*/
    'base_url' => 'https://api.b365api.com',
    'version' => 'v1',
];
