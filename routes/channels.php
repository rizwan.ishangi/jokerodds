<?php

use Illuminate\Support\Facades\Broadcast;

/*
|--------------------------------------------------------------------------
| Broadcast Channels
|--------------------------------------------------------------------------
|
| Here you may register all of the event broadcasting channels that your
| application supports. The given channel authorization callbacks are
| used to check if an authenticated user can listen to the channel.
|
*/
Broadcast::channel('event-bets.{eventId}', function () {
    return true;
});
Broadcast::channel('test-channel-echo', function () {
    return true;
});

Broadcast::channel('score-event.{eventId}', function () {
    return true;
});

Broadcast::channel('bets-average', function () {
    return true;
});

Broadcast::channel('scores-updated', function () {
    return true;
});
Broadcast::channel('bets-live', function () {
    return true;
});
/*
Broadcast::channel('App.User.{id}', function ($user, $id) {
    return (int) $user->id === (int) $id;
});*/
