<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['middleware' => 'auth:api'], function () {
//    Route::post('logout', 'Auth\LoginController@logout');
//
//    Route::get('/user', 'Auth\UserController@current');
//
//    Route::patch('settings/profile', 'Settings\ProfileController@update');
//    Route::patch('settings/password', 'Settings\PasswordController@update');
});

Route::group(['middleware' => 'guest:api'], function () {
    Route::post('login', 'Auth\LoginController@login');
    Route::post('register', 'Auth\RegisterController@register');

    Route::post('password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail');
    Route::post('password/reset', 'Auth\ResetPasswordController@reset');

    Route::post('email/verify/{user}', 'Auth\VerificationController@verify')->name('verification.verify');
    Route::post('email/resend', 'Auth\VerificationController@resend');

    Route::post('oauth/{driver}', 'Auth\OAuthController@redirectToProvider');
    Route::get('oauth/{driver}/callback', 'Auth\OAuthController@handleProviderCallback')->name('oauth.callback');

    // Tracker
    Route::get('/tracker/{ip}', 'UserTrackerController@index')->name('getTracker');
    Route::post('/tracker', 'UserTrackerController@store')->name('addGame');
    Route::delete('/tracker/{id}', 'UserTrackerController@deleteMatch')->name('deleteGame');
    Route::delete('/tracker-clear/{ip}', 'UserTrackerController@delete')->name('deleteGames');

    // Leagues
    Route::get('/leagues', 'LeagueController@index');
    Route::get('/leagues/top', 'LeagueController@topLeague');
    Route::get('/leagues/{leagueId}/results', 'LeagueController@leagueMatchesResults')->name('leagueMatches.results');
    Route::get('/leagues/{leagueId}', 'LeagueController@leagueMatches')->name('leagueMatches');

    Route::get('/widget/event/{id}', 'WidgetController@index');



    // Standings
    Route::get('/league-standings/{leagueId}', 'StandingsController@leagueStandings')->name('leagueStandings');
    Route::get('/full/league-standings/{leagueId}', 'StandingsController@fullLeagueStandings')->name('fullLeagueStandings');

    // Team
    Route::get('/team/{teamId}', 'TeamController@getTeamInfo')->name('getTeamInfo');

    // Matches
    Route::get('/full/date-matches/{date}', 'MatchController@fullDateMatches')->name('fullDateMatches');
    Route::get('/full/date-matches/{date}/main', 'MatchController@fullDateMatchesMain')->name('fullDateMatchesMain');
    Route::get('/full/date-matches/{date}/rest', 'MatchController@fullDateMatchesRest')->name('fullDateMatchesRest');
    Route::get('/date-matches/{date}', 'MatchController@dateMatches')->name('dateMatches');

    // Bets
    Route::post('/bet-average-price/', 'BetController@getBetAveragePrice')->name('betAveragePrice');
    Route::post('/bet-history/', 'BetController@getBetHistory')->name('betHistory');

    // Events
    Route::get('/events', 'EventController@eventsSidebar')->name('getEventsSidebar');
    Route::get('/event/referral', 'EventController@getReferralLink')->name('ReferralLink');
    Route::get('/event/{id}', 'EventController@index')->name('getEvent');

    // RememberCookie
    Route::post('/get/remember/cookie', 'RememberCookieController@get')->name('getRememberCookie');

    // Tracker
    Route::post('/tracker/bets', 'TrackerController@bets')->name('tracker.bets');
    Route::put('/tracker/bets/remember', 'TrackerController@remember')->name('tracker.bets.remember');
    Route::delete('/tracker/bets/forget/all', 'TrackerController@forgetAll')->name('tracker.bets.forget.all');
    Route::delete('/tracker/bets/forget', 'TrackerController@forget')->name('tracker.bets.forget');

    // Banners
    Route::post('/banners/get', 'BannerController@get')->name('banners.get');
    Route::post('/banner/clicked', 'BannerController@clicked')->name('banners.clicked.banner');
    // Images
    Route::get('/images/{entity}/{id}/{property}/{size}.{ext}', 'DynamicImageController')->name('dynamic-image');
});
