<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/widget-partners/{widget_id}/{event_id}', 'WidgetController@partners');
// Route::get('/test', function () {
//     return view('index');
// });

Route::get('/{any}', 'SpaController')->where('any', '((?!nova).)*');



// Route::get('/{any}', function () {
//     return view('app.index');
// });


// Route::get('/welcome', function () {
//     return view('welcome');
// });

// Route::get('/', function () {
//     return view('next-matches.index');
// });

// Route::get('/next', function () {
//     return view('league-next.index');
// });

// Route::get('/schedule', function () {
//     return view('league-schedule.index');
// });

// Route::get('/results', function () {
//     return view('league-results.index');
// });

// Route::get('/standings', function () {
//     return view('league-standings.index');
// });

// Route::get('/prematch', function () {
//     return view('prematch-odds.index');
// });

// Route::get('/prematch-dnb', function () {
//     return view('prematch-odds-dnb.index');
// });


// Route::get('/prematch-ah', function () {
//     return view('prematch-odds-ah.index');
// });
