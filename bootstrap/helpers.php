<?php

use App\Models\TextBlock;

if (!function_exists('check_base64_image')) {
    /**
     * @param $base64
     * @return bool
     */
    function check_base64_image($base64)
    {
        $img = imagecreatefromstring(base64_decode($base64));
        if (!$img) {
            return false;
        }

        imagepng($img, 'tmp.png');
        $info = getimagesize('tmp.png');

        unlink('tmp.png');

        if ($info[0] > 0 && $info[1] > 0 && $info['mime']) {
            return true;
        }

        return false;
    }
}

if (!function_exists('text_block')) {

    /**
     * @param string $name
     * @return string|null
     */
    function text_block(string $name): ?string
    {
        return TextBlock::getCached()
            ->where('name', $name)
            ->first()->content ?? null;
    }
}

if (!function_exists('menu_get_all')) {
    function menu_get_all(): array
    {
        $menus = [
            'main_menu' => menu_type('main_menu'),
            'secondary_menu' => menu_type('secondary_menu'),
            'vertical_menu' => menu_type('vertical_menu'),
            'currency_menu' => menu_type('currency_menu'),
            'langauge_menu' => menu_type('langauge_menu'),
        ];

        return array_map(function ($item) {
            return $item != '' ? json_decode($item, true) : [];
        }, $menus);
    }
}
