@extends('layout.app')

@section('content')
<div class="content-wrapper content-next-matches">
    @include('partials.breadcrumbs', $breadcrumbs = [
        [
            'name' => 'Odds Comparison',
            'url' => '#'
        ],
        [
            'name' => 'Football',
            'url' => '#'
        ],
        [
            'name' => 'Next Matches'
        ]
    ])

    <div class="pagesearch">
        <label class="search-label">
            <input class="search-input" type="text" placeholder="You can search by Country, League or Team">
        </label>
        <button class="search-btn"></button>
    </div>

    <div class="box no-padding box-table">
        <div class="box-table-headinfo">
            <div class="topline">
                <div class="country">
                    <div class="flag flag-icon flag-peru"></div>
                    <div class="name">Peru</div>
                </div>
                <div class="toggler">
                    <svg> <use xlink:href="#arr-down"/> </svg>
                </div>
            </div>
            <div class="bottomline">
                <div class="division">
                    <div class="icon">
                        <svg> <use xlink:href="#icon-football"/> </svg>
                    </div>
                    <div class="name">Primera Division</div>
                </div>
                <a href="#" class="show-all">
                    <div class="text">Show All Matches</div>
                    <div class="icon"> <svg> <use xlink:href="#triangle-arr-right"/> </svg> </div>
                </a>
            </div>
        </div>
        <div class="box-table-header">
            <div class="cell cell-datetime">03 Oct 2020</div>
            <div class="cell cell-team cell-empty"></div>
            <div class="cell cell-score">Score</div>
            <div class="cell cell-one">1</div>
            <div class="cell cell-x">X</div>
            <div class="cell cell-two">2</div>
            <div class="cell cell-play cell-empty"></div>
        </div>
        <div class="box-table-body">
            <div class="row">
                <div class="cell cell-datetime">
                    <div class="icon icon-finished"></div>
                    <div class="text">08:30</div>
                </div>
                <div class="cell cell-team">
                    <div class="item">
                        <span class="team-logo" style="background-image: url({{ asset('img/team1.png') }})"></span>
                        <div class="team-name team-winner">Alianza Limba</div>
                    </div>
                    <div class="item">
                        <span class="team-logo" style="background-image: url({{ asset('img/team2.png') }})"></span>
                        <div class="team-name">Club Deportivo Llacuabama</div>
                    </div>
                </div>
                <div class="cell cell-score">
                    <span class="bubble">3 : 1</span>
                </div>
                <div class="cell cell-one">
                    <span class="bubble">3.73</span>
                </div>
                <div class="cell cell-x">
                    <span class="bubble">3.73</span>
                </div>
                <div class="cell cell-two">
                    <span class="bubble">3.73</span>
                </div>
                <div class="cell cell-play">
                    <span class="play-btn"></span>
                </div>
            </div>
            <div class="row">
                <div class="cell cell-datetime">
                    <div class="icon icon-finished"></div>
                    <div class="text">15:30</div>
                </div>
                <div class="cell cell-team">
                    <div class="item">
                        <span class="team-logo" style="background-image: url({{ asset('img/team1.png') }})"></span>
                        <div class="team-name team-winner">Alianza Limba</div>
                    </div>
                    <div class="item">
                        <span class="team-logo" style="background-image: url({{ asset('img/team2.png') }})"></span>
                        <div class="team-name">Club Deportivo Llacuabama</div>
                    </div>
                </div>
                <div class="cell cell-score">
                    <span class="bubble">3 : 1</span>
                </div>
                <div class="cell cell-one">
                    <span class="bubble">3.73</span>
                </div>
                <div class="cell cell-x">
                    <span class="bubble">3.73</span>
                </div>
                <div class="cell cell-two">
                    <span class="bubble">3.73</span>
                </div>
                <div class="cell cell-play">
                    <span class="play-btn"></span>
                </div>
            </div>
            <div class="row">
                <div class="cell cell-datetime">
                    <div class="icon icon-playing"></div>
                    <div class="text red">03:18</div>
                </div>
                <div class="cell cell-team">
                    <div class="item">
                        <span class="team-logo"  style="background-image: url({{ asset('img/team1.png') }})"></span>
                        <div class="team-name">Alianza Limba</div>
                    </div>
                    <div class="item">
                        <span class="team-logo"  style="background-image: url({{ asset('img/team2.png') }})"></span>
                        <div class="team-name">Club Deportivo Llacuabama</div>
                    </div>
                </div>
                <div class="cell cell-score">
                    <span class="bubble bubble-red">1 : 0</span>
                </div>
                <div class="cell cell-one">
                    <span class="bubble">3.73</span>
                </div>
                <div class="cell cell-x">
                    <span class="bubble">3.73</span>
                </div>
                <div class="cell cell-two">
                    <span class="bubble">3.73</span>
                </div>
                <div class="cell cell-play">
                    <span class="play-btn"></span>
                </div>
            </div>
            <div class="row">
                <div class="cell cell-datetime">
                    <div class="text">08:30</div>
                </div>
                <div class="cell cell-team">
                    <div class="item">
                        <span class="team-logo" style="background-image: url({{ asset('img/team1.png') }})"></span>
                        <div class="team-name">Alianza Limba</div>
                    </div>
                    <div class="item">
                        <span class="team-logo" style="background-image: url({{ asset('img/team2.png') }})"></span>
                        <div class="team-name">Club Deportivo Llacuabama</div>
                    </div>
                </div>
                <div class="cell cell-score"></div>
                <div class="cell cell-one">
                    <span class="bubble">3.73</span>
                </div>
                <div class="cell cell-x">
                    <span class="bubble">3.73</span>
                </div>
                <div class="cell cell-two">
                    <span class="bubble">3.73</span>
                </div>
                <div class="cell cell-play">
                    <span class="play-btn"></span>
                </div>
            </div>
        </div>
    </div>

    <div class="box no-padding box-table">
        <div class="box-table-headinfo">
            <div class="topline">
                <div class="country">
                    <div class="flag flag-icon flag-peru"></div>
                    <div class="name">Peru</div>
                </div>
                <div class="toggler">
                    <svg> <use xlink:href="#arr-down"/> </svg>
                </div>
            </div>
            <div class="bottomline">
                <div class="division">
                    <div class="icon">
                        <svg> <use xlink:href="#icon-football"/> </svg>
                    </div>
                    <div class="name">Primera Division</div>
                </div>
                <a href="#" class="show-all">
                    <div class="text">Show All Matches</div>
                    <div class="icon"> <svg> <use xlink:href="#triangle-arr-right"/> </svg> </div>
                </a>
            </div>
        </div>
        <div class="box-table-header">
            <div class="cell cell-datetime">03 Oct 2020</div>
            <div class="cell cell-team cell-empty"></div>
            <div class="cell cell-score">Score</div>
            <div class="cell cell-one">1</div>
            <div class="cell cell-x">X</div>
            <div class="cell cell-two">2</div>
            <div class="cell cell-play cell-empty"></div>
        </div>
        <div class="box-table-body">
            <div class="row">
                <div class="cell cell-datetime">
                    <div class="icon icon-finished"></div>
                    <div class="text">08:30</div>
                </div>
                <div class="cell cell-team">
                    <div class="item">
                        <span class="team-logo" style="background-image: url({{ asset('img/team1.png') }})"></span>
                        <div class="team-name team-winner">Alianza Limba</div>
                    </div>
                    <div class="item">
                        <span class="team-logo" style="background-image: url({{ asset('img/team2.png') }})"></span>
                        <div class="team-name">Club Deportivo Llacuabama</div>
                    </div>
                </div>
                <div class="cell cell-score">
                    <span class="bubble">3 : 1</span>
                </div>
                <div class="cell cell-one">
                    <span class="bubble">3.73</span>
                </div>
                <div class="cell cell-x">
                    <span class="bubble">3.73</span>
                </div>
                <div class="cell cell-two">
                    <span class="bubble">3.73</span>
                </div>
                <div class="cell cell-play">
                    <span class="play-btn"></span>
                </div>
            </div>
            <div class="row">
                <div class="cell cell-datetime">
                    <div class="icon icon-finished"></div>
                    <div class="text">15:30</div>
                </div>
                <div class="cell cell-team">
                    <div class="item">
                        <span class="team-logo" style="background-image: url({{ asset('img/team1.png') }})"></span>
                        <div class="team-name team-winner">Alianza Limba</div>
                    </div>
                    <div class="item">
                        <span class="team-logo" style="background-image: url({{ asset('img/team2.png') }})"></span>
                        <div class="team-name">Club Deportivo Llacuabama</div>
                    </div>
                </div>
                <div class="cell cell-score">
                    <span class="bubble">3 : 1</span>
                </div>
                <div class="cell cell-one">
                    <span class="bubble">3.73</span>
                </div>
                <div class="cell cell-x">
                    <span class="bubble">3.73</span>
                </div>
                <div class="cell cell-two">
                    <span class="bubble">3.73</span>
                </div>
                <div class="cell cell-play">
                    <span class="play-btn"></span>
                </div>
            </div>
            <div class="row">
                <div class="cell cell-datetime">
                    <div class="icon icon-playing"></div>
                    <div class="text red">03:18</div>
                </div>
                <div class="cell cell-team">
                    <div class="item">
                        <span class="team-logo"  style="background-image: url({{ asset('img/team1.png') }})"></span>
                        <div class="team-name">Alianza Limba</div>
                    </div>
                    <div class="item">
                        <span class="team-logo"  style="background-image: url({{ asset('img/team2.png') }})"></span>
                        <div class="team-name">Club Deportivo Llacuabama</div>
                    </div>
                </div>
                <div class="cell cell-score">
                    <span class="bubble bubble-red">1 : 0</span>
                </div>
                <div class="cell cell-one">
                    <span class="bubble">3.73</span>
                </div>
                <div class="cell cell-x">
                    <span class="bubble">3.73</span>
                </div>
                <div class="cell cell-two">
                    <span class="bubble">3.73</span>
                </div>
                <div class="cell cell-play">
                    <span class="play-btn"></span>
                </div>
            </div>
            <div class="row">
                <div class="cell cell-datetime">
                    <div class="text">08:30</div>
                </div>
                <div class="cell cell-team">
                    <div class="item">
                        <span class="team-logo" style="background-image: url({{ asset('img/team1.png') }})"></span>
                        <div class="team-name">Alianza Limba</div>
                    </div>
                    <div class="item">
                        <span class="team-logo" style="background-image: url({{ asset('img/team2.png') }})"></span>
                        <div class="team-name">Club Deportivo Llacuabama</div>
                    </div>
                </div>
                <div class="cell cell-score"></div>
                <div class="cell cell-one">
                    <span class="bubble">3.73</span>
                </div>
                <div class="cell cell-x">
                    <span class="bubble">3.73</span>
                </div>
                <div class="cell cell-two">
                    <span class="bubble">3.73</span>
                </div>
                <div class="cell cell-play">
                    <span class="play-btn"></span>
                </div>
            </div>
        </div>

        <div class="box-table-headinfo">
            <div class="bottomline">
                <div class="division">
                    <div class="icon">
                        <svg> <use xlink:href="#icon-football"/> </svg>
                    </div>
                    <div class="name">Primera Division</div>
                </div>
                <a href="#" class="show-all">
                    <div class="text">Show All Matches</div>
                    <div class="icon"> <svg> <use xlink:href="#triangle-arr-right"/> </svg> </div>
                </a>
            </div>
        </div>

        <div class="box-table-header">
            <div class="cell cell-datetime">03 Oct 2020</div>
            <div class="cell cell-team cell-empty"></div>
            <div class="cell cell-score">Score</div>
            <div class="cell cell-one">1</div>
            <div class="cell cell-x">X</div>
            <div class="cell cell-two">2</div>
            <div class="cell cell-play cell-empty"></div>
        </div>
        <div class="box-table-body">
            <div class="row">
                <div class="cell cell-datetime">
                    <div class="icon icon-finished"></div>
                    <div class="text">08:30</div>
                </div>
                <div class="cell cell-team">
                    <div class="item">
                        <span class="team-logo" style="background-image: url({{ asset('img/team1.png') }})"></span>
                        <div class="team-name team-winner">Alianza Limba</div>
                    </div>
                    <div class="item">
                        <span class="team-logo" style="background-image: url({{ asset('img/team2.png') }})"></span>
                        <div class="team-name">Club Deportivo Llacuabama</div>
                    </div>
                </div>
                <div class="cell cell-score">
                    <span class="bubble">3 : 1</span>
                </div>
                <div class="cell cell-one">
                    <span class="bubble">3.73</span>
                </div>
                <div class="cell cell-x">
                    <span class="bubble">3.73</span>
                </div>
                <div class="cell cell-two">
                    <span class="bubble">3.73</span>
                </div>
                <div class="cell cell-play">
                    <span class="play-btn"></span>
                </div>
            </div>
            <div class="row">
                <div class="cell cell-datetime">
                    <div class="icon icon-finished"></div>
                    <div class="text">15:30</div>
                </div>
                <div class="cell cell-team">
                    <div class="item">
                        <span class="team-logo" style="background-image: url({{ asset('img/team1.png') }})"></span>
                        <div class="team-name team-winner">Alianza Limba</div>
                    </div>
                    <div class="item">
                        <span class="team-logo" style="background-image: url({{ asset('img/team2.png') }})"></span>
                        <div class="team-name">Club Deportivo Llacuabama</div>
                    </div>
                </div>
                <div class="cell cell-score">
                    <span class="bubble">3 : 1</span>
                </div>
                <div class="cell cell-one">
                    <span class="bubble">3.73</span>
                </div>
                <div class="cell cell-x">
                    <span class="bubble">3.73</span>
                </div>
                <div class="cell cell-two">
                    <span class="bubble">3.73</span>
                </div>
                <div class="cell cell-play">
                    <span class="play-btn"></span>
                </div>
            </div>
            <div class="row">
                <div class="cell cell-datetime">
                    <div class="icon icon-playing"></div>
                    <div class="text red">03:18</div>
                </div>
                <div class="cell cell-team">
                    <div class="item">
                        <span class="team-logo"  style="background-image: url({{ asset('img/team1.png') }})"></span>
                        <div class="team-name">Alianza Limba</div>
                    </div>
                    <div class="item">
                        <span class="team-logo"  style="background-image: url({{ asset('img/team2.png') }})"></span>
                        <div class="team-name">Club Deportivo Llacuabama</div>
                    </div>
                </div>
                <div class="cell cell-score">
                    <span class="bubble bubble-red">1 : 0</span>
                </div>
                <div class="cell cell-one">
                    <span class="bubble">3.73</span>
                </div>
                <div class="cell cell-x">
                    <span class="bubble">3.73</span>
                </div>
                <div class="cell cell-two">
                    <span class="bubble">3.73</span>
                </div>
                <div class="cell cell-play">
                    <span class="play-btn"></span>
                </div>
            </div>
            <div class="row">
                <div class="cell cell-datetime">
                    <div class="text">08:30</div>
                </div>
                <div class="cell cell-team">
                    <div class="item">
                        <span class="team-logo" style="background-image: url({{ asset('img/team1.png') }})"></span>
                        <div class="team-name">Alianza Limba</div>
                    </div>
                    <div class="item">
                        <span class="team-logo" style="background-image: url({{ asset('img/team2.png') }})"></span>
                        <div class="team-name">Club Deportivo Llacuabama</div>
                    </div>
                </div>
                <div class="cell cell-score"></div>
                <div class="cell cell-one">
                    <span class="bubble">3.73</span>
                </div>
                <div class="cell cell-x">
                    <span class="bubble">3.73</span>
                </div>
                <div class="cell cell-two">
                    <span class="bubble">3.73</span>
                </div>
                <div class="cell cell-play">
                    <span class="play-btn"></span>
                </div>
            </div>
        </div>

        <div class="box-table-headinfo">
            <div class="bottomline">
                <div class="division">
                    <div class="icon">
                        <svg> <use xlink:href="#icon-football"/> </svg>
                    </div>
                    <div class="name">1 Liga</div>
                </div>
                <a href="#" class="show-all">
                    <div class="text">Show All Matches</div>
                    <div class="icon"> <svg> <use xlink:href="#triangle-arr-right"/> </svg> </div>
                </a>
            </div>
        </div>

        <div class="box-table-header">
            <div class="cell cell-datetime">03 Oct 2020</div>
            <div class="cell cell-team cell-empty"></div>
            <div class="cell cell-score">Score</div>
            <div class="cell cell-one">1</div>
            <div class="cell cell-x">X</div>
            <div class="cell cell-two">2</div>
            <div class="cell cell-play cell-empty"></div>
        </div>
        <div class="box-table-body">
            <div class="row">
                <div class="cell cell-datetime">
                    <div class="icon icon-finished"></div>
                    <div class="text">08:30</div>
                </div>
                <div class="cell cell-team">
                    <div class="item">
                        <span class="team-logo" style="background-image: url({{ asset('img/team1.png') }})"></span>
                        <div class="team-name team-winner">Alianza Limba</div>
                    </div>
                    <div class="item">
                        <span class="team-logo" style="background-image: url({{ asset('img/team2.png') }})"></span>
                        <div class="team-name">Club Deportivo Llacuabama</div>
                    </div>
                </div>
                <div class="cell cell-score">
                    <span class="bubble">3 : 1</span>
                </div>
                <div class="cell cell-one">
                    <span class="bubble">3.73</span>
                </div>
                <div class="cell cell-x">
                    <span class="bubble">3.73</span>
                </div>
                <div class="cell cell-two">
                    <span class="bubble">3.73</span>
                </div>
                <div class="cell cell-play">
                    <span class="play-btn"></span>
                </div>
            </div>
            <div class="row">
                <div class="cell cell-datetime">
                    <div class="icon icon-finished"></div>
                    <div class="text">15:30</div>
                </div>
                <div class="cell cell-team">
                    <div class="item">
                        <span class="team-logo" style="background-image: url({{ asset('img/team1.png') }})"></span>
                        <div class="team-name team-winner">Alianza Limba</div>
                    </div>
                    <div class="item">
                        <span class="team-logo" style="background-image: url({{ asset('img/team2.png') }})"></span>
                        <div class="team-name">Club Deportivo Llacuabama</div>
                    </div>
                </div>
                <div class="cell cell-score">
                    <span class="bubble">3 : 1</span>
                </div>
                <div class="cell cell-one">
                    <span class="bubble">3.73</span>
                </div>
                <div class="cell cell-x">
                    <span class="bubble">3.73</span>
                </div>
                <div class="cell cell-two">
                    <span class="bubble">3.73</span>
                </div>
                <div class="cell cell-play">
                    <span class="play-btn"></span>
                </div>
            </div>
            <div class="row">
                <div class="cell cell-datetime">
                    <div class="icon icon-playing"></div>
                    <div class="text red">03:18</div>
                </div>
                <div class="cell cell-team">
                    <div class="item">
                        <span class="team-logo"  style="background-image: url({{ asset('img/team1.png') }})"></span>
                        <div class="team-name">Alianza Limba</div>
                    </div>
                    <div class="item">
                        <span class="team-logo"  style="background-image: url({{ asset('img/team2.png') }})"></span>
                        <div class="team-name">Club Deportivo Llacuabama</div>
                    </div>
                </div>
                <div class="cell cell-score">
                    <span class="bubble bubble-red">1 : 0</span>
                </div>
                <div class="cell cell-one">
                    <span class="bubble">3.73</span>
                </div>
                <div class="cell cell-x">
                    <span class="bubble">3.73</span>
                </div>
                <div class="cell cell-two">
                    <span class="bubble">3.73</span>
                </div>
                <div class="cell cell-play">
                    <span class="play-btn"></span>
                </div>
            </div>
            <div class="row">
                <div class="cell cell-datetime">
                    <div class="text">08:30</div>
                </div>
                <div class="cell cell-team">
                    <div class="item">
                        <span class="team-logo" style="background-image: url({{ asset('img/team1.png') }})"></span>
                        <div class="team-name">Alianza Limba</div>
                    </div>
                    <div class="item">
                        <span class="team-logo" style="background-image: url({{ asset('img/team2.png') }})"></span>
                        <div class="team-name">Club Deportivo Llacuabama</div>
                    </div>
                </div>
                <div class="cell cell-score"></div>
                <div class="cell cell-one">
                    <span class="bubble">3.73</span>
                </div>
                <div class="cell cell-x">
                    <span class="bubble">3.73</span>
                </div>
                <div class="cell cell-two">
                    <span class="bubble">3.73</span>
                </div>
                <div class="cell cell-play">
                    <span class="play-btn"></span>
                </div>
            </div>
        </div>

        <div class="box-table-headinfo">
            <div class="bottomline">
                <div class="division">
                    <div class="icon">
                        <svg> <use xlink:href="#icon-football"/> </svg>
                    </div>
                    <div class="name">99 Liga</div>
                </div>
                <a href="#" class="show-all">
                    <div class="text">Show All Matches</div>
                    <div class="icon"> <svg> <use xlink:href="#triangle-arr-right"/> </svg> </div>
                </a>
            </div>
        </div>

        <div class="box-table-header">
            <div class="cell cell-datetime">03 Oct 2020</div>
            <div class="cell cell-team cell-empty"></div>
            <div class="cell cell-score">Score</div>
            <div class="cell cell-one">1</div>
            <div class="cell cell-x">X</div>
            <div class="cell cell-two">2</div>
            <div class="cell cell-play cell-empty"></div>
        </div>
        <div class="box-table-body">
            <div class="row">
                <div class="cell cell-datetime">
                    <div class="icon icon-finished"></div>
                    <div class="text">08:30</div>
                </div>
                <div class="cell cell-team">
                    <div class="item">
                        <span class="team-logo" style="background-image: url({{ asset('img/team1.png') }})"></span>
                        <div class="team-name team-winner">Alianza Limba</div>
                    </div>
                    <div class="item">
                        <span class="team-logo" style="background-image: url({{ asset('img/team2.png') }})"></span>
                        <div class="team-name">Club Deportivo Llacuabama</div>
                    </div>
                </div>
                <div class="cell cell-score">
                    <span class="bubble">3 : 1</span>
                </div>
                <div class="cell cell-one">
                    <span class="bubble">3.73</span>
                </div>
                <div class="cell cell-x">
                    <span class="bubble">3.73</span>
                </div>
                <div class="cell cell-two">
                    <span class="bubble">3.73</span>
                </div>
                <div class="cell cell-play">
                    <span class="play-btn"></span>
                </div>
            </div>
            <div class="row">
                <div class="cell cell-datetime">
                    <div class="icon icon-finished"></div>
                    <div class="text">15:30</div>
                </div>
                <div class="cell cell-team">
                    <div class="item">
                        <span class="team-logo" style="background-image: url({{ asset('img/team1.png') }})"></span>
                        <div class="team-name team-winner">Alianza Limba</div>
                    </div>
                    <div class="item">
                        <span class="team-logo" style="background-image: url({{ asset('img/team2.png') }})"></span>
                        <div class="team-name">Club Deportivo Llacuabama</div>
                    </div>
                </div>
                <div class="cell cell-score">
                    <span class="bubble">3 : 1</span>
                </div>
                <div class="cell cell-one">
                    <span class="bubble">3.73</span>
                </div>
                <div class="cell cell-x">
                    <span class="bubble">3.73</span>
                </div>
                <div class="cell cell-two">
                    <span class="bubble">3.73</span>
                </div>
                <div class="cell cell-play">
                    <span class="play-btn"></span>
                </div>
            </div>
            <div class="row">
                <div class="cell cell-datetime">
                    <div class="icon icon-playing"></div>
                    <div class="text red">03:18</div>
                </div>
                <div class="cell cell-team">
                    <div class="item">
                        <span class="team-logo"  style="background-image: url({{ asset('img/team1.png') }})"></span>
                        <div class="team-name">Alianza Limba</div>
                    </div>
                    <div class="item">
                        <span class="team-logo"  style="background-image: url({{ asset('img/team2.png') }})"></span>
                        <div class="team-name">Club Deportivo Llacuabama</div>
                    </div>
                </div>
                <div class="cell cell-score">
                    <span class="bubble bubble-red">1 : 0</span>
                </div>
                <div class="cell cell-one">
                    <span class="bubble">3.73</span>
                </div>
                <div class="cell cell-x">
                    <span class="bubble">3.73</span>
                </div>
                <div class="cell cell-two">
                    <span class="bubble">3.73</span>
                </div>
                <div class="cell cell-play">
                    <span class="play-btn"></span>
                </div>
            </div>
            <div class="row">
                <div class="cell cell-datetime">
                    <div class="text">08:30</div>
                </div>
                <div class="cell cell-team">
                    <div class="item">
                        <span class="team-logo" style="background-image: url({{ asset('img/team1.png') }})"></span>
                        <div class="team-name">Alianza Limba</div>
                    </div>
                    <div class="item">
                        <span class="team-logo" style="background-image: url({{ asset('img/team2.png') }})"></span>
                        <div class="team-name">Club Deportivo Llacuabama</div>
                    </div>
                </div>
                <div class="cell cell-score"></div>
                <div class="cell cell-one">
                    <span class="bubble">3.73</span>
                </div>
                <div class="cell cell-x">
                    <span class="bubble">3.73</span>
                </div>
                <div class="cell cell-two">
                    <span class="bubble">3.73</span>
                </div>
                <div class="cell cell-play">
                    <span class="play-btn"></span>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
