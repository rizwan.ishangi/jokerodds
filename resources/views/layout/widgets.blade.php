<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
</head>
<body>

<div id="app-widget">
    @yield('content')
</div>

@stack('js')
<script src="{{ mix('js/app.js') }}"></script>

</body>
</html>
