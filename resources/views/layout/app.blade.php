<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>{{ config('app.name') }}</title>
    @stack('css')
    <link rel="stylesheet" href="{{ mix('css/app.css') }}?v=2">
    <link rel="manifest" href="/manifest.webmanifest">
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=G-83GRNNL21Z"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'G-83GRNNL21Z');
    </script>
    <script async src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js?client=ca-pub-6820679346373791"
            crossorigin="anonymous">
    </script>

</head>
<body>
    @yield('content')

    {{-- @include('partials.svg-icons')
    @include('partials.header')

    <div class="site-content">
        <div class="container">
            <div class="site-layout">
                <div class="layout-sidebar layout-sidebar-left">
                    @include('partials.sidebarleft')
                </div>
                <main class="main layout-content">
                    @include('partials.contenttabs')
                    @yield('content')
                </main>
                <div class="layout-sidebar layout-sidebar-right">
                    @include('partials.sidebarright')
                </div>
            </div>
        </div>
    </div>

    @include('partials.footer') --}}

    @stack('js')
    <script src="{{ mix('js/app.js') }}"></script>
</body>
</html>
