<div class="sidebar-mytracker">
    <div class="tracker-title">
        <div class="text">
            <div class="icon"></div>
            <div class="name">My Tracker</div>
            <div class="num">5</div>
        </div>
        <span class="clear">Clear</span>
    </div>
    <div class="tracker-content">
        <div class="item">
            <div class="name-line">
                <div class="icon icon-football"></div>
                <div class="name"><strong>Everton -</strong>West Ham</div>
                <div class="remove"></div>
            </div>
            <div class="numbers">
                <div class="stats-item co up">1.57</div>
                <div class="stats-item x">1X2</div>
            </div>
            <div class="score">
                <div class="stats-item play-score">47’ | 3 : 0</div>
            </div>
        </div>

        <div class="item item-win">
            <div class="name-line">
                <div class="icon icon-volley"></div>
                <div class="name"><strong>Sao Paulo U20 -</strong> Flamengo RJ U20</div>
                <div class="remove"></div>
            </div>
            <div class="numbers">
                <div class="stats-item co down">1.57</div>
                <div class="stats-item x">AH - 1.25</div>
            </div>
            <div class="score">
                <div class="stats-item flag-score">3 : 0</div>
            </div>
        </div>

        <div class="item">
            <div class="name-line">
                <div class="icon icon-hockey"></div>
                <div class="name"><strong>Ulsan Hyundai - Sangju Sangmu</strong></div>
                <div class="remove"></div>
            </div>
            <div class="numbers">
                <div class="stats-item co up">1.57</div>
                <div class="stats-item x">1X2</div>
            </div>
            <div class="score">
                <div class="stats-item play-score">08’ | 0 : 0</div>
            </div>
        </div>

        <div class="item item-win">
            <div class="name-line">
                <div class="icon icon-badminton"></div>
                <div class="name"><strong>Utsunomiya Brex -</strong> Ryukyu</div>
                <div class="remove"></div>
            </div>
            <div class="numbers">
                <div class="stats-item co equal">1.57</div>
                <div class="stats-item x">1X2</div>
            </div>
            <div class="score">
                <div class="stats-item flag-score">3 : 0</div>
            </div>
        </div>

        <div class="item item-loose">
            <div class="name-line">
                <div class="icon icon-tennis"></div>
                <div class="name">Utsunomiya Brex - <strong>Ryukyu</strong></div>
                <div class="remove"></div>
            </div>
            <div class="numbers">
                <div class="stats-item co equal">1.57</div>
                <div class="stats-item x">1X2</div>
            </div>
            <div class="score">
                <div class="stats-item flag-score">0 : 4</div>
            </div>
        </div>
    </div>
</div>

<div class="sidebar-ad-place">
    <img src="{{ asset('img/sidebar-ad-place.png') }}" alt class="ad-place-img">
</div>

<div class="sidebar-block">
    <div class="sidebar-title">Standings</div>
    <div class="sidebar-content">
        <div class="sidebar-standings">
            <div class="head">
                <div class="n">#</div>
                <div class="team">Team</div>
                <div class="mp">MP</div>
                <div class="pts">Pts</div>
            </div>
            <div class="rows">
                <div class="row">
                    <div class="team">
                        <div class="icon"></div>
                        <div class="text">Leicester</div>
                    </div>
                    <div class="mp">3</div>
                    <div class="pts">9</div>
                </div>
                <div class="row">
                    <div class="team">
                        <div class="icon"></div>
                        <div class="text">Liverpool</div>
                    </div>
                    <div class="mp">3</div>
                    <div class="pts">9</div>
                </div>
                <div class="row row-gray">
                    <div class="team">
                        <div class="icon"></div>
                        <div class="text">Everton</div>
                    </div>
                    <div class="mp">3</div>
                    <div class="pts">9</div>
                </div>
                <div class="row">
                    <div class="team">
                        <div class="icon"></div>
                        <div class="text">Aston Villa</div>
                    </div>
                    <div class="mp">3</div>
                    <div class="pts">9</div>
                </div>
                <div class="row">
                    <div class="team">
                        <div class="icon"></div>
                        <div class="text">Leeds</div>
                    </div>
                    <div class="mp">3</div>
                    <div class="pts">9</div>
                </div>
                <div class="row row-red">
                    <div class="team">
                        <div class="icon"></div>
                        <div class="text">Everton</div>
                    </div>
                    <div class="mp">3</div>
                    <div class="pts">9</div>
                </div>

                <div class="row">
                    <div class="team">
                        <div class="icon"></div>
                        <div class="text">Chelsea</div>
                    </div>
                    <div class="mp">3</div>
                    <div class="pts">9</div>
                </div>
                <div class="row">
                    <div class="team">
                        <div class="icon"></div>
                        <div class="text">Newcastle</div>
                    </div>
                    <div class="mp">3</div>
                    <div class="pts">9</div>
                </div>
                <div class="row">
                    <div class="team">
                        <div class="icon"></div>
                        <div class="text">Wes Ham</div>
                    </div>
                    <div class="mp">3</div>
                    <div class="pts">9</div>
                </div>
                <div class="row">
                    <div class="team">
                        <div class="icon"></div>
                        <div class="text">Leicester</div>
                    </div>
                    <div class="mp">3</div>
                    <div class="pts">9</div>
                </div>
                <div class="row">
                    <div class="team">
                        <div class="icon"></div>
                        <div class="text">Liverpool</div>
                    </div>
                    <div class="mp">3</div>
                    <div class="pts">9</div>
                </div>
                <div class="row">
                    <div class="team">
                        <div class="icon"></div>
                        <div class="text">Aston Villa</div>
                    </div>
                    <div class="mp">3</div>
                    <div class="pts">9</div>
                </div>
                <div class="row">
                    <div class="team">
                        <div class="icon"></div>
                        <div class="text">Leeds</div>
                    </div>
                    <div class="mp">3</div>
                    <div class="pts">9</div>
                </div>
                <div class="row">
                    <div class="team">
                        <div class="icon"></div>
                        <div class="text">Tottenham</div>
                    </div>
                    <div class="mp">3</div>
                    <div class="pts">9</div>
                </div>
                <div class="row">
                    <div class="team">
                        <div class="icon"></div>
                        <div class="text">Chelsea</div>
                    </div>
                    <div class="mp">3</div>
                    <div class="pts">9</div>
                </div>
                <div class="row">
                    <div class="team">
                        <div class="icon"></div>
                        <div class="text">Newcastle</div>
                    </div>
                    <div class="mp">3</div>
                    <div class="pts">9</div>
                </div>
                <div class="row">
                    <div class="team">
                        <div class="icon"></div>
                        <div class="text">Wes Ham</div>
                    </div>
                    <div class="mp">3</div>
                    <div class="pts">9</div>
                </div>

                <div class="row row-red">
                    <div class="team">
                        <div class="icon"></div>
                        <div class="text">West Ham</div>
                    </div>
                    <div class="mp">3</div>
                    <div class="pts">9</div>
                </div>
                <div class="row row">
                    <div class="team">
                        <div class="icon"></div>
                        <div class="text">Burnley</div>
                    </div>
                    <div class="mp">3</div>
                    <div class="pts">9</div>
                </div>
                <div class="row row">
                    <div class="team">
                        <div class="icon"></div>
                        <div class="text">Liverpool</div>
                    </div>
                    <div class="mp">3</div>
                    <div class="pts">9</div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="sidebar-ad-place">
    <img src="{{ asset('img/sidebar-ad-place.png') }}" alt class="ad-place-img">
</div>

<div class="sidebar-block">
    <div class="sidebar-title">Top Events</div>
    <div class="sidebar-content">
        <div class="sidebar-top-events">
            <div class="events-items">
                <div class="item">
                    <div class="teams">
                        <div class="team">
                            <span class="team-logo" style="background-image: url('{{ asset('img/team1.png') }}')"></span>
                            <span class="team-name">Everton</span>
                        </div>
                        <div class="team">
                            <span class="team-logo" style="background-image: url('{{ asset('img/team2.png') }}')"></span>
                            <span class="team-name">West Ham</span>
                        </div>
                    </div>
                    <div class="time">23:00</div>
                </div>

                <div class="item">
                    <div class="teams">
                        <div class="team">
                            <span class="team-logo" style="background-image: url('{{ asset('img/team1.png') }}')"></span>
                            <span class="team-name">Everton</span>
                        </div>
                        <div class="team">
                            <span class="team-logo" style="background-image: url('{{ asset('img/team2.png') }}')"></span>
                            <span class="team-name">West Ham</span>
                        </div>
                    </div>
                    <div class="time">20:00</div>
                </div>

                <div class="item">
                    <div class="teams">
                        <div class="team">
                            <span class="team-logo" style="background-image: url('{{ asset('img/team1.png') }}')"></span>
                            <span class="team-name">Everton</span>
                        </div>
                        <div class="team">
                            <span class="team-logo" style="background-image: url('{{ asset('img/team2.png') }}')"></span>
                            <span class="team-name">West Ham</span>
                        </div>
                    </div>
                    <div class="time">17:00</div>
                </div>

                <div class="item">
                    <div class="teams">
                        <div class="team">
                            <span class="team-logo" style="background-image: url('{{ asset('img/team1.png') }}')"></span>
                            <span class="team-name">Everton</span>
                        </div>
                        <div class="team">
                            <span class="team-logo" style="background-image: url('{{ asset('img/team2.png') }}')"></span>
                            <span class="team-name">West Ham</span>
                        </div>
                    </div>
                    <div class="time">14:00</div>
                </div>

                <div class="item">
                    <div class="teams">
                        <div class="team">
                            <span class="team-logo" style="background-image: url('{{ asset('img/team1.png') }}')"></span>
                            <span class="team-name">Everton</span>
                        </div>
                        <div class="team">
                            <span class="team-logo" style="background-image: url('{{ asset('img/team2.png') }}')"></span>
                            <span class="team-name">West Ham</span>
                        </div>
                    </div>
                    <div class="time">09:00</div>
                </div>
            </div>
            <label class="events-select-label">
                <select class="select">
                    <option selected>View All Events</option>
                    <option>Option 1</option>
                    <option>Option 2</option>
                    <option>Option 3</option>
                </select>
            </label>
        </div>
    </div>
</div>

<div class="sidebar-ad-place">
    <img src="{{ asset('img/sidebar-ad-place.png') }}" alt class="ad-place-img">
</div>
