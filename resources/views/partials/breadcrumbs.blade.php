<div class="breadcrumbs flex aic jcc">
    <div class="breadcrumbs-title">You’re Here:</div>
    <div class="breadcrumbs-items flex aic">
        @foreach ($breadcrumbs as $breadcrumb)
            @if (isset($breadcrumb['url']))
                <a href="{{ $breadcrumb['url'] }}" class="item">{{ $breadcrumb['name'] }}</a>
            @else
                <span class="item current">{{ $breadcrumb['name'] }}</span>
            @endif
        @endforeach
    </div>
</div>
