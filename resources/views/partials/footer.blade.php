<footer class="footer">
    <div class="container">
        <div class="footer-section footer-topline flex aic jcsb">
            <a href="/" class="logo"></a>
            <div class="links flex aic">
                <a href="#" class="item">ABOUT</a>
                <a href="#" class="item">FAQ</a>
                <a href="#" class="item">CONTACT</a>
                <a href="#" class="item">TERMS OF USE</a>
                <a href="#" class="item">Cookie Policy</a>
            </div>
            <div class="socials flex aic">
                <a href="#" class="item item-fb"></a>
                <a href="#" class="item item-inst"></a>
            </div>
        </div>
        <div class="footer-section footer-tags">
            Soccer Betting Odds | UEFA Nations League | Tennis Betting Odds | Tennis French Open Betting Odds | NBA Basketball Betting Odds | NHL Ice Hockey Betting Odds | MLB Betting Odds | NFL Betting Odds | Volleyball Betting Odds | Rugby Union Betting Odds | Rugby League Betting Odds | Cricket Betting Odds | Cricket Indian Premier League Betting Odds | Handball Betting Odds | Snooker Betting Odds | Darts Betting Odds | Premier League Betting Odds | Bundesliga Betting Odds | Italian Serie A Odds | French Ligue 1 Odds | Champions League Betting Odds | Europa League Betting Odds | Spanish LaLiga Betting Odds | NHL Betting Odds | KHL Betting Odds | NBA Betting Odds | AFL Aussie Rules Odds | In-play Betting Odds | eSports Betting Odds
        </div>
        <div class="footer-section footer-bottomtext">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Architecto aut ex hic id molestiae nam nobis perspiciatis provident quas vel?</div>
        <div class="footer-section footer-copy">Copyright 2020. text here</div>
    </div>
</footer>
