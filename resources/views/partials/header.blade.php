<header class="header">
    <div class="header-topline">
        <div class="container flex aic">
            <a href="/" class="logo"></a>
            <div class="links flex">
                <a href="#" class="link active">Odds Comparison</a>
                <a href="#" class="link">Live Scores & Results<span class="coming__soon">Coming soon</span></a>
                <a href="#" class="link">Bookmakers<span class="coming__soon">Coming soon</span></a>
            </div>
            <div class="states flex aic">
                <div class="date">
                    Sat, 19 Oct,  14:30,  GMT+3
                </div>
                <div class="country">
                    <div class="custom-select">
                        <div class="current">EU Odds</div>
                    </div>
                </div>
            </div>
            <div class="auth flex">
                <a href="#" class="btn btn-lightgray2">Sign Up<span class="coming__soon">Coming soon</span></a>
                <a href="#" class="btn btn-red">Log In <span class="icon icon-login"></span><span class="coming__soon">Coming soon</span></a>
            </div>
        </div>
    </div>
    <div class="header-navline flex aic">
        <div class="container flex aic">
            <nav class="nav header-nav flex aic">
                <a href="#" class="item current">
                    <span class="name">Football</span>
                    <span class="icon"></span>
                </a>
                <a href="#" class="item">
                    <span class="name">Tennis<span class="coming__soon">Coming soon</span></span>
                    <span class="icon"></span>
                </a>
                <a href="#" class="item">
                    <span class="name">Basketball<span class="coming__soon">Coming soon</span></span>
                    <span class="icon"></span>
                </a>
                <a href="#" class="item">
                    <span class="name">Hockey<span class="coming__soon">Coming soon</span></span>
                    <span class="icon"></span>
                </a>
                <a href="#" class="item">
                    <span class="name">Volleyball<span class="coming__soon">Coming soon</span></span>
                    <span class="icon"></span>
                </a>
                <a href="#" class="item">
                    <span class="name">Badminton<span class="coming__soon">Coming soon</span></span>
                    <span class="icon"></span>
                </a>
                <div class="item item-more">
                    <span class="name">More<span class="coming__soon">Coming soon</span></span>
                    <span class="icon"></span>
                </div>
            </nav>
            <div class="search header-search">
                <label class="search-box">
                    <input type="text" class="search-input" placeholder="Search For League, Team or Player">
                </label>
            </div>
        </div>
    </div>
    <div class="header-livescores flex aic">
        <div class="flex aic">
            <div class="name flex aic">Live Scores</div>
            <div class="scores">
                <div>
                    <span>SOCCER: Kudermetova V - Yastremska D: 2 : 0</span>
                    <span>SOCCER: Kudermetova V - Yastremska D: 2 : 0</span>
                    <span>SOCCER: Kudermetova V - Yastremska D: 2 : 0</span>
                    <span>SOCCER: Kudermetova V - Yastremska D: 2 : 0</span>
                    <span>SOCCER: Kudermetova V - Yastremska D: 2 : 0</span>
                    <span>SOCCER: Kudermetova V - Yastremska D: 2 : 0</span>
                    <span>SOCCER: Kudermetova V - Yastremska D: 2 : 0</span>
                </div>
            </div>
        </div>
    </div>
    <a href="#" class="header-banner">
        <div class="container flex">
            <img class="img" src="{{ asset('img/top-banner.png') }}" alt>
        </div>
    </a>
</header>
