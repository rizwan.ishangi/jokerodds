@extends('layout.app')

@section('content')
    <div id="app"></div>
@endsection

@push('js')
    <script>
        window.text_blocks = @json($text_blocks);
        window.menus = @json($menus);
        window.settings = @json($settings);
    </script>
@endpush
