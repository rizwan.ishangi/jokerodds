@extends('layout.app')

@section('content')
<div class="content-wrapper content-next-matches">
    @include('partials.breadcrumbs', $breadcrumbs = [
        [
            'name' => 'Odds Comparison',
            'url' => '#'
        ],
        [
            'name' => 'Football',
            'url' => '#'
        ],
        [
            'name' => 'Next Matches',
            'url' => '#'
        ],
        [
            'name' => 'Premier League Odds',
            'url' => '#'
        ],
        [
            'name' => 'Everton - West Ham'
        ]
    ])

    <div class="box odds-table">
        <div class="teams">
            <div class="team-one team-item">
                <div class="team-logo" style="background-image: url('{{ asset('img/team1bigger.png') }}')"></div>
                <div class="team-name">Everton</div>
            </div>
            <div class="score">
                <div class="score-box">
                    <div class="score-main"><span>1</span> : <span>0</span></div>
                    <div class="score-sub">( 3 : 2 )</div>
                </div>
                <div class="play">98’</div>
            </div>
            <div class="team-two team-item">
                <div class="team-logo" style="background-image: url('{{ asset('img/team2bigger.png') }}')"></div>
                <div class="team-name">West Ham</div>
            </div>
        </div>
        <div class="tabs">
            <div class="tabs-item active">
                <span class="tabname">Pre-match Odds</span>
            </div>
            <div class="tabs-item">
                <span class="tabname"><span class="icon icon-play"></span>In-Play Odds</span>
            </div>
            <div class="tabs-item">
                <span class="tabname">Game Center</span>
            </div>
        </div>
        <div class="nav">
            <div class="nav-item active">1X2</div>
            <div class="nav-item">AH</div>
            <div class="nav-item">O/U</div>
            <div class="nav-item">DNB</div>
            <div class="nav-item">DC</div>
            <div class="nav-item nav-item-more">MORE</div>
        </div>
        <div class="times">
            <div class="times-item active">
                <span class="icon">
                    <svg><use xlink:href="#time-full" /></svg>
                </span> <span class="text">Full Time</span>
            </div>
            <div class="times-item">
                <span class="icon">
                    <svg><use xlink:href="#clock-first-half"/></svg>
                </span> <span class="text">1st Half</span>
            </div>
            <div class="times-item">
                <span class="icon">
                    <svg><use xlink:href="#clock-second-half"/></svg>
                </span> <span class="text">2nd Half</span>
            </div>
        </div>
        <div class="table">
            <div class="table-head">
                <div class="cell cell-logo">
                    <div class="name">Bookmakers</div>
                </div>
                <div class="cell cell-one">
                    <div class="name">1</div>
                </div>
                <div class="cell cell-x">
                    <div class="name">X</div>
                </div>
                <div class="cell cell-two">
                    <div class="name">2</div>
                </div>
                <div class="cell cell-payout">
                    <div class="name">Payout</div>
                </div>
            </div>
            <div class="table-body">
                <div class="row">
                    <div class="cell cell-logo">
                        <div class="logo" style="background-image: url('{{ asset('img/odds-table-sample-logo.png') }}')"></div>
                        <span class="i">i</span>
                        <span class="b">B</span>
                        <a href="#" class="link"></a>
                    </div>
                    <div class="cell cell-one">
                        <span class="bubble">3.73 <span class="stats-arr stats-arr-up"></span></span>
                    </div>
                    <div class="cell cell-x">
                        <span class="bubble">3.73 <span class="stats-arr stats-arr-up"></span></span>
                    </div>
                    <div class="cell cell-two">
                        <span class="bubble">3.82 <span class="stats-arr stats-arr-down"></span></span>
                    </div>
                    <div class="cell cell-payout">
                        <span class="percent">98.3%</span>
                    </div>
                </div>
                <div class="row">
                    <div class="cell cell-logo">
                        <div class="logo" style="background-image: url('{{ asset('img/odds-table-sample-logo.png') }}')"></div>
                        <span class="i">i</span>
                        <span class="b">B</span>
                        <a href="#" class="link"></a>
                    </div>
                    <div class="cell cell-one">
                        <span class="bubble">3.73 <span class="stats-arr stats-arr-up"></span></span>
                    </div>
                    <div class="cell cell-x">
                        <span class="bubble">3.73 <span class="stats-arr stats-arr-up"></span></span>
                    </div>
                    <div class="cell cell-two">
                        <span class="bubble">3.82 <span class="stats-arr stats-arr-down"></span></span>
                    </div>
                    <div class="cell cell-payout">
                        <span class="percent">98.3%</span>
                    </div>
                </div>
                <div class="row">
                    <div class="cell cell-logo">
                        <div class="logo" style="background-image: url('{{ asset('img/odds-table-sample-logo.png') }}')"></div>
                        <span class="i">i</span>
                        <span class="b">B</span>
                        <a href="#" class="link"></a>
                    </div>
                    <div class="cell cell-one">
                        <span class="bubble">3.73 <span class="stats-arr stats-arr-up"></span></span>
                    </div>
                    <div class="cell cell-x">
                        <span class="bubble">3.73 <span class="stats-arr stats-arr-up"></span></span>
                    </div>
                    <div class="cell cell-two">
                        <span class="bubble">3.82 <span class="stats-arr stats-arr-down"></span></span>
                    </div>
                    <div class="cell cell-payout">
                        <span class="percent">98.3%</span>
                    </div>
                </div>
                <div class="row">
                    <div class="cell cell-logo">
                        <div class="logo" style="background-image: url('{{ asset('img/odds-table-sample-logo.png') }}')"></div>
                        <span class="i">i</span>
                        <span class="b">B</span>
                        <a href="#" class="link"></a>
                    </div>
                    <div class="cell cell-one">
                        <span class="bubble">3.73 <span class="stats-arr stats-arr-up"></span></span>
                    </div>
                    <div class="cell cell-x">
                        <span class="bubble">3.73 <span class="stats-arr stats-arr-up"></span></span>
                    </div>
                    <div class="cell cell-two">
                        <span class="bubble">3.82 <span class="stats-arr stats-arr-down"></span></span>
                    </div>
                    <div class="cell cell-payout">
                        <span class="percent">98.3%</span>
                    </div>
                </div>
                <div class="row">
                    <div class="cell cell-logo">
                        <div class="logo" style="background-image: url('{{ asset('img/odds-table-sample-logo.png') }}')"></div>
                        <span class="i">i</span>
                        <span class="b">B</span>
                        <a href="#" class="link"></a>
                    </div>
                    <div class="cell cell-one">
                        <span class="bubble">3.73 <span class="stats-arr stats-arr-up"></span></span>
                    </div>
                    <div class="cell cell-x">
                        <span class="bubble">3.73 <span class="stats-arr stats-arr-up"></span></span>
                    </div>
                    <div class="cell cell-two">
                        <span class="bubble">3.82 <span class="stats-arr stats-arr-down"></span></span>
                    </div>
                    <div class="cell cell-payout">
                        <span class="percent">98.3%</span>
                    </div>
                </div>
                <div class="row">
                    <div class="cell cell-logo">
                        <div class="logo" style="background-image: url('{{ asset('img/odds-table-sample-logo.png') }}')"></div>
                        <span class="i">i</span>
                        <span class="b">B</span>
                        <a href="#" class="link"></a>
                    </div>
                    <div class="cell cell-one">
                        <span class="bubble">3.73 <span class="stats-arr stats-arr-up"></span></span>
                    </div>
                    <div class="cell cell-x">
                        <span class="bubble">3.73 <span class="stats-arr stats-arr-up"></span></span>
                    </div>
                    <div class="cell cell-two">
                        <span class="bubble">3.82 <span class="stats-arr stats-arr-down"></span></span>
                    </div>
                    <div class="cell cell-payout">
                        <span class="percent">98.3%</span>
                    </div>
                </div>
                <div class="row">
                    <div class="cell cell-logo">
                        <div class="logo" style="background-image: url('{{ asset('img/odds-table-sample-logo.png') }}')"></div>
                        <span class="i">i</span>
                        <span class="b">B</span>
                        <a href="#" class="link"></a>
                    </div>
                    <div class="cell cell-one">
                        <span class="bubble">3.73 <span class="stats-arr stats-arr-up"></span></span>
                    </div>
                    <div class="cell cell-x">
                        <span class="bubble">3.73 <span class="stats-arr stats-arr-up"></span></span>
                    </div>
                    <div class="cell cell-two">
                        <span class="bubble">3.82 <span class="stats-arr stats-arr-down"></span></span>
                    </div>
                    <div class="cell cell-payout">
                        <span class="percent">98.3%</span>
                    </div>
                </div>
                <div class="row">
                    <div class="cell cell-logo">
                        <div class="logo" style="background-image: url('{{ asset('img/odds-table-sample-logo.png') }}')"></div>
                        <span class="i">i</span>
                        <span class="b">B</span>
                        <a href="#" class="link"></a>
                    </div>
                    <div class="cell cell-one">
                        <span class="bubble">3.73 <span class="stats-arr stats-arr-up"></span></span>
                    </div>
                    <div class="cell cell-x">
                        <span class="bubble">3.73 <span class="stats-arr stats-arr-up"></span></span>
                    </div>
                    <div class="cell cell-two">
                        <span class="bubble">3.82 <span class="stats-arr stats-arr-down"></span></span>
                    </div>
                    <div class="cell cell-payout">
                        <span class="percent">98.3%</span>
                    </div>
                </div>
                <div class="row">
                    <div class="cell cell-logo">
                        <div class="logo" style="background-image: url('{{ asset('img/odds-table-sample-logo.png') }}')"></div>
                        <span class="i">i</span>
                        <span class="b">B</span>
                        <a href="#" class="link"></a>
                    </div>
                    <div class="cell cell-one">
                        <span class="bubble">3.73 <span class="stats-arr stats-arr-up"></span></span>
                    </div>
                    <div class="cell cell-x">
                        <span class="bubble">3.73 <span class="stats-arr stats-arr-up"></span></span>
                    </div>
                    <div class="cell cell-two">
                        <span class="bubble">3.82 <span class="stats-arr stats-arr-down"></span></span>
                    </div>
                    <div class="cell cell-payout">
                        <span class="percent">98.3%</span>
                    </div>
                </div>
                <div class="row">
                    <div class="cell cell-logo">
                        <div class="logo" style="background-image: url('{{ asset('img/odds-table-sample-logo.png') }}')"></div>
                        <span class="i">i</span>
                        <span class="b">B</span>
                        <a href="#" class="link"></a>
                    </div>
                    <div class="cell cell-one">
                        <span class="bubble">3.73 <span class="stats-arr stats-arr-up"></span></span>
                    </div>
                    <div class="cell cell-x">
                        <span class="bubble">3.73 <span class="stats-arr stats-arr-up"></span></span>
                    </div>
                    <div class="cell cell-two">
                        <span class="bubble">3.82 <span class="stats-arr stats-arr-down"></span></span>
                    </div>
                    <div class="cell cell-payout">
                        <span class="percent">98.3%</span>
                    </div>
                </div>
                <div class="row">
                    <div class="cell cell-logo">
                        <div class="logo" style="background-image: url('{{ asset('img/odds-table-sample-logo.png') }}')"></div>
                        <span class="i">i</span>
                        <span class="b">B</span>
                        <a href="#" class="link"></a>
                    </div>
                    <div class="cell cell-one">
                        <span class="bubble">3.73 <span class="stats-arr stats-arr-up"></span></span>
                    </div>
                    <div class="cell cell-x">
                        <span class="bubble">3.73 <span class="stats-arr stats-arr-up"></span></span>
                    </div>
                    <div class="cell cell-two">
                        <span class="bubble">3.82 <span class="stats-arr stats-arr-down"></span></span>
                    </div>
                    <div class="cell cell-payout">
                        <span class="percent">98.3%</span>
                    </div>
                </div>
                <div class="row">
                    <div class="cell cell-logo">
                        <div class="logo" style="background-image: url('{{ asset('img/odds-table-sample-logo.png') }}')"></div>
                        <span class="i">i</span>
                        <span class="b">B</span>
                        <a href="#" class="link"></a>
                    </div>
                    <div class="cell cell-one">
                        <span class="bubble">3.73 <span class="stats-arr stats-arr-up"></span></span>
                    </div>
                    <div class="cell cell-x">
                        <span class="bubble">3.73 <span class="stats-arr stats-arr-up"></span></span>
                    </div>
                    <div class="cell cell-two">
                        <span class="bubble">3.82 <span class="stats-arr stats-arr-down"></span></span>
                    </div>
                    <div class="cell cell-payout">
                        <span class="percent">98.3%</span>
                    </div>
                </div>
                <div class="row">
                    <div class="cell cell-logo">
                        <div class="logo" style="background-image: url('{{ asset('img/odds-table-sample-logo.png') }}')"></div>
                        <span class="i">i</span>
                        <span class="b">B</span>
                        <a href="#" class="link"></a>
                    </div>
                    <div class="cell cell-one">
                        <span class="bubble">3.73 <span class="stats-arr stats-arr-up"></span></span>
                    </div>
                    <div class="cell cell-x">
                        <span class="bubble">3.73 <span class="stats-arr stats-arr-up"></span></span>
                    </div>
                    <div class="cell cell-two">
                        <span class="bubble">3.82 <span class="stats-arr stats-arr-down"></span></span>
                    </div>
                    <div class="cell cell-payout">
                        <span class="percent">98.3%</span>
                    </div>
                </div>
            </div>
        </div>
        <button class="load-more">Load 48 More</button>
        <div class="stats">
            <div class="stats-line avg">
                <div class="name">Average</div>
                <div class="nums">
                    <div class="num">3.73</div>
                    <div class="num">3.73</div>
                    <div class="num">3.82</div>
                    <div class="num percent">98.3%</div>
                </div>
            </div>
            <div class="stats-line highest">
                <div class="name">Highest</div>
                <div class="nums">
                    <div class="num">3.73</div>
                    <div class="num">3.73</div>
                    <div class="num">3.82</div>
                    <div class="num percent">98.3%</div>
                </div>
            </div>
        </div>
    </div>

    <div class="box bg-red c-white add-event-to-tracker">
        <div class="icon"></div>
        <div class="name">Add This Event To My Tracker</div>
        <div class="buttons flex aic">
            <button class="btn btn-small btn-white-opacity">Add</button>
            <button class="btn btn-small btn-white-opacity">Add</button>
            <button class="btn btn-small btn-white-opacity">Add</button>
            <button class="btn btn-small btn-white-opacity" disabled>Remove</button>
        </div>
    </div>
</div>
@endsection
