@extends('layout.app')

@section('content')
<div class="content-wrapper content-next-matches">
    @include('partials.breadcrumbs', $breadcrumbs = [
        [
            'name' => 'Odds Comparison',
            'url' => '#'
        ],
        [
            'name' => 'Football',
            'url' => '#'
        ],
        [
            'name' => 'Leagues',
            'url' => '#'
        ],
        [
            'name' => 'NHL'
        ]
    ])

    <div class="pagesearch">
        <label class="search-label">
            <input class="search-input" type="text" placeholder="You can search by Team or Date ">
        </label>
        <button class="search-btn"></button>
    </div>

    <div class="box">
        <div class="league-tabs">
            <div class="topline flex aic">
                <div class="name flex aic">
                    <span class="icon icon-league-nhl"></span>
                    <span class="text">NHL</span>
                </div>
                <div class="country">
                    <span class="icon icon-flag-arg"></span>
                    <span class="text">Argentina</span>
                </div>
            </div>
            <div class="tabs">
                <div class="item">Next Matches</div>
                <div class="item active">Schedule</div>
                <div class="item">Results</div>
                <div class="item">Standings</div>
            </div>
        </div>
    </div>

    <div class="box no-padding box-table">
        <div class="box-table-header bg-red c-white">
            <div class="cell cell-datetime">03 Oct 2020</div>
            <div class="cell cell-team cell-empty"></div>
            <div class="cell cell-score">Score</div>
            <div class="cell cell-one">1</div>
            <div class="cell cell-x">X</div>
            <div class="cell cell-two">2</div>
            <div class="cell cell-play cell-empty"></div>
        </div>
        <div class="box-table-body">
            <div class="row">
                <div class="cell cell-datetime">
                    <div class="icon icon-playing"></div>
                    <div class="text red">03:18</div>
                </div>
                <div class="cell cell-team">
                    <div class="item">
                        <span class="team-logo"  style="background-image: url({{ asset('img/team1.png') }})"></span>
                        <div class="team-name">Alianza Limba</div>
                    </div>
                    <div class="item">
                        <span class="team-logo"  style="background-image: url({{ asset('img/team2.png') }})"></span>
                        <div class="team-name">Club Deportivo Llacuabama</div>
                    </div>
                </div>
                <div class="cell cell-score">
                    <span class="bubble bubble-red">1 : 0</span>
                </div>
                <div class="cell cell-one">
                    <span class="bubble">3.73</span>
                </div>
                <div class="cell cell-x">
                    <span class="bubble">3.73</span>
                </div>
                <div class="cell cell-two">
                    <span class="bubble">3.73</span>
                </div>
                <div class="cell cell-play">
                    <span class="play-btn"></span>
                </div>
            </div>
            <div class="row">
                <div class="cell cell-datetime">
                    <div class="text">08:30</div>
                </div>
                <div class="cell cell-team">
                    <div class="item">
                        <span class="team-logo" style="background-image: url({{ asset('img/team1.png') }})"></span>
                        <div class="team-name">Alianza Limba</div>
                    </div>
                    <div class="item">
                        <span class="team-logo" style="background-image: url({{ asset('img/team2.png') }})"></span>
                        <div class="team-name">Club Deportivo Llacuabama</div>
                    </div>
                </div>
                <div class="cell cell-score"></div>
                <div class="cell cell-one">
                    <span class="bubble">3.73</span>
                </div>
                <div class="cell cell-x">
                    <span class="bubble">3.73</span>
                </div>
                <div class="cell cell-two">
                    <span class="bubble">3.73</span>
                </div>
                <div class="cell cell-play">
                    <span class="play-btn"></span>
                </div>
            </div>
        </div>
    </div>

    <div class="box no-padding box-table">
        <div class="box-table-header">
            <div class="cell cell-datetime">04 Oct 2020</div>
            <div class="cell cell-team cell-empty"></div>
            <div class="cell cell-score">Score</div>
            <div class="cell cell-one">1</div>
            <div class="cell cell-x">X</div>
            <div class="cell cell-two">2</div>
            <div class="cell cell-play cell-empty"></div>
        </div>
        <div class="box-table-body">
            <div class="row">
                <div class="cell cell-datetime">
                    <div class="text">08:30</div>
                </div>
                <div class="cell cell-team">
                    <div class="item">
                        <span class="team-logo" style="background-image: url({{ asset('img/team1.png') }})"></span>
                        <div class="team-name">Alianza Limba</div>
                    </div>
                    <div class="item">
                        <span class="team-logo" style="background-image: url({{ asset('img/team2.png') }})"></span>
                        <div class="team-name">Club Deportivo Llacuabama</div>
                    </div>
                </div>
                <div class="cell cell-score"></div>
                <div class="cell cell-one">
                    <span class="bubble">3.73</span>
                </div>
                <div class="cell cell-x">
                    <span class="bubble">3.73</span>
                </div>
                <div class="cell cell-two">
                    <span class="bubble">3.73</span>
                </div>
                <div class="cell cell-play">
                    <span class="play-btn"></span>
                </div>
            </div>
            <div class="row">
                <div class="cell cell-datetime">
                    <div class="text">08:30</div>
                </div>
                <div class="cell cell-team">
                    <div class="item">
                        <span class="team-logo" style="background-image: url({{ asset('img/team1.png') }})"></span>
                        <div class="team-name">Alianza Limba</div>
                    </div>
                    <div class="item">
                        <span class="team-logo" style="background-image: url({{ asset('img/team2.png') }})"></span>
                        <div class="team-name">Club Deportivo Llacuabama</div>
                    </div>
                </div>
                <div class="cell cell-score"></div>
                <div class="cell cell-one">
                    <span class="bubble">3.73</span>
                </div>
                <div class="cell cell-x">
                    <span class="bubble">3.73</span>
                </div>
                <div class="cell cell-two">
                    <span class="bubble">3.73</span>
                </div>
                <div class="cell cell-play">
                    <span class="play-btn"></span>
                </div>
            </div>
            <div class="row">
                <div class="cell cell-datetime">
                    <div class="text">08:30</div>
                </div>
                <div class="cell cell-team">
                    <div class="item">
                        <span class="team-logo" style="background-image: url({{ asset('img/team1.png') }})"></span>
                        <div class="team-name">Alianza Limba</div>
                    </div>
                    <div class="item">
                        <span class="team-logo" style="background-image: url({{ asset('img/team2.png') }})"></span>
                        <div class="team-name">Club Deportivo Llacuabama</div>
                    </div>
                </div>
                <div class="cell cell-score"></div>
                <div class="cell cell-one">
                    <span class="bubble">3.73</span>
                </div>
                <div class="cell cell-x">
                    <span class="bubble">3.73</span>
                </div>
                <div class="cell cell-two">
                    <span class="bubble">3.73</span>
                </div>
                <div class="cell cell-play">
                    <span class="play-btn"></span>
                </div>
            </div>
        </div>
    </div>

    <div class="box no-padding box-table">
        <div class="box-table-header">
            <div class="cell cell-datetime">04 Oct 2020</div>
            <div class="cell cell-team cell-empty"></div>
            <div class="cell cell-score">Score</div>
            <div class="cell cell-one">1</div>
            <div class="cell cell-x">X</div>
            <div class="cell cell-two">2</div>
            <div class="cell cell-play cell-empty"></div>
        </div>
        <div class="box-table-body">
            <div class="row">
                <div class="cell cell-datetime">
                    <div class="text">08:30</div>
                </div>
                <div class="cell cell-team">
                    <div class="item">
                        <span class="team-logo" style="background-image: url({{ asset('img/team1.png') }})"></span>
                        <div class="team-name">Alianza Limba</div>
                    </div>
                    <div class="item">
                        <span class="team-logo" style="background-image: url({{ asset('img/team2.png') }})"></span>
                        <div class="team-name">Club Deportivo Llacuabama</div>
                    </div>
                </div>
                <div class="cell cell-score"></div>
                <div class="cell cell-one">
                    <span class="bubble">3.73</span>
                </div>
                <div class="cell cell-x">
                    <span class="bubble">3.73</span>
                </div>
                <div class="cell cell-two">
                    <span class="bubble">3.73</span>
                </div>
                <div class="cell cell-play">
                    <span class="play-btn"></span>
                </div>
            </div>
        </div>
    </div>

    <div class="box no-padding box-table">
        <div class="box-table-header">
            <div class="cell cell-datetime">04 Oct 2020</div>
            <div class="cell cell-team cell-empty"></div>
            <div class="cell cell-score">Score</div>
            <div class="cell cell-one">1</div>
            <div class="cell cell-x">X</div>
            <div class="cell cell-two">2</div>
            <div class="cell cell-play cell-empty"></div>
        </div>
        <div class="box-table-body">
            <div class="row">
                <div class="cell cell-datetime">
                    <div class="text">08:30</div>
                </div>
                <div class="cell cell-team">
                    <div class="item">
                        <span class="team-logo" style="background-image: url({{ asset('img/team1.png') }})"></span>
                        <div class="team-name">Alianza Limba</div>
                    </div>
                    <div class="item">
                        <span class="team-logo" style="background-image: url({{ asset('img/team2.png') }})"></span>
                        <div class="team-name">Club Deportivo Llacuabama</div>
                    </div>
                </div>
                <div class="cell cell-score"></div>
                <div class="cell cell-one">
                    <span class="bubble">3.73</span>
                </div>
                <div class="cell cell-x">
                    <span class="bubble">3.73</span>
                </div>
                <div class="cell cell-two">
                    <span class="bubble">3.73</span>
                </div>
                <div class="cell cell-play">
                    <span class="play-btn"></span>
                </div>
            </div>
            <div class="row">
                <div class="cell cell-datetime">
                    <div class="text">08:30</div>
                </div>
                <div class="cell cell-team">
                    <div class="item">
                        <span class="team-logo" style="background-image: url({{ asset('img/team1.png') }})"></span>
                        <div class="team-name">Alianza Limba</div>
                    </div>
                    <div class="item">
                        <span class="team-logo" style="background-image: url({{ asset('img/team2.png') }})"></span>
                        <div class="team-name">Club Deportivo Llacuabama</div>
                    </div>
                </div>
                <div class="cell cell-score"></div>
                <div class="cell cell-one">
                    <span class="bubble">3.73</span>
                </div>
                <div class="cell cell-x">
                    <span class="bubble">3.73</span>
                </div>
                <div class="cell cell-two">
                    <span class="bubble">3.73</span>
                </div>
                <div class="cell cell-play">
                    <span class="play-btn"></span>
                </div>
            </div>
            <div class="row">
                <div class="cell cell-datetime">
                    <div class="text">08:30</div>
                </div>
                <div class="cell cell-team">
                    <div class="item">
                        <span class="team-logo" style="background-image: url({{ asset('img/team1.png') }})"></span>
                        <div class="team-name">Alianza Limba</div>
                    </div>
                    <div class="item">
                        <span class="team-logo" style="background-image: url({{ asset('img/team2.png') }})"></span>
                        <div class="team-name">Club Deportivo Llacuabama</div>
                    </div>
                </div>
                <div class="cell cell-score"></div>
                <div class="cell cell-one">
                    <span class="bubble">3.73</span>
                </div>
                <div class="cell cell-x">
                    <span class="bubble">3.73</span>
                </div>
                <div class="cell cell-two">
                    <span class="bubble">3.73</span>
                </div>
                <div class="cell cell-play">
                    <span class="play-btn"></span>
                </div>
            </div>
            <div class="row">
                <div class="cell cell-datetime">
                    <div class="text">08:30</div>
                </div>
                <div class="cell cell-team">
                    <div class="item">
                        <span class="team-logo" style="background-image: url({{ asset('img/team1.png') }})"></span>
                        <div class="team-name">Alianza Limba</div>
                    </div>
                    <div class="item">
                        <span class="team-logo" style="background-image: url({{ asset('img/team2.png') }})"></span>
                        <div class="team-name">Club Deportivo Llacuabama</div>
                    </div>
                </div>
                <div class="cell cell-score"></div>
                <div class="cell cell-one">
                    <span class="bubble">3.73</span>
                </div>
                <div class="cell cell-x">
                    <span class="bubble">3.73</span>
                </div>
                <div class="cell cell-two">
                    <span class="bubble">3.73</span>
                </div>
                <div class="cell cell-play">
                    <span class="play-btn"></span>
                </div>
            </div>
        </div>
    </div>

    <div class="box no-padding box-table">
        <div class="box-table-header">
            <div class="cell cell-datetime">05 Oct 2020</div>
        </div>
        <div class="box-table-body">
            <div class="row">
                <div class="cell cell-datetime">
                    <div class="text">08:30</div>
                </div>
                <div class="cell cell-team">
                    <div class="item">
                        <span class="team-logo" style="background-image: url({{ asset('img/team1.png') }})"></span>
                        <div class="team-name">Alianza Limba</div>
                    </div>
                    <div class="item">
                        <span class="team-logo" style="background-image: url({{ asset('img/team2.png') }})"></span>
                        <div class="team-name">Club Deportivo Llacuabama</div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="cell cell-datetime">
                    <div class="text">08:30</div>
                </div>
                <div class="cell cell-team">
                    <div class="item">
                        <span class="team-logo" style="background-image: url({{ asset('img/team1.png') }})"></span>
                        <div class="team-name">Alianza Limba</div>
                    </div>
                    <div class="item">
                        <span class="team-logo" style="background-image: url({{ asset('img/team2.png') }})"></span>
                        <div class="team-name">Club Deportivo Llacuabama</div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="box no-padding box-table">
        <div class="box-table-header">
            <div class="cell cell-datetime">05 Oct 2020</div>
        </div>
        <div class="box-table-body">
            <div class="row">
                <div class="cell cell-datetime">
                    <div class="text">08:30</div>
                </div>
                <div class="cell cell-team">
                    <div class="item">
                        <span class="team-logo" style="background-image: url({{ asset('img/team1.png') }})"></span>
                        <div class="team-name">Alianza Limba</div>
                    </div>
                    <div class="item">
                        <span class="team-logo" style="background-image: url({{ asset('img/team2.png') }})"></span>
                        <div class="team-name">Club Deportivo Llacuabama</div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="cell cell-datetime">
                    <div class="text">08:30</div>
                </div>
                <div class="cell cell-team">
                    <div class="item">
                        <span class="team-logo" style="background-image: url({{ asset('img/team1.png') }})"></span>
                        <div class="team-name">Alianza Limba</div>
                    </div>
                    <div class="item">
                        <span class="team-logo" style="background-image: url({{ asset('img/team2.png') }})"></span>
                        <div class="team-name">Club Deportivo Llacuabama</div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="cell cell-datetime">
                    <div class="text">08:30</div>
                </div>
                <div class="cell cell-team">
                    <div class="item">
                        <span class="team-logo" style="background-image: url({{ asset('img/team1.png') }})"></span>
                        <div class="team-name">Alianza Limba</div>
                    </div>
                    <div class="item">
                        <span class="team-logo" style="background-image: url({{ asset('img/team2.png') }})"></span>
                        <div class="team-name">Club Deportivo Llacuabama</div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="box no-padding box-table">
        <div class="box-table-header">
            <div class="cell cell-datetime">05 Oct 2020</div>
        </div>
        <div class="box-table-body">
            <div class="row">
                <div class="cell cell-datetime">
                    <div class="text">08:30</div>
                </div>
                <div class="cell cell-team">
                    <div class="item">
                        <span class="team-logo" style="background-image: url({{ asset('img/team1.png') }})"></span>
                        <div class="team-name">Alianza Limba</div>
                    </div>
                    <div class="item">
                        <span class="team-logo" style="background-image: url({{ asset('img/team2.png') }})"></span>
                        <div class="team-name">Club Deportivo Llacuabama</div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="cell cell-datetime">
                    <div class="text">08:30</div>
                </div>
                <div class="cell cell-team">
                    <div class="item">
                        <span class="team-logo" style="background-image: url({{ asset('img/team1.png') }})"></span>
                        <div class="team-name">Alianza Limba</div>
                    </div>
                    <div class="item">
                        <span class="team-logo" style="background-image: url({{ asset('img/team2.png') }})"></span>
                        <div class="team-name">Club Deportivo Llacuabama</div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
