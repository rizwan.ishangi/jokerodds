@extends('layout.app')

@section('content')
<div class="content-wrapper content-next-matches">
    @include('partials.breadcrumbs', $breadcrumbs = [
        [
            'name' => 'Odds Comparison',
            'url' => '#'
        ],
        [
            'name' => 'Football',
            'url' => '#'
        ],
        [
            'name' => 'Leagues',
            'url' => '#'
        ],
        [
            'name' => 'NHL'
        ]
    ])

    <div class="pagesearch">
        <label class="search-label">
            <input class="search-input" type="text" placeholder="You can search by Team or Date ">
        </label>
        <button class="search-btn"></button>
    </div>

    <div class="box">
        <div class="league-tabs">
            <div class="topline flex aic">
                <div class="name flex aic">
                    <span class="icon icon-league-nhl"></span>
                    <span class="text">NHL</span>
                </div>
                <div class="country">
                    <span class="icon icon-flag-arg"></span>
                    <span class="text">Argentina</span>
                </div>
            </div>
            <div class="tabs">
                <div class="item">Next Matches</div>
                <div class="item">Schedule</div>
                <div class="item">Results</div>
                <div class="item active">Standings</div>
            </div>
        </div>

        <div class="league-standings-types">
            <div class="item active">Overall</div>
            <div class="item">Home</div>
            <div class="item">Away</div>
        </div>

        <div class="league-standings-table">
            <div class="table-head">
                <div class="row">
                    <div class="cell cell-n">#</div>
                    <div class="cell cell-team">Team Name</div>
                    <div class="cell cell-p">P</div>
                    <div class="cell cell-w">W</div>
                    <div class="cell cell-d">D</div>
                    <div class="cell cell-l">L</div>
                    <div class="cell cell-goals">Goals</div>
                    <div class="cell cell-lastfive">Last 5</div>
                    <div class="cell cell-pts">Points</div>
                </div>
            </div>
            <div class="table-body">
                <div class="row row-promo-championsleague">
                    <div class="cell cell-n"></div>
                    <div class="cell cell-team">
                        <div class="team-logo" style="background-image: url('../img/team1.png')"></div>
                        <div class="team-name">Everton</div>
                    </div>
                    <div class="cell cell-p">8</div>
                    <div class="cell cell-w">6</div>
                    <div class="cell cell-d">0</div>
                    <div class="cell cell-l">2</div>
                    <div class="cell cell-goals">18 : 9</div>
                    <div class="cell cell-lastfive">
                        <i class="item item-green"></i>
                        <i class="item item-green"></i>
                        <i class="item item-green"></i>
                        <i class="item item-red"></i>
                        <i class="item item-red"></i>
                    </div>
                    <div class="cell cell-pts">18</div>
                </div>
                <div class="row row-promo-championsleague">
                    <div class="cell cell-n"></div>
                    <div class="cell cell-team">
                        <div class="team-logo" style="background-image: url('../img/team1.png')"></div>
                        <div class="team-name">Everton</div>
                    </div>
                    <div class="cell cell-p">8</div>
                    <div class="cell cell-w">6</div>
                    <div class="cell cell-d">0</div>
                    <div class="cell cell-l">2</div>
                    <div class="cell cell-goals">18 : 9</div>
                    <div class="cell cell-lastfive">
                        <i class="item item-green"></i>
                        <i class="item item-green"></i>
                        <i class="item item-green"></i>
                        <i class="item item-red"></i>
                        <i class="item item-red"></i>
                    </div>
                    <div class="cell cell-pts">18</div>
                </div>
                <div class="row row-promo-championsleague">
                    <div class="cell cell-n"></div>
                    <div class="cell cell-team">
                        <div class="team-logo" style="background-image: url('../img/team1.png')"></div>
                        <div class="team-name">Everton</div>
                    </div>
                    <div class="cell cell-p">8</div>
                    <div class="cell cell-w">6</div>
                    <div class="cell cell-d">0</div>
                    <div class="cell cell-l">2</div>
                    <div class="cell cell-goals">18 : 9</div>
                    <div class="cell cell-lastfive">
                        <i class="item item-green"></i>
                        <i class="item item-green"></i>
                        <i class="item item-green"></i>
                        <i class="item item-red"></i>
                        <i class="item item-red"></i>
                    </div>
                    <div class="cell cell-pts">18</div>
                </div>
                <div class="row row-promo-championsleague">
                    <div class="cell cell-n"></div>
                    <div class="cell cell-team">
                        <div class="team-logo" style="background-image: url('../img/team1.png')"></div>
                        <div class="team-name">Everton</div>
                    </div>
                    <div class="cell cell-p">8</div>
                    <div class="cell cell-w">6</div>
                    <div class="cell cell-d">0</div>
                    <div class="cell cell-l">2</div>
                    <div class="cell cell-goals">18 : 9</div>
                    <div class="cell cell-lastfive">
                        <i class="item item-green"></i>
                        <i class="item item-green"></i>
                        <i class="item item-green"></i>
                        <i class="item item-red"></i>
                        <i class="item item-red"></i>
                    </div>
                    <div class="cell cell-pts">18</div>
                </div>
                <div class="row row-promo-europa">
                    <div class="cell cell-n"></div>
                    <div class="cell cell-team">
                        <div class="team-logo" style="background-image: url('../img/team1.png')"></div>
                        <div class="team-name">Everton</div>
                    </div>
                    <div class="cell cell-p">8</div>
                    <div class="cell cell-w">6</div>
                    <div class="cell cell-d">0</div>
                    <div class="cell cell-l">2</div>
                    <div class="cell cell-goals">18 : 9</div>
                    <div class="cell cell-lastfive">
                        <i class="item item-green"></i>
                        <i class="item item-green"></i>
                        <i class="item item-green"></i>
                        <i class="item item-red"></i>
                        <i class="item item-red"></i>
                    </div>
                    <div class="cell cell-pts">18</div>
                </div>

                <div class="row">
                    <div class="cell cell-n"></div>
                    <div class="cell cell-team">
                        <div class="team-logo" style="background-image: url('../img/team1.png')"></div>
                        <div class="team-name">Everton</div>
                    </div>
                    <div class="cell cell-p">8</div>
                    <div class="cell cell-w">6</div>
                    <div class="cell cell-d">0</div>
                    <div class="cell cell-l">2</div>
                    <div class="cell cell-goals">18 : 9</div>
                    <div class="cell cell-lastfive">
                        <i class="item item-green"></i>
                        <i class="item item-green"></i>
                        <i class="item item-green"></i>
                        <i class="item item-red"></i>
                        <i class="item item-red"></i>
                    </div>
                    <div class="cell cell-pts">18</div>
                </div>
                <div class="row">
                    <div class="cell cell-n"></div>
                    <div class="cell cell-team">
                        <div class="team-logo" style="background-image: url('../img/team1.png')"></div>
                        <div class="team-name">Everton</div>
                    </div>
                    <div class="cell cell-p">8</div>
                    <div class="cell cell-w">6</div>
                    <div class="cell cell-d">0</div>
                    <div class="cell cell-l">2</div>
                    <div class="cell cell-goals">18 : 9</div>
                    <div class="cell cell-lastfive">
                        <i class="item item-green"></i>
                        <i class="item item-green"></i>
                        <i class="item item-green"></i>
                        <i class="item item-red"></i>
                        <i class="item item-red"></i>
                    </div>
                    <div class="cell cell-pts">18</div>
                </div>
                <div class="row">
                    <div class="cell cell-n"></div>
                    <div class="cell cell-team">
                        <div class="team-logo" style="background-image: url('../img/team1.png')"></div>
                        <div class="team-name">Everton</div>
                    </div>
                    <div class="cell cell-p">8</div>
                    <div class="cell cell-w">6</div>
                    <div class="cell cell-d">0</div>
                    <div class="cell cell-l">2</div>
                    <div class="cell cell-goals">18 : 9</div>
                    <div class="cell cell-lastfive">
                        <i class="item item-green"></i>
                        <i class="item item-green"></i>
                        <i class="item item-green"></i>
                        <i class="item item-red"></i>
                        <i class="item item-red"></i>
                    </div>
                    <div class="cell cell-pts">18</div>
                </div>
                <div class="row">
                    <div class="cell cell-n"></div>
                    <div class="cell cell-team">
                        <div class="team-logo" style="background-image: url('../img/team1.png')"></div>
                        <div class="team-name">Everton</div>
                    </div>
                    <div class="cell cell-p">8</div>
                    <div class="cell cell-w">6</div>
                    <div class="cell cell-d">0</div>
                    <div class="cell cell-l">2</div>
                    <div class="cell cell-goals">18 : 9</div>
                    <div class="cell cell-lastfive">
                        <i class="item item-green"></i>
                        <i class="item item-green"></i>
                        <i class="item item-green"></i>
                        <i class="item item-red"></i>
                        <i class="item item-red"></i>
                    </div>
                    <div class="cell cell-pts">18</div>
                </div>
                <div class="row">
                    <div class="cell cell-n"></div>
                    <div class="cell cell-team">
                        <div class="team-logo" style="background-image: url('../img/team1.png')"></div>
                        <div class="team-name">Everton</div>
                    </div>
                    <div class="cell cell-p">8</div>
                    <div class="cell cell-w">6</div>
                    <div class="cell cell-d">0</div>
                    <div class="cell cell-l">2</div>
                    <div class="cell cell-goals">18 : 9</div>
                    <div class="cell cell-lastfive">
                        <i class="item item-green"></i>
                        <i class="item item-green"></i>
                        <i class="item item-green"></i>
                        <i class="item item-red"></i>
                        <i class="item item-red"></i>
                    </div>
                    <div class="cell cell-pts">18</div>
                </div>
                <div class="row">
                    <div class="cell cell-n"></div>
                    <div class="cell cell-team">
                        <div class="team-logo" style="background-image: url('../img/team1.png')"></div>
                        <div class="team-name">Everton</div>
                    </div>
                    <div class="cell cell-p">8</div>
                    <div class="cell cell-w">6</div>
                    <div class="cell cell-d">0</div>
                    <div class="cell cell-l">2</div>
                    <div class="cell cell-goals">18 : 9</div>
                    <div class="cell cell-lastfive">
                        <i class="item item-green"></i>
                        <i class="item item-green"></i>
                        <i class="item item-green"></i>
                        <i class="item item-red"></i>
                        <i class="item item-red"></i>
                    </div>
                    <div class="cell cell-pts">18</div>
                </div>
                <div class="row">
                    <div class="cell cell-n"></div>
                    <div class="cell cell-team">
                        <div class="team-logo" style="background-image: url('../img/team1.png')"></div>
                        <div class="team-name">Everton</div>
                    </div>
                    <div class="cell cell-p">8</div>
                    <div class="cell cell-w">6</div>
                    <div class="cell cell-d">0</div>
                    <div class="cell cell-l">2</div>
                    <div class="cell cell-goals">18 : 9</div>
                    <div class="cell cell-lastfive">
                        <i class="item item-green"></i>
                        <i class="item item-green"></i>
                        <i class="item item-green"></i>
                        <i class="item item-red"></i>
                        <i class="item item-red"></i>
                    </div>
                    <div class="cell cell-pts">18</div>
                </div>
                <div class="row">
                    <div class="cell cell-n"></div>
                    <div class="cell cell-team">
                        <div class="team-logo" style="background-image: url('../img/team1.png')"></div>
                        <div class="team-name">Everton</div>
                    </div>
                    <div class="cell cell-p">8</div>
                    <div class="cell cell-w">6</div>
                    <div class="cell cell-d">0</div>
                    <div class="cell cell-l">2</div>
                    <div class="cell cell-goals">18 : 9</div>
                    <div class="cell cell-lastfive">
                        <i class="item item-green"></i>
                        <i class="item item-green"></i>
                        <i class="item item-green"></i>
                        <i class="item item-red"></i>
                        <i class="item item-red"></i>
                    </div>
                    <div class="cell cell-pts">18</div>
                </div>
                <div class="row">
                    <div class="cell cell-n"></div>
                    <div class="cell cell-team">
                        <div class="team-logo" style="background-image: url('../img/team1.png')"></div>
                        <div class="team-name">Everton</div>
                    </div>
                    <div class="cell cell-p">8</div>
                    <div class="cell cell-w">6</div>
                    <div class="cell cell-d">0</div>
                    <div class="cell cell-l">2</div>
                    <div class="cell cell-goals">18 : 9</div>
                    <div class="cell cell-lastfive">
                        <i class="item item-green"></i>
                        <i class="item item-green"></i>
                        <i class="item item-green"></i>
                        <i class="item item-red"></i>
                        <i class="item item-red"></i>
                    </div>
                    <div class="cell cell-pts">18</div>
                </div>
                <div class="row">
                    <div class="cell cell-n"></div>
                    <div class="cell cell-team">
                        <div class="team-logo" style="background-image: url('../img/team1.png')"></div>
                        <div class="team-name">Everton</div>
                    </div>
                    <div class="cell cell-p">8</div>
                    <div class="cell cell-w">6</div>
                    <div class="cell cell-d">0</div>
                    <div class="cell cell-l">2</div>
                    <div class="cell cell-goals">18 : 9</div>
                    <div class="cell cell-lastfive">
                        <i class="item item-green"></i>
                        <i class="item item-green"></i>
                        <i class="item item-green"></i>
                        <i class="item item-red"></i>
                        <i class="item item-red"></i>
                    </div>
                    <div class="cell cell-pts">18</div>
                </div>
                <div class="row">
                    <div class="cell cell-n"></div>
                    <div class="cell cell-team">
                        <div class="team-logo" style="background-image: url('../img/team1.png')"></div>
                        <div class="team-name">Everton</div>
                    </div>
                    <div class="cell cell-p">8</div>
                    <div class="cell cell-w">6</div>
                    <div class="cell cell-d">0</div>
                    <div class="cell cell-l">2</div>
                    <div class="cell cell-goals">18 : 9</div>
                    <div class="cell cell-lastfive">
                        <i class="item item-green"></i>
                        <i class="item item-green"></i>
                        <i class="item item-green"></i>
                        <i class="item item-red"></i>
                        <i class="item item-red"></i>
                    </div>
                    <div class="cell cell-pts">18</div>
                </div>
                <div class="row">
                    <div class="cell cell-n"></div>
                    <div class="cell cell-team">
                        <div class="team-logo" style="background-image: url('../img/team1.png')"></div>
                        <div class="team-name">Everton</div>
                    </div>
                    <div class="cell cell-p">8</div>
                    <div class="cell cell-w">6</div>
                    <div class="cell cell-d">0</div>
                    <div class="cell cell-l">2</div>
                    <div class="cell cell-goals">18 : 9</div>
                    <div class="cell cell-lastfive">
                        <i class="item item-green"></i>
                        <i class="item item-green"></i>
                        <i class="item item-green"></i>
                        <i class="item item-red"></i>
                        <i class="item item-red"></i>
                    </div>
                    <div class="cell cell-pts">18</div>
                </div>
                <div class="row row-promo-championship">
                    <div class="cell cell-n"></div>
                    <div class="cell cell-team">
                        <div class="team-logo" style="background-image: url('../img/team1.png')"></div>
                        <div class="team-name">Everton</div>
                    </div>
                    <div class="cell cell-p">8</div>
                    <div class="cell cell-w">6</div>
                    <div class="cell cell-d">0</div>
                    <div class="cell cell-l">2</div>
                    <div class="cell cell-goals">18 : 9</div>
                    <div class="cell cell-lastfive">
                        <i class="item item-green"></i>
                        <i class="item item-green"></i>
                        <i class="item item-green"></i>
                        <i class="item item-red"></i>
                        <i class="item item-red"></i>
                    </div>
                    <div class="cell cell-pts">18</div>
                </div>
                <div class="row row-promo-championship">
                    <div class="cell cell-n"></div>
                    <div class="cell cell-team">
                        <div class="team-logo" style="background-image: url('../img/team1.png')"></div>
                        <div class="team-name">Everton</div>
                    </div>
                    <div class="cell cell-p">8</div>
                    <div class="cell cell-w">6</div>
                    <div class="cell cell-d">0</div>
                    <div class="cell cell-l">2</div>
                    <div class="cell cell-goals">18 : 9</div>
                    <div class="cell cell-lastfive">
                        <i class="item item-green"></i>
                        <i class="item item-green"></i>
                        <i class="item item-green"></i>
                        <i class="item item-red"></i>
                        <i class="item item-red"></i>
                    </div>
                    <div class="cell cell-pts">18</div>
                </div>
                <div class="row row-promo-championship">
                    <div class="cell cell-n"></div>
                    <div class="cell cell-team">
                        <div class="team-logo" style="background-image: url('../img/team1.png')"></div>
                        <div class="team-name">Everton</div>
                    </div>
                    <div class="cell cell-p">8</div>
                    <div class="cell cell-w">6</div>
                    <div class="cell cell-d">0</div>
                    <div class="cell cell-l">2</div>
                    <div class="cell cell-goals">18 : 9</div>
                    <div class="cell cell-lastfive">
                        <i class="item item-green"></i>
                        <i class="item item-green"></i>
                        <i class="item item-green"></i>
                        <i class="item item-red"></i>
                        <i class="item item-red"></i>
                    </div>
                    <div class="cell cell-pts">18</div>
                </div>
            </div>
        </div>

        <div class="league-standings-legend">
            <div class="item item-championsleague">Promotion - Champions League (Group Stage)</div>
            <div class="item item-europaleague">Promotion - Europa League (Group Stage)</div>
            <div class="item item-championship">Relegation - Championship</div>
        </div>

        <div class="league-standings-hint">
            In the event that two (or more) teams have an equal number of points, the following rules break the tie:<br/>1. Number of victories 2. Goal difference 3. Goals scored
        </div>
    </div>

</div>
@endsection
