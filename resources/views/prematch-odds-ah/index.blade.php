@extends('layout.app')

@section('content')
<div class="content-wrapper content-next-matches">
    @include('partials.breadcrumbs', $breadcrumbs = [
        [
            'name' => 'Odds Comparison',
            'url' => '#'
        ],
        [
            'name' => 'Football',
            'url' => '#'
        ],
        [
            'name' => 'Next Matches',
            'url' => '#'
        ],
        [
            'name' => 'Premier League Odds',
            'url' => '#'
        ],
        [
            'name' => 'Everton - West Ham'
        ]
    ])

    <div class="box odds-table">
        <div class="teams">
            <div class="team-one team-item">
                <div class="team-logo" style="background-image: url('{{ asset('img/team1bigger.png') }}')"></div>
                <div class="team-name">Everton</div>
            </div>
            <div class="score">
                <div class="score-box">
                    <div class="score-main"><span>1</span> : <span>0</span></div>
                    <div class="score-sub">( 3 : 2 )</div>
                </div>
                <div class="play">98’</div>
            </div>
            <div class="team-two team-item">
                <div class="team-logo" style="background-image: url('{{ asset('img/team2bigger.png') }}')"></div>
                <div class="team-name">West Ham</div>
            </div>
        </div>
        <div class="tabs">
            <div class="tabs-item active">
                <span class="tabname">Pre-match Odds</span>
            </div>
            <div class="tabs-item">
                <span class="tabname"><span class="icon icon-play"></span>In-Play Odds</span>
            </div>
            <div class="tabs-item">
                <span class="tabname">Game Center</span>
            </div>
        </div>
        <div class="nav">
            <div class="nav-item">1X2</div>
            <div class="nav-item active">AH</div>
            <div class="nav-item">O/U</div>
            <div class="nav-item">DNB</div>
            <div class="nav-item">DC</div>
            <div class="nav-item nav-item-more">MORE</div>
        </div>
        <div class="times">
            <div class="times-item active">
                <span class="icon">
                    <svg><use xlink:href="#time-full" /></svg>
                </span> <span class="text">Full Time</span>
            </div>
            <div class="times-item">
                <span class="icon">
                    <svg><use xlink:href="#clock-first-half"/></svg>
                </span> <span class="text">1st Half</span>
            </div>
            <div class="times-item">
                <span class="icon">
                    <svg><use xlink:href="#clock-second-half"/></svg>
                </span> <span class="text">2nd Half</span>
            </div>
        </div>


        <div class="table">
            <div class="table-head">
                <div class="cell cell-handicap">
                    <div class="name">Handicap</div>
                </div>
                <div class="cell cell-odds">
                    <div class="name">Odds</div>
                </div>
                <div class="cell cell-one">
                    <div class="name">1</div>
                </div>
                <div class="cell cell-two">
                    <div class="name">2</div>
                </div>
                <div class="cell cell-payout">
                    <div class="name">Payout</div>
                </div>
                <div class="cell cell-arr cell-empty"></div>
            </div>
            <div class="table-body">
                <div class="row row-thin">
                    <div class="cell cell-handicap">
                        <div class="handicap-name">Asian Handicap</div>
                        <div class="bubble handicap-num">-4</div>
                    </div>
                    <div class="cell cell-odds">
                        <span>1</span>
                    </div>
                    <div class="cell cell-one">
                        <span class="bubble">3.73 <span class="stats-arr stats-arr-up"></span></span>
                    </div>
                    <div class="cell cell-two">
                        <span class="bubble">3.82 <span class="stats-arr stats-arr-down"></span></span>
                    </div>
                    <div class="cell cell-payout">
                        <span class="percent">98.3%</span>
                    </div>
                    <div class="cell cell-arr">
                        <div class="toggle-arr">
                            <svg><use xlink:href="#toggle-arr"/></svg>
                        </div>
                    </div>
                </div>
                <div class="row row-thin">
                    <div class="cell cell-handicap">
                        <div class="handicap-name">Asian Handicap</div>
                        <div class="bubble handicap-num">-4</div>
                    </div>
                    <div class="cell cell-odds">
                        <span>1</span>
                    </div>
                    <div class="cell cell-one">
                        <span class="bubble">3.73 <span class="stats-arr stats-arr-up"></span></span>
                    </div>
                    <div class="cell cell-two">
                        <span class="bubble">3.82 <span class="stats-arr stats-arr-down"></span></span>
                    </div>
                    <div class="cell cell-payout">
                        <span class="percent">98.3%</span>
                    </div>
                    <div class="cell cell-arr">
                        <div class="toggle-arr">
                            <svg><use xlink:href="#toggle-arr"/></svg>
                        </div>
                    </div>
                </div>
                <div class="row row-thin">
                    <div class="cell cell-handicap">
                        <div class="handicap-name">Asian Handicap</div>
                        <div class="bubble handicap-num">-4</div>
                    </div>
                    <div class="cell cell-odds">
                        <span>1</span>
                    </div>
                    <div class="cell cell-one">
                        <span class="bubble">3.73 <span class="stats-arr stats-arr-up"></span></span>
                    </div>
                    <div class="cell cell-two">
                        <span class="bubble">3.82 <span class="stats-arr stats-arr-down"></span></span>
                    </div>
                    <div class="cell cell-payout">
                        <span class="percent">98.3%</span>
                    </div>
                    <div class="cell cell-arr">
                        <div class="toggle-arr">
                            <svg><use xlink:href="#toggle-arr"/></svg>
                        </div>
                    </div>
                </div>
                <div class="row row-thin">
                    <div class="cell cell-handicap">
                        <div class="handicap-name">Asian Handicap</div>
                        <div class="bubble handicap-num">-4</div>
                    </div>
                    <div class="cell cell-odds">
                        <span>1</span>
                    </div>
                    <div class="cell cell-one">
                        <span class="bubble">3.73 <span class="stats-arr stats-arr-up"></span></span>
                    </div>
                    <div class="cell cell-two">
                        <span class="bubble">3.82 <span class="stats-arr stats-arr-down"></span></span>
                    </div>
                    <div class="cell cell-payout">
                        <span class="percent">98.3%</span>
                    </div>
                    <div class="cell cell-arr">
                        <div class="toggle-arr">
                            <svg><use xlink:href="#toggle-arr"/></svg>
                        </div>
                    </div>
                </div>
                <div class="row row-thin">
                    <div class="cell cell-handicap">
                        <div class="handicap-name">Asian Handicap</div>
                        <div class="bubble handicap-num">-4</div>
                    </div>
                    <div class="cell cell-odds">
                        <span>1</span>
                    </div>
                    <div class="cell cell-one">
                        <span class="bubble">3.73 <span class="stats-arr stats-arr-up"></span></span>
                    </div>
                    <div class="cell cell-two">
                        <span class="bubble">3.82 <span class="stats-arr stats-arr-down"></span></span>
                    </div>
                    <div class="cell cell-payout">
                        <span class="percent">98.3%</span>
                    </div>
                    <div class="cell cell-arr">
                        <div class="toggle-arr">
                            <svg><use xlink:href="#toggle-arr"/></svg>
                        </div>
                    </div>
                </div>
                <div class="row row-thin">
                    <div class="cell cell-handicap">
                        <div class="handicap-name">Asian Handicap</div>
                        <div class="bubble handicap-num">-4</div>
                    </div>
                    <div class="cell cell-odds">
                        <span>1</span>
                    </div>
                    <div class="cell cell-one">
                        <span class="bubble">3.73 <span class="stats-arr stats-arr-up"></span></span>
                    </div>
                    <div class="cell cell-two">
                        <span class="bubble">3.82 <span class="stats-arr stats-arr-down"></span></span>
                    </div>
                    <div class="cell cell-payout">
                        <span class="percent">98.3%</span>
                    </div>
                    <div class="cell cell-arr">
                        <div class="toggle-arr">
                            <svg><use xlink:href="#toggle-arr"/></svg>
                        </div>
                    </div>
                </div>
                <div class="row row-thin">
                    <div class="cell cell-handicap">
                        <div class="handicap-name">Asian Handicap</div>
                        <div class="bubble handicap-num">-4</div>
                    </div>
                    <div class="cell cell-odds">
                        <span>1</span>
                    </div>
                    <div class="cell cell-one">
                        <span class="bubble">3.73 <span class="stats-arr stats-arr-up"></span></span>
                    </div>
                    <div class="cell cell-two">
                        <span class="bubble">3.82 <span class="stats-arr stats-arr-down"></span></span>
                    </div>
                    <div class="cell cell-payout">
                        <span class="percent">98.3%</span>
                    </div>
                    <div class="cell cell-arr">
                        <div class="toggle-arr">
                            <svg><use xlink:href="#toggle-arr"/></svg>
                        </div>
                    </div>
                </div>
                <div class="row row-thin has-subtable show-subtable">
                    <div class="cell cell-handicap">
                        <div class="handicap-name">Asian Handicap</div>
                        <div class="bubble handicap-num">-4</div>
                    </div>
                    <div class="cell cell-odds">
                        <span>1</span>
                    </div>
                    <div class="cell cell-one">
                        <span class="bubble">3.73 <span class="stats-arr stats-arr-up"></span></span>
                    </div>
                    <div class="cell cell-two">
                        <span class="bubble">3.82 <span class="stats-arr stats-arr-down"></span></span>
                    </div>
                    <div class="cell cell-payout">
                        <span class="percent">98.3%</span>
                    </div>
                    <div class="cell cell-arr">
                        <div class="toggle-arr">
                            <svg><use xlink:href="#toggle-arr"/></svg>
                        </div>
                    </div>
                </div>
                <div class="subtable">
                    <div class="subtable-head">
                        <div class="cell cell-bookmaker">
                            <div class="name">Bookmaker</div>
                        </div>
                        <div class="cell cell-handicap">
                            <div class="name">Handicap</div>
                        </div>
                        <div class="cell cell-one">
                            <div class="name">1</div>
                        </div>
                        <div class="cell cell-two">
                            <div class="name">2</div>
                        </div>
                        <div class="cell cell-payout">
                            <div class="name">Payout</div>
                        </div>
                    </div>
                    <div class="subtable-table">
                        <div class="row">
                            <div class="cell cell-bookmaker">
                                <div class="logo" style="background-image: url('{{ asset('img/odds-table-sample-logo.png') }}')"></div>
                                <a href="#" class="link"></a>
                            </div>
                            <div class="cell cell-handicap">
                                <div class="bubble handicap-num">-3.25</div>
                            </div>
                            <div class="cell cell-one">
                                <span class="bubble">3.73 <span class="stats-arr stats-arr-up"></span></span>
                            </div>
                            <div class="cell cell-two">
                                <span class="bubble">3.82 <span class="stats-arr stats-arr-down"></span></span>
                            </div>
                            <div class="cell cell-payout">
                                <span class="percent">98.3%</span>
                            </div>
                        </div>
                        <div class="row">
                            <div class="cell cell-bookmaker">
                                <div class="logo" style="background-image: url('{{ asset('img/odds-table-sample-logo.png') }}')"></div>
                                <a href="#" class="link"></a>
                            </div>
                            <div class="cell cell-handicap">
                                <div class="bubble handicap-num">-3.25</div>
                            </div>
                            <div class="cell cell-one">
                                <span class="bubble">3.73 <span class="stats-arr stats-arr-up"></span></span>
                            </div>
                            <div class="cell cell-two">
                                <span class="bubble">3.82 <span class="stats-arr stats-arr-down"></span></span>
                            </div>
                            <div class="cell cell-payout">
                                <span class="percent">98.3%</span>
                            </div>
                        </div>
                        <div class="row">
                            <div class="cell cell-bookmaker">
                                <div class="logo" style="background-image: url('{{ asset('img/odds-table-sample-logo.png') }}')"></div>
                                <a href="#" class="link"></a>
                            </div>
                            <div class="cell cell-handicap">
                                <div class="bubble handicap-num">-3.25</div>
                            </div>
                            <div class="cell cell-one">
                                <span class="bubble">3.73 <span class="stats-arr stats-arr-up"></span></span>
                            </div>
                            <div class="cell cell-two">
                                <span class="bubble">3.82 <span class="stats-arr stats-arr-down"></span></span>
                            </div>
                            <div class="cell cell-payout">
                                <span class="percent">98.3%</span>
                            </div>
                        </div>
                        <div class="row">
                            <div class="cell cell-bookmaker">
                                <div class="logo" style="background-image: url('{{ asset('img/odds-table-sample-logo.png') }}')"></div>
                                <a href="#" class="link"></a>
                            </div>
                            <div class="cell cell-handicap">
                                <div class="bubble handicap-num">-3.25</div>
                            </div>
                            <div class="cell cell-one">
                                <span class="bubble">
                                    3.73 <span class="stats-arr stats-arr-up"></span>

                                    <div class="odds-queue">
                                        <div class="queue-head">
                                            <div class="title">Opening odds at:</div>
                                            <div class="when">
                                                <div class="datetime">01 Oct, 6:31</div>
                                                <div class="num">3.73</div>
                                            </div>
                                        </div>
                                        <div class="queue-items">
                                            <div class="queue-item">
                                                <div class="datetime">02 Oct, 06:31</div>
                                                <div class="num">3.73</div>
                                                <div class="diff diff-red">-0.20</div>
                                            </div>
                                            <div class="queue-item">
                                                <div class="datetime">02 Oct, 06:31</div>
                                                <div class="num">3.73</div>
                                                <div class="diff diff-red">-0.20</div>
                                            </div>
                                            <div class="queue-item">
                                                <div class="datetime">02 Oct, 08:11</div>
                                                <div class="num">3.73</div>
                                                <div class="diff diff-green">+0.13</div>
                                            </div>
                                            <div class="queue-item">
                                                <div class="datetime">02 Oct, 08:11</div>
                                                <div class="num">3.73</div>
                                                <div class="diff diff-green">+0.13</div>
                                            </div>
                                            <div class="queue-item">
                                                <div class="datetime">02 Oct, 06:31</div>
                                                <div class="num">3.73</div>
                                                <div class="diff diff-red">-0.20</div>
                                            </div>
                                            <div class="queue-item">
                                                <div class="datetime">02 Oct, 08:11</div>
                                                <div class="num">3.73</div>
                                                <div class="diff diff-green">+0.13</div>
                                            </div>
                                            <div class="queue-item">
                                                <div class="datetime">02 Oct, 08:11</div>
                                                <div class="num">3.73</div>
                                                <div class="diff diff-green">+0.13</div>
                                            </div>
                                            <div class="queue-item">
                                                <div class="datetime">02 Oct, 08:11</div>
                                                <div class="num">3.73</div>
                                                <div class="diff diff-green">+0.13</div>
                                            </div>
                                            <div class="queue-item">
                                                <div class="datetime">02 Oct, 08:11</div>
                                                <div class="num">3.73</div>
                                                <div class="diff diff-green">+0.13</div>
                                            </div>
                                            <div class="queue-item">
                                                <div class="datetime">02 Oct, 08:11</div>
                                                <div class="num">3.73</div>
                                                <div class="diff diff-green">+0.13</div>
                                            </div>
                                            <div class="queue-item">
                                                <div class="datetime">02 Oct, 08:11</div>
                                                <div class="num">3.73</div>
                                                <div class="diff diff-green">+0.13</div>
                                            </div>
                                            <div class="queue-item">
                                                <div class="datetime">02 Oct, 08:11</div>
                                                <div class="num">3.73</div>
                                                <div class="diff diff-green">+0.13</div>
                                            </div>
                                            <div class="queue-item">
                                                <div class="datetime">02 Oct, 08:11</div>
                                                <div class="num">3.73</div>
                                                <div class="diff diff-green">+0.13</div>
                                            </div>
                                            <div class="queue-item">
                                                <div class="datetime">02 Oct, 08:11</div>
                                                <div class="num">3.73</div>
                                                <div class="diff diff-green">+0.13</div>
                                            </div>
                                        </div>
                                        <div class="queue-current">
                                            Current
                                        </div>
                                    </div>
                                </span>
                            </div>
                            <div class="cell cell-two">
                                <span class="bubble">3.82 <span class="stats-arr stats-arr-down"></span></span>
                            </div>
                            <div class="cell cell-payout">
                                <span class="percent">98.3%</span>
                            </div>
                        </div>
                        <div class="row">
                            <div class="cell cell-bookmaker">
                                <div class="logo" style="background-image: url('{{ asset('img/odds-table-sample-logo.png') }}')"></div>
                                <a href="#" class="link"></a>
                            </div>
                            <div class="cell cell-handicap">
                                <div class="bubble handicap-num">-3.25</div>
                            </div>
                            <div class="cell cell-one">
                                <span class="bubble">3.73 <span class="stats-arr stats-arr-up"></span></span>
                            </div>
                            <div class="cell cell-two">
                                <span class="bubble">3.82 <span class="stats-arr stats-arr-down"></span></span>
                            </div>
                            <div class="cell cell-payout">
                                <span class="percent">98.3%</span>
                            </div>
                        </div>
                    </div>
                    <div class="stats subtable-stats">
                        <div class="stats-line avg">
                            <div class="name">Average</div>
                            <div class="nums">
                                <div class="num">3.73</div>
                                <div class="num">3.82</div>
                                <div class="num percent">98.3%</div>
                            </div>
                        </div>
                        <div class="stats-line highest">
                            <div class="name">Highest</div>
                            <div class="nums">
                                <div class="num">3.73</div>
                                <div class="num">3.82</div>
                                <div class="num percent">98.3%</div>
                            </div>
                        </div>
                    </div>
                    <div class="add-event-to-tracker subtable-tracker">
                        <div class="icon"></div>
                        <div class="name">Add This Event To My Tracker</div>
                        <div class="buttons flex aic">
                            <button class="btn btn-small btn-white-opacity">Add</button>
                            <button class="btn btn-small btn-white-opacity" disabled>Remove</button>
                            <span class="btn-close-subtable">
                                <svg><use xlink:href="#toggle-arr"/></svg>
                            </span>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>
@endsection
