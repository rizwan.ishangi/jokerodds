<template>
    <div class="sidebar-block">
        <div class="sidebar-title events">Top Events</div>
        <div v-if="!loading" class="sidebar-content">
            <div class="sidebar-top-events">
                <div class="events-items">
                    <router-link :to="`/football/match/${event.event.id}`" @click.native="scrollToTop" v-for="(event, idx) in events" :key="idx" class="item">
                        <div class="teams">
                            <div v-for="(team, index) in event.event.fixture.participants" :key="index" class="team">
                                <span
                                    class="team-logo"
                                    :style="{
                                        backgroundImage: teamLogo(team)
                                    }"
                                ></span>
                                <span class="team-name">{{ team.name }}</span>
                            </div>
                        </div>
                        <div class="time"><span v-if="eventDate">{{ eventDate(event.event.fixture.start_date) }}</span> {{ eventTime(event.event.fixture.start_date) }}</div>
                    </router-link>
                </div>
                <label v-if="false" class="events-select-label">
                    <select class="select">
                        <option selected>View All Events</option>
                        <option>Option 1</option>
                        <option>Option 2</option>
                        <option>Option 3</option>
                    </select>
                </label>
            </div>
        </div>
        <horizontal-loader v-if="loading" />
    </div>
</template>

<script>
import moment from 'moment'
import HorizontalLoader from '../HorizontalLoader'
import {mapGetters} from "vuex";
export default {
    name: 'Events',
    components: {HorizontalLoader},
    props: {
        events: {
            type: [Object, Array],
            required: true,
            default: () => {}
        },
        loading: Boolean
    },
    methods: {
        ...mapGetters(['headerClockTime', 'headerClockTimezoneOffset', 'headerClockDate']),
        eventTime(dateString) {
            if (dateString && dateString.length) {

                return moment(dateString, 'YYYY-MM-DDTHH:mm:ss').add(this.$store.getters.headerClockTimezoneOffset, 'h').format('HH:mm')
            } else {
                return '--:--'
            }
        },
        eventDate(dateString) {
            if( moment(dateString, 'YYYY-MM-DDTHH:mm:ss').add(this.$store.getters.headerClockTimezoneOffset, 'h').format('DD MMM YYYY') !== moment().format('DD MMM YYYY') ){
                return moment(dateString, 'YYYY-MM-DDTHH:mm:ss').add(this.$store.getters.headerClockTimezoneOffset, 'h').format('DD MMM')
            }
        },
        teamLogo(team) {
            let location = window.location.origin;
            if(team.image !==null){
                return `url('${location}/storage/${team.image}')`
            }
            if(team.team_image !==null){
                return `url('${location}/storage/${team.team_image}')`
            }
            return  `url('${location}/img/team_placeholder.svg')`;
        },
        scrollToTop() {
            window.scrollTo(0,0);
        }

    },
}
</script>

<style scoped>
    .item { text-decoration: none; }
</style>
