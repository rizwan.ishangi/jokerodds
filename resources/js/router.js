import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

export default new Router({
    mode: 'history',
    base: process.env.BASE_URL,
    routes: [
        {
            path: '/',
            name: 'home',
            component: () => import('./views/Home'),
            meta: {
                title: 'JokerOdds - Home',
                breadcrumbs:{
                    items:[
                        {name: 'Home'},
                    ],
                    pageType: 'home'
                }
            }
        },
        {
            path: '/football/date/:date',
            props: true,
            name: 'football.date',
            component: () => import('./views/Home'),
            meta: {
                title: 'JokerOdds - Date Matches',
                breadcrumbs:{
                    items:[
                        {name: 'Home', path: '/'},
                        {name: 'Football', path: ''},
                    ],
                   pageType: 'date'
                }
            }
        },
        {
            path: '/football/league/:leagueId/:tab?',
            props: true,
            name: 'football.league',
            component: () => import('./views/League'),
            meta: {
                title: 'JokerOdds - League',
                breadcrumbs:{
                    items:[
                        {name: 'Home',path : '/'},
                        {name: 'Football', path: '/football'},
                        {name: '',type: 'league'},
                    ],
                    pageType: 'league'
                }
            }
        },
        {
            path: '/football/match/:id/:tab?',
            props: true,
            name: 'football.match',
            component: () => import('./views/Match'),
            meta: {
                title: 'JokerOdds - Match',
                breadcrumbs:{
                    items:[
                        {name: 'Home',path: '/'},
                        {name: 'Football',path: '/football'},
                        {name: '',type: 'league',path : ''},
                        {name: '',type: 'match'},
                    ],
                    pageType: 'match'
                }
            },
        },
        { path: '/football', name: 'football', component: () => import('./views/Home'),
            meta: {
                title: 'JokerOdds - Date Matches',
                breadcrumbs:{
                    items:[
                        {name: 'Home', path: '/'},
                        {name: 'Football', path: ''},
                    ],
                    pageType: 'date'
                }
            }},
        { path: '/tennis', name: 'tennis', component: () => import('./views/Home') },
        { path: '/basketball', name: 'basketball', component: () => import('./views/Home') },
        { path: '/hockey', name: 'hockey', component: () => import('./views/Home') },
        { path: '/volleyball', name: 'volleyball', component: () => import('./views/Home') },
        { path: '/badminton', name: 'badminton', component: () => import('./views/Home') },
        { path: '/widget/:widget_id/:event_id', name: 'widget', component: () => import('./views/Widget') },

        { path: '/404', name: '404', component: () => import('./views/NotFound') },
        // { path: '*', redirect: '/404' }
    ]
})
