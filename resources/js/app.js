import 'flag-icon-css/css/flag-icon.min.css'

import './bootstrap'
import Vue from 'vue'
import App from './App.vue'
import router from "./router"
import store from './store/store'
import VueCookies from 'vue-cookies'
import VueLogger from 'vuejs-logger'
// import VueEcho from 'vue-echo-laravel'

// import './registerServiceWorker'

const isProduction = process.env.NODE_ENV === 'production';

const loggerOptions = {
    isEnabled: true,
    logLevel : isProduction ? 'error' : 'debug',
    stringifyArguments : false,
    showLogLevel : false,
    showMethodName : true,
    separator: '|',
    showConsoleColors: true
}

Vue.config.productionTip = false
Vue.use(VueCookies)
Vue.use(VueLogger, loggerOptions)
Vue.$cookies.config('7d')

// Vue.use(VueEcho, {
//     broadcaster: 'pusher',
//     host: window.location.hostname + ':6001',
// });
Vue.component('widget', require('./views/Widget').default);



if(document.getElementById('app')){
    new Vue({
        router,
        store,
        render: h => h(App),
        data() {
            return {
                text_blocks: window.text_blocks,
                menus: window.menus,
                settings: window.settings
            }
        }
    }).$mount('#app')
}




new Vue({
    store,
    el: '#app-widget',
});
