import axios from 'axios'

export default class JokerApi {
    constructor() {
        this.apiUrl = process.env.NODE_ENV === 'production' ? '/api' : process.env.MIX_API_URL || '/api'
        window.apiErrors = [];
    }

    async getAllLeagues() {
        try {
            let res = await axios.get(`${this.apiUrl}/leagues`)
            return res.data
        } catch (e) {
            // console.error('API.getAllLeagues ERR', e.message)
            // window.apiErrors.push({ 'API.getAllLeagues ERR': e.message })
        }
    }

    async getTopsLeagues() {
        try {
            let res = await axios.get(`${this.apiUrl}/leagues/top`)
            // console.log('api.getTopsLeagues', res.data)
            return res.data
        } catch (e) {
            // console.error('API.getAllLeagues ERR', e.message)
            // window.apiErrors.push({ 'API.getAllLeagues ERR': e.message })
        }
    }

    async getLeague(leagueId) {
        try {
            let res = await axios.get(`${this.apiUrl}/leagues/${leagueId}`)
            // console.log('api.getLeague', res.data.league)
            return res.data.league
        } catch (e) {
            // console.error('API.getLeague ERR', e.message)
            // window.apiErrors.push({ 'API.getLeague ERR': e.message })
        }
    }

    async getLeagueMatches(leagueId) {
        try {
            let res = await axios.get(`${this.apiUrl}/leagues/${leagueId}`)
            // console.log('api.getLeagueMatches', res.data.league.fixtures)
            return res.data.league.fixtures
        } catch (e) {
            // console.error('API.getLeagueMatches ERR', e.message)
            // window.apiErrors.push({ 'API.getLeagueMatches ERR': e.message })
        }
    }

    async getLeagueMatchesFinished(leagueId) {
        try {
            let res = await axios.get(`${this.apiUrl}/leagues/${leagueId}/results`)
            // console.log('api.getLeagueMatches', res.data.league.fixtures)
            return res.data.league.fixtures
        } catch (e) {
            // console.error('API.getLeagueMatches ERR', e.message)
            // window.apiErrors.push({ 'API.getLeagueMatches ERR': e.message })
        }
    }

    async getLeagueStandings(leagueId) {
        try {
            let res = await axios.get(`${this.apiUrl}/league-standings/${leagueId}`)
            // console.log('api.getLeagueStandings', res.data.team)
            return res.data.team
        } catch (e) {
            // console.error('API.getLeagueStandings ERR', e.message)
            // window.apiErrors.push({ 'API.getLeagueStandings ERR': e.message })
        }
    }

    async getFullLeagueStandings(leagueId) {
        try {
            let res = await axios.get(`${this.apiUrl}/full/league-standings/${leagueId}`)
            // console.log('api.getFullLeagueStandings', res.data)
            return res.data
        } catch (e) {
            // console.error('API.getFullLeagueStandings ERR:', e.message)
            // window.apiErrors.push({ 'API.getFullLeagueStandings ERR': e.message })
        }
    }

    async getMatch(matchId) {
        try {
            let res = await axios.get(`${this.apiUrl}/event/${matchId}`)
            // console.log('api.getMatch', res.data)
            return res.data
        } catch (e) {
            // console.error('API.getMatch ERR', e.message)
            // window.apiErrors.push({ 'API.getMatch  ERR': e.message })
        }
    }

    async getFullNatchesAndBetsByDate(date) {
        try {
            let res = await axios.get(`${this.apiUrl}/full/date-matches/${date}`)
            // console.log('api.getFullNatchesAndBetsByDate', res.data)
            return res.data
        } catch (e) {
            // console.error('API.getFullNatchesByDate ERR:', e.message)
            // window.apiErrors.push({ 'API.getFullNatchesByDate  ERR': e.message })
        }
    }
    async getFullMatchesRestAndBetsByDate(date) {
        try {
            let res = await axios.get(`${this.apiUrl}/full/date-matches/${date}/rest`)
            // console.log('api.getFullNatchesAndBetsByDate', res.data)
            return res.data
        } catch (e) {
            // console.error('API.getFullNatchesByDate ERR:', e.message)
            // window.apiErrors.push({ 'API.getFullNatchesByDate  ERR': e.message })
        }
    }
    async getFullMatchesMainAndBetsByDate(date) {
        try {
            let res = await axios.get(`${this.apiUrl}/full/date-matches/${date}/main`)
            // console.log('api.getFullNatchesAndBetsByDate', res.data)
            return res.data
        } catch (e) {
            // console.error('API.getFullNatchesByDate ERR:', e.message)
            // window.apiErrors.push({ 'API.getFullNatchesByDate  ERR': e.message })
        }
    }

    async getNatchesBetsByDate(date) {
        try {
            let res = await axios.get(`${this.apiUrl}/full/date-matches/${date}`)
            // console.log('api.getNatchesBetsByDate', res.data.events)
            return res.data.events
        } catch (e) {
            // console.error('API.getNatchesBetsByDate ERR:', e.message)
            // window.apiErrors.push({ 'API.getNatchesBetsByDate  ERR': e.message })
        }
    }

    async getNatchesByDate(date) {
        try {
            let res = await axios.get(`${this.apiUrl}/date-matches/${date}`)
            // console.log('api.getNatchesByDate', res)
            return res.data
        } catch (e) {
            // console.error('API.getNatchesByDate ERR:', e.message)
            // window.apiErrors.push({ 'API.getNatchesByDate  ERR': e.message })
        }
    }

    async getBetHistory(name, event_id, bookmaker_id, market_id,base_line) {
        try {
            let res = await axios.post(`${this.apiUrl}/bet-history`, {
                name,
                event_id,
                bookmaker_id,
                market_id,
                base_line
            })
            // console.log('api.getBetHistory', res)
            return res.data
        } catch (e) {
            // console.error('API.getBetHistory ERR:', e.message)
            // window.apiErrors.push({ 'API.getBetHistory  ERR': e.message })
        }
    }

    async getTopEvents() {
        try {
            let res = await axios.get(`${this.apiUrl}/events`)
            // console.log('api.getNatchesByDate', res)
            return res.data.data
        } catch (e) {
            // console.error('API.getTopEvents ERR:', e.message)
            // window.apiErrors.push({ 'API.getTopEvents  ERR': e.message })
        }
    }

    async getCookieToken() {
        try {
            let res = await axios.post(`${this.apiUrl}/get/remember/cookie`)
            // console.log('api.getCookieToken', res)
            return res.data.key
        } catch (e) {
            // console.error('API.getCookieToken ERR:', e.message)
            // window.apiErrors.push({ 'API.getCookieToken  ERR': e.message })
        }
    }

    async getTrackerItems(remember_token) {
        try {
            const config = {
                headers: {
                    'Content-Type': 'application/json',
                    'X-REMEMBER-COOKIE': remember_token
                }
            }
            let res = await axios.post(
                `${this.apiUrl}/tracker/bets`,
                {},
                config
            )
            // console.log('api.getTrackerItems', res)
            return res.data
        } catch (e) {
            // console.error('API.getTrackerItems ERR:', e.message)
            // window.apiErrors.push({ 'API.getTrackerItems  ERR': e.message })
        }
    }

    async addTrackerItem(remember_token, event_id, market_id, name, line) {
        try {
            const config = {
                headers: {
                    'Content-Type': 'application/json',
                    'X-REMEMBER-COOKIE': remember_token
                }
            }
            let res = await axios.put(
                `${this.apiUrl}/tracker/bets/remember`,
                {
                    event_id,
                    market_id,
                    name,
                    line,
                },
                config
            )
            return res.data.message
        } catch (e) {
            // console.error('API.addTrackerItem ERR:', e.message)
            // window.apiErrors.push({ 'API.addTrackerItem  ERR': e.message })
        }
    }

    async removeTrackerItem(remember_token, event_id, market_id, name,line) {
        try {
            let res = await axios.delete(
                `${this.apiUrl}/tracker/bets/forget`,
                {
                    headers: {
                        'Content-Type': 'application/json',
                        'X-REMEMBER-COOKIE': remember_token
                    },
                    data: {
                        event_id,
                        market_id,
                        name,
                        line
                    }
                }
            )
            // console.log('api.removeTrackerItem', res, res.data.message)
            return res.data.message
        } catch (e) {
            // console.error('API.removeTrackerItem ERR:', e.message)
            // window.apiErrors.push({ 'API.removeTrackerItem  ERR': e.message })
        }
    }

    async removeAllTrackerItems(remember_token) {
        try {
            let res = await axios.delete(
                `${this.apiUrl}/tracker/bets/forget/all`,
                {
                    headers: {
                        'Content-Type': 'application/json',
                        'X-REMEMBER-COOKIE': remember_token
                    }
                }
            )
            // console.log('api.removeAllTrackerItems', res, res.data.message)
            return res.data.message
        } catch (e) {
            // console.error('API.removeAllTrackerItems ERR:', e.message)
            // window.apiErrors.push({ 'API.removeAllTrackerItems  ERR': e.message })
        }
    }

    async getBanners() {
        try {
            let res = await axios.post(`${this.apiUrl}/banners/get`)
            // console.log('api.getBanners', res)
            return res.data.data
        } catch (e) {
            // window.apiErrors.push({ 'API.getBanners  ERR': e.message })
        }
    }

    async registerBannerClick(id) {
        try {
            let res = await axios.post(`${this.apiUrl}/banner/clicked`, {
                id
            })
            // console.log('api.registerBannerClick', res)
            return res.data.message
        } catch (e) {
            // window.apiErrors.push({ 'API.registerBannerClick  ERR': e.message })
        }
    }

    async getWidget(eventId) {
        try {
            let res = await axios.get(`${this.apiUrl}/widget/event/${eventId}`)
            console.log('res',res)
            return res.data
        } catch (e) {

        }
    }

    async getReferralLink() {
        try {
            let res = await axios.get(`${this.apiUrl}/event/referral`);
            return res.data.data.link
        } catch (e) {
            // console.error('API.getTopEvents ERR:', e.message)
            // window.apiErrors.push({ 'API.getTopEvents  ERR': e.message })
        }
    }

}

