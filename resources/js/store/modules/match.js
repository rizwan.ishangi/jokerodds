import JokerApi from '../../JokerApi'
import VueCookies from "vue-cookies";

const api = new JokerApi

const state = () => ({
    match: {},
    average: {},
    referralLink:[],
    matchTab:{
        preMatch    : true,
        inPlay      : false,
        gameCenter  : false,
    }
})

const getters = {
    match: state => state.match,
    matchScore: state=>state.match.event.scoreboard,
    matchReferralLink: state=>state.referralLink,
    getMatchTabPreMatchFlag: state=>state.matchTab.preMatch,
    getMatchTabInPlayFlag: state=>state.matchTab.inPlay,
    getMatchTabGameCenterFlag: state=>state.matchTab.gameCenter,
}

const actions = {
    async fetchMatch({ commit }, id) {
        let data = await api.getMatch(id)
        commit('setMatch', data)
    },
    async   fetchReferralLink({ commit }) {
        let link = await api.getReferralLink();
        commit('setReferralLink', link)
    },
    async  updateScoreBoard({ commit }, scoreboard) {
        commit('scoreBoard', scoreboard)
    },
    async  setAverage({ commit }, average) {
        commit('setAverage', average)
    },
    async  removeAverage({ commit }, average) {
        commit('removeAverage', average)
    },
    replaceAverage({ commit }, average) {
        commit('replaceAverage', average)
    },
     updateBets({ commit }, bets) {
        commit('updateBets', bets)
    },
    updateBetsAverage({ commit }, average) {
        commit('updateBetsAverage', average)
    },
    updateBetsLive({ commit }, bets) {
        commit('updateBetsLive', bets)
    },
    setActiveMatchTab({ commit }, tab) {
        commit('setActiveMatchTab', tab)
    },
}

const mutations = {
    setMatch: (state, match) => state.match = match,
    scoreBoard: (state, scoreboard) => state.match.event.scoreboard = scoreboard,
    setAverage: (state,data) => {
        state.average[data.id] = data.average
    },
    replaceAverage: (state, average) => state.average = average,
    removeAverage:(state, id) => {
        let keys = Object.keys(state.average).filter(item=>item.id !== id.average);
        keys.forEach((item)=>{
            if(id.average == item){
                console.log(state.average[item])
                delete state.average[item];

            }
        });
        /*    state.average = state.average.filter(item=>item.id !== id.average)*/
    },
    updateBets:(state, bets) => {

        for(let betkey in bets){
            if(state.match.bets.prematch[betkey] !=='undefined'){

                for (let itemKey in state.match.bets.prematch[betkey] ){
                        for ( let prematchBetsKeys in state.match.bets.prematch[betkey][itemKey]['bets']){
                            for (let prematchBetsKeysNames in state.match.bets.prematch[betkey][itemKey]['bets'][prematchBetsKeys])
                                Object.values(bets[betkey]).map((item,key)=>{
                                        if(item[prematchBetsKeys] !=='undefined'){
                                            if(item[prematchBetsKeys] !=='undefined'){
                                                for(let betName in  item[prematchBetsKeys]){
                                                    if((prematchBetsKeysNames===betName) && (item[prematchBetsKeys][betName]['base_line']===state.match.bets.prematch[betkey][itemKey]['base_line'])){
                                                        state.match.bets.prematch[betkey][itemKey]['bets'][prematchBetsKeys][prematchBetsKeysNames] = item[prematchBetsKeys][betName];
                                                    }
                                                }
                                            }
                                        }
                                })
                        }
                }
            }
        }

    },
    updateBetsAverage:(state, average) => {
        for(let averageKey in average){
            if(state.match.bets.prematch[average[averageKey]['market_id']] !=='undefined'){

                for (let itemKey in state.match.bets.prematch[average[averageKey]['market_id']] ){
                    if(state.match.bets.prematch[average[averageKey]['market_id']][itemKey]['base_line'] === average[averageKey]['base_line'] ){
                        for (let coefKey in state.match.bets.prematch[average[averageKey]['market_id']][itemKey]['coefficients'] ){
                            if(coefKey===average[averageKey]['name']){
                                state.match.bets.prematch[average[averageKey]['market_id']][itemKey]['coefficients'][coefKey] = average[averageKey]['average']
                            }
                        }
                    }

                }
            }
        }

    },
    setReferralLink: (state, link) => {
        for (let key in link){
            state.referralLink.push(link[key]);
        }
    },
    updateBetsLive:(state, bets) => {
        for (let eventKey in bets['inplay']){
            if(parseInt(eventKey) === parseInt(state.match.event.id)){
                for(let betkey in bets['inplay'][eventKey]){
                    if(state.match.bets.inplay[betkey] !=='undefined'){
                        for (let itemKey in state.match.bets.inplay[betkey] ){
                            for ( let inplayBetsKeys in state.match.bets.inplay[betkey][itemKey]['bets']){
                                for (let inplayBetsKeysNames in state.match.bets.inplay[betkey][itemKey]['bets'][inplayBetsKeys])
                                    Object.values(bets['inplay'][eventKey][betkey]).map((item,key)=>{
                                        if(item[inplayBetsKeys] !=='undefined'){
                                            if(item[inplayBetsKeys] !=='undefined'){
                                                for(let betName in  item[inplayBetsKeys]){
                                                    if((inplayBetsKeysNames===betName) && (item[inplayBetsKeys][betName]['base_line']===state.match.bets.inplay[betkey][itemKey]['base_line'])){
                                                        state.match.bets.inplay[betkey][itemKey]['bets'][inplayBetsKeys][inplayBetsKeysNames] = item[inplayBetsKeys][betName];
                                                    }
                                                }
                                            }
                                        }
                                    })
                            }
                        }
                    }
                }
            }
        }
    },

    setActiveMatchTab :(state, tab) =>{
        for(let tabKey in state.matchTab){
            state.matchTab[tabKey] = false;
        }
        state.matchTab[tab] = true;
    }

};

export default {
    state,
    getters,
    actions,
    mutations
}
