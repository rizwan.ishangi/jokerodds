
const state = () => ({
    time: '',
    date: '',
    timezoneOffset:0
})

const getters = {
    headerClockTime: state => state.time,
    headerClockDate: state => state.date,
    headerClockTimezoneOffset: state => state.timezoneOffset
}
const actions = {
    async  setHeaderClockTimezoneOffset({ commit }, data) {
        commit('setTimezoneOffset', data)
    },
    async   setHeaderClockTime({ commit }, data) {
        commit('setTime', data)
    },
    async    setHeaderClockDate({ commit }, data) {
        commit('setDate', data)
    },
}
const mutations = {
    setTimezoneOffset: (state, timezoneOffset) => state.timezoneOffset = timezoneOffset,
    setTime: (state, time) => state.time = time,
    setDate: (state, date) => state.date = date,
}

export default {
    state,
    actions,
    getters,
    mutations
}
