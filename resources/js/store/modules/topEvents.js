import JokerApi from '../../JokerApi'

const api = new JokerApi

const state = () => ({
    topEvents: {}
})

const getters = {
    topEvents: state => state.topEvents
}

const actions = {
    async fetchTopEvents({ commit }) {
        let data = await api.getTopEvents()
        commit('setTopEvents', data)
    }
}

const mutations = {
    setTopEvents: (state, topEvents) => state.topEvents = topEvents,
}

export default {
    state,
    getters,
    actions,
    mutations
}
