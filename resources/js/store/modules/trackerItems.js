import JokerApi from '../../JokerApi'

const api = new JokerApi

const state = () => ({
    trackerItems: []
})

const getters = {
    trackerItems: state => state.trackerItems
}

const actions = {
    async fetchTrackerItems({ commit }, rememberToken) {
        let data = await api.getTrackerItems(rememberToken)
        if (data) commit('setTrackerItems', data.data)
    },
     updateTrackerAverage({ commit }, average) {
         commit('updateTrackerAverage', average)
     },
    updateDateMatchesTrackerAverage({ commit }, average) {
        commit('updateDateMatchesTrackerAverage', average)
    },

}
const mutations = {
    setTrackerItems: (state, trackerItems) => state.trackerItems = trackerItems,
    updateTrackerAverage:(state, average) => {
        if(state.trackerItems.length){
            for(let averageKey in average){
                for (let itemKey in state.trackerItems){
                    if(typeof (state.trackerItems[itemKey][0]) !== 'undefined' ){
                            if(
                                (state.trackerItems[itemKey][0]['name']         === average[averageKey]['name'])      &&
                                (state.trackerItems[itemKey][0]['market_id']    === average[averageKey]['market_id']) &&
                                (state.trackerItems[itemKey][0]['base_line']    === average[averageKey]['base_line']) &&
                                (state.trackerItems[itemKey][0]['event_id']     === average[averageKey]['event_id'])
                            ){
                                state.trackerItems[itemKey][0]['oldAverage'] = state.trackerItems[itemKey][0]['average']
                                state.trackerItems[itemKey][0]['average'] = average[averageKey]['average']
                            }
                    }
                }
            }
        }
     },
    updateDateMatchesTrackerAverage:(state, average) => {
        if(state.trackerItems.length){
            for(let averageKey in average['average']){
                for (let itemKey in state.trackerItems){
                    if(typeof (state.trackerItems[itemKey][0]) !== 'undefined' ){
                        if(
                            (state.trackerItems[itemKey][0]['name']         === average['average'][averageKey]['name'])      &&
                            (state.trackerItems[itemKey][0]['market_id']    === average['average'][averageKey]['market_id']) &&
                            (state.trackerItems[itemKey][0]['base_line']    === average['average'][averageKey]['base_line']) &&
                            (state.trackerItems[itemKey][0]['event_id']     === average['average'][averageKey]['event_id'])
                        ){
                            state.trackerItems[itemKey][0]['oldAverage'] = state.trackerItems[itemKey][0]['average']
                            state.trackerItems[itemKey][0]['average'] = average['average'][averageKey]['average']
                        }
                    }

                }
            }
        }
    },
}

export default {
    state,
    getters,
    actions,
    mutations
}
