import JokerApi from '../../JokerApi'

const api = new JokerApi

const state = () => ({
    dateMatches: {},
    dateMatchesRest: {}
})

const getters = {
    dateMatches: state => state.dateMatches,
    dateMatchesRest: state => state.dateMatchesRest
}

const actions = {
    async fetchDateMatches({ commit }, date) {
        let data = await api.getFullNatchesAndBetsByDate(date);
        commit('setDateMatches', data)
    },
    async clearDateMatches({ commit }) {
        commit('setDateMatches', {})
    },

    async fetchDateMatchesRest({ commit }, date) {
        let data = await api.getFullMatchesRestAndBetsByDate(date);
        commit('pushBetsMatches', data.bets);
        commit('setDateMatchesRest', data);

    },
    async clearDateMatchesRest({ commit }) {
        commit('setDateMatchesRest', {})
    },

    async fetchDateMatchesMain({ commit }, date) {
        let data = await api.getFullMatchesMainAndBetsByDate(date);
        commit('setDateMatches', data)
    },
    updateDateMatchesAverage({ commit }, average) {
        commit('updateDateMatchesAverage', average)
    },

    updateDateMatchesScores({ commit }, scores) {
        commit('updateDateMatchesScores', scores)
    },
};

const mutations = {
    setDateMatches: (state, dateMatches) => state.dateMatches = dateMatches,
    setDateMatchesRest: (state, dateMatches) => state.dateMatchesRest = dateMatches,
    pushBetsMatches: (state, betsMatches) => state.dateMatches.bets = Object.assign({}, state.dateMatches.bets, betsMatches),

    updateDateMatchesAverage:(state, bets) => {

        if(typeof (state.dateMatches.bets[bets['event_id']]) !=='undefined'){
            for(let averageKey in bets['average']){
                if(bets['average'][averageKey]['market_id'] ===1){
                    if(typeof (state.dateMatches.bets[bets['event_id']]['prematch'][bets['average'][averageKey]['market_id']]) !=='undefined'){

                        for (let coefKey in state.dateMatches.bets[bets['event_id']]['prematch'][bets['average'][averageKey]['market_id']][0]['coefficients'] ){
                            if(coefKey===bets['average'][averageKey]['name']){
                                state.dateMatches.bets[bets['event_id']]['prematch'][bets['average'][averageKey]['market_id']][0]['coefficients'][coefKey] = bets['average'][averageKey]['average']
                            }
                        }
                    }
                }

            }
        }
    },

    updateDateMatchesScores:(state, scores) => {
        for(let dataKey in state.dateMatches.data){
            if(typeof (state.dateMatches.data[dataKey]) !=='undefined'){
                if(typeof (state.dateMatches.data[dataKey]['leagues']) !=='undefined'){
                    for(let leaguesKey in  state.dateMatches.data[dataKey]['leagues']){
                        if(typeof (state.dateMatches.data[dataKey]['leagues'][leaguesKey]) !=='undefined'){
                            if(typeof (state.dateMatches.data[dataKey]['leagues'][leaguesKey]['matches']) !=='undefined'){
                                for(let leagueMatchesKey in  state.dateMatches.data[dataKey]['leagues'][leaguesKey]['matches']){
                                    if(typeof (state.dateMatches.data[dataKey]['leagues'][leaguesKey]['matches'][leagueMatchesKey]) !=='undefined'){
                                        for(let scoreKey in scores){
                                            if(typeof (scores[scoreKey]) !=='undefined'){
                                                if(typeof (scores[scoreKey]['event_id']) !=='undefined'){
                                                    if(state.dateMatches.data[dataKey]['leagues'][leaguesKey]['matches'][leagueMatchesKey]['event_id']===scores[scoreKey]['event_id']){
                                                        if((scores[scoreKey]['status_id'] ===2) || (scores[scoreKey]['status_id']===3)){
                                                            state.dateMatches.data[dataKey]['leagues'][leaguesKey]['matches'][leagueMatchesKey]['score'] = scores[scoreKey]['results'][0]['Value'] + ':' +scores[scoreKey]['results'][1]['Value'];
                                                            state.dateMatches.data[dataKey]['leagues'][leaguesKey]['matches'][leagueMatchesKey]['status_id'] = scores[scoreKey]['status_id']
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }

};

export default {
    state,
    getters,
    actions,
    mutations
}
