import JokerApi from '../../JokerApi'

const api = new JokerApi

const state = () => ({
    betHistory: {}
})

const getters = {
    betHistory: state => state.betHistory
}

const actions = {
    async fetchBetHistory({ commit }, matchId, bookmakerName, betType) {
        let data = await api.getBetHistory(matchId, bookmakerName, betType)
        commit('setBetHistory', data)
    }
}

const mutations = {
    setBetHistory: (state, betHistory) => state.betHistory = betHistory,
}

export default {
    state,
    getters,
    actions,
    mutations
}
