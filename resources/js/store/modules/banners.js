import JokerApi from '../../JokerApi'

const api = new JokerApi

const state = () => ({
    banners: {}
})

const getters = {
    banners: state => state.banners
}

const actions = {
    async fetchBanners({ commit }) {
        let data = await api.getBanners()
        commit('setBanners', data)
    }
}

const mutations = {
    setBanners: (state, banners) => state.banners = banners,
}

export default {
    state,
    getters,
    actions,
    mutations
}
