import JokerApi from '../../JokerApi'

const api = new JokerApi

const state = () => ({
    standings: [],
    matches: {},
    matchesFinished: {},
    league: {},
    standingsTeamsLogos: [],
    allLeagues: [],
    topsLeagues: []
})

const getters = {
    standings: state => state.standings,
    standingsTeamsLogos: state => state.standingsTeamsLogos,
    matches: state => state.matches,
    matchesFinished: state => state.matchesFinished,
    league: state => state.league,
    allLeagues: state => state.allLeagues,
    topsLeagues: state => state.topsLeagues,
}

const actions = {
    async fetchStandings({ commit }, id) {
        let data = await api.getFullLeagueStandings(id)
        if ( data && data.leagueStandings ) {
            commit('setStandings', data.leagueStandings)
        }
        if ( data && data.teams ) {
            commit('setStandingsTeamsLogos', data.teams)
        }
    },
    async fetchMatches({ commit }, id) {
        let data = await api.getLeagueMatches(id)
        commit('setMatches', data)
    },
    async fetchMatchesFinished({ commit }, id) {
        let data = await api.getLeagueMatchesFinished(id)
        commit('setMatchesFinished', data)
    },
    async fetchLeague({ commit }, id) {
        let data = await api.getLeague(id)
        commit('setLeague', data)
    },
    async fetchLeagueAndMatches({ commit }, id) {
        let data = await api.getLeague(id)
        commit('setLeague', data)
        commit('setMatches', data.fixtures)
    },
    async fetchAllLeagues({ commit }) {
        let data = await api.getAllLeagues();
        commit('setAllLeagues', data)
    },
    async fetchTopsLeagues({ commit }) {
        let data = await api.getTopsLeagues();
        commit('setTopsLeagues', data)
    }
}

const mutations = {
    setStandings: (state, standings) => state.standings = standings,
    setStandingsTeamsLogos: (state, standingsTeamsLogos) => state.standingsTeamsLogos = standingsTeamsLogos,
    setMatches: (state, matches) => state.matches = matches,
    setMatchesFinished: (state, matchesFinished) => state.matchesFinished = matchesFinished,
    setLeague: (state, league) => state.league = league,
    setAllLeagues: (state, allLeagues) => state.allLeagues = allLeagues,
    setTopsLeagues: (state, topsLeagues) => state.topsLeagues = topsLeagues,
}

export default {
    state,
    getters,
    actions,
    mutations
}
