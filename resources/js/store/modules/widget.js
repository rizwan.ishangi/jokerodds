import JokerApi from '../../JokerApi'

const api = new JokerApi


const state = () => ({
    widget: [],
})

const getters = {
    widget: state => state.widget,
}

const actions = {
    async fetchWidget({ commit },id) {

        let data = await api.getWidget(id)
        console.log('dataWidget',data)
        commit('setWidget', data)
    },

    updateWidgetBets({ commit }, bets) {
        commit('updateWidgetBets', bets)
    },
    updateWidgetBetsLive({ commit }, bets) {
        commit('updateWidgetBetsLive', bets)
    },
}

const mutations = {
    setWidget: (state, widget) => state.widget = widget,
    updateWidgetBets:(state, bets) => {
        for (let itemKey in state.widget.bookmakers){
            if(typeof (state.widget.bookmakers[itemKey]) !=='undefined'){

                for (let betKey in bets){
                    if(betKey === '1'){
                        if(typeof (bets[betKey]) !=='undefined'){
                            bets[betKey].map((bet, key)=>{
                                for(let key_bet in bet){
                                    if(typeof (bet[key_bet]) !=='undefined'){
                                        if(key_bet === state.widget.bookmakers[itemKey]['name']){

                                            for(let bet_name_key in bet[key_bet]){
                                                if(typeof (bet[key_bet][bet_name_key]) !=='undefined'){

                                                    if(typeof (bet[key_bet][bet_name_key]['price']) !=='undefined'){
                                                        if(typeof (state.widget.bookmakers[itemKey]['bets']['prematch'][bet_name_key]) !=='undefined'){
                                                            state.widget.bookmakers[itemKey]['bets']['prematch'][bet_name_key]['price'] = bet[key_bet][bet_name_key]['price'];
                                                        }
                                                    }
                                                }

                                            }
                                        }
                                    }

                                }
                            })
                        }
                    }

                }
            }
        }

    },
    updateWidgetBetsLive:(state, bets) => {
        for (let itemKey in state.widget.bookmakers){
            if(typeof (state.widget.bookmakers[itemKey]) !=='undefined'){

                for (let eventKey in bets['inplay']){
                    if(parseInt(eventKey) === parseInt(state.widget.event.id)){

                        for (let betKey in bets['inplay'][eventKey]){
                            if(betKey === '1'){
                                if(typeof (bets['inplay'][eventKey][betKey]) !=='undefined'){
                                    bets['inplay'][eventKey][betKey].map((bet, key)=>{
                                        for(let key_bet in bet){
                                            if(typeof (bet[key_bet]) !=='undefined'){
                                                if(key_bet === state.widget.bookmakers[itemKey]['name']){

                                                    for(let bet_name_key in bet[key_bet]){
                                                        if(typeof (bet[key_bet][bet_name_key]) !=='undefined'){

                                                            if(typeof (bet[key_bet][bet_name_key]['price']) !=='undefined'){
                                                                if(typeof (state.widget.bookmakers[itemKey]['bets']['inplay'][bet_name_key]) !=='undefined'){
                                                                    state.widget.bookmakers[itemKey]['bets']['inplay'][bet_name_key]['price'] = bet[key_bet][bet_name_key]['price'];
                                                                }
                                                            }
                                                        }

                                                    }
                                                }
                                            }

                                        }
                                    })
                                }
                            }
                        }

                    }
                }

            }
        }

    },
};

export default {
    state,
    getters,
    actions,
    mutations
}
