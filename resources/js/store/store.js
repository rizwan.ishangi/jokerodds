import Vue from 'vue'
import Vuex from 'vuex'
import league from './modules/league'
import match from './modules/match'
import dateMatches from './modules/dateMatches'
import betHistory from './modules/betHistory'
import topEvents from './modules/topEvents'
import trackerItems from './modules/trackerItems'
import banners from './modules/banners'
import widget from './modules/widget'
import siteHeaderClock from './modules/siteHeaderClock'

Vue.use(Vuex)

export default new Vuex.Store({
    modules: {
        league,
        match,
        dateMatches,
        betHistory,
        topEvents,
        trackerItems,
        banners,
        widget,
        siteHeaderClock
    }
})
