const mix = require('laravel-mix');


mix.disableNotifications();

mix
    .js('resources/js/app.js', 'public/js')
    .sass('resources/sass/app.scss', 'public/css', { implementation: require('sass') })
    .copyDirectory('resources/img', 'public/img')
    .copyDirectory('resources/fonts', 'public/fonts')
    .babel('resources/js/service-worker.js', 'public/service-worker.js')
    .version();


if (!mix.inProduction()) {
    mix.webpackConfig({
        devtool: 'source-map'
    })
    .sourceMaps();
}
