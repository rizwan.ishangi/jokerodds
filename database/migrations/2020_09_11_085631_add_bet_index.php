<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddBetIndex extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('bet', function (Blueprint $table) {
            $table->index([
                'event_id',
                'is_latest',
                'name',
            ]);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('bet', function (Blueprint $table) {
            $table->index('event_id');

            $table->dropIndex([
                'event_id',
                'is_latest',
                'name',
            ]);
        });
    }
}
