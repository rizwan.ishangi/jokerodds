<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBetTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bet', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->unsignedBigInteger('event_id');
            $table->unsignedInteger('bookmaker_id');
            $table->unsignedInteger('market_id');

            $table->unsignedTinyInteger('status_id');
            $table->tinyInteger('settlement_id')->nullable();
            $table->string('name');
            $table->string('line')->nullable();
            $table->string('base_line')->nullable();
            $table->decimal('start_price', 10, 2);
            $table->decimal('price', 10, 2);
            $table->timestamp('last_update');

            $table->foreign('settlement_id')
                ->references('id')
                ->on('bet_settlement')
                ->onUpdate('RESTRICT')
                ->onDelete('RESTRICT');

            $table->foreign('status_id')
                ->references('id')
                ->on('bet_status')
                ->onUpdate('RESTRICT')
                ->onDelete('RESTRICT');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bet');
    }
}
