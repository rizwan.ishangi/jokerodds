<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddRestrictCountriesFieldToBookmakerTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('bookmaker', function (Blueprint $table) {
            //
            $table->text('restrict_countries')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('bookmaker', function (Blueprint $table) {
            //
            $table->dropColumn('restrict_countries');
        });
    }
}
