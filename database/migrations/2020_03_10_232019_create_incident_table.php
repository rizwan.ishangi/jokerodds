<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateIncidentTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('incident', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedBigInteger('period_id');
            $table->smallInteger('seconds')->unsigned();
            $table->tinyInteger('participant_position')->unsigned();
            $table->string('player_name', 50);
            $table->unsignedInteger('type_id');

            $table->foreign('type_id')
                ->references('id')
                ->on('incident_type')
                ->onUpdate('RESTRICT')
                ->onDelete('RESTRICT');

            $table->foreign('period_id')
                ->references('id')
                ->on('period')
                ->onUpdate('RESTRICT')
                ->onDelete('RESTRICT');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('incident');
        Schema::dropIfExists('inciedent');
    }
}
