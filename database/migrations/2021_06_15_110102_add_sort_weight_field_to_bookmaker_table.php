<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddSortWeightFieldToBookmakerTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('bookmaker', function (Blueprint $table) {
            //
            $table->unsignedSmallInteger('sort_weight')->default(0)->nullable()->after('selected');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('bookmaker', function (Blueprint $table) {
            //
            $table->dropColumn('sort_weight');
        });
    }
}
