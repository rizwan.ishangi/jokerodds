<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class BetAddApiIdField extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('bet', function (Blueprint $table) {
            $table->bigInteger('api_bet_id')
                ->after('id')
                ->comment('bet id provided by api');

            $table->boolean('is_latest');

            $table->boolean('is_inplay');

            $table->index('api_bet_id', 'bet_api_bet_id');
            $table->index(['api_bet_id', 'last_update'], 'bet_api_bet_id_last_update');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('bet', function (Blueprint $table) {
            $table->dropIndex('bet_api_bet_id');
            $table->dropIndex('bet_api_bet_id_last_update');

            $table->dropColumn(['api_bet_id', 'is_inplay', 'is_latest']);
        });
    }
}
