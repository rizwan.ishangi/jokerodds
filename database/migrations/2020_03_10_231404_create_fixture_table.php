<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFixtureTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fixture', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->unsignedInteger('location_id');
            $table->unsignedInteger('sport_id');
            $table->unsignedInteger('league_id');
            $table->unsignedInteger('status_id');
            $table->dateTime('start_date')->nullable();
            $table->dateTime('last_update')->nullable();

            $table->foreign('location_id')
                ->references('id')
                ->on('location')
                ->onUpdate('RESTRICT')
                ->onDelete('RESTRICT');

            $table->foreign('sport_id')
                ->references('id')
                ->on('sport')
                ->onUpdate('RESTRICT')
                ->onDelete('RESTRICT');

            $table->foreign('league_id')
                ->references('id')
                ->on('league')
                ->onUpdate('RESTRICT')
                ->onDelete('RESTRICT');

            $table->foreign('status_id')
                ->references('id')
                ->on('scoreboard_status')
                ->onUpdate('RESTRICT')
                ->onDelete('RESTRICT');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('fixture');
    }
}
