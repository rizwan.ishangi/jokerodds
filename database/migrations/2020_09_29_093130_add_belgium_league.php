<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddBelgiumLeague extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $model = new \App\Models\League();
        $model->id = '21728';
        $model->name = ' Belgian First Division B';
        $model->sport_id = '6046';
        $model->location_id = '176';
        $model->save();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        \App\Models\League::where('id', '=', 21728)->delete();
    }
}
