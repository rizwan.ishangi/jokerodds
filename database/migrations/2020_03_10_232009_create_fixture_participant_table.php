<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFixtureParticipantTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fixture_participant', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedBigInteger('fixture_id');
            $table->unsignedBigInteger('participant_id');
            $table->tinyInteger('position');

            $table->foreign('fixture_id')
                ->references('id')
                ->on('fixture')
                ->onUpdate('RESTRICT')
                ->onDelete('RESTRICT');

            $table->foreign('participant_id')
                ->references('id')
                ->on('participant')
                ->onUpdate('RESTRICT')
                ->onDelete('RESTRICT');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('fixture_participant');
    }
}
