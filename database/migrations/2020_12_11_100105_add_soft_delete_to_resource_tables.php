<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddSoftDeleteToResourceTables extends Migration
{
    public function up()
    {
        Schema::table('banners', function (Blueprint $table) {
            $table->softDeletes();
        });

        Schema::table('bookmaker', function (Blueprint $table) {
            $table->softDeletes();
        });

        Schema::table('event', function (Blueprint $table) {
            $table->softDeletes();
        });

        Schema::table('fixture', function (Blueprint $table) {
            $table->softDeletes();
        });

        Schema::table('location', function (Blueprint $table) {
            $table->softDeletes();
        });

        Schema::table('sport', function (Blueprint $table) {
            $table->softDeletes();
        });

        Schema::table('text_blocks', function (Blueprint $table) {
            $table->softDeletes();
        });

        Schema::table('users', function (Blueprint $table) {
            $table->softDeletes();
        });
    }

    public function down()
    {
        Schema::table('banners', function (Blueprint $table) {
            $table->dropSoftDeletes();
        });

        Schema::table('bookmaker', function (Blueprint $table) {
            $table->dropSoftDeletes();
        });

        Schema::table('event', function (Blueprint $table) {
            $table->dropSoftDeletes();
        });

        Schema::table('fixture', function (Blueprint $table) {
            $table->dropSoftDeletes();
        });

        Schema::table('location', function (Blueprint $table) {
            $table->dropSoftDeletes();
        });

        Schema::table('sport', function (Blueprint $table) {
            $table->dropSoftDeletes();
        });

        Schema::table('text_blocks', function (Blueprint $table) {
            $table->dropSoftDeletes();
        });

        Schema::table('users', function (Blueprint $table) {
            $table->dropSoftDeletes();
        });
    }
}
