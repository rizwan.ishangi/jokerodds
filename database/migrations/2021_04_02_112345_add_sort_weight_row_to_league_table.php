<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddSortWeightRowToLeagueTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('league', function (Blueprint $table) {
            $table->unsignedSmallInteger('sort_weight')->default(999)->nullable()->after('has_fixtures');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('league', function (Blueprint $table) {
            $table->dropColumn(['sort_weight']);
        });
    }
}
