<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddGoalServeFieldsToLeagueTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('league', function (Blueprint $table) {
            $table->unsignedBigInteger('goal_league_id')->after('id')->nullable();
            $table->string('goal_league_name')->after('name')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('league', function (Blueprint $table) {
            $table->dropColumn('goal_league_id');
            $table->dropColumn('goal_league_name');
        });
    }
}
