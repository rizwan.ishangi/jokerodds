<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateJackPotLeagueTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('jack_pot_league', function (Blueprint $table) {
            $table->id();
            $table->uuid('uuid')->unique('uuid');
            $table->string('name');
            $table->unsignedInteger('league_id')->comment('league table')->nullable();


        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('jack_pot_league');
    }
}
