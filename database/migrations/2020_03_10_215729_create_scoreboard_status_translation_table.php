<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateScoreboardStatusTranslationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('scoreboard_status_translation', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('status_id')->index('status_id');
            $table->unsignedInteger('language_id')->index('language_id');
            $table->string('value', 30);
            $table->string('description', 200);

            $table->foreign('status_id')
                ->references('id')
                ->on('scoreboard_status')
                ->onUpdate('RESTRICT')->onDelete('RESTRICT');

            $table->foreign('language_id')
                ->references('id')
                ->on('language')
                ->onUpdate('RESTRICT')
                ->onDelete('RESTRICT');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('scoreboard_status_translation');
    }
}
