<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddShowSidebarColumnToEventTable extends Migration
{
    public function up()
    {
        Schema::table('event', function (Blueprint $table) {
            $table->boolean('show_sidebar')->default(0);
        });
    }

    public function down()
    {
        Schema::table('event', function (Blueprint $table) {
            $table->dropColumn('show_sidebar');
        });
    }
}
