<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePeriodTypeTranslationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('period_type_translation', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('period_type_id');
            $table->unsignedInteger('language_id');
            $table->string('description', 50);

            $table->foreign('language_id')
                ->references('id')
                ->on('language')
                ->onUpdate('RESTRICT')
                ->onDelete('RESTRICT');

            $table->foreign('period_type_id')
                ->references('id')
                ->on('period_type')
                ->onUpdate('RESTRICT')
                ->onDelete('RESTRICT');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('period_type_translation');
    }
}
