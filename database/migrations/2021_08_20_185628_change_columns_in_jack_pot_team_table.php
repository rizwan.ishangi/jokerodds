<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ChangeColumnsInJackPotTeamTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('jack_pot_team', function (Blueprint $table) {
            //
            $table->dropColumn(['league_id']);
            $table->unsignedInteger('participant_id')->comment('participant table')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('jack_pot_team', function (Blueprint $table) {
            //
            $table->dropColumn(['participant_id']);
            $table->unsignedInteger('league_id')->comment('league table')->nullable();

        });
    }
}
