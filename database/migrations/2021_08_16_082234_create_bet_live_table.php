<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBetLiveTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bet_live', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('event_id');
            $table->unsignedInteger('bookmaker_id');
            $table->unsignedInteger('market_id');
            $table->unsignedTinyInteger('status_id');
            $table->tinyInteger('settlement_id')->nullable();
            $table->string('name');
            $table->string('line')->nullable();
            $table->string('base_line')->nullable();
            $table->decimal('start_price', 10, 2)->nullable();
            $table->decimal('price', 10, 2);
            $table->timestamp('last_update');

            $table->index([
                'event_id',
                'market_id',
                'name'
            ]);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bet_live');
    }
}
