<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToLocationTranslationTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('location_translation', function (Blueprint $table) {
            $table->foreign('language_id', 'location_translation_ibfk_1')
                ->references('id')
                ->on('language')
                ->onUpdate('RESTRICT')
                ->onDelete('RESTRICT');

            $table->foreign('location_id', 'location_translation_ibfk_2')
                ->references('id')
                ->on('location')
                ->onUpdate('RESTRICT')
                ->onDelete('RESTRICT');
        });
    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('location_translation', function (Blueprint $table) {
            $table->dropForeign('location_translation_ibfk_1');
            $table->dropForeign('location_translation_ibfk_2');
        });
    }
}
