<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateLocationTranslationTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('location_translation', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('location_id')->index('location_id');
            $table->unsignedInteger('language_id')->index('language_id');
            $table->string('name', 200);
        });
    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('location_translation');
    }
}
