<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddDatesForBetStatus extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('bet', function (Blueprint $table) {
            $table->timestamp('open_status_date')->nullable();
            $table->timestamp('suspended_status_date')->nullable();
            $table->timestamp('settled_status_date')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('bet', function (Blueprint $table) {
            $table->dropColumn('open_status_date');
            $table->dropColumn('suspended_status_date');
            $table->dropColumn('settled_status_date');
        });
    }
}
