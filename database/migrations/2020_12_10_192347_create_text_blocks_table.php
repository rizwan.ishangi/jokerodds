<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTextBlocksTable extends Migration
{
    public function up()
    {
        Schema::create('text_blocks', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->mediumText('content')->nullable();
        });
    }

    public function down()
    {
        Schema::dropIfExists('text_blocks');
    }
}
