<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBetHistoryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bet_history', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('api_bet_id');

            $table->unsignedBigInteger('event_id');
            $table->unsignedInteger('bookmaker_id');
            $table->unsignedInteger('market_id');
            $table->unsignedTinyInteger('status_id');
            $table->tinyInteger('settlement_id')->nullable();
            $table->string('name');
            $table->string('line')->nullable();
            $table->string('base_line')->nullable();
            $table->decimal('start_price', 10, 2);
            $table->decimal('price', 10, 2);
            $table->timestamp('last_update');
            $table->boolean('is_latest');
            $table->boolean('is_inplay');
            $table->timestamp('open_status_date')->nullable();
            $table->timestamp('suspended_status_date')->nullable();
            $table->timestamp('settled_status_date')->nullable();

            $table->index('api_bet_id', 'bet_api_bet_id');
            $table->index(['api_bet_id', 'last_update'], 'bet_api_bet_id_last_update');
            $table->index([
                'event_id',
                'is_latest',
                'name',
            ]);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bet_history');
    }
}
