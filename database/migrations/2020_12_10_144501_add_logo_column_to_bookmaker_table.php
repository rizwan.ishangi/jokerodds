<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddLogoColumnToBookmakerTable extends Migration
{
    public function up()
    {
        Schema::table('bookmaker', function (Blueprint $table) {
            $table->string('logo')->nullable();
        });
    }

    public function down()
    {
        Schema::table('bookmaker', function (Blueprint $table) {
            $table->dropColumn('logo');
        });
    }
}
