<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddSelectedFieldToBookmakerTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('bookmaker', function (Blueprint $table) {
            //
            $table->boolean('selected')->default(0)->after('ref');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('bookmaker', function (Blueprint $table) {
            //
            $table->dropColumn('selected');
        });
    }
}
