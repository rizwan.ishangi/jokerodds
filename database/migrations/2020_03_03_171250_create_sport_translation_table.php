<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateSportTranslationTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sport_translation', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('sport_id')->index('sport_id');
            $table->unsignedInteger('language_id')->index('language_id');
            $table->string('name', 200)->nullable();
        });
    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('sport_translation');
    }
}
