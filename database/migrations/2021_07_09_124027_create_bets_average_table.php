<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBetsAverageTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bets_average', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->unsignedBigInteger('event_id')->index();
            $table->unsignedInteger('market_id');
            $table->string('name');
            $table->string('base_line')->nullable();
            $table->decimal('highest', 10, 2);
            $table->decimal('average', 10, 2);
            $table->integer('count');
            $table->timestamp('last_update');

            $table->index( [
                'event_id',
                'market_id',
                'name'
            ]);
        });


    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bets_average');
    }
}
