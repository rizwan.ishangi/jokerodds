<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ChangeLeaguesIds extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $league = \App\Models\League::find(30064);
        if ($league) {
            $league->id = 35320;
            $league->save();
        }

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $league = \App\Models\League::find(35320);
        if ($league) {
            $league->id = 30064;
            $league->save();
        }
    }
}
