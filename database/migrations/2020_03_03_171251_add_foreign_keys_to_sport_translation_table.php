<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToSportTranslationTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('sport_translation', function (Blueprint $table) {
            $table->foreign('language_id', 'sport_translation_ibfk_1')
                ->references('id')
                ->on('language')
                ->onUpdate('RESTRICT')
                ->onDelete('RESTRICT');

            $table->foreign('sport_id', 'sport_translation_ibfk_2')
                ->references('id')
                ->on('sport')
                ->onUpdate('RESTRICT')
                ->onDelete('RESTRICT');
        });
    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('sport_translation', function (Blueprint $table) {
            $table->dropForeign('sport_translation_ibfk_1');
            $table->dropForeign('sport_translation_ibfk_2');
        });
    }
}
