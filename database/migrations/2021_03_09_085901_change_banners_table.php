<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ChangeBannersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('banners', function (Blueprint $table) {
            $table->dropColumn(['page']);
            $table->unsignedSmallInteger('position')->comment('Top: 1 | Left: 2 | Right: 3')->nullable()->after('active');
            $table->integer('order')->default(1)->nullable()->after('position');
            $table->text('link')->nullable()->after('id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('banners', function (Blueprint $table) {
            $table->dropColumn(['position', 'order', 'link']);
            $table->string('page')->after('clicks');
        });
    }
}
