<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnsFromGoalserviceToParticipantsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('participant', function (Blueprint $table) {
           $table->bigInteger('goal_team_id')->unsigned()->nullable()->after('id');
           $table->string('goal_team_name')->nullable()->after('name');
           $table->longText('image')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('participant', function (Blueprint $table) {
            $table->dropColumn('goal_team_id');
            $table->dropColumn('goal_team_name');
            $table->dropColumn('image');
        });
    }
}
