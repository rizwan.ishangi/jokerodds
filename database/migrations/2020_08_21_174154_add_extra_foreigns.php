<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddExtraForeigns extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('bet', function (Blueprint $table) {
            $table->foreign('event_id')
                ->references('id')
                ->on('event')
                ->onUpdate('RESTRICT')
                ->onDelete('RESTRICT');

            $table->foreign('bookmaker_id')
                ->references('id')
                ->on('bookmaker')
                ->onUpdate('RESTRICT')
                ->onDelete('RESTRICT');

            $table->foreign('market_id')
                ->references('id')
                ->on('markets')
                ->onUpdate('RESTRICT')
                ->onDelete('RESTRICT');
        });

        Schema::table('scoreboard', function (Blueprint $table) {
            $table->foreign('event_id')
                ->references('id')
                ->on('event')
                ->onUpdate('RESTRICT')
                ->onDelete('RESTRICT');
        });

        Schema::table('period', function (Blueprint $table) {
            $table->foreign('event_id')
                ->references('id')
                ->on('event')
                ->onUpdate('RESTRICT')
                ->onDelete('RESTRICT');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('bet', function (Blueprint $table) {
            $table->dropForeign('bet_event_id_foreign');
            $table->dropForeign('bet_bookmaker_id_foreign');
            $table->dropForeign('bet_market_id_foreign');
        });

        Schema::table('scoreboard', function (Blueprint $table) {
            $table->dropForeign('scoreboard_event_id_foreign');
        });

        Schema::table('period', function (Blueprint $table) {
            $table->dropForeign('period_event_id_foreign');
        });
    }
}
