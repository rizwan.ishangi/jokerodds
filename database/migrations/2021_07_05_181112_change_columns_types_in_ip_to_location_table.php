<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ChangeColumnsTypesInIpToLocationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('ip_to_location', function (Blueprint $table) {
            //
            $table->bigInteger('ip_from')->change();
            $table->bigInteger('ip_to')->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('ip_to_location', function (Blueprint $table) {
            //
            $table->integer('ip_from')->change();
            $table->integer('ip_to')->change();
        });
    }
}
