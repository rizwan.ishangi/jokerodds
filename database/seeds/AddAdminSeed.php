<?php

use Illuminate\Database\Seeder;

class AddAdminSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $admin = new \App\Models\Admin();
        $admin->name = 'Admin';
        $admin->email = 'admin@xairo.com';
        $admin->password = \Hash::make('N*fqt9uXq9');
        $admin->save();
    }
}
