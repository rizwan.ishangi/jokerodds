<?php

use Illuminate\Database\Seeder;

class AddFlagsToLocationSeeder extends Seeder {

    /**
     * Run the database seeds.
     *
     * @return  void
     */
    public function run()
    {
        $locations = DB::table('location_translation')->get();

        foreach ($locations as $location) {
           $country = DB::table(\Config::get('countries.table_name'))->where('name', 'LIKE', '%'.$location->name.'%')->first();
            if ($country) {
                DB::table('location')->where('id', $location->location_id)->update(['iso_2' => $country->iso_3166_2]);
            }

        }
    }
}
