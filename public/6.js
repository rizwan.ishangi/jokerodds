(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[6],{

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/HorizontalLoader.vue?vue&type=script&lang=js&":
/*!***************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/HorizontalLoader.vue?vue&type=script&lang=js& ***!
  \***************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
//
//
//
//
//
//
/* harmony default export */ __webpack_exports__["default"] = ({
  name: 'HorizontalLoader'
});

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/pageparts/box/Box.vue?vue&type=script&lang=js&":
/*!****************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/pageparts/box/Box.vue?vue&type=script&lang=js& ***!
  \****************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
//
//
//
//
//
//
//
//
//
//
/* harmony default export */ __webpack_exports__["default"] = ({
  name: 'Box',
  props: {
    type: {
      type: String,
      "default": 'box'
    },
    noPadding: {
      type: Boolean,
      "default": false
    }
  }
});

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/pageparts/match/MatchAddToTracker.vue?vue&type=script&lang=js&":
/*!********************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/pageparts/match/MatchAddToTracker.vue?vue&type=script&lang=js& ***!
  \********************************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _box_Box__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../box/Box */ "./resources/js/components/pageparts/box/Box.vue");
/* harmony import */ var _JokerApi__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../../JokerApi */ "./resources/js/JokerApi.js");
/* harmony import */ var vuex__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! vuex */ "./node_modules/vuex/dist/vuex.esm.js");
function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//



var api = new _JokerApi__WEBPACK_IMPORTED_MODULE_1__["default"]();
/* harmony default export */ __webpack_exports__["default"] = ({
  name: 'MatchAddToTracker',
  components: {
    Box: _box_Box__WEBPACK_IMPORTED_MODULE_0__["default"]
  },
  props: {
    eventId: {
      type: [String, Number]
    },
    marketId: {
      type: [String, Number]
    },
    bets: {
      type: [Object, Array]
    },
    activeBetType: {
      type: String
    }
  },
  data: function data() {
    return {
      types: {},
      clicked: []
    };
  },
  methods: _objectSpread(_objectSpread({}, Object(vuex__WEBPACK_IMPORTED_MODULE_2__["mapActions"])(['setAverage'])), {}, {
    processTypes: function processTypes() {
      var _this = this;

      this.types = {};
      var types = this.bets[0].coefficientsGroups;
      Object.values(types).map(function (type) {
        _this.types[type] = true;
      });
    },
    addToTracker: function addToTracker(type) {
      var _this2 = this;

      var remember_token = this.$cookies.get('api_remember_token');
      api.addTrackerItem(remember_token, this.eventId, this.marketId, type).then(function (res) {
        if (_this2.eventId && _this2.marketId) _this2.setAverage({
          marketId: type,
          average: _this2.bets[0].coefficients[type],
          id: "".concat(_this2.eventId).concat(_this2.marketId).concat(type)
        });
      }).then(function () {
        _this2.$root.$emit('updateTrackerItems');
      });
    },
    getType: function getType(allTypes, choseTypes) {
      return choseTypes.filter(function (type) {
        return allTypes[type];
      })[0];
    }
  }),
  mounted: function mounted() {
    var _this3 = this;

    // this.$log.debug(this.bets)
    this.processTypes();
    this.$root.$on('updateTrackerAverages', function (items) {
      return _this3.$cookies.set('getAverage', JSON.stringify(items));
    });
  },
  watch: {
    bets: function bets() {
      this.processTypes();
    }
  }
});

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/pageparts/match/MatchNav.vue?vue&type=script&lang=js&":
/*!***********************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/pageparts/match/MatchNav.vue?vue&type=script&lang=js& ***!
  \***********************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
/* harmony default export */ __webpack_exports__["default"] = ({
  name: 'MatchNav',
  props: {
    navItems: {
      type: Array,
      required: true
    },
    routeTab: {
      type: String
    },
    eventId: {
      required: true
    },
    active: {
      required: true
    }
  }
});

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/pageparts/match/MatchStats.vue?vue&type=script&lang=js&":
/*!*************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/pageparts/match/MatchStats.vue?vue&type=script&lang=js& ***!
  \*************************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
/* harmony default export */ __webpack_exports__["default"] = ({
  name: 'MatchStats',
  props: {
    bets: {
      type: Array,
      required: true
    }
  },
  data: function data() {
    return {
      highest: {}
    };
  },
  watch: {
    bets: function bets() {
      this.calculateMax();
    }
  },
  methods: {
    calculatePercent: function calculatePercent(values) {
      var itemsSum = 0;
      values.map(function (el) {
        itemsSum += 1 / parseFloat(el);
      });
      return (1 / itemsSum * 100).toFixed(2);
    },
    calculateMax: function calculateMax() {
      var _this = this;

      var highest = {};
      var typesUnique = this.bets[0].coefficientsGroups;
      typesUnique.map(function (type) {
        var max;
        Object.values(_this.bets[0].bets).map(function (el, index) {
          if (index === 0) {
            max = 0;
          }

          if (el[type]) {
            max = max < parseFloat(el[type].price) ? parseFloat(el[type].price) : max;
          }

          if (index + 1 === Object.keys(_this.bets[0].bets).length) {
            highest[type] = max.toFixed(2);
          }
        });
      });
      this.highest = highest;
    },
    average: function average(coeffName) {
      if (!this.bets[0].coefficients[coeffName]) return false;
      return this.bets[0].coefficients[coeffName].toFixed(2);
    }
  },
  created: function created() {
    this.calculateMax();
  }
});

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/pageparts/match/MatchTable.vue?vue&type=script&lang=js&":
/*!*************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/pageparts/match/MatchTable.vue?vue&type=script&lang=js& ***!
  \*************************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var vue_fragment__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! vue-fragment */ "./node_modules/vue-fragment/dist/vue-fragment.esm.js");
/* harmony import */ var _MatchTableRow__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./MatchTableRow */ "./resources/js/components/pageparts/match/MatchTableRow.vue");
/* harmony import */ var _MatchTableLine__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./MatchTableLine */ "./resources/js/components/pageparts/match/MatchTableLine.vue");
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//



/* harmony default export */ __webpack_exports__["default"] = ({
  name: 'MatchTable',
  props: {
    bets: {
      type: Array,
      required: true
    },
    activeBetType: {
      type: String,
      required: true
    },
    marketId: Number,
    eventId: {
      type: [String, Number]
    }
  },
  data: function data() {
    return {
      allBookmakers: false
    };
  },
  computed: {
    typeNames: function typeNames() {
      // '1X2', marketId: 1
      // 'AH', marketId: 3
      // 'O/U', marketId: 2
      // 'HT/FT', maketId: 4
      // 'DC', marketId: 7
      if (this.marketId === 1) return ['1', 'X', '2'];
      if (this.marketId === 3) return ['1', '2'];
      if (this.marketId === 2) return ['Over', 'Under'];
      if (this.marketId === 7) return ['1X', '12', '2X'];
    }
  },
  methods: {
    showAllBookmakers: function showAllBookmakers() {
      this.allBookmakers = true;
    },
    selectedBookmakers: function selectedBookmakers(bets, marketId) {
      var typeName = ['1', 'X', '2'];
      /*       if (marketId === 1) typeName = ['1', 'X', '2'];
             if (marketId === 3) typeName = ['1', '2'];
             if (marketId === 2) typeName = ['Over', 'Under'];
             if (marketId === 7) typeName = ['1X', '12', '2X'];
              if(!this.allBookmakers){
                 let n_bets ={};
                 console.log(bets[0]['bets']);
                 n_bets = Object.values(bets[0]['bets']).filter(function (bookmaker,index) {
                     return (index<15) && bookmaker[typeName[0]]['bookmaker']['selected']
                 });
                 bets[0]['bets']=n_bets;
                 return bets;
             }*/

      return bets;
    }
  },
  components: {
    MatchTableLine: _MatchTableLine__WEBPACK_IMPORTED_MODULE_2__["default"],
    MatchTableRow: _MatchTableRow__WEBPACK_IMPORTED_MODULE_1__["default"],
    Fragment: vue_fragment__WEBPACK_IMPORTED_MODULE_0__["Fragment"]
  }
});

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/pageparts/match/MatchTableLine.vue?vue&type=script&lang=js&":
/*!*****************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/pageparts/match/MatchTableLine.vue?vue&type=script&lang=js& ***!
  \*****************************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var vue_fragment__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! vue-fragment */ "./node_modules/vue-fragment/dist/vue-fragment.esm.js");
/* harmony import */ var _MatchTableRow__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./MatchTableRow */ "./resources/js/components/pageparts/match/MatchTableRow.vue");
/* harmony import */ var _JokerApi__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../JokerApi */ "./resources/js/JokerApi.js");
function _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _unsupportedIterableToArray(arr, i) || _nonIterableRest(); }

function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

function _iterableToArrayLimit(arr, i) { if (typeof Symbol === "undefined" || !(Symbol.iterator in Object(arr))) return; var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"] != null) _i["return"](); } finally { if (_d) throw _e; } } return _arr; }

function _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }

//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//



var api = new _JokerApi__WEBPACK_IMPORTED_MODULE_2__["default"]();
/* harmony default export */ __webpack_exports__["default"] = ({
  name: 'MatchTableLine',
  props: {
    line: Object,
    displayElement: {
      type: Boolean
    },
    eventId: {
      type: [String, Number]
    },
    marketId: {
      type: Number
    }
  },
  data: function data() {
    return {
      showSubtable: false
    };
  },
  computed: {
    betsCount: function betsCount() {
      return Object.keys(this.line.bets).length;
    },
    marketName: function marketName() {
      return Object.values(Object.values(this.line.bets)[0])[0].market.name;
    }
  },
  methods: {
    avgPercent: function avgPercent(values) {
      var itemsSum = 0;
      Object.values(values).map(function (el) {
        itemsSum += 1 / parseFloat(el);
      });
      return (1 / itemsSum * 100).toFixed(2);
    },
    highestPercent: function highestPercent(coeffNames) {
      var _this = this;

      var highestCoeffs = [];
      coeffNames.map(function (coeffName) {
        highestCoeffs.push(_this.highestCoeff(coeffName));
      });
      var itemsSum = 0;
      Object.values(highestCoeffs).map(function (el) {
        itemsSum += 1 / parseFloat(el);
      });
      return (1 / itemsSum * 100).toFixed(2);
    },
    highestCoeff: function highestCoeff(coeffName) {
      var highest = 0;
      Object.values(this.line.bets).map(function (bookmaker) {
        Object.entries(bookmaker).map(function (_ref) {
          var _ref2 = _slicedToArray(_ref, 2),
              cName = _ref2[0],
              cData = _ref2[1];

          if (cName === coeffName) {
            highest = parseFloat(cData.price) > highest ? parseFloat(cData.price) : highest;
          }
        });
      });
      return highest.toFixed(2);
    },
    addToTracker: function addToTracker(type) {
      var _this2 = this;

      var remember_token = this.$cookies.get('api_remember_token');
      api.addTrackerItem(remember_token, this.eventId, this.marketId, type).then(function (res) {
        _this2.$log.debug(type, _this2.marketId, _this2.eventId, 'addToTracker', res);
      }).then(function () {
        _this2.$root.$emit('updateTrackerItems');
      });
    },
    displayBookmaker: function displayBookmaker() {
      if (this.displayElement) {
        var _tmp = [];
        Object.values(this.line.bets).map(function (item, key) {
          if (item.hasOwnProperty(1)) {
            if (item[1]['bookmaker']['active']) {
              _tmp.push(1);
            }
          } else if (item.hasOwnProperty('Over')) {
            if (item['Over']['bookmaker']['active']) {
              _tmp.push(1);
            }
          }
        });
        return _tmp.length;
      }

      var tmp = [];
      Object.values(this.line.bets).map(function (item, key) {
        if (item.hasOwnProperty(1)) {
          if (item[1]['bookmaker']['selected'] && item[1]['bookmaker']['active']) {
            tmp.push(1);
          }
        } else if (item.hasOwnProperty('Over')) {
          if (item['Over']['bookmaker']['selected'] && item['Over']['bookmaker']['active']) {
            tmp.push(1);
          }
        }
      });
      return tmp.length; // return Object.values(Object.values(this.line.bets)[0])[0]['bookmaker'].selected
    }
  },
  components: {
    MatchTableRow: _MatchTableRow__WEBPACK_IMPORTED_MODULE_1__["default"],
    Fragment: vue_fragment__WEBPACK_IMPORTED_MODULE_0__["Fragment"]
  },
  watch: {}
});

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/pageparts/match/MatchTableRow.vue?vue&type=script&lang=js&":
/*!****************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/pageparts/match/MatchTableRow.vue?vue&type=script&lang=js& ***!
  \****************************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _JokerApi__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../../../JokerApi */ "./resources/js/JokerApi.js");
/* harmony import */ var _MatchTableRowHistory__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./MatchTableRowHistory */ "./resources/js/components/pageparts/match/MatchTableRowHistory.vue");
/* harmony import */ var _MatchTableRowBubble__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./MatchTableRowBubble */ "./resources/js/components/pageparts/match/MatchTableRowBubble.vue");
function _toConsumableArray(arr) { return _arrayWithoutHoles(arr) || _iterableToArray(arr) || _unsupportedIterableToArray(arr) || _nonIterableSpread(); }

function _nonIterableSpread() { throw new TypeError("Invalid attempt to spread non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

function _iterableToArray(iter) { if (typeof Symbol !== "undefined" && Symbol.iterator in Object(iter)) return Array.from(iter); }

function _arrayWithoutHoles(arr) { if (Array.isArray(arr)) return _arrayLikeToArray(arr); }

function _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _unsupportedIterableToArray(arr, i) || _nonIterableRest(); }

function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

function _iterableToArrayLimit(arr, i) { if (typeof Symbol === "undefined" || !(Symbol.iterator in Object(arr))) return; var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"] != null) _i["return"](); } finally { if (_d) throw _e; } } return _arr; }

function _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }

//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//



var api = new _JokerApi__WEBPACK_IMPORTED_MODULE_0__["default"]();
/* harmony default export */ __webpack_exports__["default"] = ({
  name: 'MatchTableRow',
  components: {
    MatchTableRowBubble: _MatchTableRowBubble__WEBPACK_IMPORTED_MODULE_2__["default"],
    MatchTableRowHistory: _MatchTableRowHistory__WEBPACK_IMPORTED_MODULE_1__["default"]
  },
  props: {
    eventId: {
      type: [String, Number]
    },
    marketId: {
      type: Number
    },
    item: {
      type: Object,
      required: true
    },
    activeBetType: {
      "default": '1x2'
    },
    idx: {
      required: true
    },
    type: String,
    displayElement: {
      type: Boolean
    },
    position: Number
  },
  data: function data() {
    return {
      betsHistory: {},
      types: {}
    };
  },
  methods: {
    calculatePercent: function calculatePercent(item) {
      var itemsSum = 0;
      Object.values(item).forEach(function (el) {
        itemsSum += 1 / parseFloat(el.price);
      });
      return (1 / itemsSum * 100).toFixed(2);
    },
    ifSet: function ifSet(value) {
      if (value !== undefined && value !== false && value !== null) {
        return value;
      } else {
        return false;
      }
    },
    processTypes: function processTypes() {
      var _this = this;

      this.types = {};
      var types = [];
      this.bets.map(function (bet) {
        Object.entries(bet.values).map(function (_ref) {
          var _ref2 = _slicedToArray(_ref, 2),
              key = _ref2[0],
              value = _ref2[1];

          types.push(key);
        });
      });

      _toConsumableArray(new Set(types)).map(function (type) {
        _this.types[type] = true;
      });
    },
    addToTracker: function addToTracker(type) {
      var _this2 = this;

      api.addTrackerItem(this.$cookies.get('api_remember_token'), this.eventId, this.item.market_id, type).then(function (res) {
        _this2.$log.debug(type, _this2.item.market_id, _this2.eventId, 'addToTracker', res);
      }).then(function () {
        _this2.$root.$emit('updateTrackerItems');
      });
    },
    getType: function getType(itemTypes, choseTypes) {
      var matchedType = itemTypes.filter(function (element) {
        return choseTypes.includes(element);
      });
      return matchedType[0];
    },
    displayBookmaker: function displayBookmaker(item) {
      var typeName = ['1', 'X', '2'];
      if (this.marketId === 1) typeName = ['1', 'X', '2'];
      if (this.marketId === 3) typeName = ['1', '2'];
      if (this.marketId === 2) typeName = ['Over', 'Under'];
      if (this.marketId === 7) typeName = ['1X', '12', '2X'];

      if (this.displayElement) {
        return item[typeName[0]]['bookmaker']['active'];
      }

      if (item[typeName[0]]['bookmaker']['selected'] && item[typeName[0]]['bookmaker']['active']) {
        return 1;
      } else return 0;
    }
  },
  watch: {
    item: function item(val) {// this.$log.debug('item', val)
    }
  },
  created: function created() {// this.$log.debug(this.item)
  },
  computed: {
    refLink: function refLink() {
      return Object.values(this.item)[0].bookmaker.ref && !Object.values(this.item)[0].bookmaker.restricted ? Object.values(this.item)[0].bookmaker.ref.replace(/(^\w+:|^)/, '') : '#';
    },
    name: function name() {
      return Object.values(this.item)[0].bookmaker.name;
    },
    logo: function logo() {
      return Object.values(this.item)[0].bookmaker.logo;
    },
    line: function line() {
      return Object.values(this.item)[0].line;
    },
    restricted: function restricted() {
      return !!Object.values(this.item)[0].bookmaker.restricted;
    }
  }
});

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/pageparts/match/MatchTableRowBubble.vue?vue&type=script&lang=js&":
/*!**********************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/pageparts/match/MatchTableRowBubble.vue?vue&type=script&lang=js& ***!
  \**********************************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _MatchTableRowHistory__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./MatchTableRowHistory */ "./resources/js/components/pageparts/match/MatchTableRowHistory.vue");
/* harmony import */ var _JokerApi__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../../JokerApi */ "./resources/js/JokerApi.js");
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//


/* harmony default export */ __webpack_exports__["default"] = ({
  name: 'MatchTableRowBubble',
  props: {
    item: Object,
    betHistory: Object
  },
  data: function data() {
    return {
      showBets: false,
      betsLoaded: false,
      bets: [],
      current: {},
      opening: {},
      info: {},
      flag: 'up'
    };
  },
  methods: {
    getHistory: function getHistory(item) {
      var _this = this;

      if (this.betsLoaded) {
        this.$log.debug('this.betsHistory.loaded === true');
        this.showBetsHistory();
      } else {
        this.$log.debug('this.betsHistory.loaded === false');
        this.showBetsHistory();
        var api = new _JokerApi__WEBPACK_IMPORTED_MODULE_1__["default"]();
        var market = item.market_id;
        var bookmaker = item.bookmaker_id;
        var event = this.$route.params.id;
        api.getBetHistory(item.name, event, bookmaker, market, item.base_line).then(function (response) {
          _this.bets = response[0].bets;
          _this.current = response[0].current;
          _this.opening = response[0].opening;
          _this.info = response[0].info;
          _this.betsLoaded = true;

          _this.$forceUpdate();

          _this.$log.debug('response', response, _this.betsHistory);
        })["catch"](function (err) {
          return console.error(err);
        });
        this.$forceUpdate();
      }
    },
    itemValuesDifferent: function itemValuesDifferent(item) {
      return item.price !== item.start_price;
    },
    itemValuesArr: function itemValuesArr(item) {
      var length = this.bets.length;

      if (!length) {
        return item.price > item.start_price ? 'up' : 'down';
      } else if (length === 1) {
        return this.bets[0]['price'] > item.start_price ? 'up' : 'down';
      }

      return this.bets[length - 1]['price'] > item.start_price ? 'up' : 'down';
    },
    ifSet: function ifSet(value) {
      if (value !== undefined && value !== false && value !== null) {
        return value;
      } else {
        return false;
      }
    },
    showBetsHistory: function showBetsHistory() {
      this.showBets = true;
    },
    hideBetsHistory: function hideBetsHistory() {
      this.showBets = false;
    }
  },
  mounted: function mounted() {
    var length = this.bets.length;

    if (!length) {
      this.flag = this.item.price > this.item.start_price ? 'up' : 'down';
    } else if (length === 1) {
      this.flag = this.bets[0]['price'] > this.item.start_price ? 'up' : 'down';
    } else {
      this.flag = this.bets[length - 1]['price'] > this.bets[length - 2]['price'] ? 'up' : 'down';
    }
  },
  watch: {
    bets: function bets(val) {
      var length = this.bets.length;

      if (!length) {
        this.flag = this.item.price > this.item.start_price ? 'up' : 'down';
      } else if (length === 1) {
        this.flag = this.bets[0]['price'] > this.item.start_price ? 'up' : 'down';
      } else {
        this.flag = this.bets[0]['price'] > this.bets[1]['price'] ? 'up' : 'down';
      }
    }
  },
  components: {
    MatchTableRowHistory: _MatchTableRowHistory__WEBPACK_IMPORTED_MODULE_0__["default"]
  }
});

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/pageparts/match/MatchTableRowHistory.vue?vue&type=script&lang=js&":
/*!***********************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/pageparts/match/MatchTableRowHistory.vue?vue&type=script&lang=js& ***!
  \***********************************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! moment */ "./node_modules/moment/moment.js");
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(moment__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var vuex__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! vuex */ "./node_modules/vuex/dist/vuex.esm.js");
/* harmony import */ var _HorizontalLoader__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../HorizontalLoader */ "./resources/js/components/HorizontalLoader.vue");
function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//



/* harmony default export */ __webpack_exports__["default"] = ({
  name: 'MatchTableRowHistory',
  components: {
    HorizontalLoader: _HorizontalLoader__WEBPACK_IMPORTED_MODULE_2__["default"]
  },
  props: {
    bets: Array,
    current: Object,
    opening: Object,
    info: Object,
    betsLoaded: Boolean
  },
  computed: _objectSpread({}, Object(vuex__WEBPACK_IMPORTED_MODULE_1__["mapGetters"])(['headerClockTime', 'headerClockTimezoneOffset', 'headerClockDate'])),
  methods: {
    hideBetsHistory: function hideBetsHistory() {
      this.$emit('hide-bets-history');
    },
    diff: function diff(currentPrice, index) {
      if (index === 0) return 'empty';

      if (parseFloat(currentPrice) < parseFloat(this.bets[index - 1].price)) {
        return 'red';
      } else if (parseFloat(currentPrice) === parseFloat(this.bets[index - 1].price)) {
        return 'empty';
      } else {
        return 'green';
      }
    },
    diffNum: function diffNum(currentPrice, index) {
      if (index === 0) return 'NULL';
      var sign = parseFloat(currentPrice) < parseFloat(this.bets[index - 1].price) ? '-' : '+';
      var result = parseFloat(this.bets[index - 1].price) - parseFloat(currentPrice);
      if (result === 0) return 'NULL';
      result = result < 0 ? -result : result;
      return "".concat(sign).concat(result.toFixed(2));
    },
    formatDatestring: function formatDatestring(ds) {
      return moment__WEBPACK_IMPORTED_MODULE_0___default()(ds, 'YYYY-MM-DDTHH:mm:ss').add(this.$store.getters.headerClockTimezoneOffset, 'h').format('DD MMM, HH:MM');
    }
  }
});

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/pageparts/match/MatchTabs.vue?vue&type=script&lang=js&":
/*!************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/pageparts/match/MatchTabs.vue?vue&type=script&lang=js& ***!
  \************************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
/* harmony default export */ __webpack_exports__["default"] = ({
  name: 'MatchTabs'
});

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/pageparts/match/MatchTeams.vue?vue&type=script&lang=js&":
/*!*************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/pageparts/match/MatchTeams.vue?vue&type=script&lang=js& ***!
  \*************************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! moment */ "./node_modules/moment/moment.js");
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(moment__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var vuex__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! vuex */ "./node_modules/vuex/dist/vuex.esm.js");
function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//


/* harmony default export */ __webpack_exports__["default"] = ({
  name: 'MatchTeams',
  props: {
    teams: {
      type: Array,
      required: true
    },
    scoreboard: {
      "default": null,
      required: true
    },
    startdate: {
      type: String,
      required: true
    },
    eventId: Number,

    /*  match: {
          type: [Array, Object],
          required: true
      },*/
    status_id: {
      type: Number,
      "default": null
    }
  },
  data: function data() {
    return {
      dateFormat: 'YYYY-MM-DDTHH:mm:ss'
    };
  },
  computed: _objectSpread(_objectSpread({}, Object(vuex__WEBPACK_IMPORTED_MODULE_1__["mapGetters"])(['headerClockTime', 'headerClockTimezoneOffset', 'headerClockDate'])), {}, {
    score: function score() {
      if (!this.scoreboard) return false;

      if (Array.isArray(this.scoreboard.results)) {
        return {
          team1: this.scoreboard.results[0]['Value'],
          team2: this.scoreboard.results[1]['Value']
        };
      }

      var splitted = this.scoreboard.results.split(':');
      return {
        team1: splitted[0],
        team2: splitted[1]
      };
    },
    isPlay: function isPlay() {
      if (!this.scoreboard) return true;

      if (this.scoreboard.status_id === 2) {
        return 2;
      }

      if (this.scoreboard.status_id === 3) {
        return 3;
      }
    },
    startDate: function startDate() {
      return moment__WEBPACK_IMPORTED_MODULE_0___default()(this.startdate, this.dateFormat).add(this.$store.getters.headerClockTimezoneOffset, 'h').format('ddd, DD MMM YYYY');
    },
    startTime: function startTime() {
      return moment__WEBPACK_IMPORTED_MODULE_0___default()(this.startdate, this.dateFormat).add(this.$store.getters.headerClockTimezoneOffset, 'h').format('HH:mm');
    }
  }),
  methods: {
    teamLogo: function teamLogo(team) {
      if (team.image !== null) {
        return "url('".concat(window.location.origin, "/storage/").concat(team.image, "')");
      }

      if (team.team_image !== null) {
        return "url('".concat(window.location.origin, "/storage/").concat(team.team_image, "')");
      }

      return "url('".concat(window.location.origin, "/img/team_placeholder.svg')");
    },
    onClick: function onClick() {
      var widget = "\n                <iframe src=\"https://jokerodds.com/widget-partners/1/".concat(this.eventId, "\" width=\"809\" height=\"53\" frameBorder=\"0\"></iframe>\n                <iframe src=\"https://jokerodds.com/widget-partners/2/").concat(this.eventId, "\" width=\"297\" height=\"200\" frameBorder=\"0\"></iframe>\n                <iframe src=\"https://jokerodds.com/widget-partners/3/0\" width=\"188\" height=\"30\" frameBorder=\"0\"></iframe>\n                ");
      var fileURL = window.URL.createObjectURL(new Blob([widget]));
      var fileLink = document.createElement('a');
      fileLink.href = fileURL;
      fileLink.setAttribute('download', 'widgets.txt');
      document.body.appendChild(fileLink);
      fileLink.click();
    }
  },
  mounted: function mounted() {
    var _this = this;

    window.Echo.channel("jokerodds_database_presence-score-event.".concat(this.eventId)).listen('ScoreUpdated', function (response) {
      _this.$store.dispatch('updateScoreBoard', response.data);

      console.log('score-updated--', response.data);
    });
  },
  watch: {// this.$log.debug('this.bets', this.bets)
  },
  destroyed: function destroyed() {
    window.Echo.leave("jokerodds_database_presence-score-event.".concat(this.eventId));
  }
});

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/pageparts/match/MatchTimes.vue?vue&type=script&lang=js&":
/*!*************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/pageparts/match/MatchTimes.vue?vue&type=script&lang=js& ***!
  \*************************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
/* harmony default export */ __webpack_exports__["default"] = ({
  name: 'MatchTimes',
  props: {
    activeType: {
      type: String,
      required: true
    },
    matchId: {
      type: String,
      required: true
    },
    eventId: {
      required: true
    }
  }
});

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/Match.vue?vue&type=script&lang=js&":
/*!***********************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/views/Match.vue?vue&type=script&lang=js& ***!
  \***********************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var vuex__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! vuex */ "./node_modules/vuex/dist/vuex.esm.js");
/* harmony import */ var _components_Breadcrumbs__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../components/Breadcrumbs */ "./resources/js/components/Breadcrumbs.vue");
/* harmony import */ var _components_pageparts_box_Box__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../components/pageparts/box/Box */ "./resources/js/components/pageparts/box/Box.vue");
/* harmony import */ var _components_pageparts_match_MatchTeams__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../components/pageparts/match/MatchTeams */ "./resources/js/components/pageparts/match/MatchTeams.vue");
/* harmony import */ var _components_pageparts_match_MatchTabs__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../components/pageparts/match/MatchTabs */ "./resources/js/components/pageparts/match/MatchTabs.vue");
/* harmony import */ var _components_pageparts_match_MatchNav__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../components/pageparts/match/MatchNav */ "./resources/js/components/pageparts/match/MatchNav.vue");
/* harmony import */ var _components_pageparts_match_MatchTimes__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../components/pageparts/match/MatchTimes */ "./resources/js/components/pageparts/match/MatchTimes.vue");
/* harmony import */ var _components_pageparts_match_MatchTable__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../components/pageparts/match/MatchTable */ "./resources/js/components/pageparts/match/MatchTable.vue");
/* harmony import */ var _components_pageparts_match_MatchStats__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../components/pageparts/match/MatchStats */ "./resources/js/components/pageparts/match/MatchStats.vue");
/* harmony import */ var _components_pageparts_match_MatchAddToTracker__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ../components/pageparts/match/MatchAddToTracker */ "./resources/js/components/pageparts/match/MatchAddToTracker.vue");
/* harmony import */ var _components_Loader__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ../components/Loader */ "./resources/js/components/Loader.vue");
function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//











/* harmony default export */ __webpack_exports__["default"] = ({
  name: 'Match',
  data: function data() {
    return {
      markets: [{
        url: '1x2',
        apiName: '1X2',
        name: '1X2',
        marketId: 1
      }, {
        url: 'ah',
        apiName: 'Asian Handicap',
        name: 'AH',
        marketId: 3
      }, {
        url: 'underover',
        apiName: 'Under/Over',
        name: 'O/U',
        marketId: 2
      }, {
        url: 'doublechance',
        apiName: 'Double Chance',
        name: 'DC',
        marketId: 7
      }],
      loading: true
    };
  },
  props: {
    tab: {
      type: String,
      "default": '1x2'
    },
    id: {
      Type: [String, Number],
      required: true
    }
  },
  computed: _objectSpread(_objectSpread({}, Object(vuex__WEBPACK_IMPORTED_MODULE_0__["mapGetters"])(['match'])), {}, {
    prematchBets: function prematchBets() {
      return this.match.bets.prematch;
    },
    selectedTab: function selectedTab() {
      var _this = this;

      return this.markets.filter(function (item) {
        return item.url.toLowerCase() === _this.tab.toLowerCase();
      })[0];
    }
  }),
  methods: _objectSpread(_objectSpread({}, Object(vuex__WEBPACK_IMPORTED_MODULE_0__["mapActions"])(['fetchMatch'])), {}, {
    stopLoading: function stopLoading() {
      this.loading = false;
    },
    startLoading: function startLoading() {
      this.loading = true;
    },
    toggleLoading: function toggleLoading() {
      this.loading = !this.loading;
    },
    fetchData: function fetchData() {
      var _this2 = this;

      this.fetchMatch(this.id)["finally"](function () {
        _this2.matchLoading = false;

        _this2.$root.$emit('setMatchName', _this2.match.event.fixture.participants.map(function (t) {
          return t.name;
        }).join(' - '));

        _this2.$root.$emit('setLeagueId', _this2.match.event.fixture.league_id);

        _this2.$root.$emit('setLeagueMatchName', _this2.match.event.fixture.name);

        _this2.stopLoading(); // this.$log.debug('this.match', this.match)

      });
    }
  }),
  mounted: function mounted() {
    window.Echo.channel("jokerodds_database_presence-event-bets.".concat(this.id)).listen('BetsUpdated', function (response) {
      console.log('to-update', response.bets); //     this.$store.dispatch('setMatch', response.data);
      //    console.log('updated');
    });
    /*  window.Echo.channel(`jokerodds_database_presence-test-channel-echo`)
          .listen('TestEcho', (response) => {
              console.log('event-broadcast',response);
              //  this.$store.commit('setMatch',response.data);
          });*/
  },
  watch: {
    id: function id() {
      this.startLoading();
      this.fetchData();
    }
  },
  created: function created() {
    // this.startLoading()
    this.fetchData();
  },
  destroyed: function destroyed() {
    window.Echo.leave("jokerodds_database_presence-event-bets.".concat(this.id));
  },
  components: {
    Loader: _components_Loader__WEBPACK_IMPORTED_MODULE_10__["default"],
    Box: _components_pageparts_box_Box__WEBPACK_IMPORTED_MODULE_2__["default"],
    Breadcrumbs: _components_Breadcrumbs__WEBPACK_IMPORTED_MODULE_1__["default"],
    MatchTeams: _components_pageparts_match_MatchTeams__WEBPACK_IMPORTED_MODULE_3__["default"],
    MatchTabs: _components_pageparts_match_MatchTabs__WEBPACK_IMPORTED_MODULE_4__["default"],
    MatchNav: _components_pageparts_match_MatchNav__WEBPACK_IMPORTED_MODULE_5__["default"],
    MatchTimes: _components_pageparts_match_MatchTimes__WEBPACK_IMPORTED_MODULE_6__["default"],
    MatchTable: _components_pageparts_match_MatchTable__WEBPACK_IMPORTED_MODULE_7__["default"],
    MatchStats: _components_pageparts_match_MatchStats__WEBPACK_IMPORTED_MODULE_8__["default"],
    MatchAddToTracker: _components_pageparts_match_MatchAddToTracker__WEBPACK_IMPORTED_MODULE_9__["default"]
  }
});

/***/ }),

/***/ "./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/sass-loader/dist/cjs.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/pageparts/match/MatchTableLine.vue?vue&type=style&index=0&id=1a700320&scoped=true&lang=scss&":
/*!****************************************************************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/css-loader!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src??ref--7-2!./node_modules/sass-loader/dist/cjs.js??ref--7-3!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/pageparts/match/MatchTableLine.vue?vue&type=style&index=0&id=1a700320&scoped=true&lang=scss& ***!
  \****************************************************************************************************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(/*! ../../../../../node_modules/css-loader/lib/css-base.js */ "./node_modules/css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, ".odds-table .table .bubble[data-v-1a700320] {\n  z-index: 0;\n}", ""]);

// exports


/***/ }),

/***/ "./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/HorizontalLoader.vue?vue&type=style&index=0&id=0011e77c&scoped=true&lang=css&":
/*!**********************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/css-loader??ref--6-1!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src??ref--6-2!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/HorizontalLoader.vue?vue&type=style&index=0&id=0011e77c&scoped=true&lang=css& ***!
  \**********************************************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(/*! ../../../node_modules/css-loader/lib/css-base.js */ "./node_modules/css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "\n.loader-wrapper[data-v-0011e77c] {\n    height: 100%;\n    width: 100%;\n}\n.lds-ellipsis[data-v-0011e77c] {\n    display: inline-block;\n    position: relative;\n    width: 80px;\n    height: 40px;\n}\n.lds-ellipsis div[data-v-0011e77c] {\n    position: absolute;\n    top: 17px;\n    width: 6px;\n    height: 6px;\n    border-radius: 50%;\n    background: #DE343A;\n    -webkit-animation-timing-function: cubic-bezier(0, 1, 1, 0);\n            animation-timing-function: cubic-bezier(0, 1, 1, 0);\n}\n.lds-ellipsis div[data-v-0011e77c]:nth-child(1) {\n    left: 8px;\n    -webkit-animation: lds-ellipsis1-data-v-0011e77c 0.6s infinite;\n            animation: lds-ellipsis1-data-v-0011e77c 0.6s infinite;\n}\n.lds-ellipsis div[data-v-0011e77c]:nth-child(2) {\n    left: 8px;\n    -webkit-animation: lds-ellipsis2-data-v-0011e77c 0.6s infinite;\n            animation: lds-ellipsis2-data-v-0011e77c 0.6s infinite;\n}\n.lds-ellipsis div[data-v-0011e77c]:nth-child(3) {\n    left: 32px;\n    -webkit-animation: lds-ellipsis2-data-v-0011e77c 0.6s infinite;\n            animation: lds-ellipsis2-data-v-0011e77c 0.6s infinite;\n}\n.lds-ellipsis div[data-v-0011e77c]:nth-child(4) {\n    left: 56px;\n    -webkit-animation: lds-ellipsis3-data-v-0011e77c 0.6s infinite;\n            animation: lds-ellipsis3-data-v-0011e77c 0.6s infinite;\n}\n@-webkit-keyframes lds-ellipsis1-data-v-0011e77c {\n0% {\n        transform: scale(0);\n}\n100% {\n        transform: scale(1);\n}\n}\n@keyframes lds-ellipsis1-data-v-0011e77c {\n0% {\n        transform: scale(0);\n}\n100% {\n        transform: scale(1);\n}\n}\n@-webkit-keyframes lds-ellipsis3-data-v-0011e77c {\n0% {\n        transform: scale(1);\n}\n100% {\n        transform: scale(0);\n}\n}\n@keyframes lds-ellipsis3-data-v-0011e77c {\n0% {\n        transform: scale(1);\n}\n100% {\n        transform: scale(0);\n}\n}\n@-webkit-keyframes lds-ellipsis2-data-v-0011e77c {\n0% {\n        transform: translate(0, 0);\n}\n100% {\n        transform: translate(24px, 0);\n}\n}\n@keyframes lds-ellipsis2-data-v-0011e77c {\n0% {\n        transform: translate(0, 0);\n}\n100% {\n        transform: translate(24px, 0);\n}\n}\n\n", ""]);

// exports


/***/ }),

/***/ "./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/pageparts/match/MatchTableRow.vue?vue&type=style&index=0&id=2693ed0e&scoped=true&lang=css&":
/*!***********************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/css-loader??ref--6-1!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src??ref--6-2!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/pageparts/match/MatchTableRow.vue?vue&type=style&index=0&id=2693ed0e&scoped=true&lang=css& ***!
  \***********************************************************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(/*! ../../../../../node_modules/css-loader/lib/css-base.js */ "./node_modules/css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "\n.logo[data-v-2693ed0e] {\n    text-decoration: none;\n    display: block;\n    color: inherit;\n}\n.bubble.bookmaker-line[data-v-2693ed0e] {\n    width: 50px;\n    height: 29px;\n    margin-left: 8px;\n}\n", ""]);

// exports


/***/ }),

/***/ "./node_modules/style-loader/index.js!./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/sass-loader/dist/cjs.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/pageparts/match/MatchTableLine.vue?vue&type=style&index=0&id=1a700320&scoped=true&lang=scss&":
/*!********************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/style-loader!./node_modules/css-loader!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src??ref--7-2!./node_modules/sass-loader/dist/cjs.js??ref--7-3!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/pageparts/match/MatchTableLine.vue?vue&type=style&index=0&id=1a700320&scoped=true&lang=scss& ***!
  \********************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {


var content = __webpack_require__(/*! !../../../../../node_modules/css-loader!../../../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../../../node_modules/postcss-loader/src??ref--7-2!../../../../../node_modules/sass-loader/dist/cjs.js??ref--7-3!../../../../../node_modules/vue-loader/lib??vue-loader-options!./MatchTableLine.vue?vue&type=style&index=0&id=1a700320&scoped=true&lang=scss& */ "./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/sass-loader/dist/cjs.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/pageparts/match/MatchTableLine.vue?vue&type=style&index=0&id=1a700320&scoped=true&lang=scss&");

if(typeof content === 'string') content = [[module.i, content, '']];

var transform;
var insertInto;



var options = {"hmr":true}

options.transform = transform
options.insertInto = undefined;

var update = __webpack_require__(/*! ../../../../../node_modules/style-loader/lib/addStyles.js */ "./node_modules/style-loader/lib/addStyles.js")(content, options);

if(content.locals) module.exports = content.locals;

if(false) {}

/***/ }),

/***/ "./node_modules/style-loader/index.js!./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/HorizontalLoader.vue?vue&type=style&index=0&id=0011e77c&scoped=true&lang=css&":
/*!**************************************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/style-loader!./node_modules/css-loader??ref--6-1!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src??ref--6-2!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/HorizontalLoader.vue?vue&type=style&index=0&id=0011e77c&scoped=true&lang=css& ***!
  \**************************************************************************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {


var content = __webpack_require__(/*! !../../../node_modules/css-loader??ref--6-1!../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../node_modules/postcss-loader/src??ref--6-2!../../../node_modules/vue-loader/lib??vue-loader-options!./HorizontalLoader.vue?vue&type=style&index=0&id=0011e77c&scoped=true&lang=css& */ "./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/HorizontalLoader.vue?vue&type=style&index=0&id=0011e77c&scoped=true&lang=css&");

if(typeof content === 'string') content = [[module.i, content, '']];

var transform;
var insertInto;



var options = {"hmr":true}

options.transform = transform
options.insertInto = undefined;

var update = __webpack_require__(/*! ../../../node_modules/style-loader/lib/addStyles.js */ "./node_modules/style-loader/lib/addStyles.js")(content, options);

if(content.locals) module.exports = content.locals;

if(false) {}

/***/ }),

/***/ "./node_modules/style-loader/index.js!./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/pageparts/match/MatchTableRow.vue?vue&type=style&index=0&id=2693ed0e&scoped=true&lang=css&":
/*!***************************************************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/style-loader!./node_modules/css-loader??ref--6-1!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src??ref--6-2!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/pageparts/match/MatchTableRow.vue?vue&type=style&index=0&id=2693ed0e&scoped=true&lang=css& ***!
  \***************************************************************************************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {


var content = __webpack_require__(/*! !../../../../../node_modules/css-loader??ref--6-1!../../../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../../../node_modules/postcss-loader/src??ref--6-2!../../../../../node_modules/vue-loader/lib??vue-loader-options!./MatchTableRow.vue?vue&type=style&index=0&id=2693ed0e&scoped=true&lang=css& */ "./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/pageparts/match/MatchTableRow.vue?vue&type=style&index=0&id=2693ed0e&scoped=true&lang=css&");

if(typeof content === 'string') content = [[module.i, content, '']];

var transform;
var insertInto;



var options = {"hmr":true}

options.transform = transform
options.insertInto = undefined;

var update = __webpack_require__(/*! ../../../../../node_modules/style-loader/lib/addStyles.js */ "./node_modules/style-loader/lib/addStyles.js")(content, options);

if(content.locals) module.exports = content.locals;

if(false) {}

/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/HorizontalLoader.vue?vue&type=template&id=0011e77c&scoped=true&":
/*!*******************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/HorizontalLoader.vue?vue&type=template&id=0011e77c&scoped=true& ***!
  \*******************************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _vm._m(0)
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "loader-wrapper flex aic jcc" }, [
      _c("div", { staticClass: "lds-ellipsis" }, [
        _c("div"),
        _c("div"),
        _c("div"),
        _c("div")
      ])
    ])
  }
]
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/pageparts/box/Box.vue?vue&type=template&id=c4ce6c64&":
/*!********************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/pageparts/box/Box.vue?vue&type=template&id=c4ce6c64& ***!
  \********************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "div",
    { class: ["box", "box-" + _vm.type, { "no-padding": _vm.noPadding }] },
    [_vm._t("default", [_vm._v("Box content")])],
    2
  )
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/pageparts/match/MatchAddToTracker.vue?vue&type=template&id=1fbb317e&":
/*!************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/pageparts/match/MatchAddToTracker.vue?vue&type=template&id=1fbb317e& ***!
  \************************************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("box", { staticClass: "bg-red c-white add-event-to-tracker" }, [
    _c("div", { staticClass: "icon" }),
    _vm._v(" "),
    _c("div", { staticClass: "name" }, [
      _vm._v("Add This Event To My Tracker")
    ]),
    _vm._v(" "),
    _c("div", { staticClass: "buttons flex aic" }, [
      _vm.types["1"] || _vm.types["Over"] || _vm.types["1X"]
        ? _c(
            "button",
            {
              staticClass: "btn btn-small btn-white-opacity",
              on: {
                click: function($event) {
                  _vm.addToTracker(_vm.getType(_vm.types, ["1", "Over", "1X"]))
                }
              }
            },
            [_vm._v("Add\n        ")]
          )
        : _vm._e(),
      _vm._v(" "),
      _vm.types["X"] || _vm.types["12"]
        ? _c(
            "button",
            {
              staticClass: "btn btn-small btn-white-opacity",
              on: {
                click: function($event) {
                  _vm.addToTracker(_vm.getType(_vm.types, ["X", "12"]))
                }
              }
            },
            [_vm._v("Add\n        ")]
          )
        : _vm._e(),
      _vm._v(" "),
      _vm.types["2"] || _vm.types["Under"] || _vm.types["X2"]
        ? _c(
            "button",
            {
              staticClass: "btn btn-small btn-white-opacity",
              on: {
                click: function($event) {
                  _vm.addToTracker(_vm.getType(_vm.types, ["2", "Under", "X2"]))
                }
              }
            },
            [_vm._v("Add\n        ")]
          )
        : _vm._e(),
      _vm._v(" "),
      _c(
        "button",
        {
          staticClass: "btn btn-small btn-white-opacity",
          attrs: { disabled: "" }
        },
        [_vm._v("Remove")]
      )
    ])
  ])
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/pageparts/match/MatchNav.vue?vue&type=template&id=7f7f86b1&":
/*!***************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/pageparts/match/MatchNav.vue?vue&type=template&id=7f7f86b1& ***!
  \***************************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "div",
    { staticClass: "nav" },
    [
      _vm._l(_vm.navItems, function(item, idx) {
        return !item.notInTabs
          ? _c(
              "router-link",
              {
                key: item.id,
                class: [
                  "nav-item",
                  {
                    active:
                      (_vm.routeTab && item.url === _vm.routeTab) ||
                      (!_vm.routeTab && idx === 0)
                  }
                ],
                attrs: {
                  to: "/football/match/" + _vm.eventId + "/" + item.url,
                  "exact-active-class": {
                    active:
                      (_vm.routeTab && item.url === _vm.routeTab) ||
                      (!_vm.routeTab && idx === 0)
                  }
                },
                on: { click: _vm.close }
              },
              [_vm._v(_vm._s(item.name))]
            )
          : _vm._e()
      }),
      _vm._v(" "),
      _vm._m(0)
    ],
    2
  )
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "nav-item nav-item-more" }, [
      _vm._v("More "),
      _c("span", { staticClass: "coming__soon" }, [_vm._v("Coming soon")])
    ])
  }
]
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/pageparts/match/MatchStats.vue?vue&type=template&id=590b3ead&":
/*!*****************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/pageparts/match/MatchStats.vue?vue&type=template&id=590b3ead& ***!
  \*****************************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", { staticClass: "stats" }, [
    _c("div", { staticClass: "stats-line avg" }, [
      _c("div", { staticClass: "name" }, [_vm._v("Average")]),
      _vm._v(" "),
      _c("div", { staticClass: "nums" }, [
        _vm.average("1") || _vm.average("1X")
          ? _c("div", { staticClass: "num" }, [
              _vm._v(
                "\n                " +
                  _vm._s(_vm.average("1") || _vm.average("1X")) +
                  "\n            "
              )
            ])
          : _vm._e(),
        _vm._v(" "),
        _vm.average("X") || _vm.average("12")
          ? _c("div", { staticClass: "num" }, [
              _vm._v(
                "\n                " +
                  _vm._s(_vm.average("X") || _vm.average("12")) +
                  "\n            "
              )
            ])
          : _vm._e(),
        _vm._v(" "),
        _vm.average("2") || _vm.average("X2")
          ? _c("div", { staticClass: "num" }, [
              _vm._v(
                "\n                " +
                  _vm._s(_vm.average("2") || _vm.average("X2")) +
                  "\n            "
              )
            ])
          : _vm._e(),
        _vm._v(" "),
        _c("div", { staticClass: "num percent" }, [
          _vm._v(
            _vm._s(
              _vm.calculatePercent(
                [].concat(Object.values(_vm.bets[0].coefficients))
              )
            ) + "%"
          )
        ])
      ])
    ]),
    _vm._v(" "),
    _c("div", { staticClass: "stats-line highest" }, [
      _c("div", { staticClass: "name" }, [_vm._v("Highest")]),
      _vm._v(" "),
      _c("div", { staticClass: "nums" }, [
        _vm.highest["1"] || _vm.highest["1X"]
          ? _c("div", { staticClass: "num" }, [
              _vm._v(
                "\n                " +
                  _vm._s(_vm.highest["1"] || _vm.highest["1X"]) +
                  "\n            "
              )
            ])
          : _vm._e(),
        _vm._v(" "),
        _vm.highest["X"] || _vm.highest["12"]
          ? _c("div", { staticClass: "num" }, [
              _vm._v(
                "\n                " +
                  _vm._s(_vm.highest["X"] || _vm.highest["12"]) +
                  "\n            "
              )
            ])
          : _vm._e(),
        _vm._v(" "),
        _vm.highest["2"] || _vm.highest["X2"]
          ? _c("div", { staticClass: "num" }, [
              _vm._v(
                "\n                " +
                  _vm._s(_vm.highest["2"] || _vm.highest["X2"]) +
                  "\n            "
              )
            ])
          : _vm._e(),
        _vm._v(" "),
        _c("div", { staticClass: "num percent" }, [
          _vm._v(
            _vm._s(
              _vm.calculatePercent([].concat(Object.values(_vm.highest)))
            ) + "%"
          )
        ])
      ])
    ])
  ])
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/pageparts/match/MatchTable.vue?vue&type=template&id=5e13865c&":
/*!*****************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/pageparts/match/MatchTable.vue?vue&type=template&id=5e13865c& ***!
  \*****************************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", { staticClass: "table" }, [
    _c(
      "div",
      { staticClass: "table-head" },
      [
        _c(
          "div",
          {
            staticClass: "cell",
            class: {
              "cell-name": _vm.marketId === 3 || _vm.marketId === 2,
              "cell-logo": _vm.marketId !== 3 && _vm.marketId !== 2
            }
          },
          [_c("div", { staticClass: "name" }, [_vm._v("Bookmakers")])]
        ),
        _vm._v(" "),
        _vm.marketId === 3 || _vm.marketId === 2
          ? _c("div", { staticClass: "cell cell-odds" }, [
              _c("div", { staticClass: "name" }, [_vm._v("Odds")])
            ])
          : _vm._e(),
        _vm._v(" "),
        _vm._l(_vm.typeNames, function(type, typeIdx) {
          return _c(
            "div",
            {
              key: typeIdx,
              staticClass: "cell",
              class:
                "cell-" +
                (type === "1" || type === "1X" || type === "Over"
                  ? "one"
                  : type === "X" || type === "12"
                  ? "x"
                  : "two")
            },
            [_c("div", { staticClass: "name" }, [_vm._v(_vm._s(type))])]
          )
        }),
        _vm._v(" "),
        _vm._m(0),
        _vm._v(" "),
        _vm.marketId === 3 || _vm.marketId === 2
          ? _c("div", { staticClass: "cell cell-arr cell-empty" })
          : _vm._e()
      ],
      2
    ),
    _vm._v(" "),
    _vm.bets
      ? _c(
          "div",
          { staticClass: "table-body" },
          [
            _vm._l(_vm.bets, function(line, lineIdx) {
              return [
                line.line !== null
                  ? _c("match-table-line", {
                      key: lineIdx,
                      attrs: {
                        line: line,
                        eventId: _vm.eventId,
                        idx: lineIdx,
                        marketId: _vm.marketId,
                        displayElement: _vm.allBookmakers
                      }
                    })
                  : _vm._l(line.bets, function(bookmaker, bookmakerIdx) {
                      return _c("match-table-row", {
                        key: bookmakerIdx,
                        attrs: {
                          idx: bookmakerIdx,
                          item: bookmaker,
                          marketId: _vm.marketId,
                          eventId: _vm.eventId,
                          displayElement: _vm.allBookmakers
                        }
                      })
                    })
              ]
            }),
            _vm._v(" "),
            !_vm.allBookmakers
              ? _c("div", { staticClass: "flex aic jcc" }, [
                  _c(
                    "button",
                    {
                      staticClass: "btn show-all",
                      on: { click: _vm.showAllBookmakers }
                    },
                    [_c("span", { staticClass: "text" }, [_vm._v("Show More")])]
                  )
                ])
              : _vm._e()
          ],
          2
        )
      : _vm._e()
  ])
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "cell cell-payout" }, [
      _c("div", { staticClass: "name" }, [_vm._v("Payout")])
    ])
  }
]
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/pageparts/match/MatchTableLine.vue?vue&type=template&id=1a700320&scoped=true&":
/*!*********************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/pageparts/match/MatchTableLine.vue?vue&type=template&id=1a700320&scoped=true& ***!
  \*********************************************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _vm.displayBookmaker()
    ? _c(
        "div",
        {
          staticClass: "row row-thin has-subtable",
          class: [_vm.showSubtable ? "show-subtable" : ""]
        },
        [
          _c("div", { staticClass: "cell cell-name" }, [
            _c("div", { staticClass: "name-text" }, [
              _vm._v(_vm._s(_vm.marketName))
            ]),
            _vm._v(" "),
            _c("div", { staticClass: "bubble name-line-num" }, [
              _vm._v(_vm._s(_vm.line.line))
            ])
          ]),
          _vm._v(" "),
          _c("div", { staticClass: "cell cell-odds" }, [
            _c("span", [_vm._v(_vm._s(_vm.betsCount))])
          ]),
          _vm._v(" "),
          _vm._l(_vm.line.coefficients, function(coeff, coeffIndex) {
            return _c(
              "div",
              { key: coeffIndex, staticClass: "cell cell-coeff" },
              [
                _c("span", { staticClass: "bubble" }, [
                  _vm._v(_vm._s(coeff.toFixed(2)))
                ])
              ]
            )
          }),
          _vm._v(" "),
          _c("div", { staticClass: "cell cell-payout" }, [
            _c("span", { staticClass: "percent" }, [
              _vm._v(_vm._s(_vm.avgPercent(_vm.line.coefficients)) + "%")
            ])
          ]),
          _vm._v(" "),
          _c("div", { staticClass: "cell cell-arr" }, [
            _c(
              "div",
              {
                staticClass: "toggle-arr",
                on: {
                  click: function($event) {
                    _vm.showSubtable = !_vm.showSubtable
                  }
                }
              },
              [
                _c("svg", [
                  _c("use", { attrs: { "xlink:href": "#toggle-arr" } })
                ])
              ]
            )
          ]),
          _vm._v(" "),
          _c("div", { staticClass: "subtable" }, [
            _c(
              "div",
              { staticClass: "subtable-head" },
              [
                _vm._m(0),
                _vm._v(" "),
                _vm._m(1),
                _vm._v(" "),
                _vm._l(_vm.line.coefficientsGroups, function(
                  coeffName,
                  coeffNameIdx
                ) {
                  return _c(
                    "div",
                    { key: coeffNameIdx, staticClass: "cell cell-coeff" },
                    [
                      _c("div", { staticClass: "name" }, [
                        _vm._v(_vm._s(coeffName))
                      ])
                    ]
                  )
                }),
                _vm._v(" "),
                _vm._m(2),
                _vm._v(" "),
                _c("div", { staticClass: "cell cell-arr cell-empty" })
              ],
              2
            ),
            _vm._v(" "),
            _c(
              "div",
              { staticClass: "subtable-table" },
              _vm._l(_vm.line.bets, function(bookmaker, bookmakerIdx) {
                return _c("match-table-row", {
                  key: bookmakerIdx,
                  attrs: {
                    type: "sub",
                    idx: bookmakerIdx,
                    eventId: _vm.eventId,
                    marketId: _vm.marketId,
                    item: bookmaker,
                    displayElement: true
                  }
                })
              }),
              1
            ),
            _vm._v(" "),
            _c("div", { staticClass: "stats subtable-stats" }, [
              _c("div", { staticClass: "stats-line avg" }, [
                _c("div", { staticClass: "name" }, [_vm._v("Average")]),
                _vm._v(" "),
                _c(
                  "div",
                  { staticClass: "nums" },
                  [
                    _vm._l(_vm.line.coefficients, function(coeff, coeffIndex) {
                      return _c(
                        "div",
                        { key: coeffIndex, staticClass: "num" },
                        [
                          _vm._v(
                            "\n                        " +
                              _vm._s(coeff.toFixed(2)) +
                              "\n                    "
                          )
                        ]
                      )
                    }),
                    _vm._v(" "),
                    _c("div", { staticClass: "num percent" }, [
                      _vm._v(
                        _vm._s(_vm.avgPercent(_vm.line.coefficients)) + "%"
                      )
                    ])
                  ],
                  2
                )
              ]),
              _vm._v(" "),
              _c("div", { staticClass: "stats-line highest" }, [
                _c("div", { staticClass: "name" }, [_vm._v("Highest")]),
                _vm._v(" "),
                _c(
                  "div",
                  { staticClass: "nums" },
                  [
                    _vm._l(_vm.line.coefficientsGroups, function(
                      coeffName,
                      coeffIndex
                    ) {
                      return _c(
                        "div",
                        { key: coeffIndex, staticClass: "num" },
                        [
                          _vm._v(
                            "\n                        " +
                              _vm._s(_vm.highestCoeff(coeffName)) +
                              "\n                    "
                          )
                        ]
                      )
                    }),
                    _vm._v(" "),
                    _c("div", { staticClass: "num percent" }, [
                      _vm._v(
                        _vm._s(
                          _vm.highestPercent(_vm.line.coefficientsGroups)
                        ) + "%"
                      )
                    ])
                  ],
                  2
                )
              ])
            ]),
            _vm._v(" "),
            _c(
              "div",
              { staticClass: "add-event-to-tracker subtable-tracker" },
              [
                _c("div", { staticClass: "icon" }),
                _vm._v(" "),
                _c("div", { staticClass: "name" }, [
                  _vm._v("Add This Event To My Tracker")
                ]),
                _vm._v(" "),
                _c("div", { staticClass: "buttons flex aic" }, [
                  _c(
                    "button",
                    {
                      staticClass: "btn btn-small btn-white-opacity",
                      on: {
                        click: function($event) {
                          return _vm.addToTracker("1")
                        }
                      }
                    },
                    [_vm._v("Add")]
                  ),
                  _vm._v(" "),
                  _c(
                    "button",
                    {
                      staticClass: "btn btn-small btn-white-opacity",
                      on: {
                        click: function($event) {
                          return _vm.addToTracker("2")
                        }
                      }
                    },
                    [_vm._v("Add")]
                  ),
                  _vm._v(" "),
                  _c(
                    "span",
                    {
                      staticClass: "btn-close-subtable",
                      on: {
                        click: function($event) {
                          _vm.showSubtable = !_vm.showSubtable
                        }
                      }
                    },
                    [
                      _c("svg", [
                        _c("use", { attrs: { "xlink:href": "#toggle-arr" } })
                      ])
                    ]
                  )
                ])
              ]
            )
          ])
        ],
        2
      )
    : _vm._e()
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "cell cell-bookmaker" }, [
      _c("div", { staticClass: "name" }, [_vm._v("Bookmaker")])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "cell cell-handicap" }, [
      _c("div", { staticClass: "name" }, [_vm._v("Handicap")])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "cell cell-payout" }, [
      _c("div", { staticClass: "name" }, [_vm._v("Payout")])
    ])
  }
]
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/pageparts/match/MatchTableRow.vue?vue&type=template&id=2693ed0e&scoped=true&":
/*!********************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/pageparts/match/MatchTableRow.vue?vue&type=template&id=2693ed0e&scoped=true& ***!
  \********************************************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _vm.displayBookmaker(_vm.item)
    ? _c(
        "div",
        { staticClass: "row" },
        [
          _c(
            "div",
            {
              staticClass: "cell",
              class:
                "" +
                (_vm.type && _vm.type === "sub"
                  ? "cell-bookmaker"
                  : "cell-logo order__logo")
            },
            [
              _c("a", {
                staticClass: "logo",
                style: {
                  backgroundImage:
                    "url('/storage/" +
                    (_vm.logo || "/img/team_placeholder.svg") +
                    "')",
                  pointerEvents: _vm.restricted ? "none" : "all"
                },
                attrs: { target: "_blank", href: _vm.refLink, title: _vm.name },
                on: { click: _vm.restricted }
              }),
              _vm._v(" "),
              _c(
                "a",
                {
                  staticClass: "name__book",
                  style: { pointerEvents: _vm.restricted ? "none" : "all" },
                  attrs: { target: "_blank", href: _vm.refLink }
                },
                [_c("span", [_vm._v(_vm._s(_vm.name))])]
              ),
              _vm._v(" "),
              _c("a", {
                staticClass: "link",
                style: { pointerEvents: _vm.restricted ? "none" : "all" },
                attrs: { target: "_blank", href: _vm.refLink }
              })
            ]
          ),
          _vm._v(" "),
          _vm.line !== null
            ? _c("div", { staticClass: "cell cell-handicap" }, [
                _c("div", { staticClass: "bubble bookmaker-line" }, [
                  _vm._v(_vm._s(_vm.line.replace(/\([^()]*\)/g, "")))
                ])
              ])
            : _vm._e(),
          _vm._v(" "),
          _vm._l(_vm.item, function(coeff, coeffIdx) {
            return _c(
              "div",
              {
                key: coeff + coeffIdx,
                staticClass: "cell cell-coeff order__coeff"
              },
              [
                _c("match-table-row-bubble", {
                  attrs: { item: coeff },
                  nativeOn: {
                    click: function($event) {
                      return _vm.addToTracker(coeff.name)
                    }
                  }
                })
              ],
              1
            )
          }),
          _vm._v(" "),
          _c("div", { staticClass: "cell cell-payout order__payout" }, [
            _c("span", { staticClass: "percent" }, [
              _vm._v(_vm._s(_vm.calculatePercent(_vm.item)) + "%")
            ])
          ]),
          _vm._v(" "),
          _vm.type && _vm.type === "sub"
            ? _c("div", { staticClass: "cell cell-arr cell-empty" })
            : _vm._e()
        ],
        2
      )
    : _vm._e()
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/pageparts/match/MatchTableRowBubble.vue?vue&type=template&id=be73b18c&scoped=true&":
/*!**************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/pageparts/match/MatchTableRowBubble.vue?vue&type=template&id=be73b18c&scoped=true& ***!
  \**************************************************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "span",
    {
      staticClass: "bubble",
      on: {
        mouseenter: function($event) {
          return _vm.getHistory(_vm.item)
        },
        mouseleave: _vm.hideBetsHistory
      }
    },
    [
      _vm._v("\n    " + _vm._s(_vm.item.price) + "\n    "),
      _c("span", { class: ["stats-arr", "stats-arr-" + _vm.flag] }),
      _vm._v(" "),
      _vm.showBets
        ? _c("match-table-row-history", {
            attrs: {
              bets: Object.values(_vm.bets),
              current: _vm.current,
              opening: _vm.opening,
              info: _vm.info,
              "bets-loaded": _vm.betsLoaded
            }
          })
        : _vm._e()
    ],
    1
  )
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/pageparts/match/MatchTableRowHistory.vue?vue&type=template&id=612c09d6&scoped=true&":
/*!***************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/pageparts/match/MatchTableRowHistory.vue?vue&type=template&id=612c09d6&scoped=true& ***!
  \***************************************************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "div",
    {
      staticClass: "odds-queue active",
      on: { mouseleave: _vm.hideBetsHistory }
    },
    [
      _vm.betsLoaded && _vm.opening
        ? _c("div", { staticClass: "queue-head" }, [
            _c("div", { staticClass: "title" }, [_vm._v("Opening odds at:")]),
            _vm._v(" "),
            _c("div", { staticClass: "when" }, [
              _c("div", { staticClass: "datetime" }, [
                _vm._v(_vm._s(_vm.formatDatestring(_vm.opening.last_update)))
              ]),
              _vm._v(" "),
              _c("div", { staticClass: "num" }, [
                _vm._v(_vm._s(_vm.opening.start_price))
              ])
            ])
          ])
        : _vm._e(),
      _vm._v(" "),
      _vm.betsLoaded && _vm.bets.length
        ? _c(
            "div",
            { staticClass: "queue-items" },
            _vm._l(_vm.bets.reverse(), function(bet, idx) {
              return _vm.diffNum(bet.price, idx) !== "NULL"
                ? _c("div", { key: idx, staticClass: "queue-item" }, [
                    _c("div", { staticClass: "datetime" }, [
                      _vm._v(_vm._s(_vm.formatDatestring(bet.last_update)))
                    ]),
                    _vm._v(" "),
                    _c("div", { staticClass: "num" }, [
                      _vm._v(_vm._s(bet.price))
                    ]),
                    _vm._v(" "),
                    _c(
                      "div",
                      { class: ["diff", "diff-" + _vm.diff(bet.price, idx)] },
                      [_vm._v(_vm._s(_vm.diffNum(bet.price, idx)))]
                    )
                  ])
                : _vm._e()
            }),
            0
          )
        : _vm._e()
    ]
  )
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/pageparts/match/MatchTabs.vue?vue&type=template&id=7d79dd40&":
/*!****************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/pageparts/match/MatchTabs.vue?vue&type=template&id=7d79dd40& ***!
  \****************************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _vm._m(0)
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "tabs" }, [
      _c("div", { staticClass: "tabs-item active" }, [
        _c("span", { staticClass: "tabname" }, [_vm._v("Pre-match Odds")])
      ]),
      _vm._v(" "),
      _c("div", { staticClass: "tabs-item" }, [
        _c("span", { staticClass: "tabname" }, [
          _c("span", { staticClass: "icon icon-play" }),
          _vm._v("In-Play Odds "),
          _c("span", { staticClass: "coming__soon" }, [_vm._v("Coming soon")])
        ])
      ]),
      _vm._v(" "),
      _c("div", { staticClass: "tabs-item" }, [
        _c("span", { staticClass: "tabname" }, [
          _vm._v("Game Center"),
          _c("span", { staticClass: "coming__soon" }, [_vm._v("Coming soon")])
        ])
      ])
    ])
  }
]
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/pageparts/match/MatchTeams.vue?vue&type=template&id=69a7bf78&":
/*!*****************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/pageparts/match/MatchTeams.vue?vue&type=template&id=69a7bf78& ***!
  \*****************************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", { staticClass: "teams" }, [
    _c("div", { staticClass: "team-one team-item" }, [
      _c("div", {
        staticClass: "team-logo",
        style: {
          backgroundImage: _vm.teamLogo(_vm.teams[0])
        }
      }),
      _vm._v(" "),
      _c("div", { staticClass: "team-name" }, [
        _vm._v(_vm._s(_vm.teams[0].name))
      ])
    ]),
    _vm._v(" "),
    _c("div", { staticClass: "score" }, [
      _c("div", { staticClass: "score-box" }, [
        !_vm.score
          ? _c("div", { staticClass: "score-main" }, [
              _vm._v("\n                    - : -\n                ")
            ])
          : _vm.isPlay == 3 || _vm.isPlay == true
          ? _c("div", { staticClass: "score-main" }, [
              _vm._v(
                "\n                    " +
                  _vm._s(_vm.score.team1) +
                  " : " +
                  _vm._s(_vm.score.team2) +
                  "\n                "
              )
            ])
          : _c("div", { staticClass: "score-main" }, [
              _c("span", [_vm._v(_vm._s(_vm.score.team1))]),
              _vm._v(" : "),
              _c("span", [_vm._v(_vm._s(_vm.score.team2))])
            ])
      ]),
      _vm._v(" "),
      _c("div", { staticClass: "score-match-time flex aic jcc" }, [
        _c("div", { staticClass: "date" }, [_vm._v(_vm._s(_vm.startDate))]),
        _vm._v(" "),
        _c("div", { staticClass: "time" }, [_vm._v(_vm._s(_vm.startTime))]),
        _vm._v(" "),
        _vm.isPlay == 3
          ? _c("div", { staticClass: "finished" }, [
              _c("div", { staticClass: "finished-icon" })
            ])
          : _vm.isPlay == 2
          ? _c("div", { staticClass: "finished" }, [
              _c("div", { staticClass: "finished-playing" })
            ])
          : _vm._e()
      ])
    ]),
    _vm._v(" "),
    _c("div", { staticClass: "team-two team-item" }, [
      _c("div", {
        staticClass: "team-logo",
        style: {
          backgroundImage: _vm.teamLogo(_vm.teams[1])
        }
      }),
      _vm._v(" "),
      _c("div", { staticClass: "team-name" }, [
        _vm._v(_vm._s(_vm.teams[1].name))
      ])
    ])
  ])
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/pageparts/match/MatchTimes.vue?vue&type=template&id=50565458&":
/*!*****************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/pageparts/match/MatchTimes.vue?vue&type=template&id=50565458& ***!
  \*****************************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "div",
    { staticClass: "times" },
    [
      _c(
        "router-link",
        {
          class: ["times-item", { active: _vm.activeType == "1X2" }],
          attrs: {
            to: "/football/match/" + _vm.eventId + "/1x2#time-full",
            "exact-active-class": "active"
          }
        },
        [
          _c("span", { staticClass: "icon" }, [
            _c("svg", [_c("use", { attrs: { "xlink:href": "#time-full" } })])
          ]),
          _vm._v(" "),
          _c("span", { staticClass: "text" }, [_vm._v("Full Time")])
        ]
      ),
      _vm._v(" "),
      _c(
        "router-link",
        {
          class: ["times-item", { active: _vm.activeType == "1X2" }],
          attrs: {
            to: "/football/match/" + _vm.eventId + "/1x2#clock-first-half",
            "exact-active-class": "active"
          }
        },
        [
          _c("span", { staticClass: "icon" }, [
            _c("svg", [
              _c("use", { attrs: { "xlink:href": "#clock-first-half" } })
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticClass: "text" }, [_vm._v("1st Half")])
        ]
      ),
      _vm._v(" "),
      _c(
        "router-link",
        {
          class: ["times-item", { active: _vm.activeType == "1X2" }],
          attrs: {
            to: "/football/match/" + _vm.eventId + "/1x2#clock-second-half",
            "exact-active-class": "active"
          }
        },
        [
          _c("span", { staticClass: "icon" }, [
            _c("svg", [
              _c("use", { attrs: { "xlink:href": "#clock-second-half" } })
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticClass: "text" }, [_vm._v("2nd Half")])
        ]
      )
    ],
    1
  )
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/Match.vue?vue&type=template&id=c3eed494&":
/*!***************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/views/Match.vue?vue&type=template&id=c3eed494& ***!
  \***************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "div",
    { staticClass: "content-wrapper content-next-matches" },
    [
      _vm.loading ? _c("loader") : _vm._e(),
      _vm._v(" "),
      _vm.match && !_vm.loading
        ? _c(
            "box",
            { staticClass: "odds-table" },
            [
              _vm.match.event
                ? _c("match-teams", {
                    attrs: {
                      teams: _vm.match.event.fixture.participants,
                      scoreboard: _vm.match.event.scoreboard,
                      startdate: _vm.match.event.fixture.start_date,
                      "event-id": _vm.id
                    }
                  })
                : _vm._e(),
              _vm._v(" "),
              _c("match-tabs"),
              _vm._v(" "),
              _c("match-nav", {
                attrs: {
                  "nav-items": _vm.markets,
                  "event-id": _vm.id,
                  "route-tab": _vm.tab,
                  active: _vm.selectedTab.marketId
                }
              }),
              _vm._v(" "),
              _vm.selectedTab.marketId === 1
                ? _c("match-times", {
                    attrs: {
                      "active-type": _vm.selectedTab.marketId,
                      "event-id": _vm.id,
                      "match-id": _vm.id
                    }
                  })
                : _vm._e(),
              _vm._v(" "),
              _vm.prematchBets && _vm.prematchBets[_vm.selectedTab.marketId]
                ? _c("match-table", {
                    attrs: {
                      bets: _vm.prematchBets[_vm.selectedTab.marketId],
                      "active-bet-type": _vm.selectedTab.apiName.toLowerCase(),
                      "market-id": _vm.selectedTab.marketId,
                      "event-id": _vm.id
                    }
                  })
                : _vm._e(),
              _vm._v(" "),
              _vm.selectedTab.marketId !== 3 &&
              _vm.selectedTab.marketId !== 2 &&
              _vm.prematchBets &&
              _vm.prematchBets[_vm.selectedTab.marketId]
                ? _c("match-stats", {
                    attrs: {
                      bets: _vm.prematchBets[_vm.selectedTab.marketId],
                      "active-bet-type": _vm.selectedTab.apiName.toLowerCase(),
                      "market-id": _vm.selectedTab.marketId,
                      "event-id": _vm.id
                    }
                  })
                : _vm._e()
            ],
            1
          )
        : _vm._e(),
      _vm._v(" "),
      _vm.selectedTab.marketId !== 3 &&
      _vm.selectedTab.marketId !== 2 &&
      !_vm.loading &&
      _vm.match &&
      _vm.match.event &&
      _vm.id &&
      _vm.prematchBets &&
      _vm.prematchBets[_vm.selectedTab.marketId]
        ? _c("match-add-to-tracker", {
            attrs: {
              "event-id": _vm.id,
              "market-id": _vm.selectedTab.marketId,
              bets: _vm.prematchBets[_vm.selectedTab.marketId],
              "active-bet-type": _vm.selectedTab.apiName.toLowerCase()
            }
          })
        : _vm._e()
    ],
    1
  )
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./resources/js/components/HorizontalLoader.vue":
/*!******************************************************!*\
  !*** ./resources/js/components/HorizontalLoader.vue ***!
  \******************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _HorizontalLoader_vue_vue_type_template_id_0011e77c_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./HorizontalLoader.vue?vue&type=template&id=0011e77c&scoped=true& */ "./resources/js/components/HorizontalLoader.vue?vue&type=template&id=0011e77c&scoped=true&");
/* harmony import */ var _HorizontalLoader_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./HorizontalLoader.vue?vue&type=script&lang=js& */ "./resources/js/components/HorizontalLoader.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _HorizontalLoader_vue_vue_type_style_index_0_id_0011e77c_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./HorizontalLoader.vue?vue&type=style&index=0&id=0011e77c&scoped=true&lang=css& */ "./resources/js/components/HorizontalLoader.vue?vue&type=style&index=0&id=0011e77c&scoped=true&lang=css&");
/* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");






/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__["default"])(
  _HorizontalLoader_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _HorizontalLoader_vue_vue_type_template_id_0011e77c_scoped_true___WEBPACK_IMPORTED_MODULE_0__["render"],
  _HorizontalLoader_vue_vue_type_template_id_0011e77c_scoped_true___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  "0011e77c",
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/components/HorizontalLoader.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/components/HorizontalLoader.vue?vue&type=script&lang=js&":
/*!*******************************************************************************!*\
  !*** ./resources/js/components/HorizontalLoader.vue?vue&type=script&lang=js& ***!
  \*******************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_HorizontalLoader_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../node_modules/babel-loader/lib??ref--4-0!../../../node_modules/vue-loader/lib??vue-loader-options!./HorizontalLoader.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/HorizontalLoader.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_HorizontalLoader_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/components/HorizontalLoader.vue?vue&type=style&index=0&id=0011e77c&scoped=true&lang=css&":
/*!***************************************************************************************************************!*\
  !*** ./resources/js/components/HorizontalLoader.vue?vue&type=style&index=0&id=0011e77c&scoped=true&lang=css& ***!
  \***************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_HorizontalLoader_vue_vue_type_style_index_0_id_0011e77c_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../node_modules/style-loader!../../../node_modules/css-loader??ref--6-1!../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../node_modules/postcss-loader/src??ref--6-2!../../../node_modules/vue-loader/lib??vue-loader-options!./HorizontalLoader.vue?vue&type=style&index=0&id=0011e77c&scoped=true&lang=css& */ "./node_modules/style-loader/index.js!./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/HorizontalLoader.vue?vue&type=style&index=0&id=0011e77c&scoped=true&lang=css&");
/* harmony import */ var _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_HorizontalLoader_vue_vue_type_style_index_0_id_0011e77c_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_HorizontalLoader_vue_vue_type_style_index_0_id_0011e77c_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0__);
/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_HorizontalLoader_vue_vue_type_style_index_0_id_0011e77c_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0__) if(["default"].indexOf(__WEBPACK_IMPORT_KEY__) < 0) (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_HorizontalLoader_vue_vue_type_style_index_0_id_0011e77c_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0__[key]; }) }(__WEBPACK_IMPORT_KEY__));


/***/ }),

/***/ "./resources/js/components/HorizontalLoader.vue?vue&type=template&id=0011e77c&scoped=true&":
/*!*************************************************************************************************!*\
  !*** ./resources/js/components/HorizontalLoader.vue?vue&type=template&id=0011e77c&scoped=true& ***!
  \*************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_HorizontalLoader_vue_vue_type_template_id_0011e77c_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../node_modules/vue-loader/lib??vue-loader-options!./HorizontalLoader.vue?vue&type=template&id=0011e77c&scoped=true& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/HorizontalLoader.vue?vue&type=template&id=0011e77c&scoped=true&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_HorizontalLoader_vue_vue_type_template_id_0011e77c_scoped_true___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_HorizontalLoader_vue_vue_type_template_id_0011e77c_scoped_true___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./resources/js/components/pageparts/box/Box.vue":
/*!*******************************************************!*\
  !*** ./resources/js/components/pageparts/box/Box.vue ***!
  \*******************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _Box_vue_vue_type_template_id_c4ce6c64___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./Box.vue?vue&type=template&id=c4ce6c64& */ "./resources/js/components/pageparts/box/Box.vue?vue&type=template&id=c4ce6c64&");
/* harmony import */ var _Box_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./Box.vue?vue&type=script&lang=js& */ "./resources/js/components/pageparts/box/Box.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _Box_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _Box_vue_vue_type_template_id_c4ce6c64___WEBPACK_IMPORTED_MODULE_0__["render"],
  _Box_vue_vue_type_template_id_c4ce6c64___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/components/pageparts/box/Box.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/components/pageparts/box/Box.vue?vue&type=script&lang=js&":
/*!********************************************************************************!*\
  !*** ./resources/js/components/pageparts/box/Box.vue?vue&type=script&lang=js& ***!
  \********************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Box_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/babel-loader/lib??ref--4-0!../../../../../node_modules/vue-loader/lib??vue-loader-options!./Box.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/pageparts/box/Box.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Box_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/components/pageparts/box/Box.vue?vue&type=template&id=c4ce6c64&":
/*!**************************************************************************************!*\
  !*** ./resources/js/components/pageparts/box/Box.vue?vue&type=template&id=c4ce6c64& ***!
  \**************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Box_vue_vue_type_template_id_c4ce6c64___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../../node_modules/vue-loader/lib??vue-loader-options!./Box.vue?vue&type=template&id=c4ce6c64& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/pageparts/box/Box.vue?vue&type=template&id=c4ce6c64&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Box_vue_vue_type_template_id_c4ce6c64___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Box_vue_vue_type_template_id_c4ce6c64___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./resources/js/components/pageparts/match/MatchAddToTracker.vue":
/*!***********************************************************************!*\
  !*** ./resources/js/components/pageparts/match/MatchAddToTracker.vue ***!
  \***********************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _MatchAddToTracker_vue_vue_type_template_id_1fbb317e___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./MatchAddToTracker.vue?vue&type=template&id=1fbb317e& */ "./resources/js/components/pageparts/match/MatchAddToTracker.vue?vue&type=template&id=1fbb317e&");
/* harmony import */ var _MatchAddToTracker_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./MatchAddToTracker.vue?vue&type=script&lang=js& */ "./resources/js/components/pageparts/match/MatchAddToTracker.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _MatchAddToTracker_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _MatchAddToTracker_vue_vue_type_template_id_1fbb317e___WEBPACK_IMPORTED_MODULE_0__["render"],
  _MatchAddToTracker_vue_vue_type_template_id_1fbb317e___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/components/pageparts/match/MatchAddToTracker.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/components/pageparts/match/MatchAddToTracker.vue?vue&type=script&lang=js&":
/*!************************************************************************************************!*\
  !*** ./resources/js/components/pageparts/match/MatchAddToTracker.vue?vue&type=script&lang=js& ***!
  \************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_MatchAddToTracker_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/babel-loader/lib??ref--4-0!../../../../../node_modules/vue-loader/lib??vue-loader-options!./MatchAddToTracker.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/pageparts/match/MatchAddToTracker.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_MatchAddToTracker_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/components/pageparts/match/MatchAddToTracker.vue?vue&type=template&id=1fbb317e&":
/*!******************************************************************************************************!*\
  !*** ./resources/js/components/pageparts/match/MatchAddToTracker.vue?vue&type=template&id=1fbb317e& ***!
  \******************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_MatchAddToTracker_vue_vue_type_template_id_1fbb317e___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../../node_modules/vue-loader/lib??vue-loader-options!./MatchAddToTracker.vue?vue&type=template&id=1fbb317e& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/pageparts/match/MatchAddToTracker.vue?vue&type=template&id=1fbb317e&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_MatchAddToTracker_vue_vue_type_template_id_1fbb317e___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_MatchAddToTracker_vue_vue_type_template_id_1fbb317e___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./resources/js/components/pageparts/match/MatchNav.vue":
/*!**************************************************************!*\
  !*** ./resources/js/components/pageparts/match/MatchNav.vue ***!
  \**************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _MatchNav_vue_vue_type_template_id_7f7f86b1___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./MatchNav.vue?vue&type=template&id=7f7f86b1& */ "./resources/js/components/pageparts/match/MatchNav.vue?vue&type=template&id=7f7f86b1&");
/* harmony import */ var _MatchNav_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./MatchNav.vue?vue&type=script&lang=js& */ "./resources/js/components/pageparts/match/MatchNav.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _MatchNav_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _MatchNav_vue_vue_type_template_id_7f7f86b1___WEBPACK_IMPORTED_MODULE_0__["render"],
  _MatchNav_vue_vue_type_template_id_7f7f86b1___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/components/pageparts/match/MatchNav.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/components/pageparts/match/MatchNav.vue?vue&type=script&lang=js&":
/*!***************************************************************************************!*\
  !*** ./resources/js/components/pageparts/match/MatchNav.vue?vue&type=script&lang=js& ***!
  \***************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_MatchNav_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/babel-loader/lib??ref--4-0!../../../../../node_modules/vue-loader/lib??vue-loader-options!./MatchNav.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/pageparts/match/MatchNav.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_MatchNav_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/components/pageparts/match/MatchNav.vue?vue&type=template&id=7f7f86b1&":
/*!*********************************************************************************************!*\
  !*** ./resources/js/components/pageparts/match/MatchNav.vue?vue&type=template&id=7f7f86b1& ***!
  \*********************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_MatchNav_vue_vue_type_template_id_7f7f86b1___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../../node_modules/vue-loader/lib??vue-loader-options!./MatchNav.vue?vue&type=template&id=7f7f86b1& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/pageparts/match/MatchNav.vue?vue&type=template&id=7f7f86b1&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_MatchNav_vue_vue_type_template_id_7f7f86b1___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_MatchNav_vue_vue_type_template_id_7f7f86b1___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./resources/js/components/pageparts/match/MatchStats.vue":
/*!****************************************************************!*\
  !*** ./resources/js/components/pageparts/match/MatchStats.vue ***!
  \****************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _MatchStats_vue_vue_type_template_id_590b3ead___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./MatchStats.vue?vue&type=template&id=590b3ead& */ "./resources/js/components/pageparts/match/MatchStats.vue?vue&type=template&id=590b3ead&");
/* harmony import */ var _MatchStats_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./MatchStats.vue?vue&type=script&lang=js& */ "./resources/js/components/pageparts/match/MatchStats.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _MatchStats_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _MatchStats_vue_vue_type_template_id_590b3ead___WEBPACK_IMPORTED_MODULE_0__["render"],
  _MatchStats_vue_vue_type_template_id_590b3ead___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/components/pageparts/match/MatchStats.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/components/pageparts/match/MatchStats.vue?vue&type=script&lang=js&":
/*!*****************************************************************************************!*\
  !*** ./resources/js/components/pageparts/match/MatchStats.vue?vue&type=script&lang=js& ***!
  \*****************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_MatchStats_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/babel-loader/lib??ref--4-0!../../../../../node_modules/vue-loader/lib??vue-loader-options!./MatchStats.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/pageparts/match/MatchStats.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_MatchStats_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/components/pageparts/match/MatchStats.vue?vue&type=template&id=590b3ead&":
/*!***********************************************************************************************!*\
  !*** ./resources/js/components/pageparts/match/MatchStats.vue?vue&type=template&id=590b3ead& ***!
  \***********************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_MatchStats_vue_vue_type_template_id_590b3ead___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../../node_modules/vue-loader/lib??vue-loader-options!./MatchStats.vue?vue&type=template&id=590b3ead& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/pageparts/match/MatchStats.vue?vue&type=template&id=590b3ead&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_MatchStats_vue_vue_type_template_id_590b3ead___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_MatchStats_vue_vue_type_template_id_590b3ead___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./resources/js/components/pageparts/match/MatchTable.vue":
/*!****************************************************************!*\
  !*** ./resources/js/components/pageparts/match/MatchTable.vue ***!
  \****************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _MatchTable_vue_vue_type_template_id_5e13865c___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./MatchTable.vue?vue&type=template&id=5e13865c& */ "./resources/js/components/pageparts/match/MatchTable.vue?vue&type=template&id=5e13865c&");
/* harmony import */ var _MatchTable_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./MatchTable.vue?vue&type=script&lang=js& */ "./resources/js/components/pageparts/match/MatchTable.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _MatchTable_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _MatchTable_vue_vue_type_template_id_5e13865c___WEBPACK_IMPORTED_MODULE_0__["render"],
  _MatchTable_vue_vue_type_template_id_5e13865c___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/components/pageparts/match/MatchTable.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/components/pageparts/match/MatchTable.vue?vue&type=script&lang=js&":
/*!*****************************************************************************************!*\
  !*** ./resources/js/components/pageparts/match/MatchTable.vue?vue&type=script&lang=js& ***!
  \*****************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_MatchTable_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/babel-loader/lib??ref--4-0!../../../../../node_modules/vue-loader/lib??vue-loader-options!./MatchTable.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/pageparts/match/MatchTable.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_MatchTable_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/components/pageparts/match/MatchTable.vue?vue&type=template&id=5e13865c&":
/*!***********************************************************************************************!*\
  !*** ./resources/js/components/pageparts/match/MatchTable.vue?vue&type=template&id=5e13865c& ***!
  \***********************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_MatchTable_vue_vue_type_template_id_5e13865c___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../../node_modules/vue-loader/lib??vue-loader-options!./MatchTable.vue?vue&type=template&id=5e13865c& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/pageparts/match/MatchTable.vue?vue&type=template&id=5e13865c&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_MatchTable_vue_vue_type_template_id_5e13865c___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_MatchTable_vue_vue_type_template_id_5e13865c___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./resources/js/components/pageparts/match/MatchTableLine.vue":
/*!********************************************************************!*\
  !*** ./resources/js/components/pageparts/match/MatchTableLine.vue ***!
  \********************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _MatchTableLine_vue_vue_type_template_id_1a700320_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./MatchTableLine.vue?vue&type=template&id=1a700320&scoped=true& */ "./resources/js/components/pageparts/match/MatchTableLine.vue?vue&type=template&id=1a700320&scoped=true&");
/* harmony import */ var _MatchTableLine_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./MatchTableLine.vue?vue&type=script&lang=js& */ "./resources/js/components/pageparts/match/MatchTableLine.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _MatchTableLine_vue_vue_type_style_index_0_id_1a700320_scoped_true_lang_scss___WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./MatchTableLine.vue?vue&type=style&index=0&id=1a700320&scoped=true&lang=scss& */ "./resources/js/components/pageparts/match/MatchTableLine.vue?vue&type=style&index=0&id=1a700320&scoped=true&lang=scss&");
/* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");






/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__["default"])(
  _MatchTableLine_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _MatchTableLine_vue_vue_type_template_id_1a700320_scoped_true___WEBPACK_IMPORTED_MODULE_0__["render"],
  _MatchTableLine_vue_vue_type_template_id_1a700320_scoped_true___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  "1a700320",
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/components/pageparts/match/MatchTableLine.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/components/pageparts/match/MatchTableLine.vue?vue&type=script&lang=js&":
/*!*********************************************************************************************!*\
  !*** ./resources/js/components/pageparts/match/MatchTableLine.vue?vue&type=script&lang=js& ***!
  \*********************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_MatchTableLine_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/babel-loader/lib??ref--4-0!../../../../../node_modules/vue-loader/lib??vue-loader-options!./MatchTableLine.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/pageparts/match/MatchTableLine.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_MatchTableLine_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/components/pageparts/match/MatchTableLine.vue?vue&type=style&index=0&id=1a700320&scoped=true&lang=scss&":
/*!******************************************************************************************************************************!*\
  !*** ./resources/js/components/pageparts/match/MatchTableLine.vue?vue&type=style&index=0&id=1a700320&scoped=true&lang=scss& ***!
  \******************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_2_node_modules_sass_loader_dist_cjs_js_ref_7_3_node_modules_vue_loader_lib_index_js_vue_loader_options_MatchTableLine_vue_vue_type_style_index_0_id_1a700320_scoped_true_lang_scss___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/style-loader!../../../../../node_modules/css-loader!../../../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../../../node_modules/postcss-loader/src??ref--7-2!../../../../../node_modules/sass-loader/dist/cjs.js??ref--7-3!../../../../../node_modules/vue-loader/lib??vue-loader-options!./MatchTableLine.vue?vue&type=style&index=0&id=1a700320&scoped=true&lang=scss& */ "./node_modules/style-loader/index.js!./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/sass-loader/dist/cjs.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/pageparts/match/MatchTableLine.vue?vue&type=style&index=0&id=1a700320&scoped=true&lang=scss&");
/* harmony import */ var _node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_2_node_modules_sass_loader_dist_cjs_js_ref_7_3_node_modules_vue_loader_lib_index_js_vue_loader_options_MatchTableLine_vue_vue_type_style_index_0_id_1a700320_scoped_true_lang_scss___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_2_node_modules_sass_loader_dist_cjs_js_ref_7_3_node_modules_vue_loader_lib_index_js_vue_loader_options_MatchTableLine_vue_vue_type_style_index_0_id_1a700320_scoped_true_lang_scss___WEBPACK_IMPORTED_MODULE_0__);
/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_2_node_modules_sass_loader_dist_cjs_js_ref_7_3_node_modules_vue_loader_lib_index_js_vue_loader_options_MatchTableLine_vue_vue_type_style_index_0_id_1a700320_scoped_true_lang_scss___WEBPACK_IMPORTED_MODULE_0__) if(["default"].indexOf(__WEBPACK_IMPORT_KEY__) < 0) (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return _node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_2_node_modules_sass_loader_dist_cjs_js_ref_7_3_node_modules_vue_loader_lib_index_js_vue_loader_options_MatchTableLine_vue_vue_type_style_index_0_id_1a700320_scoped_true_lang_scss___WEBPACK_IMPORTED_MODULE_0__[key]; }) }(__WEBPACK_IMPORT_KEY__));


/***/ }),

/***/ "./resources/js/components/pageparts/match/MatchTableLine.vue?vue&type=template&id=1a700320&scoped=true&":
/*!***************************************************************************************************************!*\
  !*** ./resources/js/components/pageparts/match/MatchTableLine.vue?vue&type=template&id=1a700320&scoped=true& ***!
  \***************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_MatchTableLine_vue_vue_type_template_id_1a700320_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../../node_modules/vue-loader/lib??vue-loader-options!./MatchTableLine.vue?vue&type=template&id=1a700320&scoped=true& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/pageparts/match/MatchTableLine.vue?vue&type=template&id=1a700320&scoped=true&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_MatchTableLine_vue_vue_type_template_id_1a700320_scoped_true___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_MatchTableLine_vue_vue_type_template_id_1a700320_scoped_true___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./resources/js/components/pageparts/match/MatchTableRow.vue":
/*!*******************************************************************!*\
  !*** ./resources/js/components/pageparts/match/MatchTableRow.vue ***!
  \*******************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _MatchTableRow_vue_vue_type_template_id_2693ed0e_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./MatchTableRow.vue?vue&type=template&id=2693ed0e&scoped=true& */ "./resources/js/components/pageparts/match/MatchTableRow.vue?vue&type=template&id=2693ed0e&scoped=true&");
/* harmony import */ var _MatchTableRow_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./MatchTableRow.vue?vue&type=script&lang=js& */ "./resources/js/components/pageparts/match/MatchTableRow.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _MatchTableRow_vue_vue_type_style_index_0_id_2693ed0e_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./MatchTableRow.vue?vue&type=style&index=0&id=2693ed0e&scoped=true&lang=css& */ "./resources/js/components/pageparts/match/MatchTableRow.vue?vue&type=style&index=0&id=2693ed0e&scoped=true&lang=css&");
/* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");






/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__["default"])(
  _MatchTableRow_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _MatchTableRow_vue_vue_type_template_id_2693ed0e_scoped_true___WEBPACK_IMPORTED_MODULE_0__["render"],
  _MatchTableRow_vue_vue_type_template_id_2693ed0e_scoped_true___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  "2693ed0e",
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/components/pageparts/match/MatchTableRow.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/components/pageparts/match/MatchTableRow.vue?vue&type=script&lang=js&":
/*!********************************************************************************************!*\
  !*** ./resources/js/components/pageparts/match/MatchTableRow.vue?vue&type=script&lang=js& ***!
  \********************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_MatchTableRow_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/babel-loader/lib??ref--4-0!../../../../../node_modules/vue-loader/lib??vue-loader-options!./MatchTableRow.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/pageparts/match/MatchTableRow.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_MatchTableRow_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/components/pageparts/match/MatchTableRow.vue?vue&type=style&index=0&id=2693ed0e&scoped=true&lang=css&":
/*!****************************************************************************************************************************!*\
  !*** ./resources/js/components/pageparts/match/MatchTableRow.vue?vue&type=style&index=0&id=2693ed0e&scoped=true&lang=css& ***!
  \****************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_MatchTableRow_vue_vue_type_style_index_0_id_2693ed0e_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/style-loader!../../../../../node_modules/css-loader??ref--6-1!../../../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../../../node_modules/postcss-loader/src??ref--6-2!../../../../../node_modules/vue-loader/lib??vue-loader-options!./MatchTableRow.vue?vue&type=style&index=0&id=2693ed0e&scoped=true&lang=css& */ "./node_modules/style-loader/index.js!./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/pageparts/match/MatchTableRow.vue?vue&type=style&index=0&id=2693ed0e&scoped=true&lang=css&");
/* harmony import */ var _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_MatchTableRow_vue_vue_type_style_index_0_id_2693ed0e_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_MatchTableRow_vue_vue_type_style_index_0_id_2693ed0e_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0__);
/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_MatchTableRow_vue_vue_type_style_index_0_id_2693ed0e_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0__) if(["default"].indexOf(__WEBPACK_IMPORT_KEY__) < 0) (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_MatchTableRow_vue_vue_type_style_index_0_id_2693ed0e_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0__[key]; }) }(__WEBPACK_IMPORT_KEY__));


/***/ }),

/***/ "./resources/js/components/pageparts/match/MatchTableRow.vue?vue&type=template&id=2693ed0e&scoped=true&":
/*!**************************************************************************************************************!*\
  !*** ./resources/js/components/pageparts/match/MatchTableRow.vue?vue&type=template&id=2693ed0e&scoped=true& ***!
  \**************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_MatchTableRow_vue_vue_type_template_id_2693ed0e_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../../node_modules/vue-loader/lib??vue-loader-options!./MatchTableRow.vue?vue&type=template&id=2693ed0e&scoped=true& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/pageparts/match/MatchTableRow.vue?vue&type=template&id=2693ed0e&scoped=true&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_MatchTableRow_vue_vue_type_template_id_2693ed0e_scoped_true___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_MatchTableRow_vue_vue_type_template_id_2693ed0e_scoped_true___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./resources/js/components/pageparts/match/MatchTableRowBubble.vue":
/*!*************************************************************************!*\
  !*** ./resources/js/components/pageparts/match/MatchTableRowBubble.vue ***!
  \*************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _MatchTableRowBubble_vue_vue_type_template_id_be73b18c_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./MatchTableRowBubble.vue?vue&type=template&id=be73b18c&scoped=true& */ "./resources/js/components/pageparts/match/MatchTableRowBubble.vue?vue&type=template&id=be73b18c&scoped=true&");
/* harmony import */ var _MatchTableRowBubble_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./MatchTableRowBubble.vue?vue&type=script&lang=js& */ "./resources/js/components/pageparts/match/MatchTableRowBubble.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _MatchTableRowBubble_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _MatchTableRowBubble_vue_vue_type_template_id_be73b18c_scoped_true___WEBPACK_IMPORTED_MODULE_0__["render"],
  _MatchTableRowBubble_vue_vue_type_template_id_be73b18c_scoped_true___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  "be73b18c",
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/components/pageparts/match/MatchTableRowBubble.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/components/pageparts/match/MatchTableRowBubble.vue?vue&type=script&lang=js&":
/*!**************************************************************************************************!*\
  !*** ./resources/js/components/pageparts/match/MatchTableRowBubble.vue?vue&type=script&lang=js& ***!
  \**************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_MatchTableRowBubble_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/babel-loader/lib??ref--4-0!../../../../../node_modules/vue-loader/lib??vue-loader-options!./MatchTableRowBubble.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/pageparts/match/MatchTableRowBubble.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_MatchTableRowBubble_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/components/pageparts/match/MatchTableRowBubble.vue?vue&type=template&id=be73b18c&scoped=true&":
/*!********************************************************************************************************************!*\
  !*** ./resources/js/components/pageparts/match/MatchTableRowBubble.vue?vue&type=template&id=be73b18c&scoped=true& ***!
  \********************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_MatchTableRowBubble_vue_vue_type_template_id_be73b18c_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../../node_modules/vue-loader/lib??vue-loader-options!./MatchTableRowBubble.vue?vue&type=template&id=be73b18c&scoped=true& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/pageparts/match/MatchTableRowBubble.vue?vue&type=template&id=be73b18c&scoped=true&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_MatchTableRowBubble_vue_vue_type_template_id_be73b18c_scoped_true___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_MatchTableRowBubble_vue_vue_type_template_id_be73b18c_scoped_true___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./resources/js/components/pageparts/match/MatchTableRowHistory.vue":
/*!**************************************************************************!*\
  !*** ./resources/js/components/pageparts/match/MatchTableRowHistory.vue ***!
  \**************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _MatchTableRowHistory_vue_vue_type_template_id_612c09d6_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./MatchTableRowHistory.vue?vue&type=template&id=612c09d6&scoped=true& */ "./resources/js/components/pageparts/match/MatchTableRowHistory.vue?vue&type=template&id=612c09d6&scoped=true&");
/* harmony import */ var _MatchTableRowHistory_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./MatchTableRowHistory.vue?vue&type=script&lang=js& */ "./resources/js/components/pageparts/match/MatchTableRowHistory.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _MatchTableRowHistory_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _MatchTableRowHistory_vue_vue_type_template_id_612c09d6_scoped_true___WEBPACK_IMPORTED_MODULE_0__["render"],
  _MatchTableRowHistory_vue_vue_type_template_id_612c09d6_scoped_true___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  "612c09d6",
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/components/pageparts/match/MatchTableRowHistory.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/components/pageparts/match/MatchTableRowHistory.vue?vue&type=script&lang=js&":
/*!***************************************************************************************************!*\
  !*** ./resources/js/components/pageparts/match/MatchTableRowHistory.vue?vue&type=script&lang=js& ***!
  \***************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_MatchTableRowHistory_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/babel-loader/lib??ref--4-0!../../../../../node_modules/vue-loader/lib??vue-loader-options!./MatchTableRowHistory.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/pageparts/match/MatchTableRowHistory.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_MatchTableRowHistory_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/components/pageparts/match/MatchTableRowHistory.vue?vue&type=template&id=612c09d6&scoped=true&":
/*!*********************************************************************************************************************!*\
  !*** ./resources/js/components/pageparts/match/MatchTableRowHistory.vue?vue&type=template&id=612c09d6&scoped=true& ***!
  \*********************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_MatchTableRowHistory_vue_vue_type_template_id_612c09d6_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../../node_modules/vue-loader/lib??vue-loader-options!./MatchTableRowHistory.vue?vue&type=template&id=612c09d6&scoped=true& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/pageparts/match/MatchTableRowHistory.vue?vue&type=template&id=612c09d6&scoped=true&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_MatchTableRowHistory_vue_vue_type_template_id_612c09d6_scoped_true___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_MatchTableRowHistory_vue_vue_type_template_id_612c09d6_scoped_true___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./resources/js/components/pageparts/match/MatchTabs.vue":
/*!***************************************************************!*\
  !*** ./resources/js/components/pageparts/match/MatchTabs.vue ***!
  \***************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _MatchTabs_vue_vue_type_template_id_7d79dd40___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./MatchTabs.vue?vue&type=template&id=7d79dd40& */ "./resources/js/components/pageparts/match/MatchTabs.vue?vue&type=template&id=7d79dd40&");
/* harmony import */ var _MatchTabs_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./MatchTabs.vue?vue&type=script&lang=js& */ "./resources/js/components/pageparts/match/MatchTabs.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _MatchTabs_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _MatchTabs_vue_vue_type_template_id_7d79dd40___WEBPACK_IMPORTED_MODULE_0__["render"],
  _MatchTabs_vue_vue_type_template_id_7d79dd40___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/components/pageparts/match/MatchTabs.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/components/pageparts/match/MatchTabs.vue?vue&type=script&lang=js&":
/*!****************************************************************************************!*\
  !*** ./resources/js/components/pageparts/match/MatchTabs.vue?vue&type=script&lang=js& ***!
  \****************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_MatchTabs_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/babel-loader/lib??ref--4-0!../../../../../node_modules/vue-loader/lib??vue-loader-options!./MatchTabs.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/pageparts/match/MatchTabs.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_MatchTabs_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/components/pageparts/match/MatchTabs.vue?vue&type=template&id=7d79dd40&":
/*!**********************************************************************************************!*\
  !*** ./resources/js/components/pageparts/match/MatchTabs.vue?vue&type=template&id=7d79dd40& ***!
  \**********************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_MatchTabs_vue_vue_type_template_id_7d79dd40___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../../node_modules/vue-loader/lib??vue-loader-options!./MatchTabs.vue?vue&type=template&id=7d79dd40& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/pageparts/match/MatchTabs.vue?vue&type=template&id=7d79dd40&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_MatchTabs_vue_vue_type_template_id_7d79dd40___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_MatchTabs_vue_vue_type_template_id_7d79dd40___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./resources/js/components/pageparts/match/MatchTeams.vue":
/*!****************************************************************!*\
  !*** ./resources/js/components/pageparts/match/MatchTeams.vue ***!
  \****************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _MatchTeams_vue_vue_type_template_id_69a7bf78___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./MatchTeams.vue?vue&type=template&id=69a7bf78& */ "./resources/js/components/pageparts/match/MatchTeams.vue?vue&type=template&id=69a7bf78&");
/* harmony import */ var _MatchTeams_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./MatchTeams.vue?vue&type=script&lang=js& */ "./resources/js/components/pageparts/match/MatchTeams.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _MatchTeams_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _MatchTeams_vue_vue_type_template_id_69a7bf78___WEBPACK_IMPORTED_MODULE_0__["render"],
  _MatchTeams_vue_vue_type_template_id_69a7bf78___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/components/pageparts/match/MatchTeams.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/components/pageparts/match/MatchTeams.vue?vue&type=script&lang=js&":
/*!*****************************************************************************************!*\
  !*** ./resources/js/components/pageparts/match/MatchTeams.vue?vue&type=script&lang=js& ***!
  \*****************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_MatchTeams_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/babel-loader/lib??ref--4-0!../../../../../node_modules/vue-loader/lib??vue-loader-options!./MatchTeams.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/pageparts/match/MatchTeams.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_MatchTeams_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/components/pageparts/match/MatchTeams.vue?vue&type=template&id=69a7bf78&":
/*!***********************************************************************************************!*\
  !*** ./resources/js/components/pageparts/match/MatchTeams.vue?vue&type=template&id=69a7bf78& ***!
  \***********************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_MatchTeams_vue_vue_type_template_id_69a7bf78___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../../node_modules/vue-loader/lib??vue-loader-options!./MatchTeams.vue?vue&type=template&id=69a7bf78& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/pageparts/match/MatchTeams.vue?vue&type=template&id=69a7bf78&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_MatchTeams_vue_vue_type_template_id_69a7bf78___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_MatchTeams_vue_vue_type_template_id_69a7bf78___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./resources/js/components/pageparts/match/MatchTimes.vue":
/*!****************************************************************!*\
  !*** ./resources/js/components/pageparts/match/MatchTimes.vue ***!
  \****************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _MatchTimes_vue_vue_type_template_id_50565458___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./MatchTimes.vue?vue&type=template&id=50565458& */ "./resources/js/components/pageparts/match/MatchTimes.vue?vue&type=template&id=50565458&");
/* harmony import */ var _MatchTimes_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./MatchTimes.vue?vue&type=script&lang=js& */ "./resources/js/components/pageparts/match/MatchTimes.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _MatchTimes_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _MatchTimes_vue_vue_type_template_id_50565458___WEBPACK_IMPORTED_MODULE_0__["render"],
  _MatchTimes_vue_vue_type_template_id_50565458___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/components/pageparts/match/MatchTimes.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/components/pageparts/match/MatchTimes.vue?vue&type=script&lang=js&":
/*!*****************************************************************************************!*\
  !*** ./resources/js/components/pageparts/match/MatchTimes.vue?vue&type=script&lang=js& ***!
  \*****************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_MatchTimes_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/babel-loader/lib??ref--4-0!../../../../../node_modules/vue-loader/lib??vue-loader-options!./MatchTimes.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/pageparts/match/MatchTimes.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_MatchTimes_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/components/pageparts/match/MatchTimes.vue?vue&type=template&id=50565458&":
/*!***********************************************************************************************!*\
  !*** ./resources/js/components/pageparts/match/MatchTimes.vue?vue&type=template&id=50565458& ***!
  \***********************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_MatchTimes_vue_vue_type_template_id_50565458___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../../node_modules/vue-loader/lib??vue-loader-options!./MatchTimes.vue?vue&type=template&id=50565458& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/pageparts/match/MatchTimes.vue?vue&type=template&id=50565458&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_MatchTimes_vue_vue_type_template_id_50565458___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_MatchTimes_vue_vue_type_template_id_50565458___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./resources/js/views/Match.vue":
/*!**************************************!*\
  !*** ./resources/js/views/Match.vue ***!
  \**************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _Match_vue_vue_type_template_id_c3eed494___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./Match.vue?vue&type=template&id=c3eed494& */ "./resources/js/views/Match.vue?vue&type=template&id=c3eed494&");
/* harmony import */ var _Match_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./Match.vue?vue&type=script&lang=js& */ "./resources/js/views/Match.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _Match_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _Match_vue_vue_type_template_id_c3eed494___WEBPACK_IMPORTED_MODULE_0__["render"],
  _Match_vue_vue_type_template_id_c3eed494___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/views/Match.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/views/Match.vue?vue&type=script&lang=js&":
/*!***************************************************************!*\
  !*** ./resources/js/views/Match.vue?vue&type=script&lang=js& ***!
  \***************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Match_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../node_modules/babel-loader/lib??ref--4-0!../../../node_modules/vue-loader/lib??vue-loader-options!./Match.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/Match.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Match_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/views/Match.vue?vue&type=template&id=c3eed494&":
/*!*********************************************************************!*\
  !*** ./resources/js/views/Match.vue?vue&type=template&id=c3eed494& ***!
  \*********************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Match_vue_vue_type_template_id_c3eed494___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../node_modules/vue-loader/lib??vue-loader-options!./Match.vue?vue&type=template&id=c3eed494& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/Match.vue?vue&type=template&id=c3eed494&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Match_vue_vue_type_template_id_c3eed494___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Match_vue_vue_type_template_id_c3eed494___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ })

}]);
//# sourceMappingURL=6.js.map