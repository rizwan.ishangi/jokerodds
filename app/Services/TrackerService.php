<?php

namespace App\Services;

use App\Models\UserTracker;

class TrackerService
{
    /**
     * @var false|string
     */

    public function getTrackerMatches($ipAddress)
    {
        return $trackerMatches = UserTracker::where('ip_address', $ipAddress)
            ->with(['event', 'event.scoreboard', 'event.fixture', 'event.fixture.participants'])
            ->get();
    }

    public function addMatchToTracker($data)
    {
        return UserTracker::updateOrCreate(
            ['event_id' => $data['event']['id']],
            ['event_id' => $data['event']['id'],
                'coefficient' => $data['bets'],
                'ip_address' => $data['ipAddress']
            ]
        );
    }

    public function clearTracker($ipAddress)
    {
        UserTracker::where('ip_address', $ipAddress)->delete();
        return [];
    }

    public function deleteMatchFromTracker($id)
    {
        return UserTracker::whereEventId($id)->delete();
    }
}
