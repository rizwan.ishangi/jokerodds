<?php


namespace App\Services\BetThreeSixFive;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;
use Illuminate\Support\Arr;
use Illuminate\Support\Carbon;

class BetThreeSixFiveService
{
    private $baseUrl;
    private $token ;
    private $version;
    private $guzzle;

    private $url = [

        'getInPlayEvents'       => 'events/inplay',
        'getUpcomingEvents'     => 'events/upcoming',
        'getEndedEvents'        => 'events/ended',
        'getEventsBySearch'     => 'events/search',
        'viewEvent'             => 'event/view',
        'getEventHistory'       => 'event/history',

        /*bet356 only*/
        'getBet365InPlay'       => 'bet365/inplay',
        'getBet365InPlayFilter' => 'bet365/inplay_filter',
        'getBet365Upcoming'     => 'bet365/upcoming',



    ];
//https://betsapi.com/docs/GLOSSARY.html#r-sportid
    public const R_SportID = [
        'Soccer'  => 1,
        'Cricket' => 3,
        /*etc*/
        ];

    public function __construct(array $extra_guzzle_parameters = [], $timeout = 30)
    {
        $parameters  = [];
        $default_guzzle_parameters = ['timeout' => $timeout];

        if(sizeof($extra_guzzle_parameters)){
            $parameters = array_merge($default_guzzle_parameters, $extra_guzzle_parameters);
        }

        $this->token    =  config('bet-three-six-five.token');
        $this->baseUrl  =  config('bet-three-six-five.base_url');
        $this->version  =  config('bet-three-six-five.version');
        $this->guzzle   =  new Client($parameters);
    }

    public function getInPlayEvents(int $sport_id = self::R_SportID['Soccer']){

        $query_param = [
            'sport_id' =>$sport_id,
        ];

        try{
            $events  = $this->guzzle->get($this->buildUrl($this->url['getInPlayEvents'],$query_param))->getBody();
            return json_decode($events,true);
        } catch (GuzzleException $e){
            //log
            return [];
        }
    }

    public function getUpcomingEvents(int $sport_id = self::R_SportID['Soccer'], string  $date='', $page = 0){
        /*
            sport_id	    Yes	R-SportID
            league_id	    No	useful when you want only one league
            team_id	        No	useful when you want only one team
            cc	            No	Eg: 'co' for Colombia (R-Countries)
            day	            No	format YYYYMMDD, eg: 20161201
            skip_esports	No	skip Esoccer, Ebasketball etc. in the result
            page
         * */

        $day = Carbon::parse($date)->format('Ymd');

        $query_param = [
            'sport_id'  => $sport_id,
            'day'       => $day
        ];

        if($page){
            $query_param = array_merge($query_param,['page'=>$page]);
        }

        try{
            $events  = $this->guzzle->get($this->buildUrl($this->url['getUpcomingEvents'],$query_param,true))->getBody();
            return json_decode($events,true);
        } catch (GuzzleException $e){
            //log
            return [];
        }
    }

    public function getEndedEvents(int $sport_id = self::R_SportID['Soccer'], string  $date='', $page = 0){
        /*
            sport_id	    Yes	R-SportID
            league_id	    No	useful when you want only one league
            team_id	        No	useful when you want only one team
            cc	            No	Eg: 'co' for Colombia (R-Countries)
            day	            No	format YYYYMMDD, eg: 20161201
            skip_esports	No	skip Esoccer, Ebasketball etc. in the result
            page
         * */

        $day = Carbon::parse($date)->format('Ymd');

        $query_param = [
            'sport_id'  => $sport_id,
            'day'       => $day
        ];

        if($page){
            $query_param = array_merge($query_param,['page'=>$page]);
        }
        try{
            $events  = $this->guzzle->get($this->buildUrl($this->url['getEndedEvents'],$query_param,true))->getBody();
            return json_decode($events,true);
        } catch (GuzzleException $e){
            //log
            return [];
        }
    }

    public function getEventBySearch(int $sport_id = self::R_SportID['Soccer'], string $home = '', string $away = '',   $date=''){
        /*
            sport_id	Yes	Reference
            home	Yes	home team ID or name
            away	Yes	away team ID or name
            time	Yes	either UTC time epoch (Limited to 365 days) or day YYYYMMDD
         * */

        $day = Carbon::parse($date)->format('Ymd');

        $query_param = [
            'sport_id'  => $sport_id,
            'home'      => $home,
            'away'      => $away,
            'time'      => $day
        ];

        try{
            $event  = $this->guzzle->get($this->buildUrl($this->url['getEventsBySearch'],$query_param))->getBody();
            return json_decode($event,true);

        } catch (GuzzleException $e){
            //log
            return [];
        }
    }

    public function getEvent($ids = ''){

        /*
            event_id	Yes	Event ID you get from events/*
            you can send multiple event_ids in one request with event_id=1,2,3,4 up to max 10 ids.
          */
        if(empty($ids)){
            return [];
        }

        $query_param = [
            'event_id'  => $ids
        ];

        try{
            $event  = $this->guzzle->get($this->buildUrl($this->url['viewEvent'],$query_param))->getBody();
            return json_decode($event,true);

        } catch (GuzzleException $e){
            //log
            return [];
        }
    }

    public function getEventHistory(int $sport_id = self::R_SportID['Soccer'], $id = 0, $qty = 0){
        /*
            event_id	Yes	Event ID you get from events/*
            you can send multiple event_ids in one request with event_id=1,2,3,4 up to max 10 ids.
          */

        if(empty($id)){
            return [];
        }

        $query_param = [
            'sport_id' => $sport_id,
            'event_id'  => $id
        ];
        if($qty){

            $query_param = array_merge($query_param, ['qty'=>$qty]);
        }
        try{
            $event  = $this->guzzle->get($this->buildUrl($this->url['viewEvent'],$query_param))->getBody();
            return json_decode($event,true);

        } catch (GuzzleException $e){
            //log
            return [];
        }
    }

    // bet 365 api
    //league.id/home.id/away.id returned from bet365/inplay_filter might be different than event/inplay.
    public function getBetInPlayFilterEvents(int $sport_id = self::R_SportID['Soccer']){

        $query_param = [
            'sport_id' =>$sport_id,
        ];

        try{
            $events  = $this->guzzle->get($this->buildUrl($this->url['getBet365InPlayFilter'],$query_param))->getBody();
            return json_decode($events,true);
        } catch (GuzzleException $e){
            //log
            return [];
        }
    }
    public function getBetInPlay(int $sport_id = self::R_SportID['Soccer']){

        $query_param = [];

        try{
            $events  = $this->guzzle->get($this->buildUrl($this->url['getBet365InPlay'],$query_param))->getBody();
            return json_decode($events,true);
        } catch (GuzzleException $e){
            //log
            return [];
        }
    }

    // use for store league_id, event_id, team_id
    public function getBetUpcomingEvents(int $sport_id = self::R_SportID['Soccer'], string  $date='', $page = 0){
        /*
            sport_id	    Yes	R-SportID
            league_id	    No	useful when you want only one league
            day	            No	format YYYYMMDD, eg: 20161201
            page
         * */

        $day = Carbon::parse($date)->format('Ymd');

        $query_param = [
            'sport_id'  => $sport_id,
            'day'       => $day
        ];

        if($page){
            $query_param = array_merge($query_param,['page'=>$page]);
        }

        try{
            $events  = $this->guzzle->get($this->buildUrl($this->url['getBet365Upcoming'],$query_param))->getBody();
            return json_decode($events,true);
        } catch (GuzzleException $e){
            //log
            return [];
        }
    }

    private function buildUrl($url, $params, $use_v2 = false){

        if($use_v2){
            return implode('/',[$this->baseUrl,'v2',$url]).'?'. Arr::query(array_merge(['token'=>$this->token],$params));
        }
        return implode('/',[$this->baseUrl,$this->version,$url]).'?'. Arr::query(array_merge(['token'=>$this->token],$params));
    }

}

