<?php

namespace App\Services\Lsports;

use GuzzleHttp\Client;
use Illuminate\Support\Arr;
use PhpAmqpLib\Channel\AMQPChannel;
use PhpAmqpLib\Connection\AMQPStreamConnection;

/**
 * Channel Creation From Connection
 */
class LsportMQ
{
    public const MODE_PREMATCH = 'prematch';
    public const MODE_INPLAY = 'inplay';

    protected AMQPChannel $channel;
    protected AMQPStreamConnection $connection;

    protected array $credentials;

    protected $prefetchSize = null; // msg size in bytes, must be null else error
    protected int $prefetchCount = 1000;
    protected ?bool $applyPerChannel = false; // can be false or null, otherwise error

    private function fetchCredentials(string $mode): void
    {
        switch ($mode) {
            case self::MODE_PREMATCH:
                $this->credentials = [
                    'host' => config('lsports.rmsHosts.prematch'),
                    'port' => config('lsports.rmsPort'),
                    'username' => config('lsports.username'),
                    'password' => config('lsports.password'),
                    'packageId' => config('lsports.prematchPackageId'),
                    'guid' => config('lsports.prematchGuid'),
                ];
                break;

            case self::MODE_INPLAY:
                $this->credentials = [
                    'host' => config('lsports.rmsHosts.inplay'),
                    'port' => config('lsports.rmsPort'),
                    'username' => config('lsports.username'),
                    'password' => config('lsports.password'),
                    'packageId' => config('lsports.inplayPackageId'),
                    'guid' => config('lsports.inplayGuid'),
                ];
                break;

            default:
                throw new \Exception('Unknown api mode: ' . $mode);
        }
    }

    public function connect(string $mode)
    {
        $this->fetchCredentials($mode);
        $this->enablePackage($mode);

        // Delay is needed in order to package completely enabled
        sleep(5);

        $this->connection = new AMQPStreamConnection(
            $this->credentials['host'],
            $this->credentials['port'],
            $this->credentials['username'],
            $this->credentials['password'],
            'Customers',
            false,
            'AMQPLAIN', //LOGIN MECHANISM
            null,
            'en_US', //Locale
            1160,
            1160,
            null,
            false,
            580
        );
    }

    private function enablePackage(string $mode)
    {
        $url = 'https://' . $mode . '.lsports.eu/' .
            ($mode == self::MODE_PREMATCH ? 'OddService' : 'api/Package') .
            '/EnablePackage?' .
            Arr::query([
                'username' => $this->credentials['username'],
                'password' => $this->credentials['password'],
                'guid' => $this->credentials['guid'],
                'packageid' => $this->credentials['packageId']
            ]);

        $guzzle = new Client([
            'timeout'  => 60 * 5,
        ]);

        $guzzle->get($url);
    }

    public function consume(\Closure $handler)
    {
        $this->channel = $this->connection->channel();
        $this->channel->basic_qos($this->prefetchSize, $this->prefetchCount, $this->applyPerChannel);
        $this->channel->basic_consume(
            '_' . $this->credentials['packageId'] . '_',
            'consumer',
            false,
            true,
            false,
            false,
            function ($msg) use ($handler) {
                $json = json_decode($msg->body);

                $handler(
                    $json->Body->Events ?? [] + $json->Body->ActiveEvents ?? []
                );
            }
        );

        while (count($this->channel->callbacks)) {
            try {
                $this->channel->wait();
            } catch (\Exception $e) {
                // Suppose $e is \ErrorException with "errno=32" or "Broken pipe" in message
                $this->connection->reconnect();

                // through all of this, $this->amqpConnection->isConnected() remains true
                // Errors with PhpAmqpLib\Exception\AMQPProtocolConnectionException:
                // CHANNEL_ERROR - expected 'channel.open'
                $this->consume($handler);
            }
        }
    }
}
