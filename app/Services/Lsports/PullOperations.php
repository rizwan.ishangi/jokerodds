<?php

namespace App\Services\Lsports;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;
use Illuminate\Support\Arr;

class PullOperations
{
    private string $username;
    private string $password;
    private string $prematchGuid;
    private string $inplayGuid;
    private string $lang = 'en';
    private Client $guzzle;

    private string $orderFixturesURL = 'https://api.lsports.eu/api/schedule/OrderFixtures';
    private string $cancelOrderFixturesURL = 'https://inplay.lsports.eu/api/schedule/CancelFixtureOrders';
    private string $getEventsURL = 'https://prematch.lsports.eu/OddService/GetEvents';
    private string $getFixturesURL = 'https://prematch.lsports.eu/OddService/GetFixtures';
    private string $getSportsURL = 'https://prematch.lsports.eu/OddService/GetSports';
    private string $getLocationsURL = 'https://prematch.lsports.eu/OddService/GetLocations';
    private string $getLeaguesURL = 'https://prematch.lsports.eu/OddService/GetLeagues';
    private string $getBookmakersURL = 'https://prematch.lsports.eu/OddService/GetBookmakers';
    private string $getMarketsURL = 'https://prematch.lsports.eu/OddService/GetMarkets';
    private string $getScoresURL = 'https://prematch.lsports.eu/OddService/GetScores';
    private string $enablePackageURL = 'https://api.lsports.eu/api/Package/EnablePackage';
    private string $disablePackageURL = 'https://api.lsports.eu/api/Package/DisablePackage';
    private string $disablePreMatchPackageURL = 'https://prematch.lsports.eu/OddService/DisablePackage';
    private string $enablePreMatchPackageUrl = 'https://prematch.lsports.eu/OddService/EnablePackage';
    private string $snapshotURL = 'https://api.lsports.eu/api/Snapshot';
    private string $getScheduleURL = 'https://inplay.lsports.eu/api/schedule/GetInPlaySchedule';

    public function __construct()
    {
        $this->username = config('lsports.username');
        $this->password = config('lsports.password');
        $this->prematchGuid = config('lsports.prematchGuid');
        $this->inplayGuid = config('lsports.inplayGuid');

        $this->guzzle = new Client([
            'timeout'  => 60 * 5,
        ]);
    }

    public function setLanguage($lang)
    {
        $this->lang = $lang;
    }

    public function getSnapshot($action = 'GetSnapshotJson', $params = null)
    {
        return $this->curl($this->snapshotURL . '/' . $action . $this->getConnectionString($params));
    }

    public function getSports($params = null)
    {
        return $this->curl($this->getSportsURL . '/' . $this->getConnectionString($params));
    }

    public function getScores($params = null)
    {
        return $this->curl($this->getScoresURL . '/' . $this->getConnectionString($params));
    }

    public function getLocations($params = null)
    {
        return $this->curl($this->getLocationsURL . '/' . $this->getConnectionString($params));
    }

    public function getLeagues($params = null)
    {
        return $this->curl($this->getLeaguesURL . '/' . $this->getConnectionString($params));
    }

    public function getBookmakers($params = null)
    {
        return $this->curl($this->getBookmakersURL . '/' . $this->getConnectionString($params));
    }

    public function getMarkets($params = null)
    {
        return $this->curl($this->getMarketsURL . '/' . $this->getConnectionString($params));
    }

    /**
     * @param  array  $filters
     * @param  null  $params
     * @return mixed
     * @throws GuzzleException
     */
    public function getEvents(array $filters, $params = null)
    {
        return $this->curl($this->getEventsURL . $this->getConnectionString($params) . '&' . Arr::query($filters));
    }

    /**
     * @return mixed
     * @throws GuzzleException
     */
    public function getSchedule()
    {
        return $this->curl($this->getScheduleURL . $this->getInplayConnectionString());
    }

    /**
     * @param $params
     * @return string
     */
    private function getConnectionString($params)
    {
        $paramsString = $params ? http_build_query($params) : '';

        return '?username=' . $this->username . '&password=' . $this->password . '&packageid=' .
            $this->prematchGuid . '&guid=' . $this->prematchGuid . '&lang=' . $this->lang . '&' . $paramsString;
    }

    /**
     * @return string
     */
    private function getInplayConnectionString()
    {
        return '?username=' . $this->username . '&password=' . $this->password . '&packageid=' .
            $this->inplayGuid . '&guid=' . $this->inplayGuid . '&lang=' . $this->lang;
    }

    /**
     * @param  null  $params
     * @return mixed
     * @throws GuzzleException
     */
    public function getFixtures($params = null)
    {
        return $this->curl($this->getFixturesURL . $this->getConnectionString($params));
    }

    /**
     * @param  bool  $enable
     *
     * @param  bool  $inplay
     * @param  null  $params
     * @return mixed
     * @throws GuzzleException
     */
    public function packageControl($enable = true, $inplay = true, $params = null)
    {
        if ($inplay) {
            $url = $enable ? $this->enablePackageURL : $this->disablePackageURL;
        } else {
            $url = $enable ? $this->enablePreMatchPackageUrl : $this->disablePreMatchPackageURL;
        }

        $url .= $this->getConnectionString($params);

        return $this->curl($url);
    }

    /**
     * @param  array  $events
     *
     * @param  null  $params
     * @return mixed
     * @throws GuzzleException
     */
    public function orderEvent($events = [], $params = null)
    {
        return $this->curl(
            $this->orderFixturesURL . $this->getConnectionString($params) .
            '&fixtureIds=' . implode(',', $events)
        );
    }

    public function cancelEventOrdering($events = [], $params = null)
    {
        return $this->curl(
            $this->cancelOrderFixturesURL . $this->getConnectionString($params) .
            '&fixtureIds=' . implode(',', $events)
        );
    }

    /**
     * @param  string  $url
     * @param  bool  $isPost
     *
     * @return mixed
     * @throws GuzzleException
     */
    private function curl($url, $isPost = false)
    {
        $response = $this->guzzle->request($isPost ? 'POST' : 'GET', $url);

        return \GuzzleHttp\json_decode($response->getBody()->getContents())->Body;
    }
}
