<?php

use PhpAmqpLib\Connection\AMQPStreamConnection;

$connection = new AMQPStreamConnection(
    'prematch-rmq.lsports.eu', // Prematch or InPlay
    5672,
    "odds@adnweb.site", // Change to your user name
    'h41lluc1f3r666', // Change to your password
    "Customers",
    false,
    'AMQPLAIN', //LOGIN MECHANISM
    null,
    'en_US', //Locale
    1160,
    1160,
    null,
    false,
    580
);


/**
 * Channel Creation From Connection
 */

$channel = $connection->channel();
/**
 *  Your queue number as received from LSPORT (WITH _)
 */
$queue = "_1090_";

/**
 * Message Consume
 * $msg->body is a buffer array , use toString to get it as JSON/XML
 */
$callback = function ($msg) {
    echo "Event Received ", $msg->body, "\n";
};

$channel->basic_qos(0, 1000, false);
$channel->basic_consume($queue, 'consumer', false, true, false, false, $callback);



/**
 * Endless while loop waiting for next message to arrive
 */
while (count($channel->callbacks)) {
    $channel->wait();
}
