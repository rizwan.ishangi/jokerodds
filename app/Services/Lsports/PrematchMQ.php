<?php

namespace App\Services\Lsports;

use App\Events\BetsUpdatedAverage;
use App\Libraries\OddsConvector\OddsConvector;
use App\Models\Bookmaker;
use App\Models\Market;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redis;
use PhpAmqpLib\Connection\AMQPStreamConnection;
use App\Models\Fixture;
use App\Models\Incident;
use App\Events\BetsUpdated;
use App\Models\Bet;
use App\Models\Period;
use App\Models\Scoreboard;
use Carbon\Carbon;

/**
 * Channel Creation From Connection
 */

class PrematchMQ
{
    protected $channel;
    /**
     * @var AMQPStreamConnection
     */
    protected $connection;

    protected $rmq_host = 'prematch-rmq.lsports.eu'; // prematch
    protected $rmq_port = 5672;

    protected $rmq_username = 'alexandr.rilskii@gmail.com';
    protected $rmq_password = 'dgsvt4e';

    protected $queue = '_2491_'; // prematch
    //    protected $queue = '_2492_'; //In-play

    protected $prefetchSize = null; // msg size in bytes, must be null else error
    protected $prefetchCount = 1000;
    protected $applyPerChannel = false; // can be false or null, otherwise error

    protected const ONLY_MARKETS = [
        1,
        282,
        284,
        3,
        53,
        283,
        2,
        77,
        337,
        7,
        25,
        151,
        4,
    ];

    public function connect()
    {
        $this->connection = new AMQPStreamConnection(
            $this->rmq_host, // HOST
            $this->rmq_port,
            $this->rmq_username, // Change to your user name
            $this->rmq_password, // Change to your password
            'Customers',
            false,
            'AMQPLAIN', //LOGIN MECHANISM
            null,
            'en_US', //Locale
            1160,
            1160,
            null,
            false,
            580
        );
    }

    public function consume()
    {
        $this->channel = $this->connection->channel();
        $this->channel->basic_qos($this->prefetchSize, $this->prefetchCount, $this->applyPerChannel);
        $this->channel->basic_consume(
            $this->queue,
            'consumer',
            false,
            true,
            false,
            false,
            function ($msg) {
                $body = json_decode($msg->body);
                $event = $body->Body->Events[0];
                $this->updateCoefficients($event);
                //echo json_encode($msg);
            }
        );


        while (count($this->channel->callbacks)) {
            try {
                $this->channel->wait();
            } catch (\Exception $e) {
                // Suppose $e is \ErrorException with "errno=32" or "Broken pipe" in message
                echo "Error Prematch consume : ".$e->getMessage().PHP_EOL;

               /* if(strstr($e->getMessage(), 'ACCESS_REFUSED')){
                    exit();
                }*/

                try{
                    $this->connection->reconnect();
                    $this->consume();
                } catch (\Exception $e){
                    exit();
                }


                // through all of this, $this->amqpConnection->isConnected() remains true
                // Errors with PhpAmqpLib\Exception\AMQPProtocolConnectionException:
                // CHANNEL_ERROR - expected 'channel.open'

            }
        }
    }

    /**
     * @param  object  $events
     * @param  Collection<Fixture>  $fixtures
     */
    private function updateCoefficients(object $event): void
    {
        //echo "1".PHP_EOL;
        $fixtures = Fixture::whereIn('id', collect([$event])->pluck('FixtureId'))->get();
        //dd($fixtures);
        //Event::whereIn('fixture_id', $fixtures->pluck('id'))->update([
        //    'bets_last_update' => now(),
        //]);
        //echo "2".PHP_EOL;
        $fixturesToEvents = $fixtures->pluck('event.id', 'id')->toArray();
        $fixturesToEvents = array_unique($fixturesToEvents);
        try {
            DB::transaction(function () use ($event, $fixturesToEvents) {
                echo "FixtureId: ".$event->FixtureId.PHP_EOL;
                // $this->output->progressAdvance(1);

                // if (empty($fixturesToEvents[$event->FixtureId])) {
                //     $this->info(' ' . $event->FixtureId . ' = Skipped ');
                //     return;
                // }

                $eventId = $fixturesToEvents[$event->FixtureId];
                //echo bets
                $bets   = [];
                //all event bets
                $eventBets = [];

                $dbBets = Bet::query();

                $where_condition_step = 0;

                $markets = collect($event->Markets)->pluck('Id')->unique()->toArray();
                $bookmaker_list = Bookmaker::pluck('id')->toArray();
                $bookmakers = [];

                foreach ($event->Markets ?: [] as $market) {

                    foreach ($market->Providers ?: [] as $provider) {
                        if(isset($provider->Id)){
                            $bookmakers[] =$provider->Id;
                        }

                        Bet::whereIn('api_bet_id', collect($provider->Bets)->pluck('Id'))->where([
                            'bookmaker_id' => $provider->Id,
                            'market_id' => $market->Id,
                        ])->update([
                            'is_latest' => false,
                        ]);

                        foreach ($provider->Bets ?: [] as $bet) {
                            if(!$where_condition_step){
                                $dbBets = $dbBets->whereRaw('(api_bet_id = ? AND  last_update= ? )',[ $bet->Id, Carbon::parse($bet->LastUpdate) ]);
                            } else{
                                $dbBets = $dbBets->orWhereRaw('(api_bet_id = ? AND   last_update= ? )',[ $bet->Id, Carbon::parse($bet->LastUpdate)]);
                            }
/*
                            Bet::updateOrInsert([
                                'api_bet_id' => $bet->Id,
                                'last_update' => Carbon::parse($bet->LastUpdate),
                            ], [
                                'event_id' => $eventId,
                                'bookmaker_id' => $provider->Id,
                                'market_id' => $market->Id,
                                'status_id' => $bet->Status,
                                'settlement_id' => $bet->Settlement ?? null,
                                'name' => $bet->Name,
                                'line' => $bet->Line ?? null,
                                'base_line' => $bet->BaseLine ?? null,
                                'start_price' => $bet->StartPrice,
                                'price' => $bet->Price,
                                'is_inplay' => false,
                                'is_latest' => true,
                                'open_status_date' => $bet->Status == Bet::OPEN_BET ? Carbon::now() : null,
                                'suspended_status_date' => $bet->Status == Bet::SUSPENDED_BET ? Carbon::now() : null,
                                'settled_status_date' => $bet->Status == Bet::SETTLED_BET ? Carbon::now() : null,
                            ]);
*/
                            if(in_array($market->Id,self::ONLY_MARKETS) && in_array($provider->Id, $bookmaker_list)){
                                    $bets[] = [
                                        'api_bet_id' => $bet->Id,
                                        'last_update' => Carbon::parse($bet->LastUpdate),
                                        'event_id' => $eventId,
                                        'bookmaker_id' => $provider->Id,
                                        'market_id' => $market->Id,
                                        'status_id' => $bet->Status,
                                        'settlement_id' => $bet->Settlement ?? null,
                                        'name' => $bet->Name,
                                        'line' => $bet->Line ?? null,
                                        'base_line' => $bet->BaseLine ?? null,
                                        'start_price' => $bet->StartPrice,
                                        'price' => $bet->Price,
                                        'is_inplay' => false,
                                        'is_latest' => true,
                                        'open_status_date' => $bet->Status == Bet::OPEN_BET ? Carbon::now() : null,
                                        'suspended_status_date' => $bet->Status == Bet::SUSPENDED_BET ? Carbon::now() : null,
                                        'settled_status_date' => $bet->Status == Bet::SETTLED_BET ? Carbon::now() : null,
                                        'bookmaker' => [],
                                        'market'    => []
                                    ];
                                // all event bets
                                $eventBets[] = [
                                    'api_bet_id'            => $bet->Id,
                                    'last_update'           => Carbon::parse($bet->LastUpdate),
                                    'event_id'              => $eventId,
                                    'bookmaker_id'          => $provider->Id,
                                    'market_id'             => $market->Id,
                                    'status_id'             => $bet->Status,
                                    'settlement_id'         => $bet->Settlement ?? null,
                                    'name'                  => $bet->Name,
                                    'line'                  => $bet->Line ?? null,
                                    'base_line'             => $bet->BaseLine ?? null,
                                    'start_price'           => $bet->StartPrice,
                                    'price'                 => $bet->Price,
                                    'is_inplay'             => false,
                                    'is_latest'             => true,
                                    'open_status_date'      => $bet->Status == Bet::OPEN_BET ? Carbon::now() : null,
                                    'suspended_status_date' => $bet->Status == Bet::SUSPENDED_BET ? Carbon::now() : null,
                                    'settled_status_date'   => $bet->Status == Bet::SETTLED_BET ? Carbon::now() : null,
                                ];
                            }


                            $where_condition_step++;
                        }
                    }
                }

                if($where_condition_step){
                    echo 'INSERT OR UPDATE'.PHP_EOL;

                    $dbBets         = $dbBets->get();
                    if($dbBets->count()){
                        $eventBets      = collect($eventBets);
                        $betsToUpdate   = $eventBets->filter(function ($bet) use ($dbBets) {
                            return $dbBets->filter(function ($item) use ($bet) {
                                return ($item->api_bet_id ==$bet['api_bet_id']) && ($item->last_update ==$bet['last_update']);
                            })->count();
                        });
                        /*->map(function ($item) use ($dbBets){

                            $db_bet = $dbBets->filter(function ($db_item) use($item){
                                return ($db_item->api_bet_id ==$item['api_bet_id']) && ($db_item->last_update ==$item['last_update']);
                            })->values()->toArray();
                                echo  $db_bet[0]['id'];
                            if($db_bet && sizeof($db_bet)){
                                return [
                                    'id'                    => $db_bet[0]['id'],
                                    'api_bet_id'            => $item['api_bet_id'],
                                    'last_update'           => Carbon::parse($item['last_update']),
                                    'event_id'              => $item['event_id'],
                                    'bookmaker_id'          => $item['bookmaker_id'],
                                    'market_id'             => $item['market_id'],
                                    'status_id'             => $item['status_id'],
                                    'settlement_id'         => $item['settlement_id'] ?? null,
                                    'name'                  => $item['name'],
                                    'line'                  => $item['line'] ?? null,
                                    'base_line'             => $item['base_line'] ?? null,
                                    'start_price'           => $item['start_price'],
                                    'price'                 => $item['price'],
                                    'is_inplay'             => false,
                                    'is_latest'             => true,
                                    'open_status_date'      => $item['open_status_date'],
                                    'suspended_status_date' => $item['suspended_status_date'],
                                    'settled_status_date'   => $item['settled_status_date'],
                                ];
                            } else {
                                return [];
                            }
                        })->toArray();*/
                        $tmp = [];
                        foreach ($betsToUpdate as $betToUpdate){
                            foreach ($dbBets as $dbBet) {
                                if(($dbBet->api_bet_id ==$betToUpdate['api_bet_id'])
                                    && ($dbBet->last_update ==$betToUpdate['last_update'])
                                    && in_array($betToUpdate['market_id'], self::ONLY_MARKETS)
                                    && in_array($betToUpdate['bookmaker_id'],$bookmaker_list)
                                )
                                {
                                    $tmp [] = [
                                        'id'                    => $dbBet->id,
                                        //   'api_bet_id'            => $betToUpdate['api_bet_id'],
                                        //   'last_update'           => Carbon::parse($betToUpdate['last_update']),
                                        //  'event_id'              => $betToUpdate['event_id'],
                                        'bookmaker_id'          => $betToUpdate['bookmaker_id'],
                                        'market_id'             => $betToUpdate['market_id'],
                                        'status_id'             => $betToUpdate['status_id'],
                                        //   'settlement_id'         => $betToUpdate['settlement_id'] ?? null,
                                        'name'                  => $betToUpdate['name'],
                                        'line'                  => $betToUpdate['line'] ?? null,
                                        'base_line'             => $betToUpdate['base_line'] ?? null,
                                        'start_price'           => $betToUpdate['start_price'],
                                        'price'                 => $betToUpdate['price'],
                                        'is_inplay'             => '0',
                                        'is_latest'             => '1',
                                        'open_status_date'      => $betToUpdate['open_status_date'],
                                        'suspended_status_date' => $betToUpdate['suspended_status_date'],
                                        'settled_status_date'   =>  $betToUpdate['settled_status_date'],
                                    ];
                                }
                            }
                        }

                        $betsToUpdate = $tmp;

                        $betsToInsert   = $eventBets->filter(function ($bet) use ($dbBets, $markets) {
                            return !($dbBets->filter(function ($item) use ($bet,$markets) {
                                return ($item->api_bet_id ==$bet['api_bet_id']) && ($item->last_update ==$bet['last_update']);
                            })->count());
                        })->filter(function ($bet) use ( $markets, $bookmaker_list){
                            return in_array($bet['market_id'],self::ONLY_MARKETS) && in_array($bet['bookmaker_id'],$bookmaker_list);
                        });
                        //INSERT
                        echo 'BULK INSERT'.PHP_EOL;
                        foreach (  array_chunk($betsToInsert->toArray(), 250) as $data) {
                            Bet::insert($data);
                        }
                        // Bet::insert($betsToInsert->toArray());
                        //UPDATE
                        //echo 'To update--'.$this->updateBatch('bet',$betsToUpdate).PHP_EOL;
                        echo 'BULK UPDATE'.PHP_EOL;
                        foreach (  array_chunk($betsToUpdate, 100) as $data) {
                            $this->updateBatch('bet',$data);
                        }
                        // $this->updateBatch('bet',$betsToUpdate);
                    } else{
                        foreach (  array_chunk($eventBets, 250) as $data) {
                            Bet::insert($data);
                        }
                    }

                    unset($dbBets, $eventBets);

                    echo 'INSERT success'.PHP_EOL;
                    if ($event->Livescore) {
                        /** @var Carbon $fixtureDate */
                        $fixtureDate = Fixture::whereId($event->FixtureId)->value('start_date');

                        if ($fixtureDate->greaterThan(now())) {
                            $this->output->error('Livescore for future fixture: ' . $event->FixtureId);
                        } else {
                            Scoreboard::updateOrInsert([
                                'event_id' => $eventId,
                            ], [
                                'status_id' => $event->Livescore->Scoreboard->Status,
                                'time' => $event->Livescore->Scoreboard->Time,
                                'results' => collect($event->Livescore->Scoreboard->Results)->map(function ($result) {
                                    return $result->Value;
                                })->join(':'),
                            ]);
                            foreach ($event->Livescore->Periods as $period) {
                                Period::updateOrInsert([
                                    'type_id' => $period->Type,
                                    'event_id' => $eventId,
                                ], [
                                    'is_finished' => $period->IsFinished,
                                    'is_confirmed' => $period->IsConfirmed,
                                    'results' => collect($period->Results)->map(function ($result) {
                                        return $result->Value;
                                    })->join(':'),
                                ]);

                                $periodId = Period::where([
                                    'type_id' => $period->Type,
                                    'event_id' => $eventId,
                                ])->first()->id;

                                foreach ($period->Incidents ?? [] as $incident) {
                                    Incident::updateOrInsert([
                                        'period_id' => $periodId,
                                        'seconds' => $incident->Seconds,
                                        'participant_position' => $incident->ParticipantPosition,
                                        'player_name' => $incident->PlayerName,
                                        'type_id' => $incident->IncidentType,
                                    ]);
                                }
                            }
                        }
                    }
                    //get new average
                    if(sizeof($bets)){
                        $average_bets = Bet::select([DB::raw('avg(price) as average'),DB::raw('max(price) as highest'),'market_id', 'name','base_line'])
                            ->where('event_id',$eventId)
                            ->whereIn('market_id',array_values(array_intersect($markets,self::ONLY_MARKETS)))
                            ->where('is_latest',1)
                            ->groupBy(['market_id','base_line', 'name'])
                            ->get()
                            ->map(function ($item) use($eventId){
                                return[
                                    'market_id' => $item->market_id,
                                    'average'   => (new OddsConvector($item->average))->handle(),
                                    'highest'   => $item->highest,
                                    'name'      => $item->name,
                                    'base_line' => $item->base_line,
                                    'event_id'  => $eventId
                                ];
                            })->toArray();
                        //add to redis
                        $data = [];

                        foreach ($average_bets as $average_bet) {
                            $key = 'average#'.$average_bet['event_id'].'#'.$average_bet['market_id'].'#'.$average_bet['name'];
                            if(!is_null($average_bet['base_line'])) {
                                $key .= $average_bet['base_line'];
                            }
                            $data[$key] = $average_bet['highest'].'#'.$average_bet['average'];
                        }

                        if(sizeof($data)){

                            $fixtureDate = Fixture::whereId($event->FixtureId)->value('start_date');

                            if($fixtureDate->greaterThan(now()->setTime(0,0,0))){
                                $seconds = $fixtureDate->addHours(48)->diffInSeconds(now());
                            } else{
                                $seconds = 3600;
                            }

                            Redis::pipeline(function ($pipe) use ($data,$seconds) {
                                foreach ($data as $key => $value) {
                                    $pipe->setEX($key,$seconds,$value);
                                }
                            });

                        }

                        $bookmakerList  = Bookmaker::whereIn('id', array_unique($bookmakers) )->get();
                        $marketList     = Market::whereIn('id', array_unique($markets))->get();

                        $bets = collect($bets)->map(function ($bet) use($bookmakerList, $marketList) {

                            $bet['market'] = $marketList->filter(function ($item) use ($bet) {
                                return $item->id == $bet['market_id'];
                            })->first()->toArray();

                            $bet['bookmaker'] = $bookmakerList->filter(function ($item) use ($bet) {
                                return $item->id == $bet['bookmaker_id'];
                            })->first()->toArray();

                            return $bet;

                        })->toArray();

                        $eventAverage = $average_bets;
                        event(new BetsUpdated($eventId, $bets, $eventAverage));
                        event(new BetsUpdatedAverage($eventId, $eventAverage));
                    }
                }

            });
        } catch (\Exception $e) {
            //$this->output->error('Unable to update cefs: ' . $e->getMessage());
            echo "Error: ".$e->getMessage().PHP_EOL;
        }
    }

    private function updateBatch($tableName = "", $multipleData = []){

        if( $tableName && !empty($multipleData) ) {

            // column or fields to update
            $updateColumn = array_keys($multipleData[0]);
            $referenceColumn = $updateColumn[0]; //e.g id
            unset($updateColumn[0]);
            $whereIn = "";

            $q = "UPDATE ".$tableName." SET ";
            foreach ( $updateColumn as $uColumn ) {
                $q .=  $uColumn." = CASE ";

                foreach( $multipleData as $data ) {

                    if($referenceColumn =='is_inplay'){
                        $q .= "WHEN ".$referenceColumn." = ".$data[$referenceColumn]." THEN 0 ";

                    }else if($referenceColumn =='is_latest'){
                        $q .= "WHEN ".$referenceColumn." = ".$data[$referenceColumn]." THEN 1 ";
                    }else if($referenceColumn =='name'){
                        $q .= "WHEN ".$referenceColumn." = ".$data[$referenceColumn]." THEN '".addslashes($data[$uColumn])."' ";
                    }  else {

                        if(!($data[$uColumn] == '') && !is_null($data[$uColumn])){
                            $q .= "WHEN ".$referenceColumn." = ".$data[$referenceColumn]." THEN '".$data[$uColumn]."' ";
                        } else{
                            $q .= "WHEN ".$referenceColumn." = ".$data[$referenceColumn]." THEN  NULL ";
                        }
                    }



                }
                $q .= "ELSE ".$uColumn." END, ";
            }
            foreach( $multipleData as $data ) {
                $whereIn .= "'".$data[$referenceColumn]."', ";
            }
            $q = rtrim($q, ", ")." WHERE ".$referenceColumn." IN (".  rtrim($whereIn, ', ').")";

            // Update
            return DB::update(DB::raw($q));
            /*       // to test
                   return $q;*/

        } else {
            return false;
        }

    }

}
