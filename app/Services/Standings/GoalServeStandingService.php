<?php


namespace App\Services\Standings;


use App\Contracts\Standings\StandingInterface;
use App\Models\GoalServeTeam;
use App\Services\GoalServe\GoalOperations;
use App\Services\GoalServe\GoalService;
use GuzzleHttp\Exception\GuzzleException;

class GoalServeStandingService implements StandingInterface
{

    private $goalService;
    public function __construct(){


        $this->goalService = new GoalService(new GoalOperations());
    }

    public function leagueStandings($leagueId){
        try {
            return $this->goalService->getStandingsByLeagueId($leagueId);
        } catch (GuzzleException $e) {
            return $e->getMessage();
        }
    }

    public function fullLeagueStandings($leagueId){
        try {
            $leagueStandings = $this->goalService->getStandingsByLeagueIdData($leagueId);
            $teamIds = [];
            if (is_array($leagueStandings)) {
                foreach ($leagueStandings as $group) {
                    if (isset($group->team)) {
                        foreach ($group->team as $team) {
                            $teamIds[] = $team->{'@id'};
                            $new_team = GoalServeTeam::where('team_id',intval($team->{'@id'}))->first();
                            if(is_null($new_team)){
                                $new_team = new GoalServeTeam();
                                $new_team->name             = $team->{'@name'};
                                $new_team->team_id          = $team->{'@id'};
                                $new_team->participant_id   = 0;
                                $new_team->image            = null;
                                $new_team->save();
                            }
                        }
                    }
                }
            } else {
                foreach ($leagueStandings->team as $team) {
                    $teamIds[] = $team->{'@id'};

                    $new_team = GoalServeTeam::where('team_id',intval($team->{'@id'}))->first();
                    if(is_null($new_team)){
                        $new_team = new GoalServeTeam();
                        $new_team->name             = $team->{'@name'};
                        $new_team->team_id          = $team->{'@id'};
                        $new_team->participant_id   = 0;
                        $new_team->image            = null;
                        $new_team->save();
                    }
                }
            }

            $teams = GoalServeTeam::select(['goalserve_teams.team_id', 'goalserve_teams.image','participant.image as participant_image'])
                ->join('participant','goalserve_teams.participant_id','=','participant.id')->whereIn('team_id', $teamIds)
                ->get()->map(function ($team){
                    return (object)[
                        $team['team_id']=>[
                            'image' => $team['image'],
                            'participant_image'   =>$team['participant_image']
                        ]
                    ];
                });

            return response()->json([
                'leagueStandings' => $leagueStandings,
                'teams' => $teams,
            ]);
        } catch (GuzzleException $e) {
            return response()->json(['message' => $e->getMessage()], 500);
        } catch (\Exception $e) {
            return response()->json(['message' => 'Something went wrong'], 500);
        }
     }
}
