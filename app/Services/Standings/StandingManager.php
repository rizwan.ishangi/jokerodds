<?php


namespace App\Services\Standings;


use App\Contracts\Standings\StandingInterface;

class StandingManager implements StandingInterface
{
    private $service;

    public function __construct($request)
    {

        if(isset($request['service'])){

            switch ($request['service']){
                case 'goalserve' : $this->service = new GoalServeStandingService();
                    break;
                case 'jack-pot':   $this->service = new JackPotStandingService();
                    break;
                default:           $this->service = new GoalServeStandingService();
            }

        } else{

            $this->service = new GoalServeStandingService();
        }
    }

    public function leagueStandings($leagueId)
    {
        return $this->service->leagueStandings($leagueId);
    }

    public function fullLeagueStandings($leagueId)
    {
        return $this->service->fullLeagueStandings($leagueId);
    }
}
