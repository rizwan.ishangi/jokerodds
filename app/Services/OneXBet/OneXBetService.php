<?php


namespace App\Services\OneXBet;


use App\Helpers\TickToTimestamp;
use App\Models\FixtureParticipant;
use App\Models\OneXBetTeam;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;
use Illuminate\Support\Arr;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;

class OneXBetService
{
    protected const ONE_X_BET_MARKET_LIST =[
        /*1x2*/             1,2,3,
        /*DC*/              4,5,6,
        /*Under/Over*/      9,10,
        /*Asian Handicap*/  3829,3830
    ];
    private Client $client;
    private $baseUrl;
    private $uri = [
        'getLiveFootballEventsWIthOdds'       => '/PartLive/GetAllFeedGames',
        'getLineFootballEvents'               => '/PartLine/GetAllFeedGames'
    ];
    public function __construct()
    {
        $this->baseUrl = config('one-x-bet.base_url');
        $this->client  = new Client(['base_uri' => $this->baseUrl, 'timeout'=>120 ]);
    }
    public function getLiveFootballEventsWIthOdds(){
        $query_param = [
            'lng'           => 'en',
            'sportid'       => 1,
            'eventtypesid'  => implode(',',self::ONE_X_BET_MARKET_LIST)
        ];
        $data = [];
        try{
            $data  = $this->client->get($this->buildUrl($this->uri['getLiveFootballEventsWIthOdds'],$query_param))->getBody()->getContents();
            return json_decode($data);
        } catch (GuzzleException $e){
            return $data;
        }
    }

    public function getLineFootballEvents(){
        $query_param = [
            'lng'           => 'en',
            'sportid'       => 1,
            'eventtypesid'  => implode(',',self::ONE_X_BET_MARKET_LIST),
        ];
        $data = [];
        try{
            $data  = $this->client->get($this->buildUrl($this->uri['getLineFootballEvents'],$query_param))->getBody()->getContents();
            return json_decode($data);
        } catch (GuzzleException $e){
            return $data;
        }
    }

    private function buildUrl($url, $params){
        return implode('/',[$this->baseUrl,$url]).'?'. Arr::query($params);
    }

    public function formatBetsToLSport($events,$data){
//get only P0, V1
        $events = DB::table('event')->select(['event.id','fixture.start_date','fixture.id as fixture_id','league.id as league_id','league.name as league_name'])
            ->join('fixture','fixture.id','=','event.fixture_id')
            ->join('scoreboard','scoreboard.event_id','=','event.id')
            ->leftJoin('league','league.id','=','fixture.league_id')
            ->where('scoreboard.status_id',2)
            ->whereIN('event.id',$events)
            ->get();
        $fixtures = $events->pluck('fixture_id');

        $participants = FixtureParticipant::select(['fixture_participant.fixture_id','fixture_participant.position','participant.id','participant.name'])
            ->leftJoin('participant','fixture_participant.participant_id','=','participant.id')
            ->whereIn('fixture_participant.fixture_id',$fixtures)
            ->get();

        $syncEvents = [];
        $events = $events->map(function ($event) use ($participants){
            $tmp_participants = $participants->filter(function ($participant) use($event){
                return $participant->fixture_id==$event->fixture_id;
            })->toArray();
            $event->participants = $tmp_participants;
            return $event;
        });


        $filteredData = collect($data)->filter(function ($event){
            return  ($event->V==1) && ($event->P==0) && (!$event->F) && ($event->T==1);
        });
        $oneXbetTeamsToSync = [];

         $filteredData->map(function ($item) use(&$oneXbetTeamsToSync){
             $oneXbetTeamsToSync[] = $item->H;
             $oneXbetTeamsToSync[] = $item->A;
        });

        $oneXbetTeamsToSync = OneXBetTeam::whereIn('name',$oneXbetTeamsToSync)->get();


        $datToFormat = $filteredData->filter(function ($event) use($events, &$syncEvents, $oneXbetTeamsToSync){
            $events = $events->filter(function ($item) use ($event, $oneXbetTeamsToSync){

                $league_name   = explode('.',$event->C);

                if(sizeof($league_name) > 1){
                    $league_name   = str_replace([' '],'',$league_name[1]);
                } else{
                    $league_name   = str_replace([' '],'',$league_name[0]);
                }

                $xBetTeamHome  = strtoupper(str_replace([' '],'',$event->H));
                $xBetTeamGuest = strtoupper(str_replace([' '],'',$event->A));

                $lSportHome    = collect($item->participants)->filter(function ($participant){
                    return $participant['position']==1;
                })->first()['name'];

                $lSportGuest   = collect($item->participants)->filter(function ($participant){
                    return $participant['position']==2;
                })->first()['name'];

                $sml           = similar_text(strtolower($league_name), strtolower(str_replace([' '],'',$item->league_name)), $lprt);
                $smt           = similar_text($league_name, $item->league_name,$tprt);
                $smh           = similar_text($xBetTeamHome, strtoupper($lSportHome),$hprt);
                $sma           = similar_text($xBetTeamGuest, strtoupper($lSportGuest),$aprt);



                if(  TickToTimestamp::transform($event->D) == Carbon::parse($item->start_date)->format('Y-m-d H:i:s') &&
                    // check league
                    $lprt > 60 &&
                    // check team
                    $tprt > 60 &&
                    //home
                    $hprt >60  &&
                    // guest
                    $aprt >60 ){
                    return true;
                }
                // check participants
                $lSportHomeId   = collect($item->participants)->filter(function ($participant){
                    return $participant['position']==1;
                })->first()['id'];

                $lSportGuestId   = collect($item->participants)->filter(function ($participant){
                    return $participant['position']==2;
                })->first()['id'];

                $oneXBetHome = $oneXbetTeamsToSync->filter(function ($item) use($event){
                    return $item->name == $event->H;
                });

                $oneXBetGuest = $oneXbetTeamsToSync->filter(function ($item) use($event){
                    return $item->name == $event->A;
                });

                if($oneXBetHome->count() && $oneXBetGuest->count()){
                    if(($oneXBetHome->first()->participant_id == $lSportHomeId) && ($oneXBetGuest->first()->participant_id == $lSportGuestId)){
                        return true;
                    }
                }
                return false;

            });
            if($events->count()){
                $syncEvents[$event->I] = $events->first()->id;
            }
            return $events->count();
        });
        $dataToInsert = [];
        foreach ($datToFormat as $event) {
            foreach ($event->EE as $key => $bet) {

                    $event_id       =   $syncEvents[$event->I];
                    $bookmaker_id   =   145;
                    $market_id      =   0;
                    $status_id      =   2;
                    $settlement_id  =   null;
                    $name           =   '';
                    $line           =   null;
                    $base_line      =   null;
                    $start_price    =   null;
                    $price          =   null;
                    $last_update    =   now()->format('Y-m-d H:i:s');

                switch ($bet->T){
                    case 1: $market_id = 1; $name = 1;
                    break;
                    case 2: $market_id = 1; $name = 'X';
                        break;
                    case 3: $market_id = 1; $name = 2;
                        break;
                    case 4: $market_id = 7; $name = '1X';
                        break;
                    case 5: $market_id = 7; $name = '12';
                        break;
                    case 6: $market_id = 7; $name = '2X';
                        break;
                    case 9: $market_id = 2; $name = 'Over';  $line = $bet->P; $base_line = $bet->P;
                        break;
                    case 10: $market_id = 2; $name = 'Under'; $line = $bet->P; $base_line = $bet->P;
                        break;
                    case 3829: $market_id = 3; $name = 1; $line = $bet->P.' (0-0)'; $base_line = $bet->P.' (0-0)';
                        break;
                    case 3830: $market_id = 3; $name = 2; $line = $bet->P.' (0-0)'; $base_line = $bet->P.' (0-0)';
                        break;

                }

                $dataToInsert[] = [
                                'event_id'       =>   $event_id,
                                'bookmaker_id'   =>   $bookmaker_id,
                                'market_id'      =>   $market_id,
                                'status_id'      =>   $status_id,
                                'settlement_id'  =>   $settlement_id,
                                'name'           =>   $name,
                                'line'           =>   $line,
                                'base_line'      =>   $base_line,
                                'start_price'    =>   $start_price,
                                'price'          =>   $bet->C,
                                'last_update'    =>   $last_update,
                ];
            }
        }
        return $dataToInsert;
    }

}
