<?php

namespace App\Services\GoalServe;

use App\Models\League;
use App\Models\Participant;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;
use Illuminate\Support\Facades\Log;

class GoalOperations
{

    private $getFixturesURL =
        'https://www.goalserve.com/getfeed/f8d30291e0484499d9fc08d7e5247a9c/soccerfixtures/data/mapping?json=1';
    private $getLeagueStandings =
        'https://www.goalserve.com/getfeed/f8d30291e0484499d9fc08d7e5247a9c/standings/';
    private $getLeagueTeams =
        'https://www.goalserve.com/getfeed/f8d30291e0484499d9fc08d7e5247a9c/soccerleague/';
    private $getTeamInfo =
        'https://www.goalserve.com/getfeed/f8d30291e0484499d9fc08d7e5247a9c/soccerstats/team/';
    /**
     * @var Client
     */
    private $guzzle;

    public function __construct()
    {
        $this->guzzle = new Client(['timeout'  => 60 * 5]);
    }

    /**
     * @return mixed
     * @throws GuzzleException
     */
    public function getFixtures()
    {
        return $this->curl($this->getFixturesURL);
    }

    /**
     * Get league standings
     *
     * @param  League  $league
     * @return mixed
     * @throws GuzzleException
     */
    public function getLeagueStandings(League $league)
    {
        $leagueId = $league->goal_league_id;
//        $leagueId = 1204;
        if ($leagueId) {
            return $this->curl($this->getLeagueStandings . $leagueId . '.xml?json=1');
        }
        return [];
    }


    /**
     * Get teams by league
     *
     * @param League $league
     * @return array|mixed
     * @throws GuzzleException
     */
    public function getLeagueTeams(League $league)
    {
        $leagueId = $league->goal_league_id;
        if ($leagueId) {
            return $this->curl($this->getLeagueTeams . $leagueId .'?json=1');
        }
        return [];
    }

    public function getTeamInfo(Participant $participant)
    {
        $teamId = $participant->goal_team_id;
        if ($teamId) {
            try {
                return $this->curl($this->getTeamInfo . $teamId . '?json=1');
            } catch (GuzzleException $e) {
                Log::error($e->getMessage());
                throw new \Exception($e->getMessage());
            }
        }
    }

    /**
     * @param  string  $url
     * @param  bool  $isPost
     *
     * @return mixed
     * @throws GuzzleException
     */
    private function curl($url, $isPost = false)
    {
        $response = $this->guzzle->request($isPost ? 'POST' : 'GET', $url);
        return $response->getBody()->getContents();
    }
}
