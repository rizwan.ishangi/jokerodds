<?php
/**
 * Created by PhpStorm.
 * User: alex.cebotari@ourbox.org
 * Date: 30.03.2021
 * Time: 18:14
 */

namespace App\Services\GoalServe;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;

/**
 * Class GoalServeService
 * @package App\Services\GoalServe
 */
class GoalServeService
{
    /**
     * @var Client $curl
     */
    private $curl;

    /**
     * @var string $getLeaguesURL
     */
    private $getLeaguesURL = 'https://www.goalserve.com/getfeed/f8d30291e0484499d9fc08d7e5247a9c/soccerfixtures/data/mapping';

    /**
     * @var string $getLeagueStandings
     */
    private $getLeagueStandings = 'https://www.goalserve.com/getfeed/f8d30291e0484499d9fc08d7e5247a9c/standings/';

    /**
     * GoalServeService constructor.
     *
     * @param  Client  $curl
     */
    public function __construct(Client $curl)
    {
        $this->curl = $curl;
    }

    /**
     * @return string
     * @throws GuzzleException
     */
    public function getLeaguesJSON(): string
    {
        return $this->curl->get($this->getLeaguesURL . '?json=1')->getBody()->getContents();
    }

    /**
     * @param  int  $leagueId
     *
     * @return string
     * @throws GuzzleException
     */
    public function getLeagueStandingsByLeagueIdJSON(int $leagueId): string
    {
        return $this->curl->get($this->getLeagueStandings . $leagueId . '.xml?json=1')->getBody()->getContents();
    }
}
