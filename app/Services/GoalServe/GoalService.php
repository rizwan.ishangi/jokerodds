<?php

namespace App\Services\GoalServe;

use App\Models\League;
use App\Models\Participant;
use GuzzleHttp\Exception\GuzzleException;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Log;

/**
 * @property mixed fixtures
 */
class GoalService
{
    /**
     * @var GoalOperations
     */
    private $goalServeOperations;

    public function __construct(GoalOperations $goalServeOperations)
    {
        return $this->goalServeOperations = $goalServeOperations;
    }

    /**
     * Fetch Fixtures from GoalServe
     *
     * @return mixed
     * @throws GuzzleException
     */
    public function fetchFixtures()
    {
        $data = json_decode($this->goalServeOperations->getFixtures());
        $this->fixtures = $data->fixtures->mapping;
        return $this->fixtures;
    }

    /**
     * Fill League with the new properties
     */
    public function storeFixtures()
    {
        $data = collect($this->fixtures);
        $leagues = League::with(['location', 'sport'])->whereHas('location')->get();

        foreach ($leagues as $league) {
            $data->filter(function ($item) use ($league) {
                return $this->goalGoalProperty($item, '@name') === $league->name &&
                        $this->goalGoalProperty($item, '@country') === $league->location->name;
            })->map(function ($value) use ($league) {
                $league->fill([
                    'goal_league_id' => $this->goalGoalProperty($value, '@id'),
                    'goal_league_name' => $this->goalGoalProperty($value, '@name')
                    ]);
                $league->save();
            });
        }
    }

    /**
     * Get property
     *
     * @param $item
     * @param $property
     * @return mixed
     */
    public function goalGoalProperty($item, $property)
    {
        return $item->$property;
    }

    public function getStandingsByLeagueIdData($leagueId)
    {
        $league = League::whereId($leagueId)->firstOrFail();
        try{
            $data = $this->goalServeOperations->getLeagueStandings($league);
        } catch (\Exception $e){
            $data = [];
        }
        if ($data) {
            $data = json_decode($data);
            return $data->standings->tournament  ?? [];
        }
        return [];
    }

    /**
     * Get standings by league ID
     *
     * @param $leagueId
     * @return mixed
     * @throws GuzzleException
     */
    public function getStandingsByLeagueId($leagueId)
    {
        $league = League::whereId($leagueId)->firstOrFail();
        $data = $this->goalServeOperations->getLeagueStandings($league);
        if ($data) {
            $data = json_decode($data);
            return response()->json($data->standings->tournament);
        }
        return [];
    }

    /**
     * Fetch and update participant data from goal service
     *
     * @throws GuzzleException
     */
    public function fetchTeamIds()
    {
        $leagues = League::all();

        foreach ($leagues as $league) {
            if (!$league->goal_league_id) {
                continue;
            }

            $data = $this->goalServeOperations->getLeagueTeams($league);

            if (empty($data)) {
                continue;
            }

            $data = json_decode($data);

            if (empty($data) || empty($data->league) || empty($data->league->team)) {
                continue;
            }

            $teams = collect($data->league->team);

            if ($teams) {
                $teams->filter(function ($item) use ($league, $data) {
                    Participant::where('name', $this->goalGoalProperty($item, '@name'))

                        ->update(['goal_team_id' => $this->goalGoalProperty($item, '@id'),
                            'goal_team_name' => $this->goalGoalProperty($item, '@name')]);
                });
            }
        }
    }

    /**
     * Fetch and update participant image from goal service
     *
     * @throws \Exception
     */
    public function fetchTeamImage()
    {
        $teams = Participant::all();
        foreach ($teams as $team) {

            $data = $this->goalServeOperations->getTeamInfo($team);

            if (empty($data)) {
                continue;
            }

            $data = json_decode($data);

            if (empty($data->teams->team->image)) {
                Log::info('skip image', [$team->goal_team_id]);
                continue;
            }

            $team->fill(['image' => $data->teams->team->image]);
            $team->save();
        }

    }

    /**
     * Get Team Info from database
     *
     * @param  int  $teamId
     *
     * @return array|JsonResponse
     */
    public function getTeamInfo(int $teamId)
    {
        $team = Participant::select(['name', 'goal_team_name', 'image'])
            ->where('goal_team_id', $teamId)
            ->first();

        if (!empty($team)) {
            return response()->json(['team' => $team], 200);
        }

        return [];
    }
}
