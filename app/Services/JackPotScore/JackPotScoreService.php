<?php


namespace App\Services\JackPotScore;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Exception\GuzzleException;


class JackPotScoreService
{
    private string $api_key;
    private Client $client;

    private $uri = [
        'getAllTournamentCalendarMatches'       => '/api/tournamentSchedule',
        'getAllTournamentCalendarMatchesByDate' => '/api/matchSchedule',
        'getAllMatchesByDate'                   => '/api/tournamentScheduleWithMatchDetail',
        'getTournamentCalendarStanding'         => '/api/teamStanding',
    ];

    public function __construct()
    {
        $this->api_key      =  config('jack-pot-score.api-key');
        $this->client       =  new Client(['base_uri' => config('jack-pot-score.base_url')]);
    }

    public function  getAllTournamentCalendarMatches($data){
        //data format  ['tmclId' => '8l3o9v8n8tva0bb2cds2dhatw']

        return $this->getData(__FUNCTION__,$data);
    }
    public function  getAllTournamentCalendarMatchesByDate($data){
        //data format  ['tmclId' => '8l3o9v8n8tva0bb2cds2dhatw', 'date'=>'2021-05-09']
        return $this->getData(__FUNCTION__,$data);
    }
    public function  getAllMatchesByDate($data){
        //data format  ['date'=>'2021-05-09']
        return $this->getData(__FUNCTION__,$data);
    }

    public function  getTournamentCalendarStanding($data){
        //data format  ['tmclId' => '8l3o9v8n8tva0bb2cds2dhatw']
        return $this->getData(__FUNCTION__,$data);
    }

    private function getData($url,$data){
        $options      = [
            'body'      => json_encode($data),
            'headers'   => [ 'api-key' => $this->api_key, 'content-type' => 'application/json'],
        ];
        try{
            $result         = $this->client->post($this->uri[$url], $options)->getBody()->getContents();
            return json_decode($result);
        } catch (GuzzleException $e){
            return [];
        }
    }
}
