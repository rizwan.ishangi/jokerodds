<?php

namespace App\Events;

use App\Models\BetStatus;
use App\Models\Event;
use App\Repositories\BetRepository;
use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class TestEcho implements ShouldBroadcast
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $message;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(string $message)
    {
        //
        $this->message=$message;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new PresenceChannel('test-channel-echo');
    }

    public function broadcastWith(){

        $event = Event::whereId(166041)->with([
            'fixture'=> function ($query) {
                $query->select(['fixture.*','league.name'])
                    ->join('league','league.id','=','fixture.league_id');
            },
            'fixture.participants' => function ($query) {
                $query->select(['fixture_participant.*','participant.*','goalserve_teams.image as team_image'])
                    ->leftJoin('goalserve_teams','fixture_participant.participant_id','=','goalserve_teams.participant_id');
            },
            'scoreboard'])
            ->whereHas('fixture', function ($query) {
                $query->where('fixture.status_id', '!=', BetStatus::STATUS_DELETED);
            })
            ->firstOrFail();

        $betRepository = new BetRepository();

        return [
            'data'=>[
                'event' => (array)$event,
                'bets' => [
                    'prematch' => (array)$betRepository->betsByEvent($event->id, false),
                    'inplay'   => (array)$betRepository->betsByEvent($event->id, true),
                ],
            ]
        ];

    }
}
