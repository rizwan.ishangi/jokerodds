<?php

namespace App\Events;

use App\Libraries\OddsConvector\OddsConvector;
use App\Models\BetStatus;
use App\Models\Event;
use App\Repositories\BetRepository;
use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Collection;

class BetsUpdated implements ShouldBroadcast
{
    use Dispatchable;
    use InteractsWithSockets;
    use SerializesModels;

    /**
     * @var int
     */
    public $eventId;
    public $bets;
    public $average;
    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(int $eventId, array $bets, array $average)
    {
        $this->eventId  = $eventId;
        $this->bets     = $bets;
        $this->average  = $average;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new PresenceChannel('event-bets.' . $this->eventId);
    }

    public function broadcastWith(){
        $eventBets = $this->bets;
        $data = collect($eventBets)->groupBy('market_id');
        $prematch = [];

        foreach ($data as $marketId => $bets) {
            if (empty($marketId)) {
                continue;
            }
            $prematch[$marketId] = collect([]);
            foreach ($bets->groupBy('base_line') as $line => $lineBets) {
                $prematch[$marketId]->push(
                    $lineBets->groupBy('bookmaker.name')->map(function ($bets) {
                        return $bets->keyBy('name')->map(function ($bet) {
                            return $bet;
                        });
                    }),
                    );
            }
            $prematch[$marketId] = $prematch[$marketId]->sortBy('line', SORT_ASC)->values();
        }
        return [
                'bets' => [
                    'prematch'   => $prematch,
                    'average'   =>$this->average
                ],
        ];
    }
}
