<?php

namespace App\Events;

use App\Models\Bookmaker;
use App\Models\Market;
use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class BetsLiveUpdated implements ShouldBroadcast
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public $bets;
    public function __construct($bets)
    {
        //
        $this->bets = $bets;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new PresenceChannel('bets-live');
    }
    public function broadcastWith(){
        $inPlay = [];
        $eventBets = $this->bets;

        $bookmakerList  = Bookmaker::whereIn('id',
            collect($eventBets)->map(function ($item){
                return $item['bookmaker_id'];
            })->unique()->toArray()
        )->get();

        $marketList     = Market::whereIn('id',
            collect($eventBets)->map(function ($item){
                return $item['market_id'];
            })->unique()->toArray())->get();


        $eventBets = collect($eventBets)->map(function ($bet) use($bookmakerList, $marketList) {

            $bet['market'] = $marketList->filter(function ($item) use ($bet) {
                return $item->id == $bet['market_id'];
            })->first()->toArray();

            $bet['bookmaker'] = $bookmakerList->filter(function ($item) use ($bet) {
                return $item->id == $bet['bookmaker_id'];
            })->first()->toArray();

            return $bet;

        })->toArray();



        $data = collect($eventBets)->groupBy('event_id');


        foreach ($data as $eventId => $eventBets) {

            $eventBets = collect($eventBets)->groupBy('market_id');

            foreach ($eventBets as $marketId => $bets) {
                if (empty($marketId)) {
                    continue;
                }

                $inPlay[$eventId][$marketId] = collect([]);
                $bets->groupBy(function ($item){
                    return (string)$item['base_line'];
                });
                foreach ($bets->groupBy(function ($item){return (string)$item['base_line'];}) as $line => $lineBets) {
                    $inPlay[$eventId][$marketId]->push(
                        $lineBets->groupBy('bookmaker.name')->map(function ($bets) {
                            return $bets->keyBy('name')->map(function ($bet) {
                                return $bet;
                            });
                        }),
                        );
                }
                $inPlay[$eventId][$marketId] = $inPlay[$eventId][$marketId]->sortBy('line', SORT_ASC)->values();
            }

        }

        return [
            'bets' => [
                'inplay'   => $inPlay
            ],
        ];
    }
}
