<?php

namespace App\Events;


use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class BetsUpdatedAverage implements ShouldBroadcast
{
    use Dispatchable, InteractsWithSockets, SerializesModels;


    public $eventId;
    public $bets;
    public $average;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(int $eventId, array $average)
    {
        $this->eventId  = $eventId;
        $this->average  = $average;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new PresenceChannel('bets-average');
    }

    public function broadcastWith(){
        return [
            'bets' => [
                'event_id'  => $this->eventId ,
                'average'   => $this->average
            ],
        ];
    }
}
