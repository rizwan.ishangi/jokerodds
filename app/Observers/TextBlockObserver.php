<?php

namespace App\Observers;

use App\Models\TextBlock;
use Illuminate\Support\Facades\Cache;

class TextBlockObserver
{
    /**
     * Handle the text block "saving" event.
     *
     * @param TextBlock $textBlock
     */
    public function saving(TextBlock $textBlock)
    {
        Cache::forget('text_blocks');
    }

    /**
     * Handle the text block "deleting" event.
     *
     * @param TextBlock $textBlock
     */
    public function deleting(TextBlock $textBlock)
    {
        Cache::forget('text_blocks');
    }
}
