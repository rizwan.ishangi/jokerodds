<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class BetHistotyRequest extends FormRequest
{
    public function rules()
    {
        return [
            'name' => [
                'required',
                'string'
            ],
            'event_id' => [
                'required',
                'numeric'
            ],
            'bookmaker_id' => [
                'required',
                'numeric'
            ],
            'market_id' => [
                'required',
                'numeric'
            ]
        ];
    }

    public function authorize()
    {
        return true;
    }
}
