<?php

namespace App\Http\Controllers;

use App\Models\Banner;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

/**
 * Class BannerController
 * @package App\Http\Controllers
 */
class BannerController extends Controller
{
    /**
     * @return JsonResponse
     */
    public function get(): JsonResponse
    {
        $data = [
            'top' => Banner::select(['id', 'link', 'img', 'order', 'views'])
                ->where('active', true)
                ->where('position', 1)
                ->inRandomOrder()
                ->limit(1)
                ->get(),
            'left' => Banner::select(['id', 'link', 'img', 'order', 'views'])
                ->where('active', true)
                ->where('position', 2)
                ->inRandomOrder()
                ->limit(3)
                ->get(),
            'right' => Banner::select(['id', 'link', 'img', 'order', 'views'])
                ->where('active', true)
                ->where('position', 3)
                ->inRandomOrder()
                ->limit(3)
                ->get(),
        ];

        foreach ($data as $key => $datum) {
            $data[$key] = $datum->sortBy('order');
            foreach ($datum as $banner) {
                $banner->views = $banner->views + 1;
                $banner->save();
            }
        }

        return response()->json(['data' => $data], 200);
    }

    /**
     * @param  Request  $request
     *
     * @return JsonResponse
     */
    public function clicked(Request $request): JsonResponse
    {
        $validator = Validator::make($request->all(), [
            'id' => 'required|numeric|exists:banners,id'
        ]);

        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()->first()], 422);
        }

        $banner = Banner::select(['id', 'clicks'])->where('id', $request->input('id'))->first();
        if (is_null($banner)) {
            return response()->json(['error' => 'Banner not found!'], 422);
        }

        $banner->clicks = $banner->clicks + 1;
        $banner->save();
        return response()->json(['message' => 'ok'], 200);
    }
}
