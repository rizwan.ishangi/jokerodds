<?php

namespace App\Http\Controllers;


use App\Models\BetStatus;
use App\Models\Event;
use App\Models\Fixture;
use App\Repositories\BetRepository;
use App\Repositories\EventRepository;
use App\Repositories\FixtureRepository;
use App\Repositories\ScoreboardStatusRepository;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Carbon;
use Vyuldashev\LaravelOpenApi\Annotations as OpenApi;

/**
 * @OpenApi\PathItem()
 */
class MatchController extends Controller
{
    /**
     * @var FixtureRepository $fixtureRepository
     */
    private $fixtureRepository;

    /**
     * MatchController constructor.
     *
     * @param  FixtureRepository  $fixtureRepository
     */
    public function __construct(FixtureRepository $fixtureRepository) {
        $this->fixtureRepository = $fixtureRepository;
    }

    /**
     * Get matches by date.
     *
     * A method to get active matches by the selected date with bets.
     *
     * @OpenApi\Operation(tags="match",method="GET")
     *
     * @param  string  $date
     *
     * @return JsonResponse
     */
    public function fullDateMatches(string $date = ''): JsonResponse
    {
        if (empty($date) || Carbon::parse($date)->isToday()) {
            $date = now();
        } else {
            $date = Carbon::parse($date);
        }

        $data = $this->fixtureRepository->getDateFixturesWithBets($date);
        return response()->json($data, 200);
    }

    public function fullDateMatchesRest(string $date = ''): JsonResponse
    {
        if (empty($date) || Carbon::parse($date)->isToday()) {
            $date = now();
        } else {
            $date = Carbon::parse($date);
        }

        $data = $this->fixtureRepository->getDateFixturesWithBets($date,0);

        return response()->json($data, 200);
    }
    public function fullDateMatchesMain(string $date = ''): JsonResponse
    {
        if (empty($date) || Carbon::parse($date)->isToday()) {
            $date = now();
        } else {
            $date = Carbon::parse($date);
        }

        $data = $this->fixtureRepository->getDateFixturesWithBets($date,1);

        return response()->json($data, 200);
    }
}
