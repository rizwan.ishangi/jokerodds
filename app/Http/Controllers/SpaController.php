<?php

namespace App\Http\Controllers;

use App\Models\TextBlock;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\View\View;

class SpaController extends Controller
{
    /**
     * Get the SPA view.
     *
     * @return Application|Factory|View
     */
    public function __invoke()
    {
        $text_blocks = TextBlock::getCached()->toArray();
        $menus = menu_get_all();
        $settings = nova_get_settings();

        return view('app.index', compact('text_blocks', 'menus', 'settings'));
    }
}
