<?php

namespace App\Http\Controllers;

use App\Models\BetStatus;
use App\Models\Event;
use App\Models\Bookmaker;
use App\Models\ReferralLink;
use App\Repositories\BetRepository;
use App\Repositories\EventRepository;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;
use Vyuldashev\LaravelOpenApi\Annotations as OpenApi;

/**
 * @OpenApi\PathItem()
 */
class EventController extends Controller
{
    /**
     * @var EventRepository $eventRepository
     */
    private $eventRepository;

    /**
     * @var BetRepository $betRepository
     */
    private $betRepository;

    public function __construct(
        EventRepository $eventRepository,
        BetRepository $betRepository
    ) {
        $this->eventRepository = $eventRepository;
        $this->betRepository = $betRepository;
    }

    /**
     * Get information about the event
     *
     * Get information about the event using its id.
     *
     * @OpenApi\Operation(tags="event",method="GET")
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(int $id)
    {
        $event = Event::whereId($id)->with([
            'fixture'=> function ($query) {
                $query->select(['fixture.*','league.name'])
                    ->join('league','league.id','=','fixture.league_id');
            },
            'fixture.participants' => function ($query) {
                $query->distinct()->select(['fixture_participant.*','participant.*','goalserve_teams.image as team_image'])
                    ->leftJoin('goalserve_teams','fixture_participant.participant_id','=','goalserve_teams.participant_id');
            },
            'scoreboard'])
            ->whereHas('fixture', function ($query) {
                $query->where('fixture.status_id', '!=', BetStatus::STATUS_DELETED);
            })
            ->firstOrFail();

        return response()->json([
            'event' => $event,
            'bets' => [
                'prematch' => $this->betRepository->betsByEvent($event->id, false),
                'inplay' => $this->betRepository->betsLiveByEvent($event->id),
            ],
        ]);
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function eventsSidebar()
    {
       $data =  Cache::remember('eventsSidebar',300,function (){
            $events = Event::select(['id', 'fixture_id', 'show_sidebar'])
                ->with([
                    'fixture' => function ($q) {
                        $q->select(['id', 'sport_id', 'league_id', 'status_id', 'start_date']);
                    },
                    'fixture.participants' => function ($query) {
                        $query->distinct()->select(['fixture_participant.*','participant.*','goalserve_teams.image as team_image'])
                            ->leftJoin('goalserve_teams','fixture_participant.participant_id','=','goalserve_teams.participant_id');
                    },
                    'scoreboard' => function ($q) {
                        $q->select(['id', 'event_id', 'status_id', 'time', 'results']);
                    },
                ])
                ->whereHas('fixture', function ($q) {
                    $q->where('fixture.status_id', '!=', BetStatus::STATUS_DELETED);
                })
                ->where('show_sidebar', true)
                ->get();
            $result = [];
            $events->each(function ($item) use (&$result) {
                $result[$item->id] = [
                    'event' => $item,
                    /*       'bets' => [
                               'prematch' => $this->betRepository->betsByEvent($item->id, false),
                               'inplay' => $this->betRepository->betsByEvent($item->id, true),
                           ]*/
                ];
            });

            return $result;
        });

        return response()->json([
            'data' => $data
        ]);
    }

    public function getReferralLink(){

        $link = ReferralLink::select(['link'])->where('active',true)->orderBy('sort_weight','DESC')->pluck('link')->toArray();
        return response()->json([
            'data' => compact('link')
        ]);
    }
}
