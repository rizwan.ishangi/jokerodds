<?php

namespace App\Http\Controllers;
use App\Http\Requests\BetHistotyRequest;
use App\Models\Bet;
use App\Models\Fixture;
use App\Models\Market;
use App\Repositories\BetRepository;
use Illuminate\Http\Request;
use Vyuldashev\LaravelOpenApi\Annotations as OpenApi;

/**
 * @OpenApi\PathItem()
 */
class BetController extends Controller
{
    /**
     * @var BetRepository
     */
    private $betRepository;

    public function __construct(BetRepository $betRepository)
    {
        $this->betRepository = $betRepository;
    }

    /**
     * Get bet average price.
     *
     * Method for obtaining the average bid price
     *
     * @OpenApi\Operation(tags="bet",method="POST")
     *
     * @param Request $request
     *
     * @return array
     */
    public function getBetAveragePrice(Request $request)
    {
        $market = Market::where('name', '1x2')->firstOrFail();
        $fixtures = Fixture::whereIn('id', $request->all())->get();

        $bets = [];
        foreach ($fixtures as $fixture) {
            $bets[$fixture->id] = $fixture->event->bets()
                ->select(\DB::raw('avg(price) as average_price'), 'event_id', 'name')
                ->where('is_latest', 1)
                ->where('market_id', $market->id)
                ->whereIn('name', [1, 'X', 2])
                ->groupBy('event_id', 'name')
                ->get();
        }

        return $bets;
    }

    /**
     * Get betting history.
     *
     * A method to get a betting history.
     *
     * @OpenApi\Operation(tags="bet",method="POST")
     *
     * @param BetHistotyRequest $request
     *
     * @return mixed
     */
    public function getBetHistory(BetHistotyRequest $request)
    {
        $data = $request->all();

        return response()->json([
            $this->betRepository->betsHistoryByEvent(
                $data['name'],
                $data['event_id'],
                $data['bookmaker_id'],
                $data['market_id'],
                $data['base_line'],
            )
        ]);
    }
}
