<?php

namespace App\Http\Controllers;

use App\Models\RememberCookie;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class RememberCookieController extends Controller
{
    /**
     * @return JsonResponse
     */
    public function get(): JsonResponse
    {
        try {
            $uuid = (string) Str::uuid();
            while (RememberCookie::where('uuid', $uuid)->first()) {
                $uuid = (string) Str::uuid();
            }

            $cookie = new RememberCookie();
            $cookie->uuid = $uuid;
            $cookie->save();

            return response()->json([
                'key' => $cookie->uuid
            ], 200);
        } catch (Exception $e) {
            return response()->json([
                'message' => 'Something went wrong =('
            ], 500);
        }
    }
}
