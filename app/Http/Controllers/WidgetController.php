<?php

namespace App\Http\Controllers;


use App\Libraries\OddsConvector\OddsConvector;
use App\Models\Bet;
use App\Models\BetLive;
use App\Models\BetStatus;
use App\Models\Event;
use App\Models\Fixture;
use App\Models\Bookmaker;
use App\Repositories\BetRepository;
use App\Repositories\EventRepository;
use Illuminate\Http\JsonResponse;

/**
 * @OpenApi\PathItem()
 */
class WidgetController extends Controller
{
    /**
     * @var EventRepository $eventRepository
     */
    /**
     * @var BetRepository $betRepository
     */
    private $betRepository;

    public function __construct(
        EventRepository $eventRepository,
        BetRepository $betRepository
    ) {

        $this->betRepository = $betRepository;
    }

    /**
     * Get matches by date.
     *
     * A method to get active matches by the selected date with bets.
     *
     * @OpenApi\Operation(tags="match",method="GET")
     *
     * @param  string  $date
     *
     * @return JsonResponse
     */
    public function index(int $id)
    {
        // 1xbet - 145 , bet365 - 4, pinnacle - 5
        // 1x2 marketId = 1
        $event      = Event::with('scoreboard')->whereId($id)->firstOrFail();
        $bookmakers = Bookmaker::select(['id','name','logo'])->whereIn('id',[ 4 ,8, 145])->get();
        $bets       = Bet::where('event_id',$id)->where('market_id',1)
                          ->whereIn('bookmaker_id',[ 4 ,8, 145])
                          ->get();

        $betsLive       = BetLive::where('event_id',$id)->where('market_id',1)
            ->whereIn('bookmaker_id',[ 4 ,8, 145])
            ->get();


        $bookmakers  = $bookmakers->map(function ($bookmaker) use ($bets, $betsLive){

            $bets = $bets->filter(function ($bet) use ($bookmaker){
                    return $bet->bookmaker_id==$bookmaker->id;
            })->map(function ($bet){
                        return collect([
                            'id'=>$bet->id,
                            'name'  =>$bet->name,
                            'status_id' =>$bet->status_id,
                            'start_price' =>$bet->start_price,
                            'price' =>$bet->price,
                            'last_update' =>$bet->last_update,
                            'is_latest' =>$bet->is_latest,
                            'is_inplay' =>$bet->is_inplay
                        ]);
            });

            $betsLive = $betsLive->filter(function ($bet) use ($bookmaker){
                return $bet->bookmaker_id==$bookmaker->id;
            })->map(function ($bet){
                return collect([
                    'id'=>$bet->id,
                    'name'  =>$bet->name,
                    'status_id' =>$bet->status_id,
                    'price' =>$bet->price,
                ]);
            });

            $in_play = $betsLive->groupBy('name')->map(function ($bets){
                $bets = collect($bets);
                return $bets->sortByDesc('id')->first();
            });

            $prematch = $bets->filter(function ($bet){
                return !$bet['is_inplay'];
            })->groupBy('name')->map(function ($bets){
                $bets = collect($bets);
                return $bets->sortByDesc('id')->first();
            });

            return [
                'id'  =>$bookmaker->id,
                'name'=>$bookmaker->name,
                'logo'=>$bookmaker->logo,
                'bets'=>[
                        'prematch' =>$prematch,
                        'inplay'   =>$in_play,
                ]
            ];
        });


        return response()->json([
            'event' => $event,
            'bookmakers' =>$bookmakers
        ]);
    }


    public function partners($widget_id,$event_id){

        return view('widget_partners',compact('widget_id','event_id'));
    }
}
