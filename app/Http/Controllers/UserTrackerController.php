<?php

namespace App\Http\Controllers;

use App\Models\UserTracker;
use App\Services\TrackerService;
use Illuminate\Http\Request;
use Vyuldashev\LaravelOpenApi\Annotations as OpenApi;

/**
 * @OpenApi\PathItem()
 */
class UserTrackerController extends Controller
{
    /**
     * @var TrackerService
     */
    private TrackerService $trackerService;

    /**
     * UserTrackerController constructor.
     *
     * @param TrackerService $trackerService
     */
    public function __construct(TrackerService $trackerService)
    {
        $this->trackerService = $trackerService;
    }

    /**
     *  Get matches tracked by ip address
     *
     * A method for obtaining information on watched matches from the user's ip address.
     *
     * @OpenApi\Operation(tags="tracker",method="GET")
     *
     * @param $ipAddress
     *
     * @return mixed
     */
    public function index(string $ipAddress)
    {
        return $this->trackerService->getTrackerMatches($ipAddress);
    }

    /**
     * Store match
     *
     * Method for saving the last watched match.
     *
     * @OpenApi\Operation(tags="tracker",method="POST")
     *
     * @param Request $request
     *
     * @return mixed
     */
    public function store(Request $request)
    {
        $data = $request->get('formData');
        return $this->trackerService->addMatchToTracker($data);
    }

    /**
     * Delete match
     *
     * Method for removing a match from the tracker list.
     *
     * @OpenApi\Operation(tags="tracker",method="DELETE")
     *
     * @param $id
     *
     * @return mixed
     */
    public function deleteMatch(int $id)
    {
        return $this->trackerService->deleteMatchFromTracker($id);
    }

    /**
     * Delete tracker
     *
     * Method for deleting tracker data of watched matches
     *
     * @OpenApi\Operation(tags="tracker",method="DELETE")
     *
     * @param $ipAddress
     *
     * @return array
     */
    public function delete(string $ipAddress)
    {
        return $this->trackerService->clearTracker($ipAddress);
    }
}
