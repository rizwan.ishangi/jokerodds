<?php

namespace App\Http\Controllers;

use App\Services\GoalServe\GoalService;
use Illuminate\Http\Request;

/**
 * Class TeamController
 * @package App\Http\Controllers
 * @OpenApi\PathItem()
 */
class TeamController extends Controller
{
    /**
     * @var GoalService $goalService
     */
    private $goalService;

    /**
     * TeamController constructor.
     *
     * @param  GoalService  $goalService
     */
    public function __construct(GoalService $goalService)
    {
        $this->goalService = $goalService;
    }

    public function getTeamInfo(int $teamId)
    {
        try {
            return $this->goalService->getTeamInfo($teamId);
        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }
}
