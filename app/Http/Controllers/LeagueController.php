<?php

namespace App\Http\Controllers;

use App\Libraries\OddsConvector\OddsConvector;
use App\Models\BetStatus;
use App\Models\Fixture;
use App\Models\League;
use App\Models\Location;
use App\Models\Market;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;
use Vyuldashev\LaravelOpenApi\Annotations as OpenApi;
use function Clue\StreamFilter\fun;

/**
 * @OpenApi\PathItem()
 */
class LeagueController extends Controller
{
    /**
     * Get available leagues
     *
     * Method for obtaining available locations with leagues.
     *
     * @OpenApi\Operation(tags="league",method="GET")
     *
     * @return Location[]|Builder[]|Collection
     */
    public function index()
    {
        $locations = Cache::remember('Locations',300,function(){
            /* Get all fixture ids that have bets loaded */
            $fixtureIds = DB::table('fixture')
                ->select(['id'])
                ->whereBetween('start_date', [
                    now()->setTime(0, 0, 0),
                    now()->addDays(365)->setTime(23, 59, 59),
                ])
                ->where('status_id', '!=', BetStatus::STATUS_DELETED);

            /* Get all fixture ids that have event with bets */
            $fixturesWithBets_sub = DB::table('event')
                ->select(['id', 'fixture_id', DB::raw('(select 1 from `bet` where `event`.`id` = `bet`.`event_id` limit 1) as `bets_count`')])
                ->whereIn('fixture_id', $fixtureIds);

            $fixturesWithBets = DB::query()->fromSub($fixturesWithBets_sub,'sub')->whereRaw("bets_count=1")->select('fixture_id')->pluck('fixture_id');

            $location_ids = Fixture::select('location_id')->whereIn('id',$fixturesWithBets)->pluck('location_id');


            $league_ids = Fixture::select('league_id')->whereIn('id',$fixturesWithBets)->pluck('league_id');


            return  $locations = Location::where('active', true)
                ->whereIn('location.id',$location_ids)
                ->where('has_fixtures', true)
                ->with([
                    'leagues' => function ($q) use($league_ids) {
                        $q->whereIn('id',$league_ids);
                        $q->where('active', true);
                        $q->where('has_fixtures', true);
                        $q->orderByDesc('sort_weight');
                    },
                ])
                ->addSelect(['location.id'])
                ->orderBy('name')
                ->get();
        });
        return $locations;
    }


    public function topLeague(){

        $league = Cache::remember('topLeagues',600,function(){
           return League::select(['id','name','img'])->where('show_sidebar','=',true)->orderBy('id','desc')->get();
        });
        return $league;
    }

    /**
     *  Get available league matches
     *
     * Method for obtaining available league games
     *
     * @OpenApi\Operation(tags="league",method="GET")
     *
     * @param  int  $leagueId
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function leagueMatches(int $leagueId)
    {
        $market = Market::where('name', '1x2')->firstOrFail();

        $league = League::query()->select([
            'id',
            'goal_league_id',
            'name',
            'goal_league_name',
            'img',
            'sport_id',
            'location_id',
            'active',
            'important',
            'has_fixtures',
            'sort_weight'
        ])->with([
            'location',
            'fixtures' => function ($query) {
                $query->where('start_date', '>=', Carbon::now()->setHours(0)->setMinutes(0)->setSeconds(0)->format('Y-m-d H:i:s'))
                    ->where('status_id','!=', BetStatus::STATUS_DELETED);
            },
            'fixtures.participants' => function ($query) {
                $query->distinct()->select(['fixture_participant.*','participant.*','goalserve_teams.image as team_image'])
                    ->leftJoin('goalserve_teams','fixture_participant.participant_id','=','goalserve_teams.participant_id')
                    ->orderBy('fixture_participant.position', 'ASC');
            },
            'fixtures.event',
            'fixtures.event.scoreboard',
            'fixtures.event.bets' => function ($query) use ($market) {
                $query->select(\DB::raw('avg(price) as average_price'), 'event_id', 'name')
                    ->where('market_id', $market->id)
                    ->whereIn('name', [1, 'X', 2])
                    ->groupBy('event_id', 'name')
                    ->where('is_latest', 1);
            },
        ])
            ->where(['id' => $leagueId])
            ->where('active', true)
            ->where('has_fixtures', true)
            ->orderBy('sort_weight', 'desc')
            ->firstOrFail()
            ->toArray();
        foreach ($league['fixtures'] as $index => &$fixture) {
            if (!empty($fixture['event']['bets'])) {
                $fixture['bets'] = collect($fixture['event']['bets'])->keyBy('name')->map(function (&$item) {
                    $item['average_price'] = (new OddsConvector($item['average_price']))->handle();
                    return $item;
                })->toArray();
            }
        }

        $league['fixtures'] = collect($league['fixtures'])->sortBy(function ($fixture) {
            return Carbon::parse($fixture['start_date'])->timestamp;
        })->groupBy(function ($fixture) {
            return Carbon::parse($fixture['start_date'])->setHours(0)->setMinutes(0)->setSeconds(0)->timestamp;
        });

        return response()->json([
            'league' => $league,
        ]);
    }

    /**
     * @param  int  $leagueId
     *
     * @return JsonResponse
     */
    public function leagueMatchesResults(int $leagueId): JsonResponse
    {
        $market = Market::where('name', '1x2')->firstOrFail();

        $league = League::query()->select([
            'id',
            'goal_league_id',
            'name',
            'goal_league_name',
            'img',
            'sport_id',
            'location_id',
            'active',
            'has_fixtures',
        ])->with([
            'location',
            'fixtures' => function ($query) {
                $query->where('start_date', '<=', Carbon::now()->setHours(0)->setMinutes(0)->setSeconds(0)->format('Y-m-d H:i:s'));
            },
            'fixtures.participants' => function ($query) {
                $query->distinct()->select(['fixture_participant.*','participant.*','goalserve_teams.image as team_image'])
                    ->leftJoin('goalserve_teams','fixture_participant.participant_id','=','goalserve_teams.participant_id')
                    ->orderBy('fixture_participant.position', 'ASC');
            },
            'fixtures.event',
            'fixtures.event.scoreboard',
            'fixtures.event.bets' => function ($query) use ($market) {
                $query->select(\DB::raw('avg(price) as average_price'), 'event_id', 'name')
                    ->where('market_id', $market->id)
                    ->whereIn('name', [1, 'X', 2])
                    ->groupBy('event_id', 'name')
                    ->where('is_latest', 1);
            },
        ])
            ->where(['id' => $leagueId])
            ->where('active', true)
            ->where('has_fixtures', true)
            ->firstOrFail()
            ->toArray();

        foreach ($league['fixtures'] as $index => &$fixture) {
            if (!empty($fixture['event']['bets'])) {
                $fixture['bets'] = collect($fixture['event']['bets'])->keyBy('name')->map(function (&$item) {
                    $item['average_price'] = (new OddsConvector($item['average_price']))->handle();
                    return $item;
                })->toArray();
            }
        }

        $league['fixtures'] = collect($league['fixtures'])->sortByDesc(function ($fixture) {
            return Carbon::parse($fixture['start_date'])->timestamp;
        })->groupBy(function ($fixture) {
            return Carbon::parse($fixture['start_date'])->setHours(0)->setMinutes(0)->setSeconds(0)->timestamp;
        });

        return response()->json([
            'league' => $league,
        ]);
    }
}
