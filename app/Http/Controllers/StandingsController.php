<?php

namespace App\Http\Controllers;

use App\Contracts\Standings\StandingInterface;
use App\Models\GoalServeTeam;
use App\Services\GoalServe\GoalService;
use GuzzleHttp\Exception\GuzzleException;
use Illuminate\Http\JsonResponse;
use Vyuldashev\LaravelOpenApi\Annotations as OpenApi;

/**
 * @OpenApi\PathItem()
 */
class StandingsController extends Controller
{

    private $standingManager;
    public function __construct(StandingInterface $standingManager){
        $this->standingManager = $standingManager;
    }

    public function leagueStandings(int $leagueId)
    {

        return $this->standingManager->leagueStandings($leagueId);
    }

    public function fullLeagueStandings(int $leagueId){

        return $this->standingManager->fullLeagueStandings($leagueId);
    }

}
