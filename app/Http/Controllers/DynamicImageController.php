<?php

namespace App\Http\Controllers;

use App\Libraries\ImageGenerator;
use App\Models\GoalServeTeam;
use App\Models\League;
use App\Models\Participant;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\Routing\ResponseFactory;
use Illuminate\Http\Response;

class DynamicImageController extends Controller
{
    /**
     * @var array|string[]
     */
    private $allowedEntities = [
        'league' => League::class,
        'goal_serve_team' => GoalServeTeam::class,
        'participant'     => Participant::class
    ];

    /**
     * @var array|string[]
     */
    private $allowedSizes = [
        '64',
        '170'
    ];

    /**
     * @param $entity
     * @param $id
     * @param $property
     * @param $size
     * @return Application|ResponseFactory|Response|object
     */
    public function __invoke($entity, $id, $property, $size)
    {
        try {
            if (!in_array($entity, array_keys($this->allowedEntities)) || !in_array($size, $this->allowedSizes)) {
                return response('Not found')
                    ->setStatusCode(404);
            }
            $model = $this->allowedEntities[$entity]::find($id);

            if (!$model) {
                return response('Not found')
                    ->setStatusCode(404);
            }
            if ($entity=='participant'){
                return ImageGenerator::generate($model->$property, $size,$id);
            }
            return ImageGenerator::render($model->$property, $size);
        } catch (\Throwable $exception) {
            \Log::error($exception);
            return response('Not found')
                ->setStatusCode(404);
        }
    }
}
