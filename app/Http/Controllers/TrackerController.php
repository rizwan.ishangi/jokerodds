<?php

namespace App\Http\Controllers;

use App\Libraries\OddsConvector\OddsConvector;
use App\Models\Bet;
use App\Models\BetStatus;
use App\Models\Event;
use App\Models\RememberCookie;
use App\Models\Tracker;
use Exception;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redis;
use Validator;

/**
 * Class TrackerController
 * @package App\Http\Controllers
 */
class TrackerController extends Controller
{
    /**
     * @param Request $request
     *
     * @return \Illuminate\Validation\Validator
     */
    private function validateRequest(Request $request): \Illuminate\Validation\Validator
    {
        return Validator::make($request->all(), [
            'event_id' => 'required|numeric|exists:event,id',
            'market_id' => 'required|numeric|exists:markets,id',
            'name' => 'required|string',
        ]);
    }

    /**
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function remember(Request $request): JsonResponse
    {
        try {
            $validator = $this->validateRequest($request);
            if ($validator->fails()) {
                return response()->json(['error' => $validator->errors()->first()], 400);
            }

            $cookie = RememberCookie::select(['id'])->where('uuid', $request->header('X-REMEMBER-COOKIE'))->first();
            if (is_null($cookie)) {
                return response()->json(['error' => 'Cookie not found'], 400);
            }


            $result = Tracker::updateOrCreate(
                ['remember_cookie_id' => $cookie->id,
                    'event_id' => $request->input('event_id'),
                    'market_id' => $request->input('market_id'),
                    'name' => $request->input('name'),
                    'base_line' => $request->input('line')
                ]
            );
            if (!$result) {
                return response()->json(['error' => 'Could not save the tracker =('], 400);
            }

            return response()->json(['message' => 'ok'], 200);
        } catch (Exception $e) {
            return response()->json(['error' => 'Server error'], 500);
        }
    }

    /**
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function forget(Request $request): JsonResponse
    {
        try {
            $validator = $this->validateRequest($request);
            if ($validator->fails()) {
                return response()->json(['error' => $validator->errors()->first()], 400);
            }

            $cookie = RememberCookie::select(['id'])->where('uuid', $request->header('X-REMEMBER-COOKIE'))->first();
            if (is_null($cookie)) {
                return response()->json(['error' => 'Cookie not found'], 400);
            }

            Tracker::where('remember_cookie_id', $cookie->id)
                ->where('event_id', $request->input('event_id'))
                ->where('market_id', $request->input('market_id'))
                ->where('name', $request->input('name'))
                ->where('base_line', $request->input('line'))
                ->delete();
            return response()->json(['message' => 'ok'], 200);
        } catch (Exception $e) {
            return response()->json(['error' => $e->getMessage()], 500);
        }
    }

    /**
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function forgetAll(Request $request): JsonResponse
    {
        try {
            $cookie = RememberCookie::select(['id'])->where('uuid', $request->header('X-REMEMBER-COOKIE'))->first();
            if (is_null($cookie)) {
                return response()->json(['error' => 'Cookie not found'], 400);
            }

            Tracker::where('remember_cookie_id', $cookie->id)->delete();
            return response()->json(['message' => 'ok'], 200);
        } catch (Exception $e) {
            return response()->json(['error' => $e->getMessage()]);
        }
    }

    /**
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function bets(Request $request): JsonResponse
    {
        try {
            $cookie = RememberCookie::select(['id'])->where('uuid', $request->header('X-REMEMBER-COOKIE'))->first();
            if (is_null($cookie)) {
                return response()->json(['error' => 'Cookie not found'], 400);
            }

            $trackers = Tracker::select(['user_id', 'remember_cookie_id', 'event_id', 'market_id', 'name','base_line'])
                ->where('remember_cookie_id', $cookie->id)
                ->get();

            if ($trackers->isEmpty()) {
                return response()->json(['data' => []], 200);
            }

            $data = [];
            $test_redis = [];
                //prepare data to collect from redis
                $redisKeys  = [];
                //if empty redis get from DB
                //prepare keys to redis
                foreach ($trackers as  $tracker) {
                    $key = 'average#'.$tracker->event_id.'#'.$tracker->market_id.'#'.$tracker->name;
                    if(!is_null($tracker->base_line)) {
                        $key .= $tracker->base_line;
                    }

                    $redisData = Redis::get($key);
                    if(!is_null($redisData) && !empty($value)){
                        $values = explode('#',$value);

                        $test_redis[]=[
                            'event_id'  => $tracker->event_id,
                            'market_id' => $tracker->market_id,
                            'name'      => $tracker->name,
                            'base_line' => $tracker->base_line,
                            'line' => $tracker->base_line,
                            'average'   => $values[1] ?? ''
                        ];
                    } else {

                        $average_bets = Bet::select([DB::raw('avg(price) as average'),DB::raw('max(price) as highest'),'market_id', 'name','base_line'])
                            ->where('event_id', $tracker->event_id)
                            ->where('market_id', $tracker->market_id)
                            ->where('is_latest',1)
                            ->where('name',$tracker->name)
                            ->where('base_line', $tracker->base_line)
                            ->where('status_id', '!=', BetStatus::STATUS_DELETED)
                            ->groupBy(['market_id','base_line', 'name'])
                            ->get();

                        foreach ($average_bets as $average_bet) {
                            $test_redis[] =[
                                'market_id' => $average_bet->market_id,
                                'average'   => (new OddsConvector($average_bet->average))->handle(),
                                'highest'   => $average_bet->highest,
                                'name'      => $average_bet->name,
                                'base_line' => $average_bet->base_line,
                                'line'      => $average_bet->line,
                                'event_id'  =>  $tracker->event_id
                            ];
                        }
                    }

                }
                // get average from redis
                $eventsInTracker = collect($test_redis)->map(function ($item){
                    return $item->event_id ?? $item['event_id'];
                })->unique()->toArray();

                $trackerEvents = Event::select(['id', 'fixture_id'])->with(
                    [
                        'fixture' => function ($q) {
                            $q->select(['id', 'sport_id', 'league_id', 'status_id', 'start_date']);
                        },
                        'fixture.participants' => function ($q) {
                            $q->select(['participant.id', 'name', 'image']);
                        },
                        'scoreboard' => function ($q) {
                            $q->select(['id', 'event_id', 'status_id', 'time', 'results']);
                        },
                    ]
                )->whereIn('id', $eventsInTracker)->get();

                $data = collect($test_redis)->map(function ($item) use ($trackerEvents){
                    $item['event'] = $trackerEvents->filter(function ($ev) use ($item){
                        return $ev->id==$item['event_id'];
                    })->values()->toArray()[0];

                    return [$item];
                });

/*
            foreach ($trackers->groupBy(['event_id', 'market_id','base_line']) as $tracker) {
                foreach ($tracker as $track) {
                    foreach ($track as $element) {
                        foreach ($element as $item) {
                            // Get all bets that matches event + market + name filter

                            $bets = Bet::select([
                                'id',
                                'event_id',
                                'market_id',
                                'status_id',
                                'name',
                                'line',
                                'base_line',
                                'start_price',
                                'price',
                            ])
                                ->where('event_id', $item['event_id'])
                                ->where('market_id', $item['market_id'])
                                ->where('name', $item['name'])
                                ->where('is_latest', 1)
                                ->where('status_id', '!=', BetStatus::STATUS_DELETED)
                                ->where('base_line', $item['base_line'])
                                ->orderBy('price', 'desc')
                                ->get();
                            if ($bets->isEmpty()) {
                                return response()->json(['data' => []], 200);
                            }

                            // Filtering bets to match trackers data
                            $betsFiltered = $bets->reject(function ($item) use ($trackers) {
                                foreach ($trackers as $tracker) {
                                    if ($item->event_id === $tracker->event_id   &&
                                        $item->market_id === $tracker->market_id &&
                                        $item->name === $tracker->name           &&
                                        $item->base_line === $tracker->base_line
                                    ) {
                                        return false;
                                    }
                                }
                                return true;
                            });

                            // Getting max value for each group
                            $collection = $betsFiltered->groupBy(['event_id', 'market_id', 'base_line'])->map(function ($events) {
                                return $events->map(function ($markets) {
                                    return $markets->first();
                                });
                            });

                            // Get clear data and remove unused trash
                            $betsIds = $collection->flatten(2)->pluck('id')->toArray();
                            unset($collection, $betsFiltered);

                            // Filtering bets
                            $bets = $bets->reject(function ($bet) use ($betsIds) {
                                if (in_array($bet->id, $betsIds)) {
                                    return false;
                                }
                                return true;
                            });

                            // Loading data
                            $data [] = $bets->load([
                                'event' => function ($query) {
                                    $query->select(['id', 'fixture_id']);
                                },
                                'event.fixture' => function ($q) {
                                    $q->select(['id', 'sport_id', 'league_id', 'status_id', 'start_date']);
                                },
                                'event.fixture.participants' => function ($q) {
                                    $q->select(['participant.id', 'name', 'image']);
                                },
                                'event.scoreboard' => function ($q) {
                                    $q->select(['id', 'event_id', 'status_id', 'time', 'results']);
                                },
                            ]);

                        }

                    }
                }
            }
*/
            return response()->json(['data' => $data, /*'test'=>$test_redis*/], 200);
        } catch (Exception $e) {
            return response()->json(['error' => $e->getMessage()], 500);
        }
    }
}
