<?php

namespace App\Policies;

use App\Models\Admin;
use Illuminate\Auth\Access\HandlesAuthorization;

class BookmakerPolicy
{
    use HandlesAuthorization;

    public function view(Admin $user)
    {
        return $user->hasPermissionTo('view bookmakers');
    }

    public function create(Admin $user)
    {
        return $user->hasPermissionTo('create bookmakers');
    }

    public function update(Admin $user)
    {
        return $user->hasPermissionTo('edit bookmakers');
    }

    public function delete(Admin $user)
    {
        return $user->hasPermissionTo('delete bookmakers');
    }

    public function restore(Admin $user)
    {
        return $user->hasPermissionTo('restore bookmakers');
    }

    public function forceDelete(Admin $user)
    {
        return $user->hasPermissionTo('force delete bookmakers');
    }
}
