<?php

namespace App\Policies;

use App\Models\Admin;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class AdminPolicy
{
    use HandlesAuthorization;

    public function view(Admin $user)
    {
        return $user->hasPermissionTo('view admins');
    }

    public function create(Admin $user)
    {
        return $user->hasPermissionTo('create admins');
    }

    public function update(Admin $user)
    {
        return $user->hasPermissionTo('edit admins');
    }

    public function delete(Admin $user)
    {
        return $user->hasPermissionTo('delete admins');
    }

    public function restore(Admin $user)
    {
        return $user->hasPermissionTo('restore admins');
    }

    public function forceDelete(Admin $user)
    {
        return $user->hasPermissionTo('force delete admins');
    }
}
