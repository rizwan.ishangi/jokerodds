<?php

namespace App\Policies;

use App\Models\Admin;
use Illuminate\Auth\Access\HandlesAuthorization;

class TeamPolicy
{
    use HandlesAuthorization;

    public function view(Admin $user)
    {
        return $user->hasPermissionTo('view teams');
    }

    public function create(Admin $user)
    {
        return $user->hasPermissionTo('create teams');
    }

    public function update(Admin $user)
    {
        return $user->hasPermissionTo('edit teams');
    }

    public function delete(Admin $user)
    {
        return $user->hasPermissionTo('delete teams');
    }

    public function restore(Admin $user)
    {
        return $user->hasPermissionTo('restore teams');
    }

    public function forceDelete(Admin $user)
    {
        return $user->hasPermissionTo('force delete teams');
    }
}
