<?php

namespace App\Policies;

use Aiman\MenuBuilder\Http\Models\Menu;
use App\Models\Admin;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class MenuBuilderPolicy
{
    use HandlesAuthorization;

    public function view(Admin $user)
    {
        return $user->hasPermissionTo('view menus');
    }

    public function create(Admin $user)
    {
        return $user->hasPermissionTo('create menus');
    }

    public function update(Admin $user)
    {
        return $user->hasPermissionTo('edit menus');
    }

    public function delete(Admin $user)
    {
        return $user->hasPermissionTo('delete menus');
    }

    public function restore(Admin $user)
    {
        return $user->hasPermissionTo('restore menus');
    }

    public function forceDelete(Admin $user)
    {
        return $user->hasPermissionTo('force delete menus');
    }
}
