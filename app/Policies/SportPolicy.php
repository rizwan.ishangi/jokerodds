<?php

namespace App\Policies;

use App\Models\Admin;
use Illuminate\Auth\Access\HandlesAuthorization;

class SportPolicy
{
    use HandlesAuthorization;

    public function view(Admin $user)
    {
        return $user->hasPermissionTo('view sports');
    }

    public function create(Admin $user)
    {
        return $user->hasPermissionTo('create sports');
    }

    public function update(Admin $user)
    {
        return $user->hasPermissionTo('edit sports');
    }

    public function delete(Admin $user)
    {
        return $user->hasPermissionTo('delete sports');
    }

    public function restore(Admin $user)
    {
        return $user->hasPermissionTo('restore sports');
    }

    public function forceDelete(Admin $user)
    {
        return $user->hasPermissionTo('force delete sports');
    }
}
