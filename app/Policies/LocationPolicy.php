<?php

namespace App\Policies;

use App\Models\Admin;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class LocationPolicy
{
    use HandlesAuthorization;

    public function view(Admin $user)
    {
        return $user->hasPermissionTo('view locations');
    }

    public function create(Admin $user)
    {
        return $user->hasPermissionTo('create locations');
    }

    public function update(Admin $user)
    {
        return $user->hasPermissionTo('edit locations');
    }

    public function delete(Admin $user)
    {
        return $user->hasPermissionTo('delete locations');
    }

    public function restore(Admin $user)
    {
        return $user->hasPermissionTo('restore locations');
    }

    public function forceDelete(Admin $user)
    {
        return $user->hasPermissionTo('force delete locations');
    }
}
