<?php

namespace App\Policies;

use App\Models\Admin;
use Illuminate\Auth\Access\HandlesAuthorization;

class TextBlockPolicy
{
    use HandlesAuthorization;

    public function view(Admin $user)
    {
        return $user->hasPermissionTo('view text blocks');
    }

    public function create(Admin $user)
    {
        return $user->hasPermissionTo('create text blocks');
    }

    public function update(Admin $user)
    {
        return $user->hasPermissionTo('edit text blocks');
    }

    public function delete(Admin $user)
    {
        return $user->hasPermissionTo('delete text blocks');
    }

    public function restore(Admin $user)
    {
        return $user->hasPermissionTo('restore text blocks');
    }

    public function forceDelete(Admin $user)
    {
        return $user->hasPermissionTo('force delete text blocks');
    }
}
