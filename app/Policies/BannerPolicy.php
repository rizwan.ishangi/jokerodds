<?php

namespace App\Policies;

use App\Models\Admin;
use Illuminate\Auth\Access\HandlesAuthorization;

class BannerPolicy
{
    use HandlesAuthorization;

    public function view(Admin $user)
    {
        return $user->hasPermissionTo('view banners');
    }

    public function create(Admin $user)
    {
        return $user->hasPermissionTo('create banners');
    }

    public function update(Admin $user)
    {
        return $user->hasPermissionTo('edit banners');
    }

    public function delete(Admin $user)
    {
        return $user->hasPermissionTo('delete banners');
    }

    public function restore(Admin $user)
    {
        return $user->hasPermissionTo('restore banners');
    }

    public function forceDelete(Admin $user)
    {
        return $user->hasPermissionTo('force delete banners');
    }
}
