<?php

namespace App\Policies;

use App\Models\Admin;
use Illuminate\Auth\Access\HandlesAuthorization;

class UserPolicy
{
    use HandlesAuthorization;

    public function view(Admin $user)
    {
        return $user->hasPermissionTo('view users');
    }

    public function create(Admin $user)
    {
        return $user->hasPermissionTo('create users');
    }

    public function update(Admin $user)
    {
        return $user->hasPermissionTo('edit users');
    }

    public function delete(Admin $user)
    {
        return $user->hasPermissionTo('delete users');
    }

    public function restore(Admin $user)
    {
        return $user->hasPermissionTo('restore users');
    }

    public function forceDelete(Admin $user)
    {
        return $user->hasPermissionTo('force delete users');
    }
}
