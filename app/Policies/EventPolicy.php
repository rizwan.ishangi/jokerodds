<?php

namespace App\Policies;

use App\Models\Admin;
use Illuminate\Auth\Access\HandlesAuthorization;

class EventPolicy
{
    use HandlesAuthorization;

    public function view(Admin $user)
    {
        return $user->hasPermissionTo('view events');
    }

    public function create(Admin $user)
    {
        return $user->hasPermissionTo('create events');
    }

    public function update(Admin $user)
    {
        return $user->hasPermissionTo('edit events');
    }

    public function delete(Admin $user)
    {
        return $user->hasPermissionTo('delete events');
    }

    public function restore(Admin $user)
    {
        return $user->hasPermissionTo('restore events');
    }

    public function forceDelete(Admin $user)
    {
        return $user->hasPermissionTo('force delete events');
    }
}
