<?php

namespace App\Policies;

use App\Models\Admin;
use Illuminate\Auth\Access\HandlesAuthorization;

class RolePolicy
{
    use HandlesAuthorization;

    public function view(Admin $user)
    {
        return $user->hasPermissionTo('view roles') || Admin::count() == 1;
    }

    public function create(Admin $user)
    {
        return $user->hasPermissionTo('create roles') || Admin::count() == 1;
    }

    public function update(Admin $user)
    {
        return $user->hasPermissionTo('edit roles') || Admin::count() == 1;
    }

    public function delete(Admin $user)
    {
        return $user->hasPermissionTo('delete roles') || Admin::count() == 1;
    }
}
