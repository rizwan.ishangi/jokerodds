<?php

namespace App\Policies;

use App\Models\Admin;
use Illuminate\Auth\Access\HandlesAuthorization;

class PagePolicy
{
    use HandlesAuthorization;

    public function view(Admin $user)
    {
        return $user->hasPermissionTo('view pages');
    }

    public function create(Admin $user)
    {
        return $user->hasPermissionTo('create pages');
    }

    public function update(Admin $user)
    {
        return $user->hasPermissionTo('edit pages');
    }

    public function delete(Admin $user)
    {
        return $user->hasPermissionTo('delete pages');
    }

    public function restore(Admin $user)
    {
        return $user->hasPermissionTo('restore pages');
    }

    public function forceDelete(Admin $user)
    {
        return $user->hasPermissionTo('force delete pages');
    }
}
