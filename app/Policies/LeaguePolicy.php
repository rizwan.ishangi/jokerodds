<?php

namespace App\Policies;

use App\Models\Admin;
use Illuminate\Auth\Access\HandlesAuthorization;

class LeaguePolicy
{
    use HandlesAuthorization;

    public function view(Admin $user)
    {
        return $user->hasPermissionTo('view leagues');
    }

    public function create(Admin $user)
    {
        return $user->hasPermissionTo('create leagues');
    }

    public function update(Admin $user)
    {
        return $user->hasPermissionTo('edit leagues');
    }

    public function delete(Admin $user)
    {
        return $user->hasPermissionTo('delete leagues');
    }

    public function restore(Admin $user)
    {
        return $user->hasPermissionTo('restore leagues');
    }

    public function forceDelete(Admin $user)
    {
        return $user->hasPermissionTo('force delete leagues');
    }
}
