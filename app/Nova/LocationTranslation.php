<?php

namespace App\Nova;

use App\Models\LocationTranslation as LocationTranslationModel;
use Illuminate\Http\Request;
use Laravel\Nova\Fields\BelongsTo;
use Laravel\Nova\Fields\ID;
use Laravel\Nova\Fields\Number;
use Laravel\Nova\Fields\Text;

class LocationTranslation extends Resource
{
    public static $model = LocationTranslationModel::class;

    public static $title = 'name';

    public static $search = [
        'id', 'name'
    ];

    public static $displayInNavigation = false;

    public function fields(Request $request)
    {
        return [
            ID::make()->sortable(),

            Text::make('Name')
                ->sortable()
                ->rules('required'),

            Number::make('Language Id')
                ->sortable()
                ->rules('required', 'integer'),

            BelongsTo::make('Location', 'location', Location::class)->hideFromIndex(),
        ];
    }

    public function cards(Request $request)
    {
        return [];
    }

    public function filters(Request $request)
    {
        return [];
    }

    public function lenses(Request $request)
    {
        return [];
    }

    public function actions(Request $request)
    {
        return [];
    }
}
