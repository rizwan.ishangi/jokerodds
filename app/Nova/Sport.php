<?php

namespace App\Nova;

use App\Models\Sport as SportModel;
use Davidpiesse\NovaToggle\Toggle;
use Illuminate\Http\Request;
use Laravel\Nova\Fields\HasOne;
use Laravel\Nova\Fields\ID;
use Laravel\Nova\Fields\Image;
use Laravel\Nova\Fields\Number;
use Laravel\Nova\Fields\Text;
use Titasgailius\SearchRelations\SearchesRelations;

class Sport extends Resource
{
    use SearchesRelations;

    public static $model = SportModel::class;

    public static $title = 'name';

    public static $search = [
        //
    ];

    public static $group = 'Odds';

    public static $searchable = false;

    public static $searchRelations = [
        'translation' => ['name'],
    ];

    public function fields(Request $request)
    {
        return [
            ID::make()->sortable(),

            Text::make("Name", "id")
                ->resolveUsing(function ($sportId) {
                    return \App\Models\SportTranslation::where('sport_id', $sportId)->first()->name ?? "-";
                })
                ->onlyOnIndex()
                ->sortable(),

            Number::make('Display Priority')
                ->sortable()
                ->rules('required', 'integer'),

            Toggle::make('Active', 'active')
                ->trueValue('1')->falseValue('0')->showLabels()
                ->rules('required', 'in:0,1')->sortable(),

            Image::make('Img', 'img')->disableDownload()->rules('mimes:png,jpg,jpeg'),

            HasOne::make('SportTranslation', 'translation', SportTranslation::class)->inline($request)->hideFromIndex(),
        ];
    }

    public function cards(Request $request)
    {
        return [];
    }

    public function filters(Request $request)
    {
        return [
            new Filters\StatusFilter
        ];
    }

    public function lenses(Request $request)
    {
        return [];
    }

    public function actions(Request $request)
    {
        return [];
    }
}
