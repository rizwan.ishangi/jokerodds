<?php

namespace App\Nova\Filters;

use Illuminate\Http\Request;
use Laravel\Nova\Filters\Filter;

class StatusFilter extends Filter
{
    public $name = 'Status';

    public $component = 'select-filter';

    public function apply(Request $request, $query, $value)
    {
        return $query->where('active', $value);
    }

    public function options(Request $request)
    {
        return [
            'Active' => 1,
            'Inactive' => 0,
        ];
    }
}
