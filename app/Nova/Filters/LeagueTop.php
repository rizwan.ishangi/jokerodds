<?php

namespace App\Nova\Filters;

use Illuminate\Http\Request;
use Laravel\Nova\Filters\Filter;

class LeagueTop extends Filter
{
    public function apply(Request $request, $query, $value)
    {
        return $query->where('show_sidebar', $value);
    }

    public function options(Request $request)
    {
        return [
            'Top leagues' => 1,
            'Not top leagues' => 0,
        ];
    }
}
