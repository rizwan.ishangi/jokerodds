<?php

namespace App\Nova\Filters;

use App\Models\Location;
use Illuminate\Http\Request;
use Laravel\Nova\Filters\Filter;

class LeagueCountry extends Filter
{
    public $component = 'select-filter';

    public function apply(Request $request, $query, $value)
    {
        return $query->whereHas('location.translation', function ($query) use ($value) {
            $query->where('location_id', $value);
        });
    }

    /**
     * Get the filter's available options.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function options(Request $request)
    {
        $locations = Location::with(['translation'])
            ->get()
            ->sortBy('translation.name')
            ->pluck('id', 'translation.name');


        return $locations->toArray();
    }
}
