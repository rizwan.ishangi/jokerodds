<?php

namespace App\Nova\Filters;

use Illuminate\Http\Request;
use Laravel\Nova\Filters\Filter;

class TeamImage extends Filter
{
    public $name = 'Image';

    public function apply(Request $request, $query, $value)
    {
        if ($value == 1) {
            $query->whereNotNull('image');
        } else {
            $query->whereNull('image');
        }


        return $query;
    }

    public function options(Request $request)
    {
        return [
            'With image' => 1,
            'Without image' => 0
        ];
    }
}
