<?php

namespace App\Nova;

use App\Models\Bookmaker as BookmakerModel;
use Davidpiesse\NovaToggle\Toggle;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Laravel\Nova\Fields\ID;
use Laravel\Nova\Fields\Image;
use Laravel\Nova\Fields\Number;
use Laravel\Nova\Fields\Text;
use OptimistDigital\MultiselectField\Multiselect;

class Bookmaker extends Resource
{
    public static $model = BookmakerModel::class;

    public static $title = 'name';

    public static $search = [
        'name'
    ];

    public static $group = 'Odds';

    public function fields(Request $request)
    {
        return [
            ID::make()->sortable(),

            Text::make('Name')
                ->sortable()
                ->rules('required'),

            Text::make('Ref', 'ref')->rules('nullable', 'string', 'url'),

            Image::make('Logo', 'logo')->disableDownload()->rules('mimes:png,jpg,jpeg'),

            Toggle::make('Active', 'active')
                ->trueValue('1')->falseValue('0')->showLabels()
                ->rules('required', 'in:0,1')->sortable(),
            Toggle::make('Selected', 'selected')
                ->trueValue('1')->falseValue('0')->showLabels()
                ->rules('required', 'in:0,1')->sortable(),
            Number::make('Sort weight')
                ->rules('nullable', 'integer', 'min:0', 'max:999')
                ->sortable(),
            MultiSelect::make('Restrict countries')
                ->options(DB::table('countries')
                ->select('id','name')->get()
                ->pluck('name','id')
                ->toArray())
                ->optionsLimit(10)
        ];
    }

    public function cards(Request $request)
    {
        return [];
    }

    public function filters(Request $request)
    {
        return [
            new Filters\StatusFilter
        ];
    }

    public function lenses(Request $request)
    {
        return [];
    }

    public function actions(Request $request)
    {
        return [];
    }
}
