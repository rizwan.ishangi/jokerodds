<?php

namespace App\Nova;

use App\Models\Fixture as FixtureModel;
use Illuminate\Http\Request;
use Laravel\Nova\Fields\BelongsTo;
use Laravel\Nova\Fields\BelongsToMany;
use Laravel\Nova\Fields\Date;
use Laravel\Nova\Fields\HasOne;
use Laravel\Nova\Fields\ID;
use Laravel\Nova\Fields\Number;

class Fixture extends Resource
{
    public static $model = FixtureModel::class;

    public static $title = 'participantsName';

    public static $search = [
        //
    ];

    public static $group = 'Odds';

    public static $displayInNavigation = false;

    public function fields(Request $request)
    {
        return [
            ID::make()->sortable(),

            Number::make('Sport Id')
                ->sortable()
                ->rules('required', 'integer'),

            Number::make('Status Id')
                ->sortable()
                ->rules('required', 'integer'),

            Date::make('Start Date')
                ->sortable()
                ->rules('nullable', 'date'),

            Date::make('Last Update')
                ->sortable()
                ->rules('nullable', 'date'),

            HasOne::make('Event', 'event', Event::class),

            BelongsTo::make('League', 'league', League::class),

            BelongsTo::make('Location', 'location', Location::class),

            BelongsToMany::make('Participants', 'participants', Participant::class)
        ];
    }

    public function cards(Request $request)
    {
        return [];
    }

    public function filters(Request $request)
    {
        return [];
    }

    public function lenses(Request $request)
    {
        return [];
    }

    public function actions(Request $request)
    {
        return [];
    }
}
