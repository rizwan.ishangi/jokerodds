<?php

namespace App\Nova;


use App\Models\Event as EventModel;
use Illuminate\Http\Request;
use Laravel\Nova\Fields\BelongsTo;
use Laravel\Nova\Fields\DateTime;
use Laravel\Nova\Fields\ID;
use Laravel\Nova\Fields\Text;
use Titasgailius\SearchRelations\SearchesRelations;

class Event extends Resource
{
    use SearchesRelations;

    public static $model = EventModel::class;

    public static $title = 'name';

    public static $search = [
        'id', 'bets_last_update'
    ];

    public static $group = 'Odds';

    public static $searchRelations = [
        'fixture.participants' => ['name'],
    ];

    public function fields(Request $request)
    {
        return [
            ID::make()->sortable(),

            BelongsTo::make('Name', 'fixture', Fixture::class),

            DateTime::make('Bets Last Update')
                ->sortable()
                ->rules('required')->exceptOnForms(),
        ];
    }

    public function cards(Request $request)
    {
        return [];
    }

    public function filters(Request $request)
    {
        return [];
    }

    public function lenses(Request $request)
    {
        return [];
    }

    public function actions(Request $request)
    {
        return [];
    }
}
