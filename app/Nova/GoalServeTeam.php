<?php

namespace App\Nova;

use App\Models\GoalServeTeam as GoalServeTeamModel;

use App\Nova\Filters\TeamImage;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use Laravel\Nova\Fields\BelongsTo;
use Laravel\Nova\Fields\ID;
use Laravel\Nova\Fields\Image;
use Laravel\Nova\Fields\Number;
use Laravel\Nova\Fields\Text;
use Laravel\Nova\Http\Requests\NovaRequest;

class GoalServeTeam extends Resource
{
    const DEFAULT_INDEX_ORDER = 'name';

    public static $model = GoalServeTeamModel::class;

    public static $title = 'name';

    public static $search = [
        'name'
    ];

    public static $group = 'Odds';

    public function fields(Request $request)
    {
        return [
            ID::make()->sortable(),

            Text::make('Name')
                ->sortable()
                ->rules('required'),

            Number::make('Goalserve ID', 'team_id')
                ->exceptOnForms(),

            Image::make('Image')
                ->sortable(),

            BelongsTo::make('Participant', 'participant', Participant::class)->searchable()->hideFromIndex(),
        ];
    }

    public function cards(Request $request)
    {
        return [];
    }

    public function filters(Request $request)
    {
        return [
            new TeamImage()
        ];
    }

    public function lenses(Request $request)
    {
        return [];
    }

    public function actions(Request $request)
    {
        return [];
    }

    public static function label() {
        return 'Teams';
    }

    public static function indexQuery(NovaRequest $request, $query)
    {
        $query->when(empty($request->get('orderBy')), function(Builder $q) {
            $q->getQuery()->orders = [];

            return $q->orderBy(static::DEFAULT_INDEX_ORDER);
        });
    }
}
