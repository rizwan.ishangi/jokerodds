<?php

namespace App\Nova;

use App\Models\Page as PageModel;
use Illuminate\Http\Request;
use Laravel\Nova\Fields\ID;
use Laravel\Nova\Fields\Text;
use Laravel\Nova\Fields\Textarea;
use BayAreaWebPro\NovaFieldCkEditor\CkEditor;

class Page extends Resource
{
    public static $model = PageModel::class;

    public static $title = 'title';

    public static $search = [
        'title',
        'content',
    ];

    public static $group = 'CMS';

    public function fields(Request $request)
    {
        return [
            ID::make()
                ->exceptOnForms()
                ->sortable(),

            Text::make('Title')
                ->rules('required','string')
                ->stacked(),

            Text::make('Slug')
                ->rules('required','string')
                ->hideFromIndex()
                ->stacked(),

            CkEditor::make('Content')
                ->rules('nullable','string')
                ->mediaBrowser()
                ->linkBrowser()
                ->snippets([
                    ['name' =>'Cool Snippet', 'html'=> '<h1>Snippet1</h1>'],
                    ['name' =>'Cool Snippet', 'html'=> '<h1>Snippet2</h1>'],
                    ['name' =>'Cool Snippet', 'html'=> '<h1>Snippet3</h1>'],
                    ['name' =>'Cool Snippet', 'html'=> '<h1>Snippet4</h1>'],
                ])
                ->hideFromIndex()
                ->stacked(),

            Text::make('Meta Title')
                ->rules('nullable','string'),

            Textarea::make('Meta Description')
                ->rules('nullable','string'),
        ];
    }

    public function cards(Request $request)
    {
        return [];
    }

    public function filters(Request $request)
    {
        return [];
    }

    public function lenses(Request $request)
    {
        return [];
    }

    public function actions(Request $request)
    {
        return [];
    }
}
