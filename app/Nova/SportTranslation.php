<?php

namespace App\Nova;

use App\Models\SportTranslation as SportTranslationModel;
use Illuminate\Http\Request;
use Laravel\Nova\Fields\BelongsTo;
use Laravel\Nova\Fields\ID;
use Laravel\Nova\Fields\Number;
use Laravel\Nova\Fields\Text;

class SportTranslation extends Resource
{
    public static $model = SportTranslationModel::class;

    public static $displayInNavigation = false;

    public static $title = 'name';

    public static $search = [
        'id',
    ];

    public function fields(Request $request)
    {
        return [
            ID::make()->sortable(),

            Text::make('Name')
                ->sortable()
                ->rules('nullable'),

            Number::make('Language Id')
                ->sortable()
                ->rules('required', 'integer', 'in:1'),
        ];
    }

    public function cards(Request $request)
    {
        return [];
    }

    public function filters(Request $request)
    {
        return [];
    }

    public function lenses(Request $request)
    {
        return [];
    }

    public function actions(Request $request)
    {
        return [];
    }
}
