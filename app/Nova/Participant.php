<?php

namespace App\Nova;

use App\Models\Participant as ParticipantModel;
use App\Nova\Filters\TeamImage;
use Davidpiesse\NovaToggle\Toggle;
use Illuminate\Http\Request;
use Laravel\Nova\Fields\ID;
use Laravel\Nova\Fields\Image;
use Laravel\Nova\Fields\Number;
use Laravel\Nova\Fields\Text;

class Participant extends Resource
{
    public static $model = ParticipantModel::class;

    public static $title = 'name';

    public static $search = [
        'id', 'name'
    ];
    public static $group = 'Odds';

    public static $displayInNavigation = true;

    public function fields(Request $request)
    {
        return [
            ID::make()->sortable(),

            Text::make('Name')
                ->sortable()
                ->rules('required'),

            Image::make('Image')
                ->sortable(),
        ];
    }

    public function cards(Request $request)
    {
        return [];
    }

    public function filters(Request $request)
    {
        return [
            new TeamImage()
        ];
    }

    public function lenses(Request $request)
    {
        return [];
    }

    public function actions(Request $request)
    {
        return [];
    }
}
