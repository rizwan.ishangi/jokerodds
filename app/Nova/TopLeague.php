<?php

namespace App\Nova;

use App\Models\League as LeagueModel;
use App\Nova\Actions\League\AddTopLeague;
use App\Nova\Actions\League\RemoveTopLeague;
use Davidpiesse\NovaToggle\Toggle;
use Illuminate\Http\Request;
use Laravel\Nova\Fields\HasMany;
use Laravel\Nova\Fields\HasOne;
use Laravel\Nova\Fields\ID;
use Laravel\Nova\Fields\Image;
use Laravel\Nova\Fields\Number;
use Laravel\Nova\Fields\Text;
use Laravel\Nova\Http\Requests\NovaRequest;

class TopLeague extends Resource
{
    public static $model = LeagueModel::class;

    public static $title = 'name';

    public static $search = [
        'name'
    ];

    public static $group = 'Odds';

    public function fields(Request $request)
    {
        return [
            ID::make()->sortable(),

            Text::make('Name')
                ->sortable()
                ->rules('required'),

            Text::make("Country", "location_id")
                ->resolveUsing(function ($locationId) {
                    return \App\Models\Location::find($locationId)->name ?? "-";
                })
                ->onlyOnIndex()
                ->sortable(),

            Number::make('Goal League Id')
                ->sortable()
                ->rules('nullable', 'integer'),

            Text::make('Goal League Name')
                ->sortable()
                ->rules('nullable'),

            Toggle::make('Active', 'active')
                ->trueValue('1')->falseValue('0')->showLabels()
                ->rules('required', 'in:0,1')->sortable(),

            Image::make('Img')
                ->sortable()
                ->rules('nullable', 'mimes:png,jpg,jpeg')->disableDownload(),

            HasMany::make('Fixtures', 'fixtures', \App\Nova\Fixture::class),

            HasOne::make('Location', 'location', \App\Nova\Location::class),

            HasOne::make('Sport', 'sport', \App\Nova\Sport::class),
        ];
    }

    public function cards(Request $request)
    {
        return [];
    }

    public function filters(Request $request)
    {
        return [];
    }

    public function lenses(Request $request)
    {
        return [];
    }

    public function actions(Request $request)
    {
        return [
            (new AddTopLeague())
                ->canSee(function($request) {
                    return $request->user()->hasPermissionTo('add top leagues');
                })
                ->confirmText('Are you sure you want to add to top this League ?')
                ->confirmButtonText('Add league'),
            (new RemoveTopLeague())
                ->canSee(function($request) {
                    return $request->user()->hasPermissionTo('remove top leagues');
                })
                ->confirmText('Are you sure you want to remove this League from top?')
                ->confirmButtonText('Yes, remove'),
        ];
    }

    public static function indexQuery(NovaRequest $request, $query)
    {
        return $query->where('show_sidebar', 1);
    }

    public static function authorizedToCreate(Request $request)
    {
        return false;
    }

    public function authorizedToDelete(Request $request)
    {
        return false;
    }

    public function authorizedToView(Request $request)
    {
        return false;
    }
}
