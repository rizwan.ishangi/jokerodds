<?php

namespace App\Nova;

use App\Models\TextBlock as TextBlockModel;
use BayAreaWebPro\NovaFieldCkEditor\CkEditor;
use Illuminate\Http\Request;
use Laravel\Nova\Fields\ID;
use Laravel\Nova\Fields\Text;

class TextBlock extends Resource
{
    public static $model = TextBlockModel::class;

    public static $title = 'name';

    public static $search = [
        'name'
    ];

    public static $group = 'CMS';

    public function fields(Request $request)
    {
        return [
            ID::make()->sortable(),

            Text::make('Name')
                ->sortable()
                ->rules('required', 'max:255'),

            CkEditor::make('Content')
                ->rules('nullable')
                ->hideFromIndex()
                ->mediaBrowser()
                ->stacked(),
        ];
    }

    public function cards(Request $request)
    {
        return [];
    }

    public function filters(Request $request)
    {
        return [];
    }

    public function lenses(Request $request)
    {
        return [];
    }

    public function actions(Request $request)
    {
        return [];
    }
}
