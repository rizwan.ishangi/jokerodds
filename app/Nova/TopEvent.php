<?php

namespace App\Nova;

use App\Models\Event as EventModel;
use App\Nova\Actions\Event\AddTopEvent;
use App\Nova\Actions\Event\RemoveTopEvent;
use Illuminate\Http\Request;
use Laravel\Nova\Fields\BelongsTo;
use Laravel\Nova\Fields\DateTime;
use Laravel\Nova\Fields\ID;
use Laravel\Nova\Http\Requests\NovaRequest;
use Titasgailius\SearchRelations\SearchesRelations;

class TopEvent extends Resource
{
    use SearchesRelations;

    public static $model = EventModel::class;

    public static $title = 'name';

    public static $search = [
        'id', 'bets_last_update'
    ];

    public static $searchRelations = [
        'fixture.participants' => ['name'],
    ];

    public static $group = 'Odds';

    public function fields(Request $request)
    {
        return [
            ID::make()->sortable(),

            BelongsTo::make('Fixture', 'fixture', Fixture::class),

            DateTime::make('Bets Last Update')
                ->sortable()
                ->rules('required')->exceptOnForms(),
        ];
    }

    public function cards(Request $request)
    {
        return [];
    }

    public function filters(Request $request)
    {
        return [];
    }

    public function lenses(Request $request)
    {
        return [];
    }

    public function actions(Request $request)
    {
        return [
            (new AddTopEvent())
                ->canSee(function($request) {
                    return $request->user()->hasPermissionTo('add top events');
                })
                ->confirmText('Are you sure you want to add to top this Event ?')
                ->confirmButtonText('Add event'),
            (new RemoveTopEvent())
                ->canSee(function($request) {
                    return $request->user()->hasPermissionTo('remove top events');
                })
                ->confirmText('Are you sure you want to remove this Event from top ?')
                ->confirmButtonText('Yes, remove')
        ];
    }

    public static function indexQuery(NovaRequest $request, $query)
    {
        return $query->where('show_sidebar', 1);
    }

    public static function authorizedToCreate(Request $request)
    {
        return false;
    }

    public function authorizedToDelete(Request $request)
    {
        return false;
    }

    public function authorizedToView(Request $request)
    {
        return false;
    }
}
