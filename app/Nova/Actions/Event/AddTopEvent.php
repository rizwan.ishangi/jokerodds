<?php

namespace App\Nova\Actions\Event;

use App\Models\Event as EventModel;
use App\Nova\Event;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Collection;
use Laravel\Nova\Fields\ActionFields;
use Sloveniangooner\SearchableSelect\SearchableSelect;

class AddTopEvent extends  \Brightspot\Nova\Tools\DetachedActions\DetachedAction
{
    use InteractsWithQueue, Queueable;

    public $name = 'Add Event to top';

    /**
     * Perform the action on the given models.
     *
     * @param  \Laravel\Nova\Fields\ActionFields  $fields
     * @param  \Illuminate\Support\Collection  $models
     * @return mixed
     */
    public function handle(ActionFields $fields, Collection $models)
    {
        EventModel::findOrFail($fields['event'])->update(['show_sidebar' => 1]);
    }

    /**
     * Get the fields available on the action.
     *
     * @return array
     */
    public function fields()
    {
        return [
            SearchableSelect::make("Event")->resource(Event::class)->max(50)
        ];
    }
}
