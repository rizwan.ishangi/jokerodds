<?php

namespace App\Nova\Actions\Event;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Collection;
use Laravel\Nova\Actions\Action;
use Laravel\Nova\Fields\ActionFields;

class RemoveTopEvent extends Action
{
    use InteractsWithQueue, Queueable;

    public $name = 'Remove';

    public $showOnTableRow = true;

    public function handle(ActionFields $fields, Collection $models)
    {
        $models->first()->update(['show_sidebar' => 0]);
    }

    public function fields()
    {
        return [];
    }

    public function actionClass()
    {
        return 'btn-danger';
    }
}
