<?php

namespace App\Nova\Actions\League;

use App\Models\League as LeagueModel;
use App\Nova\League;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Collection;
use Laravel\Nova\Fields\ActionFields;
use Sloveniangooner\SearchableSelect\SearchableSelect;

class AddTopLeague extends \Brightspot\Nova\Tools\DetachedActions\DetachedAction
{
    use InteractsWithQueue, Queueable;

    public $name = 'Add League to top';

    public function handle(ActionFields $fields, Collection $models)
    {
        LeagueModel::findOrFail($fields['league'])->update(['show_sidebar' => 1]);
    }

    public function fields()
    {
        return [
            SearchableSelect::make("League")->resource(League::class)->max(50)
        ];
    }
}
