<?php

namespace App\Nova;

use App\Models\Location as LocationModel;
use Davidpiesse\NovaToggle\Toggle;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use Laravel\Nova\Fields\HasMany;
use Laravel\Nova\Fields\HasOne;
use Laravel\Nova\Fields\ID;
use Laravel\Nova\Fields\Image;
use Laravel\Nova\Fields\Number;
use Laravel\Nova\Fields\Text;
use Laravel\Nova\Http\Requests\NovaRequest;
use Saumini\Count\RelationshipCount;
use Titasgailius\SearchRelations\SearchesRelations;

class Location extends Resource
{
    use SearchesRelations;

    public static $model = LocationModel::class;

    public static $title = 'fullName';

    public static $search = [
        'iso_2',
    ];

    public static $group = 'Odds';

    public static $searchable = false;

    public static $searchRelations = [
        'translation' => ['name'],
    ];

    public function fields(Request $request)
    {
        return [
            ID::make()->sortable(),

            Text::make('Name', 'translation.name')->exceptOnForms(),

            Text::make('Iso 2')
                ->rules('required'),

            Number::make('Sort weight')
                ->rules('nullable', 'integer', 'min:0', 'max:999')
                ->sortable(),

            Toggle::make('Active', 'active')
                ->trueValue('1')
                ->falseValue('0')
                ->showLabels()
                ->rules('required', 'in:0,1'),

            Toggle::make('Has Fixtures', 'has_fixtures')
                ->trueValue('1')
                ->falseValue('0')
                ->exceptOnForms(),

            RelationshipCount::make('Leagues Count', 'leagues'),

            Image::make('Img')
                ->rules('nullable', 'mimes:png,jpg,jpeg')->disableDownload(),

            HasMany::make('Leagues', 'leagues', League::class),

            HasOne::make('Translation', 'translation', LocationTranslation::class)->inline($request)->hideFromIndex(),

        ];
    }

    public function cards(Request $request)
    {
        return [];
    }

    public function filters(Request $request)
    {
        return [
            new Filters\StatusFilter,
        ];
    }

    public function lenses(Request $request)
    {
        return [];
    }

    public function actions(Request $request)
    {
        return [];
    }

    public static function indexQuery(NovaRequest $request, $query)
    {
        $query->when(empty($request->get('orderBy')), function (Builder $q) {
            $q->getQuery()->orders = [];

            return $q->orderBy('name');
        });
    }
}
