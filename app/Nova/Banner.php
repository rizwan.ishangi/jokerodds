<?php

namespace App\Nova;

use App\Models\Banner as BannerModel;
use Davidpiesse\NovaToggle\Toggle;
use Illuminate\Http\Request;
use Laravel\Nova\Fields\ID;
use Laravel\Nova\Fields\Image;
use Laravel\Nova\Fields\Number;
use Laravel\Nova\Fields\Select;
use Laravel\Nova\Fields\Text;

class Banner extends Resource
{
    public static $model = BannerModel::class;

    public static $title = 'page';

    public static $search = [
        'page',
    ];

    public static $group = 'CMS';

    public function fields(Request $request)
    {
        return [
            ID::make()->sortable(),

            Text::make('Link')
                ->rules('required', 'url'),

            Number::make('Views')
                ->sortable()
                ->rules('required', 'integer')->onlyOnIndex(),

            Number::make('Clicks')
                ->sortable()
                ->rules('required', 'integer')->onlyOnIndex(),

            Toggle::make('Active', 'active')
                ->trueValue('1')->falseValue('0')->showLabels()
                ->rules('required', 'in:0,1')->sortable(),

            Select::make('Position')
                ->sortable()
                ->options([
                    '1' => 'Top',
                    '2' => 'Left',
                    '3' => 'Right',
                ]),

            Number::make('Order')
                ->sortable()
                ->rules('required', 'numeric', 'integer')
                ->default(1),

            Image::make('Image', 'img')
                ->rules('required', 'mimes:jpg,jpeg,png,gif'),
        ];
    }

    public function cards(Request $request)
    {
        return [];
    }

    public function filters(Request $request)
    {
        return [
            new Filters\StatusFilter,
        ];
    }

    public function lenses(Request $request)
    {
        return [];
    }

    public function actions(Request $request)
    {
        return [];
    }
}
