<?php

namespace App\Nova;

use App\Models\League as LeagueModel;
use Davidpiesse\NovaToggle\Toggle;
use Illuminate\Http\Request;
use Laravel\Nova\Fields\HasMany;
use Laravel\Nova\Fields\HasOne;
use Laravel\Nova\Fields\ID;
use Laravel\Nova\Fields\Image;
use Laravel\Nova\Fields\Number;
use Laravel\Nova\Fields\Select;
use Laravel\Nova\Fields\Text;
use Laravel\Nova\Http\Requests\NovaRequest;

class League extends Resource
{
    public static $model = LeagueModel::class;

    public static $title = 'name';

    public static $search = [
        'name',
    ];

    public static $group = 'Odds';

    public static $with = ['location'];

    public function fields(Request $request)
    {
        return [
            ID::make()->sortable(),

            Text::make('Name')
                ->sortable()
                ->rules('required'),

            Text::make("Country", "location_id")
                ->resolveUsing(function ($locationId) {
                    return \App\Models\Location::find($locationId)->name ?? "-";
                })
                ->onlyOnIndex()
                ->sortable(),

            Select::make('Goal League Id')
                ->nullable()
                ->options(\App\Models\GoalServeLeague::pluck('id','id')->toArray())
                ->displayUsingLabels(),

            Select::make('Goal League Name')
                ->nullable()
                ->options(\App\Models\GoalServeLeague::pluck('name')->toArray())
                ->searchable()
                ->displayUsingLabels()
                ->hideFromIndex(),

/*
            Number::make('Goal League Id')
                ->sortable()
                ->rules('nullable', 'integer'),

            Text::make('Goal League Name')
                ->sortable()
                ->rules('nullable'),
*/
            Toggle::make('Active', 'active')
                ->trueValue('1')
                ->falseValue('0')
                ->showLabels()
                ->rules('required', 'in:0,1')->sortable(),

            Toggle::make('Important', 'important')
                ->trueValue('1')
                ->falseValue('0')
                ->showLabels()
                ->rules('required', 'in:0,1')->sortable(),

            Toggle::make('Has Fixtures', 'has_fixtures')
                ->trueValue('1')
                ->falseValue('0')
                ->exceptOnForms(),

            Toggle::make('Top league', 'show_sidebar')
                ->trueValue('1')
                ->falseValue('0')
                ->showLabels()
                ->rules('required', 'in:0,1')
                ->sortable(),

            Number::make('Sort weight')
                ->rules('nullable', 'integer', 'min:0', 'max:999')
                ->sortable(),

            Image::make('Img')
                ->sortable()
                ->rules('nullable', 'mimes:png,jpg,jpeg')->disableDownload(),

            HasMany::make('Fixtures', 'fixtures', Fixture::class),

            HasOne::make('Location', 'location', Location::class),

            HasOne::make('Sport', 'sport', Sport::class),
        ];
    }

    public function cards(Request $request)
    {
        return [];
    }

    public function filters(Request $request)
    {
        return [
            new Filters\LeagueCountry,
            new Filters\StatusFilter,
            new Filters\LeagueTop,
        ];
    }

    public function lenses(Request $request)
    {
        return [];
    }

    public function actions(Request $request)
    {
        return [];
    }

    public static function indexQuery(NovaRequest $request, $query)
    {
        return $query;
    }
}
