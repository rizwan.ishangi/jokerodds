<?php


namespace App\Providers;
use App\Services\OneXBet\OneXBetService;
use Illuminate\Support\ServiceProvider;

class OneXBetServiceProvider extends ServiceProvider
{
    public function register()
    {
        $this->app->bind('oneXBet',OneXBetService::class);
    }
}
