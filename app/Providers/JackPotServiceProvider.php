<?php


namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class JackPotServiceProvider extends ServiceProvider
{
    public function register()
    {
        $this->app->bind('jackPotScore','App\Services\JackPotScore\JackPotScoreService');
    }
}
