<?php

namespace App\Providers;

use App\Models\TextBlock;
use App\Observers\TextBlockObserver;
use Illuminate\Support\ServiceProvider;

class ObserverServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     */
    public function boot()
    {
        TextBlock::observe(TextBlockObserver::class);
    }
}
