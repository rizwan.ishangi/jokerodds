<?php

namespace App\Providers;

use App\Contracts\Standings\StandingInterface;
use App\Libraries\GoalServe\GoalServeClient;
use App\Services\Standings\StandingManager;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\ServiceProvider;
use Laravel\Dusk\DuskServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        if ($this->app->runningUnitTests()) {
            Schema::defaultStringLength(191);
        }

        $this->app->bind('goal-serve-client', GoalServeClient::class);
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        if ($this->app->environment('local', 'testing') && class_exists(DuskServiceProvider::class)) {
            $this->app->register(DuskServiceProvider::class);
        }
        $this->app->bind(StandingInterface::class,function ($app){
            return new StandingManager(request()->all());
        });
    }
}
