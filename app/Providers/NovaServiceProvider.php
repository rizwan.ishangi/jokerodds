<?php

namespace App\Providers;

use App\Nova\Role;
use Davidpiesse\NovaToggle\Toggle;
use Illuminate\Support\Facades\Gate;
use Laravel\Nova\Cards\Help;
use Laravel\Nova\Fields\Boolean;
use Laravel\Nova\Fields\Number;
use Laravel\Nova\Fields\Text;
use Laravel\Nova\Nova;
use Laravel\Nova\NovaApplicationServiceProvider;

class NovaServiceProvider extends NovaApplicationServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();

        \OptimistDigital\NovaSettings\NovaSettings::addSettingsFields(function () {
            return [
                Text::make('Title', 'title'),
                Text::make('Description', 'description'),
                Toggle::make('Registration (on/off)', 'registration')
                    ->trueValue('1')
                    ->falseValue('0')
                    ->showLabels()
                    ->trueLabel('On')
                    ->falseLabel('Off'),
                Text::make('Copyright', 'copyright'),
                Text::make('Facebook url', 'facebook_url'),
                Text::make('Instagram url', 'instagram_url'),
            ];
        }, [
            'title' => 'string',
            'description' => 'string',
            'registration' => 'boolean',
            'copyright' => 'string',
            'facebook_url' => 'string',
            'instagram_url' => 'string'
        ]);
    }

    /**
     * Register the Nova routes.
     *
     * @return void
     */
    protected function routes()
    {
        Nova::routes()
                ->withAuthenticationRoutes()
                ->withPasswordResetRoutes()
                ->register();
    }

    /**
     * Register the Nova gate.
     *
     * This gate determines who can access Nova in non-local environments.
     *
     * @return void
     */
    protected function gate()
    {
        Gate::define('viewNova', function ($user) {
            return in_array($user->email, [
                'admin@xairo.com',
                'alex.cebotari@ourbox.org'
            ]);
        });
    }

    /**
     * Get the cards that should be displayed on the default Nova dashboard.
     *
     * @return array
     */
    protected function cards(): array
    {
        return [
            //
        ];
    }

    /**
     * Get the extra dashboards that should be displayed on the Nova dashboard.
     *
     * @return array
     */
    protected function dashboards(): array
    {
        return [];
    }

    /**
     * Get the tools that should be listed in the Nova sidebar.
     *
     * @return array
     */
    public function tools(): array
    {
        return [
            new \Aiman\MenuBuilder\MenuBuilder,
            new \OptimistDigital\NovaSettings\NovaSettings,
            \Pktharindu\NovaPermissions\NovaPermissions::make()
                ->roleResource(Role::class),
        ];
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
