<?php

namespace App\Providers;

use Aiman\MenuBuilder\Http\Models\Menu;
use App\Models\Admin;
use App\Models\Banner;
use App\Models\Bookmaker;
use App\Models\Event;
use App\Models\GoalServeTeam;
use App\Models\League;
use App\Models\Location;
use App\Models\Page;
use App\Models\Role;
use App\Models\Sport;
use App\Models\TextBlock;
use App\Models\User;
use App\Policies\AdminPolicy;
use App\Policies\BannerPolicy;
use App\Policies\BookmakerPolicy;
use App\Policies\EventPolicy;
use App\Policies\LeaguePolicy;
use App\Policies\LocationPolicy;
use App\Policies\MenuBuilderPolicy;
use App\Policies\PagePolicy;
use App\Policies\RolePolicy;
use App\Policies\SportPolicy;
use App\Policies\TeamPolicy;
use App\Policies\TextBlockPolicy;
use App\Policies\UserPolicy;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Gate;
use Pktharindu\NovaPermissions\Traits\ValidatesPermissions;

class AuthServiceProvider extends ServiceProvider
{
    use ValidatesPermissions;
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        'App\Model' => 'App\Policies\ModelPolicy',
        User::class => UserPolicy::class,
        Banner::class => BannerPolicy::class,
        Bookmaker::class => BookmakerPolicy::class,
        League::class => LeaguePolicy::class,
        Sport::class => SportPolicy::class,
        TextBlock::class => TextBlockPolicy::class,
        Page::class => PagePolicy::class,
        GoalServeTeam::class => TeamPolicy::class,
        Location::class => LocationPolicy::class,
        Event::class => EventPolicy::class,
        Role::class => RolePolicy::class,
        Admin::class => AdminPolicy::class,
        Menu::class => MenuBuilderPolicy::class,
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        foreach (config('nova-permissions.permissions') as $key => $permissions) {
            Gate::define($key, function (Admin $user) use ($key) {
                if ($this->nobodyHasAccess($key)) {
                    return true;
                }

                return $user->hasPermissionTo($key);
            });
        }
    }
}
