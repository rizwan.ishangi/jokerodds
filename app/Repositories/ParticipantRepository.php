<?php
/**
 * Created by PhpStorm.
 * User: alex.cebotari@ourbox.org
 * Date: 26.03.2021
 * Time: 23:44
 */

namespace App\Repositories;

use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;

/**
 * Class ParticipantRepository
 * @package App\Repositories
 */
class ParticipantRepository
{
    /**
     * @param  array  $fixtureIds
     *
     * @return Collection
     */
    public function getParticipantsByFixtureIds(array $fixtureIds): Collection
    {
        $fixtureParticipants = DB::table('fixture_participant')
            ->select(['id', 'fixture_id', 'participant_id', 'position'])
            ->whereIn('fixture_id', $fixtureIds)
            ->get();

        $participants = DB::table('participant')
            ->select(['id', 'name', 'image'])
            ->whereIn('id', $fixtureParticipants->pluck('participant_id')->unique()->values()->toArray())
            ->get()
            ->groupBy('id');

        return $fixtureParticipants->map(function ($item) use ($participants) {
            if (isset($participants[$item->participant_id])) {
                $datum = new \stdClass();
                $datum->id = $item->participant_id;
                $datum->fixture_id = $item->fixture_id;
                $datum->name = $participants[$item->participant_id][0]->name;
                $datum->image = $participants[$item->participant_id][0]->image;
                return $datum;
            }
            return null;
        })->filter(function ($item) {
            return (!is_null($item));
        });
    }
}
