<?php

namespace App\Repositories;

use App\Models\Bet;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;

/**
 * Class EventRepository
 * @package App\Repositories
 */
class EventRepository
{
    /**
     * @param  array  $fixtureIds
     *
     * @return Collection
     */
    public function getEventsWithScoreboardsByFixtureIds(array $fixtureIds): Collection
    {
        $events = DB::table('event')
            ->select(['id', 'fixture_id'])
            ->whereIn('fixture_id', $fixtureIds)
            ->get();

        $scoreboards = DB::table('scoreboard')
            ->select(['id', 'event_id', 'status_id', 'results'])
            ->whereIn('event_id', $events->pluck('id')->unique()->values()->toArray())
            ->get()
            ->groupBy('event_id');

        $eventsData = [];
        foreach ($events as $event) {
            if (isset($scoreboards[$event->id])) {
                $event->scoreboard = $scoreboards[$event->id][0];
            } else {
                $event->scoreboard = null;
            }
            $eventsData[] = $event;
        }

        $events = collect($eventsData);
        unset($eventsData, $scoreboards);

        return $events;
    }

    /**
     * @param int   $eventId
     * @param array $filterBetNames
     *
     * @return Collection
     */
    public function eventBets(int $eventId, array $filterBetNames = ['1', 'X', '2'])
    {
        return Bet::query()
            ->whereHas('market', function ($query) {
                $query->where('name', '1X2');
            })
            ->whereIn('name', $filterBetNames)
            ->where('event_id', $eventId)
            ->get();
    }

    /**
     * @param int $eventId
     *
     * @return Collection
     */
    public function event1x2Coefficients(int $eventId)
    {
        return $this->eventBets($eventId, ['1', 'X', '2'])
            ->groupBy('name')
            ->map(function ($items) {
                return collect($items)->avg('price');
            });
    }
}
