<?php
/**
 * Created by PhpStorm.
 * User: alex.cebotari@ourbox.org
 * Date: 26.03.2021
 * Time: 23:34
 */

namespace App\Repositories;

use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;

/**
 * Class LocationRepository
 * @package App\Repositories
 */
class LocationRepository
{
    /**
     * @param  array  $locationIds
     *
     * @return Collection
     */
    public function getActiveLocationsWithTranslationsByLocationIds(array $locationIds = []): Collection
    {
        $locations = DB::table('location')
            ->select(['id', 'iso_2', 'active'])
            ->where('active', true)
            ->whereIn('id', $locationIds)
            ->get();

        $translations = DB::table('location_translation')
            ->select(['id', 'name', 'location_id'])
            ->whereIn('location_id', $locations->pluck('id')->unique()->values()->toArray())
            ->get()
            ->groupBy('location_id')
            ->toArray();

        $locationsData = [];
        foreach ($locations as $location) {
            if (isset($translations[$location->id])) {
                $location->translation = $translations[$location->id][0];
            } else {
                $location->translation = null;
            }
            $locationsData[] = $location;
        }

        $locations = collect($locationsData);
        unset($locationsData, $translations);

        return $locations;
    }
}
