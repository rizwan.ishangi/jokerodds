<?php

namespace App\Repositories;

use App\Libraries\OddsConvector\OddsConvector;
use App\Models\Bet;
use App\Models\BetHistory;
use App\Models\BetLive;
use App\Models\Event;
use App\Models\IpToLocation;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redis;

class BetRepository
{
    protected const ONLY_MARKETS = [
        1,
        282,
        284,
        3,
        53,
        283,
        2,
        77,
        337,
        7,
        25,
        151,
        4,
    ];

    /**
     * This script gets data from the tables bet, bookmaker, markets and generates data for transfer to the front.
     *
     * @param  array  $events
     *
     * @return array
     */
    public function betsByEvents(array $events, $from_history_table): array
    {
        /* If array of eventsIds is empty, return empty array */
        if (empty($events)) {
            return [];
        }

        $table = 'bet';
        if($from_history_table){
            $table = 'bet_history';
        }

        $bets = DB::table($table)
            ->select(['id', 'event_id', 'bookmaker_id', 'market_id', 'name', 'line', 'base_line', 'start_price', 'price', 'last_update', 'is_latest', 'is_inplay'])
            ->whereIn('market_id', self::ONLY_MARKETS)
            ->whereIn('event_id', $events)
            ->where('is_latest', true)
            ->get();

        /* if bets is empty */
        if ($bets->isEmpty()) {
            return [];
        }

        $pinnacle_bets = DB::table($table)
            ->select(['id', 'event_id', 'bookmaker_id', 'market_id', 'name', 'line', 'base_line', 'start_price', 'price', 'last_update', 'is_latest', 'is_inplay'])
            ->where('market_id',835)
            ->whereIn('event_id', $events)
            ->where('is_latest', true)
            ->get();

        $pinnacle_bets->map(function ($bet){
            $bet->market_id = 2;
            return $bet;
        });

        $bets = $bets->merge($pinnacle_bets);


        $bookmakers = DB::table('bookmaker')
            ->select(['id', 'name', 'logo', 'ref'])
            ->whereIn('id', $bets->pluck('bookmaker_id')->unique()->values()->toArray())
            ->where('active', true)
            ->get()
            ->groupBy('id')
            ->toArray();
        $markets = DB::table('markets')
            ->select(['id', 'name', 'active'])
            ->whereIn('id', $bets->pluck('market_id')->unique()->values()->toArray())
            ->where('active', true)
            ->get()
            ->groupBy('id')
            ->toArray();

        $data = [];
        foreach ($bets as $bet) {
            if (isset($bookmakers[$bet->bookmaker_id]) && isset($markets[$bet->market_id])) {
                $datum = $bet;
                $datum->bookmaker = $bookmakers[$bet->bookmaker_id][0];
                $datum->market = $markets[$bet->market_id][0];
                $data[] = $datum;
            }
        }
        $collection = collect($data);

        /* clear data */
        unset($bets, $bookmakers, $markets);

        /* If collection is empty */
        if ($collection->isEmpty()) {
            return [];
        }

        return $collection->groupBy('event_id')->map(function ($bets) {
            return [
                'prematch' => $bets->filter(function ($bet) {
                    return (!$bet->is_inplay);
                })->groupBy('market_id')->map(function ($market) {
                    return $market->groupBy('base_line')->map(function ($bets, $line) {
                        return [
                            'line' => $line ? floatval(str_replace('(0-0)', '', $line)) : null,
                            'coefficientsGroups' => $bets->unique('name')->pluck('name')->toArray(),
                            'coefficients' => $bets->groupBy('name')->map(function ($bet) {
                                return (new OddsConvector($bet->avg('price')))->handle();
                            })->toArray()
                        ];
                    })
                        ->values()
                        ->toArray();
                })->toArray(),
                'inplay' => $bets->filter(function ($bet) {
                    return ($bet->is_inplay);
                })->groupBy('market_id')->map(function ($market) {
                    return $market->groupBy('base_line')->map(function ($bets, $line) {
                        return [
                            'line' => $line ? floatval(str_replace('(0-0)', '', $line)) : null,
                            'coefficientsGroups' => $bets->unique('name')->pluck('name')->toArray(),
                            'coefficients' => $bets->groupBy('name')->map(function ($bet) {
                                return (new OddsConvector($bet->avg('price')))->handle();
                            })->toArray()
                        ];
                    })
                        ->values()
                        ->toArray();
                })->toArray(),
            ];
        })->toArray();
    }
    public function betsByEventsHome(array $events, $from_history_table): array
    {
        /* If array of eventsIds is empty, return empty array */
        if (empty($events)) {
            return [];
        }

        $table = 'bet';
        if($from_history_table){
            $table = 'bet_history';
        }
        $bets = DB::table($table)
            ->select(['id', 'event_id', 'bookmaker_id', 'market_id', 'name', 'line', 'base_line', 'start_price', 'price', 'last_update', 'is_latest', 'is_inplay'])
            ->where('market_id', 1)
            ->whereIn('event_id', $events)
            ->where('is_latest', true)
            ->get();
        /* if bets is empty */
        if ($bets->isEmpty()) {
            return [];
        }
        $bookmakers = DB::table('bookmaker')
            ->select(['id', 'name', 'logo', 'ref'])
            ->whereIn('id', $bets->pluck('bookmaker_id')->unique()->values()->toArray())
            ->where('active', true)
            ->get()
            ->groupBy('id')
            ->toArray();
        $markets = DB::table('markets')
            ->select(['id', 'name', 'active'])
            ->whereIn('id', $bets->pluck('market_id')->unique()->values()->toArray())
            ->where('active', true)
            ->get()
            ->groupBy('id')
            ->toArray();

        $data = [];
        foreach ($bets as $bet) {
            if (isset($bookmakers[$bet->bookmaker_id]) && isset($markets[$bet->market_id])) {
                $datum = $bet;
                $datum->bookmaker = $bookmakers[$bet->bookmaker_id][0];
                $datum->market = $markets[$bet->market_id][0];
                $data[] = $datum;
            }
        }
        $collection = collect($data);

        /* clear data */
        unset($bets, $bookmakers, $markets);

        /* If collection is empty */
        if ($collection->isEmpty()) {
            return [];
        }

        return $collection->groupBy('event_id')->map(function ($bets) {
            return [
                'prematch' => $bets->filter(function ($bet) {
                    return (!$bet->is_inplay);
                })->groupBy('market_id')->map(function ($market) {
                    return $market->groupBy('base_line')->map(function ($bets, $line) {
                        return [
                            'line' => $line ? floatval(str_replace('(0-0)', '', $line)) : null,
                            'coefficientsGroups' => $bets->unique('name')->pluck('name')->toArray(),
                            'coefficients' => $bets->groupBy('name')->map(function ($bet) {
                                return (new OddsConvector($bet->avg('price')))->handle();
                            })->toArray()
                        ];
                    })
                        ->values()
                        ->toArray();
                })->toArray(),
                'inplay' => $bets->filter(function ($bet) {
                    return ($bet->is_inplay);
                })->groupBy('market_id')->map(function ($market) {
                    return $market->groupBy('base_line')->map(function ($bets, $line) {
                        return [
                            'line' => $line ? floatval(str_replace('(0-0)', '', $line)) : null,
                            'coefficientsGroups' => $bets->unique('name')->pluck('name')->toArray(),
                            'coefficients' => $bets->groupBy('name')->map(function ($bet) {
                                return (new OddsConvector($bet->avg('price')))->handle();
                            })->toArray()
                        ];
                    })
                        ->values()
                        ->toArray();
                })->toArray(),
            ];
        })->toArray();
    }

    /**
     * @param  int  $eventId
     * @param  bool  $isInplay
     *
     * @return array
     */
    public function betsByEvent(int $eventId, bool $isInplay = false)
    {

        $from_history_table = 0;

        $max_history_date  = DB::table('history_date')
            ->where('removed',1)
            ->orderByDesc('id')
            ->first();

        if($max_history_date){
            $start_date = Event::find($eventId)->fixture()->first()->start_date;

            if((Carbon::parse($max_history_date->date_to)->gt(Carbon::parse($start_date)))){
                $from_history_table = 1;
            }
        }



        if($from_history_table){

            $data = BetHistory::where('event_id',$eventId)
                ->with('bookmaker')
                ->with('market')
                ->whereIn('market_id', self::ONLY_MARKETS)
                ->where('is_inplay', $isInplay)
                ->where('is_latest', 1)
                ->get();
            $pinnacle_bets =  BetHistory::where('event_id',$eventId)
                ->with('bookmaker')
                ->with('market')
                ->where('market_id',835)
                ->where('bookmaker_id',4)
                ->where('is_inplay', $isInplay)
                ->where('is_latest', 1)
                ->get();
        } else{

            $data = Bet::whereEventId($eventId)
                ->with('bookmaker')
                ->with('market')
                ->whereIn('market_id', self::ONLY_MARKETS)
                ->where('is_inplay', $isInplay)
                ->where('is_latest', 1)
                ->get();

            $pinnacle_bets =  Bet::whereEventId($eventId)
                ->with('bookmaker')
                ->with('market')
                ->where('market_id',835)
                ->where('bookmaker_id',4)
                ->where('is_inplay', $isInplay)
                ->where('is_latest', 1)
                ->get();

        }
        $pinnacle_bets->map(function ($bet){
            $bet->market_id = 2;
            $bet->market->name = 'Under/Over';
            return $bet;
        });

        $data_merge = $data->merge($pinnacle_bets)->groupBy('market_id');


        $result = [];
        $ip = intval(str_replace('.','',request()->ip()));

        $country = IpToLocation::select(['countries.id'])
                        ->join('countries','ip_to_location.iso_3166_2','=','countries.iso_3166_2')
                        ->whereRaw("$ip > ip_to_location.ip_from")
                        ->whereRaw("$ip <= ip_to_location.ip_to")
                        ->first();
        if(is_null($country)){
            $country = (object)['id'=>0];
        }

        foreach ($data_merge as $marketId => $bets) {
            if (empty($marketId)) {
                continue;
            }

            $result[$marketId] = collect([]);

            /**
             * @var string $line
             * @var Collection $lineBets
             */
            foreach ($bets->groupBy('base_line') as $line => $lineBets) {
                $result[$marketId]->push([
                    'line' => $line ? floatval(str_replace('(0-0)', '', $line)) : null,
                    'base_line' => $line ? $line : null,
                    'coefficientsGroups' => $lineBets->unique('name')->pluck('name'),
                    'coefficients' => $lineBets->groupBy('name')->map(function ($item) {
                        return (new OddsConvector($item->avg('price')))->handle();

                    }),
                    'coefficientsCount' => $lineBets->groupBy('name')->map(function ($item) {
                            return $item->count();
                    }),
                    'bets' => $lineBets->groupBy('bookmaker.name')->map(function ($bets) use ($country) {
                        return $bets->keyBy('name')->map(function ($bet) use($country){
                            $restrict_countries = json_decode($bet->bookmaker->restrict_countries);
                            $bet->bookmaker->restricted = !is_null($restrict_countries) ?  in_array((string)$country->id, $restrict_countries) : false;
                            return $bet;
                        });
                    }),
                ]);
            }

            $result[$marketId] = $result[$marketId]->sortBy('line', SORT_ASC)->values();
        }

        return $result;
    }

    public function betsLiveByEvent(int $eventId)
    {

        $data = BetLive::where('event_id',$eventId)
            ->with('bookmaker')
            ->with('market')
            ->get();
        $data = $data->groupBy('bookmaker_id')->map(function ($bets){
            $bets = collect($bets);
            return $bets->groupBy('market_id')->map(function ($bets,$key){
                $bets = collect($bets);
                return $bets->groupBy('base_line')->map(function ($bets){
                    return $bets->groupBy('name')->sortByDesc('id')->map(function ($bets){
                        return $bets->last();
                    });
                });
            });
        });
        $data = $data->flatten(3);
        $data = $data->groupBy('market_id');
        $result = [];
        $ip = intval(str_replace('.','',request()->ip()));

        $country = IpToLocation::select(['countries.id'])
            ->join('countries','ip_to_location.iso_3166_2','=','countries.iso_3166_2')
            ->whereRaw("$ip > ip_to_location.ip_from")
            ->whereRaw("$ip <= ip_to_location.ip_to")
            ->first();
        if(is_null($country)){
            $country = (object)['id'=>0];
        }

        foreach ($data as $marketId => $bets) {
            if (empty($marketId)) {
                continue;
            }
            $result[$marketId] = collect([]);

            /**
             * @var string $line
             * @var Collection $lineBets
             */
            foreach ($bets->groupBy('base_line') as $line => $lineBets) {
                $result[$marketId]->push([
                    'line' => $line ? floatval(str_replace('(0-0)', '', $line)) : null,
                    'base_line' => $line ? $line : null,
                    'coefficientsGroups' => $lineBets->unique('name')->pluck('name'),
                    'coefficients' => $lineBets->groupBy('name')->map(function ($item) {
                        return (new OddsConvector($item->avg('price')))->handle();
                    }),
                    'bets' => $lineBets->groupBy('bookmaker.name')->map(function ($bets) use ($country) {
                        return $bets->keyBy('name')->map(function ($bet) use($country){
                            $restrict_countries = json_decode($bet->bookmaker->restrict_countries);
                            $bet->bookmaker->restricted = !is_null($restrict_countries) ?  in_array((string)$country->id, $restrict_countries) : false;
                            return $bet;
                        });
                    }),
                ]);
            }

            $result[$marketId] = $result[$marketId]->sortBy('line', SORT_ASC)->values();
        }
        return $result;
    }

    /**
     * Get bets history based on specific event, bookmaker and market
     *
     * @param  int  $eventID
     * @param  string  $bookmakerID
     * @param  string  $betName
     * @param  string  $marketID
     *
     * @return array
     */
    public function betsHistoryByEvent(string $betName, int $eventID, string $bookmakerID, string $marketID, $base_line): array
    {

        $data = Bet::select(['bet.*','markets.name as market_name', 'bookmaker.name as bookmaker_name'])
            ->distinct()
            -> join('bookmaker','bet.bookmaker_id','=','bookmaker.id')
            -> join('markets','bet.market_id','=','markets.id')
            ->where('bet.event_id', $eventID)
            ->where('bet.bookmaker_id', $bookmakerID)
            ->where('bet.market_id', $marketID)
            ->where('bet.name', $betName)
            ->where('base_line', $base_line)
            ->where('open_status_date','<>',NULL)
            ->orderByDesc('bet.id')
            ->get()
  /*     $data = Bet::whereEventId($eventID)
            ->where('name', $betName)
            ->where('base_line', $base_line)
            ->where('open_status_date','<>',NULL)
            ->with('bookmaker', 'market')
            ->whereHas('bookmaker', function (Builder $query) use ($bookmakerID) {
                $query->whereId($bookmakerID);
            })
            ->whereHas('market', function (Builder $query) use ($marketID) {
                $query->whereId($marketID);
            })
            ->orderByDesc('id')
            ->distinct('last_update')
            ->get()
              ->map(function (Bet $bet) {
                $bet->market_name = $bet->market->name;
                $bet->bookmaker_name = $bet->bookmaker->name;
                return $bet->only(
                    'price',
                    'start_price',
                    'last_update',
                    'market_name',
                    'bookmaker_name'
                );
            })

*/
         ->map(function (Bet $bet) {
                return $bet->only(
                    'price',
                    'start_price',
                    'last_update',
                    'market_name',
                    'bookmaker_name'
                );
            })->map(function ($bet) {
                $bet['price_converted'] = (new OddsConvector($bet['price']))->handle();
                return $bet;
            })->unique('last_update');


        return [
            'info' => [
                'bookmaker_id' => $bookmakerID,
                'market_id' => $marketID,
            ],
            'bets' => $data->take(15),
            'current' => $data->first(),
            'opening' => $data->last(),
        ];
    }

    public function betsByEventsFromRedis(array $events){
        $data = [];

        foreach ($events as  $event) {
            $key = 'average#'.$event.'#1#1';
            $data[$key] =  '';

            $key = 'average#'.$event.'#1#2';
            $data[$key] =  '';

            $key = 'average#'.$event.'#1#X';
            $data[$key] =  '';
        }

        $eventData = Redis::mget(array_keys($data));
        $index = 0;

        foreach ($data as $key => $value) {
            $data[$key] = $eventData[$index];
            $index ++;
        }
        $prematch = [];

        foreach ($data as $key => $datum) {
            $keys = explode('#',$key);
            $values = explode('#',$datum);
            $prematch[]=[
                'event_id' =>$keys[1],
                'market_id'=>$keys[2],
                'base_line' => null,
                'name'      => $keys[3],
                'average'   => $values[1] ?? ''
            ];
        }

        $prematch = collect($prematch);

        $bets = $prematch->filter(function ($item){
            return $item['average'] !== '';
        })->groupBy('event_id')->map(function ($bets){
            return [
                'prematch' => $bets->groupBy('market_id')->map(function ($market) {
                    return $market->groupBy('base_line')->map(function ($bets, $line) {
                        return [
                            'line' => null,
                            'coefficientsGroups' => $bets->unique('name')->pluck('name')->toArray(),
                            'coefficients' => $bets->groupBy('name')->map(function ($bet) {
                                return (new OddsConvector($bet[0]['average']))->handle();
                            })->toArray()
                        ];
                    })->values();
                })->toArray(),
                'inplay' =>[]
            ];
        })->toArray();

        return $bets;

    }
}
