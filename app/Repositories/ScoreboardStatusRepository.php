<?php
/**
 * Created by PhpStorm.
 * User: alex.cebotari@ourbox.org
 * Date: 26.03.2021
 * Time: 14:57
 */

namespace App\Repositories;

/**
 * Class ScoreboardStatusRepository
 * @package App\Repositories
 */
class ScoreboardStatusRepository
{
    public const STATUS_NOT_STARTED_YET = 1;

    public const STATUS_IN_PROGRESS = 2;

    public const STATUS_FINISHED = 3;

    public const STATUS_CANCELLED = 4;

    public const STATUS_POSTPONED = 5;

    public const STATUS_INTERRUPTED = 6;

    public const STATUS_ABANDONED = 7;

    public const STATUS_COVERAGE_LOST = 8;

    public const STATUS_ABOUT_TO_START = 9;

    /**
     * @return string[]
     */
    public static function statusList(): array
    {
        return [
            self::STATUS_NOT_STARTED_YET => 'The event has not started yet.',
            self::STATUS_IN_PROGRESS => 'The event is live.',
            self::STATUS_FINISHED => 'The event is finished.',
            self::STATUS_CANCELLED => 'The event has been cancelled.',
            self::STATUS_POSTPONED => 'The event has been postponed.',
            self::STATUS_INTERRUPTED => 'The event has been interrupted.',
            self::STATUS_ABANDONED => 'The event has been abandoned.',
            self::STATUS_COVERAGE_LOST => 'The coverage for this event has been lost.',
            self::STATUS_ABOUT_TO_START => 'The event has not started but is about to.',
        ];
    }
}
