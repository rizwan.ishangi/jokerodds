<?php
/**
 * Created by PhpStorm.
 * User: alex.cebotari@ourbox.org
 * Date: 26.03.2021
 * Time: 22:19
 */

namespace App\Repositories;

use App\Models\BetStatus;
use App\Models\Fixture;
use Carbon\Carbon;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;

/**
 * Class FixtureRepository
 * @package App\Repositories
 */
class FixtureRepository
{
    /**
     * @var EventRepository $eventRepository
     */
    private $eventRepository;

    /**
     * @var BetRepository $betRepository
     */
    private $betRepository;

    /**
     * @var LocationRepository $locationRepository
     */
    private $locationRepository;

    /**
     * @var LeagueRepository $leagueRepository
     */
    private $leagueRepository;

    /**
     * @var ParticipantRepository $participantRepository
     */
    private $participantRepository;

    /**
     * @param  array  $fixtureIds
     *
     * @return array
     */
    private function getFixturesWithRelatedData(array $fixtureIds): array
    {
        $fixtures = DB::table('fixture')
            ->select('id', 'location_id', 'sport_id', 'league_id', 'start_date')
            ->whereIn('id', $fixtureIds)
            ->get();

        $locations = $this->locationRepository->getActiveLocationsWithTranslationsByLocationIds(
            $fixtures->pluck('location_id')->unique()->values()->toArray()
        )->groupBy('id');

        $leagues = $this->leagueRepository->getActiveLeaguesByIds(
            $fixtures->pluck('league_id')->unique()->values()->toArray()
        )->groupBy('id');

        $participants = $this->participantRepository->getParticipantsByFixtureIds(
            $fixtureIds
        )->groupBy('fixture_id');

        $events = $this->eventRepository->getEventsWithScoreboardsByFixtureIds(
            $fixtureIds
        )->groupBy('fixture_id');

        $fixturesData = [];
        foreach ($fixtures as $fixture) {
            if (
                isset($locations[$fixture->location_id]) &&
                isset($leagues[$fixture->league_id]) &&
                isset($participants[$fixture->id]) &&
                isset($events[$fixture->id])
            ) {
                $fixture->location = $locations[$fixture->location_id][0];
                $fixture->league = $leagues[$fixture->league_id][0];
                $fixture->participants = $participants[$fixture->id];
                $fixture->event = $events[$fixture->id][0];
                $fixturesData[] = $fixture;
            }
        }

        $fixtures = collect($fixturesData);
        /* Clear data */
        unset($fixturesData, $locations, $leagues, $participants, $events);

        /* Data array */
        $data = [];

        /* Change Data format for frontend */
        $fixtures->each(function ($item) use (&$data) {
            $data[$item->location_id]['active'] = $item->location->active;
            $data[$item->location_id]['location_id'] = $item->location->id;
            $data[$item->location_id]['iso_2'] = $item->location->iso_2;
            $data[$item->location_id]['name'] = optional($item->location->translation)->name;

            $data[$item->location_id]['leagues'][$item->league_id]['id'] = $item->league->id;
            $data[$item->location_id]['leagues'][$item->league_id]['name'] = $item->league->name;
            $data[$item->location_id]['leagues'][$item->league_id]['img'] = $item->league->img;
            $data[$item->location_id]['leagues'][$item->league_id]['matches'][] = [
                'event_id' => $item->event->id,
                'fixture_start_date' => \Carbon\Carbon::parse($item->start_date)->format('Y-m-d H:i:s'),
                'status_id' => (isset($item->event->scoreboard) && isset($item->event->scoreboard->status_id))
                    ? $item->event->scoreboard->status_id
                    : null,
                'status' => (isset($item->event->scoreboard) && isset($item->event->scoreboard->status_id))
                    ? ScoreboardStatusRepository::statusList()[$item->event->scoreboard->status_id]
                    : 'No data',
                'score' => (isset($item->event->scoreboard) && isset($item->event->scoreboard->results))
                    ? $item->event->scoreboard->results
                    : null,
                'participants' => $item->participants->map(function ($participant) {
                    return [
                        'participant_id' => $participant->id,
                        'participant_name' => $participant->name,
                        'image' => $participant->image,
                    ];
                })->toArray(),
            ];
        });

        /* Clear data */
        unset($fixtures);

        return $data;
    }

    /**
     * @param  array  $fixtureIds
     *
     * @return array
     */
    private function getStructuredDataOnFixtures(array $fixtureIds,$important = 'all'): array
    {
        /* Get all data for fixture */
        $fixtures = Fixture::select(['id', 'location_id', 'sport_id', 'league_id', 'start_date'])
            ->whereIn('id', $fixtureIds)
            ->with([
                'locationSimple' => function ($q) { // Using LocationSimpleModel instead of Location Model in case of broken pickup data
                    $q->select(['id', 'iso_2', 'active', 'has_fixtures','sort_weight']);
                },
                'locationSimple.translation' => function ($q) {
                    $q->select(['id', 'name', 'location_id']);
                },
                'league' => function ($q) {
                    $q->select(['id', 'name', 'img', 'active', 'important', 'has_fixtures', 'sort_weight']);
                },
                'participants' => function ($q) {
                     $q->select(['participant.id', 'participant.name', 'position','participant.image','goalserve_teams.image as team_image'])->leftJoin(
                         'goalserve_teams','participant.id','=','goalserve_teams.participant_id'
                     );
                },
                'event' => function ($q) {
                    $q->select(['id', 'fixture_id']);
                },
                'event.scoreboard' => function ($q) {
                    $q->select(['id', 'event_id', 'status_id', 'results']);
                },
            ])
            ->whereHas('league', function ($q) use($important) {
                $q->where('active', true);
                $q->where('has_fixtures', true);
                if( $important !== 'all'){
                    $q->where('important',$important);
                }
            })
            ->whereHas('location', function ($q) {
                $q->where('active', true);
                $q->where('has_fixtures', true);
            })
            ->get();

        /* Data array */
        $data = [];

        /* Change Data format for frontend */
        $fixtures->each(function ($item) use (&$data) {
            $data[$item->location_id]['active'] = $item->locationSimple->active;
            $data[$item->location_id]['location_id'] = $item->locationSimple->id;
            $data[$item->location_id]['iso_2'] = $item->locationSimple->iso_2;
            $data[$item->location_id]['sort_weight'] = $item->locationSimple->sort_weight;
            $data[$item->location_id]['name'] = optional($item->locationSimple->translation)->name;

            $data[$item->location_id]['leagues'][$item->league_id]['id'] = $item->league->id;
            $data[$item->location_id]['leagues'][$item->league_id]['name'] = $item->league->name;
            $data[$item->location_id]['leagues'][$item->league_id]['img'] = $item->league->img;
            $data[$item->location_id]['leagues'][$item->league_id]['important'] = $item->league->important;
            $data[$item->location_id]['leagues'][$item->league_id]['sort_weight'] = $item->league->sort_weight;
            $data[$item->location_id]['leagues'][$item->league_id]['matches'][] = [
                'event_id' => $item->event->id,
                'fixture_start_date' => $item->start_date->format('Y-m-d H:i:s'),
                'status_id' => optional($item->event->scoreboard)->status_id,
                'status' => (!is_null(optional($item->event->scoreboard)->status_id))
                    ? ScoreboardStatusRepository::statusList()[optional($item->event->scoreboard)->status_id]
                    : 'No data',
                'score' => optional($item->event->scoreboard)->results,
                'participants' => $item->participants->map(function ($participant) {
                    return [
                        'participant_id' => $participant->id,
                        'participant_name' => $participant->name,
                        'team_image' => $participant->team_image,
                        'image' => $participant->image,
                    ];
                })->toArray(),
            ];
        });

        // Sort 'sort_weight' DESC and 'name' 'ASC'
        foreach ($data as $key => $location) {
            $data[$key]['leagues'] = collect($location['leagues'])->sort(function ($a, $b) {
                return [$b['sort_weight'], $a['name']] <=> [$a['sort_weight'], $b['name']];
            })->values()->toArray();
        }

        /* Clear data */
        unset($fixtures);

        return $data;
    }

    /**
     * FixtureRepository constructor.
     *
     * @param  EventRepository  $eventRepository
     * @param  BetRepository  $betRepository
     * @param  LocationRepository  $locationRepository
     * @param  LeagueRepository  $leagueRepository
     * @param  ParticipantRepository  $participantRepository
     */
    public function __construct(
        EventRepository $eventRepository,
        BetRepository $betRepository,
        LocationRepository $locationRepository,
        LeagueRepository $leagueRepository,
        ParticipantRepository $participantRepository
    ) {
        $this->eventRepository = $eventRepository;
        $this->betRepository = $betRepository;
        $this->locationRepository = $locationRepository;
        $this->leagueRepository = $leagueRepository;
        $this->participantRepository = $participantRepository;
    }

    /**
     * @param  \Carbon\Carbon  $date
     *
     * @return array
     */
    public function getDateFixturesWithBets(\Carbon\Carbon $date, $important = 'all'): array
    {
        $from_history_table = 0;

        $max_history_date  = DB::table('history_date')->max('date_to');

        if(($max_history_date && Carbon::parse($max_history_date)->gt(Carbon::parse($date)))){
            $from_history_table = 1;
        }

        if($from_history_table){

            $data = Cache::remember($date,600,function() use ($date,$important){

                /* Get all fixture ids that have bets loaded */
                $fixtureIds = DB::table('fixture')
                    ->select(['id'])
                    ->whereBetween('start_date', [
                        $date->setTime(0, 0, 0),
                        (clone $date)->setTime(23, 59, 59),
                    ])
                    ->where('status_id', '!=', BetStatus::STATUS_DELETED);

                    $fixturesWithBets_sub = DB::table('event')
                        ->select(['id', 'fixture_id', DB::raw('(select 1 from `bet_history` where `event`.`id` = `bet_history`.`event_id` limit 1) as `bets_count`')])
                        ->whereIn('fixture_id', $fixtureIds);

                $fixturesWithBets = DB::query()->fromSub($fixturesWithBets_sub,'sub')->whereRaw("bets_count=1")->get();

                /* Get event ids and fixture ids */
                $eventIds = $fixturesWithBets->pluck('id')->unique()->values()->toArray();
                $fixtureIds = $fixturesWithBets->pluck('fixture_id')->unique()->values()->toArray();

                /* Clear unused data */
                unset($fixturesWithBets);

                /**
                 * 2 methods
                 *  $this->getFixturesWithRelatedData($fixtureIds) -> Only DB class
                 *  $this->getStructuredDataOnFixtures($fixtureIds) -> With Models
                 */
                return [
                    'data' => $this->getStructuredDataOnFixtures($fixtureIds,$important),
                    'bets' => $this->betRepository->betsByEventsHome($eventIds,1),
                ];
            });

            return [
                'data' => $data['data'],
                'bets' => $data['bets'],
            ];
        }



        /* Get all fixture ids that have bets loaded */

        if(($important==0) || ($important==1)){

            $fixtureIds = DB::table('fixture')
                ->select(['fixture.id'])
                ->leftJoin('league','fixture.league_id','=','league.id')
                ->where('league.important',$important)
                ->whereBetween('start_date', [
                    $date->setTime(0, 0, 0),
                    (clone $date)->setTime(23, 59, 59),
                ])
                ->where('status_id', '!=', BetStatus::STATUS_DELETED);

        } else{

            $fixtureIds = DB::table('fixture')->select(['id'])
                ->whereBetween('start_date', [
                    $date->setTime(0, 0, 0),
                    (clone $date)->setTime(23, 59, 59),
                ])
                ->where('status_id', '!=', BetStatus::STATUS_DELETED);
        }
        /* Get all fixture ids that have event with bets */
        $fixturesWithBets_sub = DB::table('event')
            ->select(['id', 'fixture_id', DB::raw('(select 1 from `bet` where `event`.`id` = `bet`.`event_id` limit 1) as `bets_count`')])
            ->whereIn('fixture_id', $fixtureIds);


        $fixturesWithBets = DB::query()->fromSub($fixturesWithBets_sub,'sub')->whereRaw("bets_count=1")->get();

        /* Get event ids and fixture ids */
        $eventIds = $fixturesWithBets->pluck('id')->unique()->values()->toArray();
        $fixtureIds = $fixturesWithBets->pluck('fixture_id')->unique()->values()->toArray();

        /* Clear unused data */
        unset($fixturesWithBets);

        /**
         * 2 methods
         *  $this->getFixturesWithRelatedData($fixtureIds) -> Only DB class
         *  $this->getStructuredDataOnFixtures($fixtureIds) -> With Models
         */
        if(now()->setTime(0,0,0)->greaterThan($date)){
            //return from redis
            return [
                'data' => $this->getStructuredDataOnFixtures($fixtureIds,$important),
                'bets' => $this->betRepository->betsByEventsHome($eventIds,0),
            ];
        }
            return [
                'data' => $this->getStructuredDataOnFixtures($fixtureIds,$important),
                'bets' => $this->betRepository->betsByEventsFromRedis($eventIds)
               //   'bets' => $this->betRepository->betsByEventsHome($eventIds,0),
            ];
    }
}
