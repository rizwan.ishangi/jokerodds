<?php
/**
 * Created by PhpStorm.
 * User: alex.cebotari@ourbox.org
 * Date: 26.03.2021
 * Time: 23:41
 */

namespace App\Repositories;

use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;

/**
 * Class LeagueRepository
 * @package App\Repositories
 */
class LeagueRepository
{
    /**
     * @param  array  $leagueIds
     *
     * @return Collection
     */
    public function getActiveLeaguesByIds(array $leagueIds): Collection
    {
        return DB::table('league')
            ->select(['id', 'name', 'img', 'active'])
            ->whereIn('id', $leagueIds)
            ->where('active', true)
            ->get();
    }
}
