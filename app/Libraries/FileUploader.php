<?php


namespace App\Helpers;


use Illuminate\Contracts\Filesystem\FileNotFoundException;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Storage;

class FileUploader
{
    /**
     * @param UploadedFile $uploadedFile
     * @param $basePath
     * @return string
     * @throws FileNotFoundException
     */
    public static function upload(UploadedFile $uploadedFile, string $basePath): string
    {
        $path = self::generatePath($uploadedFile, $basePath);

        Storage::disk('public')->put($path, $uploadedFile->get());

        return $path;
    }

    /**
     * @param array $fileList
     * @param string $basePath
     * @return Collection
     */
    public static function uploadMultiple(array $fileList, string $basePath = 'files'): Collection
    {
        return collect($fileList)->map(function ($file) use ($basePath) {
            return self::upload($file, $basePath);
        });
    }

    protected static function generatePath(UploadedFile $uploadedFile, $basePath): string
    {
        $name = md5(
                microtime(true) . '-' . $uploadedFile->getClientOriginalName()
            ) . '.' . $uploadedFile->getClientOriginalExtension();

        $date = now()->toArray();
        $dateToPath = collect($date)->filter(function ($val, $key) {
            return $key == 'year' || $key == 'month' || $key == 'day';
        })->implode(DIRECTORY_SEPARATOR);

        $storagePath = $basePath . DIRECTORY_SEPARATOR . $dateToPath;

        return $storagePath . DIRECTORY_SEPARATOR . $name;
    }
}
