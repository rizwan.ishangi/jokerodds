<?php


namespace App\Libraries;


use Illuminate\Contracts\Filesystem\FileNotFoundException;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\Routing\ResponseFactory;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Intervention\Image\Facades\Image;

class ImageGenerator
{
    /**
     * @param $imagePath
     * @param int $size
     * @return Application|ResponseFactory|Response
     * @throws FileNotFoundException
     */
    public static function render($imagePath, int $size)
    {
        if (!Storage::disk('public')->exists($imagePath)) {
            return response('Not found')
                ->setStatusCode(404);
        }

        $originalImage = Storage::disk('public')->get($imagePath);

        $newImage = Image::cache(function ($image) use ($originalImage, $size) {
            $image->make($originalImage)
                ->widen($size);
        }, 60 * 24 * 31);

        return response($newImage)
            ->header('Content-type', 'image/png');
    }
    public static function generate($image_base_64, int $size,int $id)
    {
        $newImage = Image::cache(function ($image) use ($image_base_64, $size) {
            $image->make($image_base_64)
                ->widen($size);
        }, 60 * 24 * 31);

        return response($newImage)
            ->header('Content-type', 'image/png');
    }


    /**
     * @param $model
     * @param $property
     * @param $size
     * @return string|null
     */
    public static function generateUrl($model, string $property, int $size)
    {
        if (!Storage::disk('public')->exists($model->$property)) {
            return null;
        }

        $ext = pathinfo(storage_path($model->$property), PATHINFO_EXTENSION);

        return route(
            'dynamic-image',
            [
                'entity' => Str::snake(class_basename($model)),
                'id' => $model->id ?? 0,
                'property' => $property,
                'size' => $size,
                'ext' => $ext
            ],
            false
        );
    }
}
