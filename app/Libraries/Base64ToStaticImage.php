<?php


namespace App\Libraries;


use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;

class Base64ToStaticImage
{
    private static function generatePath(string $basePath): string
    {
        $name = md5(microtime(true)) . '.jpg';

        $date = now()->toArray();
        $dateToPath = collect($date)->filter(function ($val, $key) {
            return $key == 'year' || $key == 'month' || $key == 'day';
        })->implode(DIRECTORY_SEPARATOR);

        $storagePath = $basePath . DIRECTORY_SEPARATOR . $dateToPath;


        return $storagePath . DIRECTORY_SEPARATOR . $name;
    }

    public static function convert(string $base64, string $basePath)
    {
        $path = self::generatePath($basePath);

        if (check_base64_image($base64)) {
            $static = Image::make($base64)->encode('jpg');

            Storage::disk('public')->put($path, $static);

            return $path;
        }

        return null;
    }
}
