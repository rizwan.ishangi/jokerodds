<?php


namespace App\Libraries\GoalServe;


use ArrayIterator;
use DebugBar\DebugBar;
use GuzzleHttp\Client as HttpClient;
use GuzzleHttp\Promise\PromiseInterface;
use Illuminate\Support\Collection;
use Illuminate\Support\LazyCollection;

class GoalServeClient
{
    /**
     * GoalServe feed base url
     */
    const BASE_URL = 'https://www.goalserve.com/getfeed/f8d30291e0484499d9fc08d7e5247a9c/';

    /**
     * @var HttpClient
     */
    private $httpClient;

    /**
     * @var Collection
     */
    private $data;

    /**
     * GoalServeClient constructor.
     * @param HttpClient $client
     */
    public function __construct(HttpClient $client)
    {
        $this->httpClient = $client;
    }

    /**
     * @param string $targetPath
     * @return \Psr\Http\Message\ResponseInterface
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    private function request(string $targetPath)
    {
        return $this->httpClient->get(self::BASE_URL . $targetPath . '?json=1');
    }

    /**
     * @param $response
     */
    public function decodeResponse($response)
    {
        try {
            $data = json_decode($response, true);
            $this->data = collect($data)->forget('?xml');
        } catch (\Throwable $exception) {
            $this->data = collect();
        }
    }

    /**
     * @param Collection $data
     * @return Collection
     */
    private function sanitizeKeys(Collection $data)
    {
        $data = $data->mapWithKeys(function ($val, $key) {
            $key = str_replace('@', '', $key);

            if (is_array($val)) {
                $val = $this->sanitizeKeys(collect($val));
                $val = collect($val);
            }

            return [$key => $val];
        });

        return $data;
    }

    /**
     * @param $path
     * @return GoalServeResponse
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function get($path): GoalServeResponse
    {
        $this->request($path);

        return $this->getData();
    }

    /**
     * @param int $id
     * @return GoalServeResponse
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function getTeam(int $id)
    {
        $request = $this->request('soccerstats/team/' . $id);

        $this->decodeResponse($request->getBody()->getContents());

        return $this->getData();
    }

    /**
     * @return GoalServeResponse
     */
    public function getData()
    {
        return new GoalServeResponse($this->sanitizeKeys($this->data)->first());
    }
}
