<?php


namespace App\Libraries\GoalServe;


use Illuminate\Support\Collection;

class GoalServeResponse extends Collection
{
    public function __get($key)
    {
        return !is_a($this->get($key), 'Illuminate\Support\Collection') ? $this->get($key) : new self($this->get($key));
    }
}
