<?php
/**
 * Created by PhpStorm.
 * User: alex.cebotari@ourbox.org
 * Date: 24.03.2021
 * Time: 15:16
 */

namespace App\Libraries\OddsConvector;

/**
 * Class OddsConvector
 * @package App\Libraries\OddsConvector
 */
class OddsConvector
{
    /**
     * @var float $odd
     */
    private $odd;

    /**
     * @param  float  $odd
     * @param  float  $err
     *
     * @return string
     */
    private function decimalToFractional($odd, float $err = 0.001): string
    {
        if ($err <= 0.0 || $err >= 1.0) {
            $err = 0.001;
        }

        $key = ($odd < 0) ? -1 : 0;
        $sign = ($odd > 0) ? 1 : $key;

        if ($sign === -1) {
            $odd = abs($odd);
        }

        if ($sign !== 0) {
            // $err is the maximum relative $err; convert to absolute
            $err *= $odd;
        }

        $n = (int) floor($odd);
        $odd -= $n;

        if ($odd < $err) {
            return (string) ($sign * $n) . '/1';
        }

        if (1 - $err < $odd) {
            return (string) ($sign * ($n + 1)) . '/1';
        }

        // The lower fraction is 0/1
        $lowerN = 0;
        $lowerD = 1;

        // The upper fraction is 1/1
        $upper_n = 1;
        $upper_d = 1;

        while (true) {
            // The middle fraction is ($lowerN + $upper_n) / (lower_d + $upper_d)
            $middleN = $lowerN + $upper_n;
            $middleD = $lowerD + $upper_d;

            if ($middleD * ($odd + $err) < $middleN) {
                // real + $err < middle : middle is our new upper
                $upper_n = $middleN;
                $upper_d = $middleD;
            } elseif ($middleN < ($odd - $err) * $middleD) {
                // middle < real - $err : middle is our new lower
                $lowerN = $middleN;
                $lowerD = $middleD;
            } else {
                // Middle is our best fraction
                return (string) (($n * $middleD + $middleN) * $sign) . '/' . (string) $middleD;
            }
        }
    }

    /**
     * @param  float  $odd
     *
     * @return int
     */
    private function decimalToMoneyline(float $odd): ?int
    {
        if ($odd < 1.0) {
            return null;
        }

        if ($odd === 1.0) {
            $value = 0;
        } elseif ($odd >= 2) {
            $value = 100 * ($odd - 1);
        } else {
            $value = -100 / ($odd - 1);
        }

        return round($value, 2);
    }

    /**
     * OddsConvector constructor.
     *
     * @param  float  $odd
     */
    public function __construct(float $odd)
    {
        $this->odd = $odd;
    }

    /**
     * @return string|null
     */
    public function handle()
    {
        $request = request();

        if ($request->has('mode') && in_array($request->input('mode'), ['decimal', 'fractional', 'moneyline'])) {
            switch ($request->input('mode')) {
                case 'fractional':
                    return self::decimalToFractional($this->odd);
                case 'moneyline':
                    return self::decimalToMoneyline($this->odd);
                default:
                    return $this->odd;
            }
        }

        return $this->odd;
    }
}
