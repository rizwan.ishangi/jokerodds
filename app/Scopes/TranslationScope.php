<?php

namespace App\Scopes;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Scope;

class TranslationScope implements Scope
{
    /**
     * Apply the scope to a given Eloquent query builder.
     *
     * @param  Builder $builder
     * @param  Model   $model
     * @return void
     */
    public function apply(Builder $builder, Model $model)
    {
        $modelTable = $model->getTable();

        $builder->select($modelTable . '.*');

        $builder->join($modelTable . '_translation', $modelTable . '.id', '=', $modelTable . '_id')
            ->where('language_id', 1) // TODO: add User::getLanguage() here
            ->addSelect('name');
    }
}
