<?php


namespace App\Contracts\Standings;


interface StandingInterface
{
    public function leagueStandings($leagueId);
    public function fullLeagueStandings($leagueId);
}
