<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\ScoreboardStatusTranslation
 *
 * @property int $id
 * @property int $status_id
 * @property int $language_id
 * @property string $value
 * @property string $description
 * @method static \Illuminate\Database\Eloquent\Builder|ScoreboardStatusTranslation newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|ScoreboardStatusTranslation newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|ScoreboardStatusTranslation query()
 * @method static \Illuminate\Database\Eloquent\Builder|ScoreboardStatusTranslation whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ScoreboardStatusTranslation whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ScoreboardStatusTranslation whereLanguageId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ScoreboardStatusTranslation whereStatusId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ScoreboardStatusTranslation whereValue($value)
 * @mixin \Eloquent
 */
class ScoreboardStatusTranslation extends Model
{
    protected $table = 'scoreboard_status_translation';
}
