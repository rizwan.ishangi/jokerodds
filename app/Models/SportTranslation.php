<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\SportTranslation
 *
 * @property int $id
 * @property int $sport_id
 * @property int $language_id
 * @property string|null $name
 * @method static \Illuminate\Database\Eloquent\Builder|SportTranslation newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|SportTranslation newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|SportTranslation query()
 * @method static \Illuminate\Database\Eloquent\Builder|SportTranslation whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|SportTranslation whereLanguageId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|SportTranslation whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|SportTranslation whereSportId($value)
 * @mixin \Eloquent
 */
class SportTranslation extends Model
{
    protected $table = 'sport_translation';

    public $timestamps = false;

    protected $fillable = ['name', 'sport_id', 'language_id'];

    public function sport()
    {
        return $this->belongsTo(Sport::class, 'sport_id', 'id');
    }
}
