<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * App\Models\Event
 *
 * @method static \Illuminate\Database\Eloquent\Builder|Event newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Event newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Event query()
 * @mixin \Eloquent
 * @property int $id
 * @property int $fixture_id
 * @property int|null $livescore_id
 * @method static \Illuminate\Database\Eloquent\Builder|Event whereFixtureId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Event whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Event whereLivescoreId($value)
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Fixture[] $fixtures
 * @property-read int|null $fixtures_count
 * @property string $bets_last_update
 * @method static \Illuminate\Database\Eloquent\Builder|Event whereBetsLastUpdate($value)
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Bet[] $bets
 * @property-read int|null $bets_count
 * @property-read \App\Models\Scoreboard|null $scoreboard
 * @property-read \App\Models\Fixture $fixture
 */
class Event extends Model
{
    use SoftDeletes;

    protected $table = 'event';

    public $timestamps = false;

    protected $casts = [
        'bets_last_update' => 'datetime'
    ];

    protected $guarded = ['id'];

    public function fixture()
    {
        return $this->belongsTo(Fixture::class);
    }

    public function bets()
    {
        return $this->hasMany(Bet::class);
    }

    public function scoreboard()
    {
        return $this->hasOne(Scoreboard::class);
    }

    public function getNameAttribute()
    {
        return $this->fixture->participantsName;
    }
}
