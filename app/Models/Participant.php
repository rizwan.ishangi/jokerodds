<?php

namespace App\Models;

use App\Scopes\ActiveScope;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Participant
 *
 * @property int $id
 * @property int $participant_id
 * @property string $name
 * @property int $active
 * @method static \Illuminate\Database\Eloquent\Builder|Participant newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Participant newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Participant query()
 * @method static \Illuminate\Database\Eloquent\Builder|Participant whereActive($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Participant whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Participant whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Participant whereParticipantId($value)
 * @mixin \Eloquent
 */
class Participant extends Model
{
    protected $table = 'participant';
    protected $guarded = [
        'id'
    ];

    public $timestamps = false;

    protected static function boot()
    {
        parent::boot();

        //static::addGlobalScope(new ActiveScope());
    }

    public function goalServe()
    {
        return $this->hasOne(GoalServeTeam::class, 'participant_id', 'id');
    }
}
