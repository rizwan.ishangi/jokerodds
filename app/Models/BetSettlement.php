<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\BetSettlement
 *
 * @property int $id
 * @property string $value
 * @property string $decriptions
 * @method static \Illuminate\Database\Eloquent\Builder|BetSettlement newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|BetSettlement newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|BetSettlement query()
 * @method static \Illuminate\Database\Eloquent\Builder|BetSettlement whereDecriptions($value)
 * @method static \Illuminate\Database\Eloquent\Builder|BetSettlement whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|BetSettlement whereValue($value)
 * @mixin \Eloquent
 * @property string $description
 * @method static \Illuminate\Database\Eloquent\Builder|BetSettlement whereDescription($value)
 */
class BetSettlement extends Model
{
    protected $table = 'bet_settlement';
}
