<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\ScoreboardStatus
 *
 * @property int $id
 * @property int $active
 * @method static \Illuminate\Database\Eloquent\Builder|ScoreboardStatus newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|ScoreboardStatus newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|ScoreboardStatus query()
 * @method static \Illuminate\Database\Eloquent\Builder|ScoreboardStatus whereActive($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ScoreboardStatus whereId($value)
 * @mixin \Eloquent
 */
class ScoreboardStatus extends Model
{
    protected $table = 'scoreboard_status';
}
