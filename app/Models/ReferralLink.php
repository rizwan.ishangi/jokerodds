<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ReferralLink extends Model
{
    //
    protected $table='referral_link';
    public $timestamps = false;
}
