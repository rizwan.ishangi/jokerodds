<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * App\Models\Bookmaker
 *
 * @method static \Illuminate\Database\Eloquent\Builder|Bookmaker newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Bookmaker newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Bookmaker query()
 * @mixin \Eloquent
 * @property int $id
 * @property string $name
 * @property int $active
 * @method static \Illuminate\Database\Eloquent\Builder|Bookmaker whereActive($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Bookmaker whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Bookmaker whereName($value)
 */
class Bookmaker extends Model
{
    use SoftDeletes;

    protected $table = 'bookmaker';

    public $timestamps = false;
}
