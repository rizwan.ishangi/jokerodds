<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\PeriodTypeTranslation
 *
 * @property int $id
 * @property int $period_type_id
 * @property int $language_id
 * @property string $description
 * @method static \Illuminate\Database\Eloquent\Builder|PeriodTypeTranslation newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|PeriodTypeTranslation newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|PeriodTypeTranslation query()
 * @method static \Illuminate\Database\Eloquent\Builder|PeriodTypeTranslation whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PeriodTypeTranslation whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PeriodTypeTranslation whereLanguageId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PeriodTypeTranslation wherePeriodTypeId($value)
 * @mixin \Eloquent
 */
class PeriodTypeTranslation extends Model
{
    protected $table = 'period_type_translation';
}
