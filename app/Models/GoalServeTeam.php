<?php

namespace App\Models;

use App\Libraries\ImageGenerator;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class GoalServeTeam extends Model
{
    use SoftDeletes;

    protected $table = 'goalserve_teams';

    protected $fillable = [
        'participant_id',
        'team_id',
        'name',
        'image'
    ];

    protected $casts = [
        'team_id' => 'integer'
    ];

    protected $appends = [
        'img_170'
    ];

    public function participant()
    {
        return $this->belongsTo(Participant::class, 'participant_id', 'id');
    }

    public function getImg170Attribute()
    {
        return ImageGenerator::generateUrl($this, 'image', 170);
    }
}
