<?php

namespace App\Models;

use App\Scopes\ActiveScope;
use App\Scopes\TranslationScope;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * App\Models\Location
 *
 * @property int $id
 * @property string|null $img
 * @property int $active
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\League[] $leagues
 * @property-read int|null $leagues_count
 * @method static \Illuminate\Database\Eloquent\Builder|Location newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Location newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Location query()
 * @method static \Illuminate\Database\Eloquent\Builder|Location whereActive($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Location whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Location whereImg($value)
 * @mixin \Eloquent
 * @property string $iso_2
 * @method static \Illuminate\Database\Eloquent\Builder|Location whereIso2($value)
 */
class Location extends Model
{
    use SoftDeletes;

    protected $table = 'location';

    protected $withCount = ['leagues'];

    public $timestamps = false;

    protected static function boot()
    {
        parent::boot();

        static::addGlobalScope(new TranslationScope());
        static::addGlobalScope(new ActiveScope());
    }

    public function leagues()
    {
        return $this->hasMany('App\Models\League');
    }

    public function translation()
    {
        return $this->hasOne(LocationTranslation::class, 'location_id', 'id');
    }

    public function getFullNameAttribute()
    {
        return $this->translation->name;
    }
}
