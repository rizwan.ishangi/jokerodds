<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Scoreboard
 *
 * @method static \Illuminate\Database\Eloquent\Builder|Scoreboard newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Scoreboard newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Scoreboard query()
 * @mixin \Eloquent
 * @property int $id
 * @property int $event_id
 * @property int $status_id
 * @property int $time
 * @property mixed $results
 * @method static \Illuminate\Database\Eloquent\Builder|Scoreboard whereEventId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Scoreboard whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Scoreboard whereResults($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Scoreboard whereStatusId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Scoreboard whereTime($value)
 */
class Scoreboard extends Model
{
    protected $table = 'scoreboard';
}
