<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Banner extends Model
{
    use SoftDeletes;

    protected $table = 'banners';

    protected $fillable = [
        'img',
        'views',
        'clicks',
        'page',
        'active'
    ];

    public function addView(): void
    {
        $this->views->increment();
    }

    public function addClick(): void
    {
        $this->clicks->increment();
    }

    public function scopePage($query, string $pageName)
    {
        return $query->where('page', $pageName);
    }
}
