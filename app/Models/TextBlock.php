<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Cache;

class TextBlock extends Model
{
    use SoftDeletes;

    protected $table = 'text_blocks';

    protected $fillable = [
        'name',
        'content'
    ];

    public $timestamps = false;

    public function scopeName($query, string $blockName)
    {
        return $query->where('name', $blockName);
    }

    public static function getCached() {
        return Cache::remember('text_blocks', 60 * 24, function () {
            return static::get();
        });
    }
}
