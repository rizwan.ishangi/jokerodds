<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\FixtureParticipant
 *
 * @property int $id
 * @property int $fixture_id
 * @property int $participant_id
 * @property int $position
 * @method static \Illuminate\Database\Eloquent\Builder|FixtureParticipant newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|FixtureParticipant newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|FixtureParticipant query()
 * @method static \Illuminate\Database\Eloquent\Builder|FixtureParticipant whereFixtureId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FixtureParticipant whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FixtureParticipant whereParticipantId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FixtureParticipant wherePosition($value)
 * @mixin \Eloquent
 */
class FixtureParticipant extends Model
{
    protected $table = 'fixture_participant';
}
