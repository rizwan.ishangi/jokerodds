<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\IncidentType
 *
 * @method static \Illuminate\Database\Eloquent\Builder|IncidentType newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|IncidentType newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|IncidentType query()
 * @mixin \Eloquent
 * @property int $id
 * @property int $sport_id
 * @property string $description
 * @method static \Illuminate\Database\Eloquent\Builder|IncidentType whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|IncidentType whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|IncidentType whereSportId($value)
 */
class IncidentType extends Model
{
    protected $table = 'incident_type';
}
