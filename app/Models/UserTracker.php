<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserTracker extends Model
{
    protected $table = 'user_trackers';

    protected $casts = [
        'coefficient' => 'array'
    ];

    protected $with = ['event'];

    protected $fillable = ['event_id', 'coefficient', 'ip_address'];

    public function event()
    {
        return $this->hasOne(Event::class, 'id', 'event_id');
    }
}
