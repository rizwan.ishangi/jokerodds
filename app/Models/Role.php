<?php


namespace App\Models;


use Pktharindu\NovaPermissions\Role as BaseRole;

class Role extends BaseRole
{
    public function users()
    {
        return $this->belongsToMany(config('nova-permissions.user_model', 'App\Models\User'), config('nova-permissions.table_names.role_user', 'role_user'), 'role_id', 'admin_id');
    }
}
