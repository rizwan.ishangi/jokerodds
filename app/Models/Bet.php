<?php

namespace App\Models;

use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use SebastianBergmann\Comparator\Book;

/**
 * App\Models\Bet
 *
 * @property int $id
 * @property string $name
 * @property int $status_id
 * @property string $statrt_price
 * @property string $price
 * @property int $settlement_id
 * @property string $last_update
 * @method static Builder|Bet newModelQuery()
 * @method static Builder|Bet newQuery()
 * @method static Builder|Bet query()
 * @method static Builder|Bet whereId($value)
 * @method static Builder|Bet whereLastUpdate($value)
 * @method static Builder|Bet whereName($value)
 * @method static Builder|Bet wherePrice($value)
 * @method static Builder|Bet whereSettlementId($value)
 * @method static Builder|Bet whereStatrtPrice($value)
 * @method static Builder|Bet whereStatusId($value)
 * @mixin Eloquent
 * @property int $event_id
 * @property int $bookmaker_id
 * @property int $market_id
 * @property string|null $line
 * @property string|null $base_line
 * @property string $start_price
 * @method static Builder|Bet whereBaseLine($value)
 * @method static Builder|Bet whereBookmakerId($value)
 * @method static Builder|Bet whereEventId($value)
 * @method static Builder|Bet whereLine($value)
 * @method static Builder|Bet whereMarketId($value)
 * @method static Builder|Bet whereStartPrice($value)
 * @property-read Market $market
 * @property int $api_bet_id bet id provided by api
 * @property int $is_latest
 * @property int $is_inplay
 * @property-read \App\Models\Bookmaker $bookmaker
 * @property-read \App\Models\Event $event
 * @method static Builder|Bet whereApiBetId($value)
 * @method static Builder|Bet whereIsInplay($value)
 * @method static Builder|Bet whereIsLatest($value)
 */
class Bet extends Model
{
    public const OPEN_BET = 1;
    public const SUSPENDED_BET = 2;
    public const SETTLED_BET = 3;

    protected $table = 'bet';

    public $timestamps = false;

    protected $casts = [
        'price' => 'decimal:2',
        'start_price' => 'decimal:2'
    ];

    public function event()
    {
        return $this->belongsTo(Event::class);
    }

    public function market()
    {
        return $this->belongsTo(Market::class);
    }

    public function bookmaker()
    {
        return $this->belongsTo(Bookmaker::class, 'bookmaker_id', 'id');
    }
}
