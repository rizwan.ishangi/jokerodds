<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\LocationTranslation
 *
 * @property int $id
 * @property int $location_id
 * @property int $language_id
 * @property string $name
 * @method static \Illuminate\Database\Eloquent\Builder|LocationTranslation newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|LocationTranslation newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|LocationTranslation query()
 * @method static \Illuminate\Database\Eloquent\Builder|LocationTranslation whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|LocationTranslation whereLanguageId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|LocationTranslation whereLocationId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|LocationTranslation whereName($value)
 * @mixin \Eloquent
 */
class LocationTranslation extends Model
{
    protected $table = 'location_translation';

    protected $fillable = [
        'location_id',
        'language_id',
        'name'
    ];

    public function location() {
        return $this->belongsTo(Location::class, 'location_id', 'id');
    }
}
