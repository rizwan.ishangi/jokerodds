<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class LocationSimpleModel extends Model
{
    use SoftDeletes;

    protected $table = 'location';

    public $timestamps = false;

    public function leagues()
    {
        return $this->hasMany('App\Models\League', 'location_id', 'id');
    }

    public function translation()
    {
        return $this->hasOne(LocationTranslation::class, 'location_id', 'id');
    }
}
