<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\PeriodType
 *
 * @method static \Illuminate\Database\Eloquent\Builder|PeriodType newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|PeriodType newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|PeriodType query()
 * @mixin \Eloquent
 * @property int $id
 * @property int $sport_id
 * @method static \Illuminate\Database\Eloquent\Builder|PeriodType whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PeriodType whereSportId($value)
 */
class PeriodType extends Model
{
    protected $table = 'period_type';
}
