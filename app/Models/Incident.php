<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Incident
 *
 * @method static \Illuminate\Database\Eloquent\Builder|Incident newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Incident newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Incident query()
 * @mixin \Eloquent
 * @property int $id
 * @property int $period_id
 * @property int $seconds
 * @property int $participant_position
 * @property string $player_name
 * @property int $type_id
 * @method static \Illuminate\Database\Eloquent\Builder|Incident whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Incident whereParticipantPosition($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Incident wherePeriodId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Incident wherePlayerName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Incident whereSeconds($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Incident whereTypeId($value)
 */
class Incident extends Model
{
    protected $table = 'incident';
}
