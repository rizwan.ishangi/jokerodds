<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Period
 *
 * @method static \Illuminate\Database\Eloquent\Builder|Period newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Period newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Period query()
 * @mixin \Eloquent
 * @property int $id
 * @property int $type_id
 * @property int $event_id
 * @property int $is_finished
 * @property int $is_confirmed
 * @property mixed $results
 * @method static \Illuminate\Database\Eloquent\Builder|Period whereEventId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Period whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Period whereIsConfirmed($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Period whereIsFinished($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Period whereResults($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Period whereTypeId($value)
 */
class Period extends Model
{
    protected $table = 'period';
}
