<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BetHistory extends Model
{
    //

    public const OPEN_BET = 1;
    public const SUSPENDED_BET = 2;
    public const SETTLED_BET = 3;
    protected $table = 'bet_history';
    public $timestamps = false;

    protected $casts = [
        'price' => 'decimal:2',
        'start_price' => 'decimal:2'
    ];

    public function event()
    {
        return $this->belongsTo(Event::class);
    }

    public function market()
    {
        return $this->belongsTo(Market::class);
    }

    public function bookmaker()
    {
        return $this->belongsTo(Bookmaker::class, 'bookmaker_id', 'id');
    }
}
