<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BetLive extends Model
{
    //
    protected $table ='bet_live';

    public function market()
    {
        return $this->belongsTo(Market::class);
    }

    public function bookmaker()
    {
        return $this->belongsTo(Bookmaker::class, 'bookmaker_id', 'id');
    }

}
