<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class OneXBetLeague extends Model
{
    //
    protected $table = 'one_x_bet_league';
    public $timestamps = false;

    public function lSportLeague(){
        return $this->belongsTo(League::class,'league_id','id');
    }
}
