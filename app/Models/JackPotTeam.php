<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class JackPotTeam extends Model
{
    //
    protected $table = 'jack_pot_team';
    public $timestamps  = false;
    public function participant(){
        return $this->belongsTo(Participant::class,'participant_id', 'id');
    }
}
