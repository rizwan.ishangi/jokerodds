<?php

namespace App\Models;

use Carbon\Carbon;
use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * App\Models\Fixture
 *
 * @property int $id
 * @property int $location_id
 * @property int $sport_id
 * @property int $league_id
 * @property int $status_id
 * @property string|null|Carbon $start_date
 * @property string|null|Carbon $last_update
 * @property-read Collection|Participant[] $participants
 * @property-read int|null $participants_count
 * @method static Builder|Fixture newModelQuery()
 * @method static Builder|Fixture newQuery()
 * @method static Builder|Fixture query()
 * @method static Builder|Fixture whereFixtureId($value)
 * @method static Builder|Fixture whereId($value)
 * @method static Builder|Fixture whereLastUpdate($value)
 * @method static Builder|Fixture whereLeagueId($value)
 * @method static Builder|Fixture whereLocationId($value)
 * @method static Builder|Fixture whereSportId($value)
 * @method static Builder|Fixture whereStartDate($value)
 * @method static Builder|Fixture whereStatusId($value)
 * @mixin Eloquent
 * @property-read \App\Models\Event|null $event
 * @property-read \App\Models\League $league
 * @property-read \App\Models\Location $location
 */
class Fixture extends Model
{
    use SoftDeletes;

    protected $table = 'fixture';

    protected $guarded = ['id'];

    protected $dates = [
        'start_date',
        'last_update',
    ];

    public function participants()
    {
        return $this->belongsToMany(Participant::class);
    }

    public function event()
    {
        return $this->hasOne(Event::class, 'fixture_id', 'id');
    }

    public function bets()
    {
        return $this->hasManyThrough(Bet::class, Event::class, 'fixture_id', 'event_id', 'id' , 'id');
    }

    public function location()
    {
        return $this->belongsTo(Location::class);
    }

    public function locationSimple()
    {
        return $this->belongsTo(LocationSimpleModel::class, 'location_id', 'id');
    }

    public function league()
    {
        return $this->belongsTo(League::class);
    }

    public function getParticipantsNameAttribute()
    {
        return $this->participants->implode('name', ' - ');
    }
}
