<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\BetStatus
 *
 * @property int $id
 * @property string $value
 * @property string $description
 * @method static \Illuminate\Database\Eloquent\Builder|BetStatus newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|BetStatus newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|BetStatus query()
 * @method static \Illuminate\Database\Eloquent\Builder|BetStatus whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|BetStatus whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|BetStatus whereValue($value)
 * @mixin \Eloquent
 */
class BetStatus extends Model
{
    public const STATUS_OPEN = 1;
    public const STATUS_SUSPENDED = 2;
    public const STATUS_SETTLED = 3;
    public const STATUS_DELETED = 4;

    protected $table = 'bet_status';
}
