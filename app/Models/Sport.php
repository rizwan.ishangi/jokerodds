<?php

namespace App\Models;

use App\Scopes\ActiveScope;
use App\Scopes\TranslationScope;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * App\Models\Sport
 *
 * @property int $id
 * @property string|null $img
 * @property int $active
 * @property int $display_priority
 * @method static \Illuminate\Database\Eloquent\Builder|Sport newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Sport newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Sport query()
 * @method static \Illuminate\Database\Eloquent\Builder|Sport whereActive($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Sport whereDisplayPriority($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Sport whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Sport whereImg($value)
 * @mixin \Eloquent
 */
class Sport extends Model
{
    use SoftDeletes;

    protected $table = 'sport';

    protected $guarded = [
        'id'
    ];

    public $timestamps = false;

    protected static function boot()
    {
        parent::boot();


        static::addGlobalScope(new TranslationScope());
//        static::addGlobalScope(new ActiveScope());
    }


    public function translation()
    {
        return $this->hasOne(SportTranslation::class, 'sport_id', 'id');
    }

    public function scopeActive($query)
    {
        return $query->where('active', 1);
    }
}
