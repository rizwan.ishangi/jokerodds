<?php

namespace App\Models;

use App\Libraries\ImageGenerator;
use App\Scopes\ActiveScope;
use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Intervention\Image\Facades\Image;

/**
 * App\Models\League
 *
 * @property int $id
 * @property string $name
 * @property string|null $img
 * @property int|null $location_id
 * @property int $active
 * @property-read Collection|Fixture[] $fixtures
 * @property-read int|null $fixtures_count
 * @method static Builder|League newModelQuery()
 * @method static Builder|League newQuery()
 * @method static Builder|League query()
 * @method static Builder|League whereActive($value)
 * @method static Builder|League whereId($value)
 * @method static Builder|League whereImg($value)
 * @method static Builder|League whereLocationId($value)
 * @method static Builder|League whereName($value)
 * @mixin Eloquent
 * @property int $sport_id
 * @method static Builder|League whereSportId($value)
 */
class League extends Model
{
    use SoftDeletes;

    public $timestamps = false;
    protected $table = 'league';
    protected $guarded = [
        'id'
    ];

    protected $fillable = ['goal_league_id', 'goal_league_name', 'show_sidebar', 'img'];

    protected $casts = [
        'show_sidebar' => 'boolean'
    ];

    protected $appends = [
        'img_64'
    ];

    protected static function boot()
    {
        parent::boot();
    }

    public function fixtures()
    {
        return $this->hasMany(Fixture::class);
    }

    public function location()
    {
        return $this->hasOne(Location::class, 'id', 'location_id');
    }

    public function locationSimple()
    {
        return $this->hasOne(LocationSimpleModel::class, 'id', 'location_id');
    }

    public function sport()
    {
        return $this->hasOne(Sport::class, 'id', 'sport_id');
    }

    public function scopeActive($query)
    {
        return $query->where('active', 1);
    }

    public function scopeInSidebar($query)
    {
        return $query->where('show_sidebar', 1);
    }

    public function scopeNotInSidebar($query)
    {
        return $query->where('show_sidebar', 0);
    }

    public function getImg64Attribute()
    {
        return ImageGenerator::generateUrl($this, 'img', 64);
    }
}
