<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class OneXBetTeam extends Model
{
    //
    protected $table = 'one_x_bet_team';
    public $timestamps = false;

    public function participant(){
        return $this->belongsTo(Participant::class,'participant_id', 'id');
    }

}
