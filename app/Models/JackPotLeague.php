<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class JackPotLeague extends Model
{
    //
    protected $table    = 'jack_pot_league';
    public $timestamps  = false;

    public function league(){
        return $this->belongsTo(League::class,'league_id','id');
    }
}
