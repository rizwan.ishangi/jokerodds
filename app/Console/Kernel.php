<?php

namespace App\Console;

use App\Console\Commands\GoalServe\SyncTeams;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;
use Spatie\ShortSchedule\ShortSchedule;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        //
        SyncTeams::class,
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        // $schedule->command('inspire')
        //          ->hourly();

        $schedule->command('pull:repeating')->everyTwoHours();
        $schedule->command('pull:repeating --season')->weeklyOn(1, '12:00');

        $schedule->command('check:leagues-and-locations')->dailyAt('00:00');
        $schedule->command('check:leagues-and-locations')->dailyAt('03:00');
        $schedule->command('check:leagues-and-locations')->dailyAt('6:00');
        $schedule->command('check:leagues-and-locations')->dailyAt('10:00');
        $schedule->command('check:leagues-and-locations')->dailyAt('14:00');
        $schedule->command('check:leagues-and-locations')->dailyAt('18:00');
        $schedule->command('check:leagues-and-locations')->dailyAt('22:00');

        /**
         * Update a portion of events
         */
      //  $schedule->command('pull:coefficients instant')->everyMinute();
     //   $schedule->command('pull:coefficients instant --inplay')->everyTwoMinutes();
     //    $schedule->command('pull:coefficients near')->everyThreeHours();
    //    $schedule->command('pull:coefficients all')->everyFourMinutes();
        $schedule->command('history:add-bets')->everyTenMinutes();
        $schedule->command('history:remove-event-bets')->everySixHours();

        $schedule->command('1xbet:new-leagues')->everyThirtyMinutes();
        $schedule->command('1xbet:new-teams')->everyFiveMinutes();
        $schedule->command('jack-pot:new-leagues --interval=day --withTeams=true')->daily();
        // $schedule->command('tracker:score')->everyMinute();
        /**
         * Fetch Fixtures from GoalServe API
         * @doc https://www.goalserve.com/getfeed/f8d30291e0484499d9fc08d7e5247a9c/soccerfixtures/data/mapping
         */
        $schedule->command('pull:goal fixtures')->daily();
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__ . '/Commands');

        include base_path('routes/console.php');
    }

    protected function shortSchedule(ShortSchedule $shortSchedule)
    {
        $shortSchedule->command('tracker:score')->everySeconds(30)->withoutOverlapping();
    }
}
