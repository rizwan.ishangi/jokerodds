<?php

namespace App\Console\Commands;

use App\Models\League;
use App\Models\Location;
use App\Services\Lsports\PullOperations;
use Illuminate\Console\Command;

class PullLeagues extends Command
{
    public const ENG_LANG_ID = 1;

    public const LIMIT_SPORTS_IDS = [
        6046, // football
    ];
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'pull:new-leagues';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    private PullOperations $pullOperations;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(PullOperations $pullOperations)
    {
        $this->pullOperations = $pullOperations;
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $this->output->note('Pulling leagues list...');

        $lSportleagues = $this->pullOperations->getLeagues();
        $locations = Location::withTrashed()->pluck('name', 'id');
        $currentLeagues = League::withTrashed()->pluck('id')->toArray();
      //  $toAdd = [];


        $newLeagueCnt = 0;
        foreach ($lSportleagues as $league) {
            if (!in_array($league->SportId, self::LIMIT_SPORTS_IDS)) {
                continue;
            }

            /**
             * Skip leagues from unnecessary location
             */
            if (empty($locations[$league->LocationId])) {
                $this->output->warning('League ' . $league->Name . ' rejected b/c no location added in database');
                continue;
            }


            if(!in_array($league->Id, $currentLeagues)){

                             League::insert(
                                     ['id' => $league->Id,
                                         'name' => $league->Name,
                                         'img' => '',
                                         'sport_id' => $league->SportId,
                                         'location_id' => $league->LocationId,
                                         'active' => true,
                                     ]
             );
           //     $toAdd[] = $league;
                $newLeagueCnt++;
            }


        }
       // $this->output->success('New Leagues: ' . print_r($toAdd));
        $this->output->success('Successfully pulled leagues: ' . $newLeagueCnt);
        return 0;
    }
}
