<?php

namespace App\Console\Commands;

use GuzzleHttp\Client;
use Illuminate\Console\Command;

class EnableLSportApi extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'enable:lsport';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command to enable api';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->username = config('lsports.username');
        $this->password = config('lsports.password');
        $this->guid = config('lsports.prematchGuid');
        $this->url = 'https://prematch.lsports.eu/OddService/EnablePackage';

        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $params = [
            'username' => $this->username,
            'password' => $this->password,
            'guid' => $this->guid
        ];

        $query = '?' . http_build_query($params);

        $client = New Client([
            'timeout' => 60 * 5
        ]);

        $response = $client->request('GET', $this->url . $query);

        dd(json_decode($response->getBody()->getContents()));
        return 0;
    }
}
