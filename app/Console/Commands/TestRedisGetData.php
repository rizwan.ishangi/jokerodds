<?php

namespace App\Console\Commands;

use App\Libraries\OddsConvector\OddsConvector;
use App\Models\BetsAverage;
use App\Models\BetStatus;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redis;
use Symfony\Component\Console\Output\ConsoleOutput;

class TestRedisGetData extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'redis:get-test-data';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $out = new ConsoleOutput();

        $fixtureIds = DB::table('fixture')->select(['id'])
            ->whereBetween('start_date', [
                now()->setTime(0, 0, 0),
                now()->setTime(23, 59, 59),
            ])
            ->where('status_id', '!=', BetStatus::STATUS_DELETED);

        /* Get all fixture ids that have event with bets */
            $fixturesWithBets_sub = DB::table('event')
            ->select(['id', 'fixture_id', DB::raw('(select 1 from `bet` where `event`.`id` = `bet`.`event_id` limit 1) as `bets_count`')])
            ->whereIn('fixture_id', $fixtureIds);

        $fixturesWithBets = DB::query()->fromSub($fixturesWithBets_sub,'sub')->whereRaw("bets_count=1")->get();

        /* Get event ids and fixture ids */
        $eventIds = $fixturesWithBets->pluck('id')->unique()->values()->toArray();

        $data = [];

        foreach ($eventIds as  $event) {
            $key = 'average#'.$event.'#1#1';
            $data[$key] =  '';

            $key = 'average#'.$event.'#1#2';
            $data[$key] =  '';

            $key = 'average#'.$event.'#1#X';
            $data[$key] =  '';
        }

        $eventData = Redis::mget(array_keys($data));
        $index = 0;

        foreach ($data as $key => $value) {
            $data[$key] = $eventData[$index];
            $index ++;
        }
        $prematch = [];

        foreach ($data as $key => $datum) {
            $keys = explode('#',$key);
              $values = explode('#',$datum);
            $prematch[]=[
                'event_id' =>$keys[1],
                'market_id'=>$keys[2],
                'base_line' => null,
                'name'      => $keys[3],
                'average'   => $values[1] ?? ''
            ];
        }


        $prematch = collect($prematch);

             $bets = $prematch->filter(function ($item){
                 return $item['average'] !== '';
             })->groupBy('event_id')->map(function ($bets){
                 return [
                     'prematch' => $bets->groupBy('market_id')->map(function ($market) {
                         return $market->groupBy('base_line')->map(function ($bets, $line) {
                             return [
                                 'line' => null,
                                 'coefficientsGroups' => $bets->unique('name')->pluck('name')->toArray(),
                                 'coefficients' => $bets->groupBy('name')->map(function ($bet) {
                                     return (new OddsConvector($bet[0]['average']))->handle();
                                 })->toArray()
                             ];
                         })->values();
                     })->toArray()
                 ];
             })->toArray();

        $out->writeln(print_r($bets));
        return 0;
    }
}
