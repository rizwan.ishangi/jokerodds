<?php

namespace App\Console\Commands;

use App\Events\BetsUpdated;
use Illuminate\Console\Command;

class TestEventEcho extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'event:test-echo';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        BetsUpdated::dispatch(166041);
        return 0;
    }
}
