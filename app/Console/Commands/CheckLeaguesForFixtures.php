<?php

namespace App\Console\Commands;

use App\Models\League;
use Illuminate\Console\Command;

class CheckLeaguesForFixtures extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'check:leagues';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command to check leagues for existing fixtures';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle(): void
    {
        $this->info('Processing');
        $leagues = League::select('id')->withCount('fixtures')->get();
        $leaguesWithFixtures = $leagues->filter(function ($league) {
            return $league->fixtures_count > 0;
        });
        $leaguesWithoutFixtures = $leagues->filter(function ($league) {
            return $league->fixtures_count === 0;
        });

        if ($leaguesWithFixtures->count() > 0) {
            League::whereIn('id', $leaguesWithFixtures->pluck('id')->toArray())->update(['has_fixtures' => true]);
        }
        if ($leaguesWithoutFixtures->count() > 0) {
            League::whereIn('id', $leaguesWithoutFixtures->pluck('id')->toArray())->update(['has_fixtures' => false]);
        }
        $this->info('Job done!');
    }
}
