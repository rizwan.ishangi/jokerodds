<?php

namespace App\Console\Commands;

use App\Models\Bet;
use App\Models\BetSettlement;
use App\Models\BetStatus;
use App\Models\Bookmaker;
use App\Models\Event;
use App\Models\Fixture;
use App\Models\FixtureParticipant;
use App\Models\IncidentType;
use App\Models\Language;
use App\Models\League;
use App\Models\Location;
use App\Models\LocationTranslation;
use App\Models\Market;
use App\Models\Participant;
use App\Models\PeriodType;
use App\Models\PeriodTypeTranslation;
use App\Models\ScoreboardStatus;
use App\Models\ScoreboardStatusTranslation;
use App\Models\Sport;
use App\Models\SportTranslation;
use App\Services\Lsports\PullOperations;
use App\Traits\EventsPullTrait;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminated\Console\WithoutOverlapping;

class InitialPullData extends Command
{
    use EventsPullTrait;

    public const ENG_LANG_ID = 1;

    public const FETCH_EVENTS_DAYS_AGO = 3;

    public const LIMIT_SPORTS_IDS = [
        6046, // football
    ];

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'pull:initial';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command allows to fetch data from api, including dictionaries';

    private PullOperations $pullOperations;

    /**
     * Create a new command instance.
     *
     * @param  PullOperations  $pullOperations
     */
    public function __construct(PullOperations $pullOperations)
    {
        $this->pullOperations = $pullOperations;

        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        \DB::statement('SET FOREIGN_KEY_CHECKS=0');

        Market::truncate();
        SportTranslation::truncate();
        Sport::truncate();
        League::truncate();
        Bookmaker::truncate();
        LocationTranslation::truncate();
        Location::truncate();
        Language::truncate();
        Fixture::truncate();
        Event::truncate();
        FixtureParticipant::truncate();
        Participant::truncate();
        Bet::truncate();
        BetStatus::truncate();
        BetSettlement::truncate();
        ScoreboardStatus::truncate();
        ScoreboardStatusTranslation::truncate();
        PeriodType::truncate();
        PeriodTypeTranslation::truncate();
        IncidentType::truncate();

        \DB::statement('SET FOREIGN_KEY_CHECKS=1');

        Language::insert([
            'id' => self::ENG_LANG_ID,
            'name' => 'English',
            'abbreviation' => 'eng',
        ]);

        $this->initBetDictionaries();
        sleep(5);

        $this->pullMarkets();
        sleep(5);

        $this->pullSports();
        sleep(5);

        $this->pullLocations();
        sleep(5);

        $this->pullLeagues();
        sleep(5);

        $this->pullBookmakers();
        sleep(5);

        $this->pullEvents();
    }

    private function initBetDictionaries()
    {
        $statuses = [
            ['1', 'Open', 'The bet is open (bets can be placed)'],
            ['2', 'Suspended', 'The bet is suspended (bets cannot be placed)'],
            [
                '3', 'Settled',
                'The bet is settled (resulted) – a settlement is determined'
            ],
        ];

        foreach ($statuses as $status) {
            BetStatus::insert([
                'id' => $status[0],
                'value' => $status[1],
                'description' => $status[2],
            ]);
        }

        $settlements = [
            [-1, 'Cancelled', 'The bet settlement is cancelled'],
            [1, 'Loser', 'The bet has lost'],
            [2, 'Winner', 'The bet has won'],
            [3, 'Refund', 'There should be a refund'],
            [4, 'HalfLost', 'Half of the bet has lost'],
            [5, 'HalfWon', 'Half of the bet has won'],
        ];

        foreach ($settlements as $settlement) {
            BetSettlement::insert([
                'id' => $settlement[0],
                'value' => $settlement[1],
                'description' => $settlement[2],
            ]);
        }

        $scoreboardStatuses = [
            [1, 'not started yet', 'The event has not started yet'],
            [2, 'in progress', 'The event is live'],
            [3, 'finished', 'The event is finished'],
            [4, 'cancelled', 'The event has been cancelled'],
            [5, 'postponed', 'The event has been postponed'],
            [6, 'interrupted', 'The event has been interrupted'],
            [7, 'abandoned', 'The event has been abandoned'],
            [8, 'coverage lost', 'The coverage for this event has been lost'],
            [9, 'about to start', 'The event has not started but is about to'],
        ];

        foreach ($scoreboardStatuses as $status) {
            ScoreboardStatus::insert([
                'id' => $status[0],
                'active' => true,
            ]);

            ScoreboardStatusTranslation::insert([
                'status_id' => $status[0],
                'language_id' => self::ENG_LANG_ID,
                'value' => $status[1],
                'description' => $status[2],
            ]);
        }
    }

    private function pullBookmakers()
    {
        $this->output->note('Pulling bookmakers list...');
        $bookmakers = $this->pullOperations->getBookmakers();

        foreach ($bookmakers as $bookmaker) {
            Bookmaker::insert(
                ['id' => $bookmaker->Id,
                'name' => $bookmaker->Name,
                'active' => true]
            );
        }

        $this->output->success('Successfully pulled bookmakers: ' . count($bookmakers));
    }

    private function pullLocations()
    {
        $this->output->note('Pulling locations list...');
        $locations = $this->pullOperations->getLocations();

        foreach ($locations as $location) {
            $id = Location::insertGetId([
                'id' => $location->Id,
                'active' => true,
                'iso_2' => '',
            ]);

            LocationTranslation::insert([
                'location_id' => $id,
                'name' => $location->Name,
                'language_id' => self::ENG_LANG_ID,
            ]);
        }

        $this->output->success('Successfully pulled locations: ' . count($locations));
    }

    private function pullSports()
    {
        $this->output->note('Pulling sports list...');
        $sports = $this->pullOperations->getSports();

        foreach ($sports as $index => $sport) {
            $id = Sport::insertGetId([
                'id' => $sport->Id,
                'img' => '',
                'active' => true,
                'display_priority' => $index + 1,
            ]);

            SportTranslation::insert([
                'sport_id' => $id,
                'language_id' => self::ENG_LANG_ID,
                'name' => $sport->Name,
            ]);
        }

        $periodTypes = [
            [10, '1st Half'],
            [20, '2nd Half'],
            [30, 'Overtime 1st Half'],
            [35, 'Overtime 2nd Half'],
            [50, 'Penalties'],
            [100, 'Full time'],
            [101, 'Full time after overtime'],
            [102, 'Full time after penalties'],
        ];

        foreach ($periodTypes as $periodType) {
            PeriodType::insert([
                'id' => $periodType[0],
                'sport_id' => 6046,
            ]);

            PeriodTypeTranslation::insert([
                'period_type_id' => $periodType[0],
                'language_id' => self::ENG_LANG_ID,
                'description' => $periodType[1],
            ]);
        }

        $incidentTypes = [
            [1, 'Corners'],
            [6, 'Yellow cards'],
            [7, 'Red cards'],
            [8, 'Penalties'],
            [9, 'Goal'],
            [10, 'Substitutions'],
            [24, 'Own goal'],
            [25, 'Penalty goal'],
            [40, 'Missed penalty'],
            [27, 'Unknown'],
        ];

        foreach ($incidentTypes as $type) {
            IncidentType::insert([
                'id' => $type[0],
                'sport_id' => 6046,
                'description' => $type[1],
            ]);
        }

        $this->output->success('Successfully pulled sports: ' . count($sports));
    }

    private function pullLeagues()
    {
        $this->output->note('Pulling leagues list...');

        $leagues = $this->pullOperations->getLeagues();

        $locations = Location::pluck('name', 'id');

        foreach ($leagues as $league) {
            if (!in_array($league->SportId, self::LIMIT_SPORTS_IDS)) {
                continue;
            }

            /**
             * Skip leagues from unnecessary location
             */
            if (empty($locations[$league->LocationId])) {
                $this->output->warning('League ' . $league->Name . ' rejected b/c no location added in database');
                continue;
            }

            League::insert(
                ['id' => $league->Id,
                'name' => $league->Name,
                'img' => '',
                'sport_id' => $league->SportId,
                'location_id' => $league->LocationId,
                'active' => true,
                ]
            );
        }

        $this->output->success('Successfully pulled leagues: ' . count($leagues));
    }

    private function pullMarkets()
    {
        $this->output->note('Pulling markets list...');

        $markets = $this->pullOperations->getMarkets();

        foreach ($markets as $market) {
            Market::insert([
                'id' => $market->Id,
                'name' => $market->Name,
                'active' => true,
            ]);
        }

        $this->output->success('Successfully pulled markets: ' . count($markets));
    }

    private function pullEvents()
    {
        $this->output->note('Pulling events list...');

        $fromDate = Carbon::today()->subDays(self::FETCH_EVENTS_DAYS_AGO);
        $toDate = Carbon::today()->addWeek();

        $eventsCount = 0;

        while (!$fromDate->greaterThanOrEqualTo($toDate)) {
            $this->output->note('Updating events for day ' . $fromDate);

            $eventsCount += $this->pullEventsForRange(
                $fromDate,
                (clone $fromDate)->addDay()
            );

            $fromDate->addDay();
        }

        $this->output->success('Successfully pulled events: ' . $eventsCount);
    }
}
