<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Services\Lsports\LsportMQ;
use App\Services\Lsports\PullOperations;
use App\Services\Lsports\PrematchMQ;

class DebugMQ extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'debug:mq';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
	echo "RMQ Consume started".PHP_EOL;
	$mc = new PrematchMQ;
	$mc->connect();
	$mc->consume();

	//dd($r);

        //return 0;
    }
}
