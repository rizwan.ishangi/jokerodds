<?php

namespace App\Console\Commands;

use App\Libraries\OddsConvector\OddsConvector;
use App\Models\Bet;
use App\Models\BetsAverage;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Symfony\Component\Console\Output\ConsoleOutput;

class CalculateLatestBetsAverage extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'bets_average:add';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    protected const ONLY_MARKETS = [
        1,
        282,
        284,
        3,
        53,
        283,
        2,
        77,
        337,
        7,
        25,
        151,
        4,
    ];

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $out = new ConsoleOutput();
        $out->writeln("Start script");

        $event_list = Bet::select(['bet.event_id','fixture.start_date'])
            ->distinct()
            ->join('event','bet.event_id','=','event.id')
            ->join('fixture','event.fixture_id','=','fixture.id')
            ->where('is_latest',1)
            ->get();

        $chunck_events = $event_list->chunk(1000);
        $step =0;
        foreach ($chunck_events as $events) {
            $out->writeln("current step - ".$step);
            foreach ($events as $event){
                $bets = Bet::where('event_id', $event->event_id)
                    ->where('is_latest', 1)
                    ->whereIn('market_id', self::ONLY_MARKETS)
                    ->where('is_inplay', 0)
                    ->get();

                $average_bets = $bets->groupBy('market_id')->map(function ($market) use($event){
                    return $market->groupBy('base_line')->map(function ($bets, $line) use($event) {
                        return $bets->groupBy('name')->map(function($bets) use ($event){
                            return [
                                'event_id'      => $bets->last()->event_id,
                                'market_id'     => $bets->last()->market_id,
                                'name'          => $bets->last()->name,
                                'base_line'     => $bets->last()->base_line,
                                'highest'       => $bets->max('price'),
                                'average'       => (new OddsConvector($bets->avg('price')))->handle(),
                                'count'         => $bets->count(),
                                //use in redis expire time
                                'last_update'   =>  $event->start_date
                            ];
                        });
                    });
                });
                $data = $average_bets->flatten(2)->toArray();
                BetsAverage::insert($data);
            }
            $step++;
        }
        return 0;
    }
}
