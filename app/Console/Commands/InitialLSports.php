<?php

namespace App\Console\Commands;

use App\Models\Location;
use App\Models\LocationTranslation;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class InitialLSports extends Command
{

    public const ENG_LANG_ID = 1;
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:lsports';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $arrayLsports = [
            'Chile',
            'China',
            'Ecuador',
            'Estonia',
            'Gambia',
            'Ireland',
            'Latvia',
            'Lithuania',
            'New Zealand',
            'Norway',
            'Paraguay',
            'Peru',
            'Tanzania',
            'USA',
            'Venezuela'
        ];

        $path = base_path('GetLocations.json');
        $retList = \File::get($path);
        $decodeFile = json_decode($retList);
        foreach ($decodeFile->Body as $location) {
            foreach ($arrayLsports as $arrayLsport) {
                if ($location->Name == $arrayLsport) {
                    $currentId = DB::table('location')->where('id',$location->Id)->get();
                    if (!$currentId) {
                        $loc = new Location();
                        $loc->id = $location->Id;
                        $loc->active = true;
                        $loc->iso_2 = '';
                        $loc->save();

                        LocationTranslation::insert([
                            'location_id' => $loc->id,
                            'name' => $location->Name,
                            'language_id' => self::ENG_LANG_ID,
                        ]);
                    }else{
                        DB::table('location')->where('id',$location->Id)->update(['active' => 1]);
                    }
                }
            }
        }
    }
}
