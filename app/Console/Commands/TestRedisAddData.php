<?php

namespace App\Console\Commands;

use App\Models\BetsAverage;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redis;
use Predis\Client;
use Symfony\Component\Console\Output\ConsoleOutput;

class TestRedisAddData extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'redis:add-test-data {--offset=}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $out = new ConsoleOutput();
        $out->writeln('Start script');
        $out->writeln('option--'.$this->option('offset'));
        $out->writeln('type--'.gettype($this->option('offset')));
        $offset = $this->option('offset');
        if(!(empty($this->option('offset')) && intval($this->option('offset')))|| ($this->option('offset')==0)){
            $average_bets = DB::table('bets_average')
                                ->offset($offset*10000)
                                ->limit(10000)
                                ->get();
            $data =[];
            if(!$average_bets->count()){
                $out->writeln('no results');
                return 1;
            }

            Redis::pipeline(function ($pipe) use ($average_bets) {

                foreach ($average_bets as $average_bet) {

                    $key = 'average#'.$average_bet->event_id.'#'.$average_bet->market_id.'#'.$average_bet->name;
                    if(!is_null($average_bet->base_line)) {
                        $key .= $average_bet->base_line;
                    }

                    $fixtureDate = Carbon::parse($average_bet->last_update);

                    if($fixtureDate->greaterThan(now()->setTime(0,0,0))){
                        $seconds = $fixtureDate->addHours(48)->diffInSeconds(now());
                    } else{
                        $seconds = 3600;
                    }
                    $pipe->setEX($key,$seconds,$average_bet->highest.'#'.$average_bet->average);
                }
            });
            $out->writeln('Finish!');
        }

        /*
         Redis::pipeline(function ($pipe) use($data){
             foreach ($data as $key => $value) {
                 $pipe->set("$key", $value);
           }
         });*/
        return 1;
    }
}
