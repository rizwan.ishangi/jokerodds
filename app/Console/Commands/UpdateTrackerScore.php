<?php

namespace App\Console\Commands;

use App\Events\BetsLiveUpdated;
use App\Events\EventsScores;
use App\Events\ScoreUpdated;
use App\Facades\OneXBet;
use App\Models\BetLive;
use App\Models\BetStatus;
use App\Models\Event;
use App\Models\Fixture;
use App\Models\Scoreboard;
use App\Services\Lsports\PullOperations;
use App\Services\TrackerService;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminated\Console\WithoutOverlapping;

class UpdateTrackerScore extends Command
{
    use WithoutOverlapping;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'tracker:score';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update game score in tracker';

    private PullOperations $pullOperations;
    /**
     * @var TrackerService
     */
    private TrackerService $trackerService;
    private string $currentDate;

    /**
     * Create a new command instance.
     * @param  PullOperations  $pullOperations
     * @param  TrackerService  $trackerService
     */
    public function __construct(
        PullOperations $pullOperations,
        TrackerService $trackerService
    ) {
        $this->pullOperations = $pullOperations;
        $this->trackerService = $trackerService;
        $this->currentDate = Carbon::now()->format("Y-m-d H:i");
        parent::__construct();
    }

    public function handle()
    {
        Log::channel('commands')->info('Command {tracker:scores}' );
       /*
        $trackerMatches = $this->trackerService->getTrackerMatches();

        if (empty($trackerMatches)) {
            return;
        }
        $activeFixtureIds = $trackerMatches->filter(function ($match) {
            $matchStatus = $match->event->scoreboard ? $match->event->scoreboard->status_id : '';
            return ($this->currentDate >= Carbon::parse($match->event->fixture->start_date)->format("Y-m-d H:i"))
                && $matchStatus === 2;
        })->map(function ($data) {
            return $data->event->fixture->id;
        });

        if (empty($activeFixtureIds)) {
            return;
        }
*/
        $date_from = now()->subMinutes(150)->format('Y-m-d H:i:s');
        $date_to = now()->format('Y-m-d H:i:s');

        $activeFixtureIds = Fixture::select(['id'])->whereBetween('start_date', [$date_from, $date_to])
            ->where('status_id', '!=', BetStatus::STATUS_DELETED)->get()->pluck('id');

        $score_updates = [];

        //1xbet
        $preparedToOneXBet = [];
        $oneXBetData = [];

        foreach ($activeFixtureIds->chunk(100) as $chunk) {
            $chunk = collect($chunk);
            $scores = $this->pullOperations->getScores([
                'Fixtures' => $chunk->join(','),
            ]);
            foreach ($scores as $score) {
                $event = Event::where('fixture_id', $score->FixtureId)->firstOrFail();

                Scoreboard::updateOrInsert([
                    'event_id' => $event->id,
                ], [
                    'status_id' => $score->Livescore->Scoreboard->Status,
                    'time' => $score->Livescore->Scoreboard->Time,
                    'results' => collect($score->Livescore->Scoreboard->Results)
                        ->map(function ($result) {
                            return $result->Value;
                        })->join(':'),
                ]);

                $data = [
                    'event_id' => $event->id,
                    'status_id' => $score->Livescore->Scoreboard->Status,
                    'time' => $score->Livescore->Scoreboard->Time,
                    'results' => collect($score->Livescore->Scoreboard->Results)
                ];

                $score_updates[] = [
                    'event_id' => $event->id,
                    'status_id' => $score->Livescore->Scoreboard->Status,
                    'results' => collect($score->Livescore->Scoreboard->Results)
                ];

                event(new ScoreUpdated($data, $event->id));
            }
        }

        if(sizeof($score_updates)){
            event(new EventsScores($score_updates));
        }

        $events = [];
        foreach ($score_updates as $event) {
            if($event['status_id'] == 2 ){
                $events[] = $event['event_id'];
            }
        }

        //get live odds from 1xbet
        $betsData      = OneXBet::getLiveFootballEventsWIthOdds();
        $formattedBets = OneXBet::formatBetsToLSport($events,$betsData);
        BetLive::insert($formattedBets);
        //broadcast TODO
        event(new BetsLiveUpdated($formattedBets));
        //check and delete events
     /*
        $betLiveEvents = BetLive::select('event_id as id')->pluck('id')->unique()->toArray();
        $betEvents = DB::table('event')->select(['event.id','fixture.status_id'])
            ->join('fixture','fixture.id','=','event.fixture_id')
            ->whereIn('event.id',$betLiveEvents)
            ->get();
        $liveBetsToDelete = $betEvents->filter(function ($bet){
            return $bet->status_id !==2;
        })->pluck('id')->unique()->toArray();
        BetLive::whereIn('event_id',$liveBetsToDelete)->delete();
        */
        Log::channel('commands')->info('Command {tracker:scores} END' );
    }
}
