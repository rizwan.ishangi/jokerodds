<?php

namespace App\Console\Commands;

use App\Facades\JackPotScore;
use App\Models\JackPotLeague;
use App\Models\JackPotTeam;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Symfony\Component\Console\Output\ConsoleOutput;

class PullJackPotLeagues extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'jack-pot:new-leagues {--interval=} {--withTeams=false}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $out = new ConsoleOutput();
        $out->writeln('Start script');
        $interval = $this->option('interval');
        switch ($interval){
            case 'day'   :   $days = 1;
            break;
            case 'month' : $days  = now()->endOfMonth()->diffInDays(now()->startOfMonth());
            break;
            default      : $out->writeln('Invalid interval value. Try --interval=day or --interval=month');
            return 1;
        }
        $dataToInsert = [];
        $currentLeagues = JackPotLeague::select('uuid')->get()->toArray();
        for ($day = 1; $day <=$days; $day ++){
            $result =  JackPotScore::getAllMatchesByDate(['date'=>now()->addDays($day)->format('Y-m-d')]);
            if($result->status){
                foreach ($result->modal as $league){
                    if(!in_array($league->tournamentCalendar->id, $currentLeagues)){
                        $dataToInsert[] = [
                            'name'        =>  $league->competition->name.' '.$league->competition->competitionCode,
                            'uuid'        =>  $league->tournamentCalendar->id,
                            'league_id'   =>  null
                        ];
                    }
                }
            }
        }
        $dataToInsert = collect($dataToInsert)->unique('uuid')->toArray();
        JackPotLeague::insert($dataToInsert);

        if ($this->option('withTeams')){
            $teamsToInsert = [];
            $currentTeams = JackPotTeam::select('uuid')->get()->toArray();
            foreach ($dataToInsert as $tmclId){
                $result = JackPotScore::getAllTournamentCalendarMatches(['tmclId' => $tmclId['uuid']]);
                foreach ($result->modal->matchDate  as $item){
                    if($item->numberOfGames){
                        foreach ($item->match as $match) {
                            if(!in_array($match->homeContestantId, $currentTeams)){
                                $teamsToInsert[] = [
                                    'name' => $match->homeContestantOfficialName,
                                    'uuid' => $match->homeContestantId,
                                    'participant_id' => null
                                ];
                            }
                            if(!in_array($match->awayContestantId, $currentTeams)){
                                $teamsToInsert[] = [
                                    'name' => $match->awayContestantOfficialName,
                                    'uuid' => $match->awayContestantId,
                                    'participant_id' => null
                                ];
                            }
                        }
                    }
                }
            }

            $teamsToInsert = collect($teamsToInsert)->unique('uuid')->toArray();
            JackPotTeam::insert($teamsToInsert);
        };
        $out->writeln('Finish script');
        return 1;
    }
}
