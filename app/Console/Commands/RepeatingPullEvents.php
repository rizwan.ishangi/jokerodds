<?php

namespace App\Console\Commands;

use App\Services\Lsports\PullOperations;
use App\Traits\EventsPullTrait;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;
use Illuminated\Console\WithoutOverlapping;

class RepeatingPullEvents extends Command
{
    use EventsPullTrait, WithoutOverlapping;

    public const LIMIT_SPORTS_IDS = [
        6046, // football
    ];

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'pull:repeating {--season}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Fetches events with some repeating, should be triggered by cron on daily basis';

    private PullOperations $pullOperations;

    /**
     * Create a new command instance.
     *
     * @param  PullOperations  $pullOperations
     */
    public function __construct(PullOperations $pullOperations)
    {
        $this->pullOperations = $pullOperations;

        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        Log::channel('commands')->info('Command {pull:repeating}' );
        Log::info(Carbon::now() . ' Start RepeatingPullEvents');
        $this->output->note('Pulling events list...');

        $fromDate = Carbon::now()->subDay();

        if ($this->option('season')) {
            $toDate = Carbon::now()->addMonths(9);
        } else {
            $toDate = Carbon::now()->addWeek();
        }

        $eventsCount = 0;

        while (!$fromDate->greaterThanOrEqualTo($toDate)) {
            $this->output->note('Updating events for day ' . $fromDate);

            $eventsCount += $this->pullEventsForRange(
                $fromDate,
                (clone $fromDate)->addDay()
            );

            $fromDate->addDay();
        }

        file_put_contents(
            storage_path('repeating-pull-events.txt'),
            'Pulling events between ' . $fromDate . ' - ' . $toDate . ': ' . $eventsCount . "\n",
            FILE_APPEND
        );

        Log::info(Carbon::now() . ' End RepeatingPullEvents');
        Log::channel('commands')->info('Command {pull:repeating} END' );
        $this->output->success('Successfully pulled events: ' . $eventsCount);
    }
}
