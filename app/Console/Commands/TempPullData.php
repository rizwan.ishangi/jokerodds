<?php

namespace App\Console\Commands;

use App\Models\Bookmaker;
use App\Models\League;
use App\Models\Location;
use App\Models\LocationTranslation;
use App\Services\Lsports\PullOperations;
use App\Traits\EventsPullTrait;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class TempPullData extends Command
{
    use EventsPullTrait;

    public const ENG_LANG_ID = 1;

    public const LIMIT_SPORTS_IDS = [
        6046, // football
    ];

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'pull:temp';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command allows to insert or update location, bookmakers, leagues';

    private PullOperations $pullOperations;

    /**
     * Create a new command instance.
     *
     * @param  PullOperations  $pullOperations
     */
    public function __construct(PullOperations $pullOperations)
    {
        $this->pullOperations = $pullOperations;

        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        \DB::statement('SET FOREIGN_KEY_CHECKS=0');

        $this->pullLocations();
        sleep(5);

        $this->pullLeagues();
        sleep(5);

        $this->pullBookmakers();
        sleep(5);
    }

    private function pullBookmakers()
    {
        $this->output->note('Pulling bookmakers list...');
        $bookmakers = $this->pullOperations->getBookmakers();

        foreach ($bookmakers as $bookmaker) {
            $bookmakerExists = DB::table('bookmaker')->where('id', $bookmaker->Id)->first();
            if ($bookmakerExists) {
                continue;
            }

            Bookmaker::insert(
                ['id' => $bookmaker->Id,
                'name' => $bookmaker->Name,
                'active' => true]
            );
        }

        $this->output->success('Successfully pulled bookmakers: ' . count($bookmakers));
    }

    private function pullLocations()
    {
        $this->output->note('Pulling locations list...');
        $locations = $this->pullOperations->getLocations();

        foreach ($locations as $location) {
            $locationExists = DB::table('location')->where('id', $location->Id)->first();
            if ($locationExists) {
                continue;
            }

            $id = Location::insertGetId([
                'id' => $location->Id,
                'active' => true,
                'iso_2' => '',
            ]);

            LocationTranslation::insert([
                'location_id' => $id,
                'name' => $location->Name,
                'language_id' => self::ENG_LANG_ID,
            ]);
        }

        $this->output->success('Successfully pulled locations: ' . count($locations));
    }

    private function pullLeagues()
    {
        Log::channel('commands')->info('Command {pull:temp}' );
        $this->output->note('Pulling leagues list...');

        $leagues = $this->pullOperations->getLeagues();

        $locations = Location::pluck('name', 'id');

        foreach ($leagues as $league) {
            $leagueExists = DB::table('league')->where('id', $league->Id)->first();
            if ($leagueExists) {
                continue;
            }

            if (!in_array($league->SportId, self::LIMIT_SPORTS_IDS)) {
                continue;
            }

            /**
             * Skip leagues from unnecessary location
             */
            if (empty($locations[$league->LocationId])) {
                $this->output->warning('League ' . $league->Name . ' rejected b/c no location added in database');
                continue;
            }

            League::insert(
                ['id' => $league->Id,
                'name' => $league->Name,
                'img' => '',
                'sport_id' => $league->SportId,
                'location_id' => $league->LocationId,
                'active' => true,
                ]
            );
        }

        $this->output->success('Successfully pulled leagues: ' . count($leagues));
        Log::channel('commands')->info('Command {pull:temp} END' );
    }
}
