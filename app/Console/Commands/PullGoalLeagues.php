<?php

namespace App\Console\Commands;

use App\Models\GoalServeLeague;
use App\Services\GoalServe\GoalOperations;
use Illuminate\Console\Command;

class PullGoalLeagues extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'goalserve:new-leagues';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function handle()
    {
        $this->output->note('Pulling goalserve leagues ...');

        $operations = new GoalOperations();
        $data = json_decode($operations->getFixtures());
        $fixtures = $data->fixtures->mapping;
        $data = collect($fixtures);
        $currentLeagues = GoalServeLeague::pluck('id')->toArray();
        $dataInsert = $data->filter(function ($item) use($currentLeagues){
            return !in_array($item->{'@id'},$currentLeagues );
        })->map(function ($item) use($currentLeagues){
            return ['id'=>$item->{'@id'},'name'=>$item->{'@name'}];
        })->toArray();

        GoalServeLeague::insert($dataInsert);
        $this->output->success('Successfully pulled leagues: ' . sizeof($dataInsert));
        return 0;
    }
}
