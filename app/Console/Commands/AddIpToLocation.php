<?php

namespace App\Console\Commands;

use App\Models\IpToLocation;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\File;
use PhpOffice\PhpSpreadsheet\Reader\Ods;
use Symfony\Component\Console\Output\ConsoleOutput;

class AddIpToLocation extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'ip_to_location:add';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $out = new ConsoleOutput();
        $out->writeln("Start script");
        if(File::exists(base_path('IP2LOCATION-LITE-DB1.ods'))){
            $data = [];
            $reader = new Ods();
            $spreadsheet  = $reader->load(base_path('IP2LOCATION-LITE-DB1.ods'));
            $sheet        = $spreadsheet->getActiveSheet();
            $count        = $sheet->getHighestRow("A");
            $out->writeln('Total lines - '.$count);
            $current_step = 0;

            for($i=2; $i<= $count; $i++){
                if(!empty($sheet->getCell("A".$i)->getValue())){
                    if($current_step % 50000  == 0){
                        $out->writeln('current read step - '.$current_step);
                    }
                    $data[] = [
                        'ip_from' =>intval($sheet->getCell("A".$i)->getValue()),
                        'ip_to' =>intval($sheet->getCell("B".$i)->getValue()),
                        'iso_3166_2' =>$sheet->getCell("C".$i)->getValue(),
                        'country' =>$sheet->getCell("D".$i)->getValue(),
                    ];
                    $current_step ++;
                } else break 1;
            }
            try{
                IpToLocation::truncate();

                $chunk_data = array_chunk($data,1000);
                $chunk_step = 0;

                foreach ($chunk_data as $datum){
                    $out->writeln('Insert step - '.$chunk_step);
                    IpToLocation::insert($datum);
                    $chunk_step++;
                }
                $out->writeln('success');
            } catch(\Exception $e){
                $out->writeln($e->getMessage());
            }
        }
        return 1;
    }
}
