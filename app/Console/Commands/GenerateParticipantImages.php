<?php

namespace App\Console\Commands;

use App\Models\Participant;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Intervention\Image\Facades\Image;

class GenerateParticipantImages extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'participants:create-images';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
      //  DB::table('participant_image_base64')->truncate();
        File::cleanDirectory(public_path('/images/vendor/participants/'));

        DB::insert('INSERT INTO participant_image_base64(participant_id,image) SELECT id,image from participant');
        $images = Participant::select(['id','image'])->where('image','<>',NULL)->get();

        foreach ($images as $image) {
            if(!is_null($image->image)){
                $g_image = Image::make($image->image);
                $name = $image->id.'.'.str_replace('image/','',$g_image->mime());
                $path = '/images/vendor/participants/'.$name;
                $path = public_path($path);
                $g_image->save($path);
                Participant::where('id',$image->id)->update(['image'=>$name]);
            }
        }
        return 1;
    }
}
