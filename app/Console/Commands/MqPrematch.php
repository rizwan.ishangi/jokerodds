<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Services\Lsports\PrematchMQ;

class MqPrematch extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'mq:prematch';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Connects to LSport Rabbit MQ to collect fixtures data. Run in screen/as service to have persistent connection.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        echo "RMQ Consume started".PHP_EOL;
        $mc = new PrematchMQ;
        try {
            $mc->connect();

        } catch (\Exception $e){
            return 1;
        }

        $mc->consume();
        return 1;
    }
}
