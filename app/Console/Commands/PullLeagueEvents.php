<?php

namespace App\Console\Commands;

use App\Services\Lsports\PullOperations;
use App\Traits\EventsPullTrait;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;
use Illuminated\Console\WithoutOverlapping;

class PullLeagueEvents extends Command
{
    use WithoutOverlapping, EventsPullTrait;

    public const LIMIT_SPORTS_IDS = [
        6046, // football
    ];

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'pull:leagueevents {league_ids}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Pull events by league id or comma separated list of league ids.';

    private PullOperations $pullOperations;

    /**
     * Create a new command instance.
     *
     * @param  PullOperations  $pullOperations
     */
    public function __construct(PullOperations $pullOperations)
    {
        $this->pullOperations = $pullOperations;

        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        Log::channel('commands')->info('Command {pull:leagueevents}, league_ids {' . $this->argument('league_ids') . '}');

        $league_ids = explode(',', $this->argument('league_ids'));

        foreach ($league_ids as $league_id) {
            $fromDate = Carbon::now()->subDay();
            $toDate = Carbon::now()->addWeek();

            $eventsCount = 0;

            while (!$fromDate->greaterThanOrEqualTo($toDate)) {
                $this->output->note('Updating events for day ' . $fromDate);

                $eventsCount += $this->pullEventsForRange(
                    $fromDate,
                    (clone $fromDate)->addDay()
                );

                $fromDate->addDay();
            }

            file_put_contents(
                storage_path('pull-league-events.txt'),
                'Pulling events League: '.$league_id.' between ' . $fromDate . ' - ' . $toDate . ': ' . $eventsCount . PHP_EOL,
                FILE_APPEND
            );
        }


        Log::channel('commands')->info('Command {pull:leagueevents}, league_ids {' . $this->argument('league_ids') . '}' . ' END!');
    }
}
