<?php

namespace App\Console\Commands;

use App\Services\Lsports\LsportMQ;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;

class TestLog extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'test:log';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command to log data from RabbitMQ';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $lspotMq = new LsportMQ();
        $lspotMq->connect(LsportMQ::MODE_PREMATCH);

        $lspotMq->consume(function (array $events) {
            if (!empty($events)) {
                Log::info('NEW EVENTS------------------------------------------------');
                $data = json_encode($events);
                if ($data) {
                    Log::info($data);
                } else {
                    Log::info('[]');
                }
                Log::info('NEW EVENTS END--------------------------------------------');
            }
        });
        return 0;
    }
}
