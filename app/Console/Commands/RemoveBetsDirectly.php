<?php

namespace App\Console\Commands;

use App\Models\Bet;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;

class RemoveBetsDirectly extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'history:remove-bets-directly';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {

        Log::channel('commands')->info('Start Remove bets directly');
        $step = 0;
        $total_deleted = 0;
        do {
            $deleted_items =  Bet::where('last_update','<','2021-06-01 00:00:00')
                ->limit(10000)
                ->delete();
            if($step % 20 == 0){
                Log::channel('commands')->info('Step deleted directly -'.$step);
                Log::channel('commands')->info('Deleted directly-'.$total_deleted);
            }
            $step++;
            $total_deleted += (int)$deleted_items;
            sleep(2);

        } while($deleted_items >0);

        Log::channel('commands')->info('Total deleted directly-'.$total_deleted);

        return 1;
    }
}
