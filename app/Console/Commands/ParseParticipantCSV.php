<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class ParseParticipantCSV extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'parse:participant-csv';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command to parse participant csv file.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $this->info('Processing...');

        $this->line('Getting data from a file...');
        $filepath = database_path('/participant.csv');
        $delimiter = ',';
        if (!file_exists($filepath) || !is_readable($filepath)) {
            $this->error('File do not exist or is unreadable');
            return 0;
        }

        $this->line('Parsing the data...');
        $header = null;
        $data = [];
        if (($handle = fopen($filepath, 'r')) !== false) {
            while (($row = fgetcsv($handle, 1000, $delimiter)) !== false) {
                if (!$header) {
                    $header = $row;
                } else {
                    $data[] = array_combine($header, $row);
                }
            }
            fclose($handle);
        }
        $participants = collect($data);
        unset($data);

        $this->line('Making changes to database...');
        $bar = $this->output->createProgressBar($participants->count());
        $bar->start();
        foreach ($participants as $participant) {
            if (!empty($participant['goal_team_id'])) {
                DB::table('participant')->where('id', $participant['id'])->update([
                    'goal_team_id' => $participant['goal_team_id'],
                    'goal_team_name' => $participant['goal_team_name']
                ]);
            }
            $bar->advance();
        }
        $bar->finish();

        $this->line('');

        $this->info('Success!');

        return 0;
    }
}
