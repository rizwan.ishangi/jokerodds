<?php

namespace App\Console\Commands;

use App\Services\GoalServe\GoalService;
use GuzzleHttp\Exception\GuzzleException;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;
use Illuminated\Console\WithoutOverlapping;

class PullGoalFixtures extends Command
{
    use WithoutOverlapping;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'pull:goal {mode} {--id=}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Pull data from  GoalServe API';

    /**
     * @var GoalService
     */
    private GoalService $goalService;

    /**
     * Create a new command instance.
     *
     * @param  GoalService  $goalService
     */
    public function __construct(GoalService $goalService)
    {
        $this->goalService = $goalService;

        parent::__construct();
    }


    /**
     * Execute the console command.
     *
     * @return mixed
     * @throws GuzzleException
     */
    public function handle()
    {
        Log::channel('commands')->info('Command {pull:goal}, mode {' . $this->argument('mode') . '}' );
        $id = $this->option('id') ?? null;
        switch ($this->argument('mode')) {
            case 'fixtures':
                $this->goalService->fetchFixtures();
                $this->goalService->storeFixtures();
                break;
            case 'standings':
                return $this->goalService->getStandingsByLeagueId($id);
                break;
            case 'team':
                $this->goalService->fetchTeamIds();
                $this->goalService->fetchTeamImage();
                break;
            default:
                $this->error('Unknown mode');
        }
        Log::channel('commands')->info('Command {pull:goal}, mode {' . $this->argument('mode') . '}' );
    }
}
