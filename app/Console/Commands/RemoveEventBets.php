<?php

namespace App\Console\Commands;

use App\Models\Bet;
use App\Models\Event;
use App\Models\Fixture;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class RemoveEventBets extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'history:remove-event-bets';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {

        $interval = DB::table('history_date')
                    ->where('removed',0)
                    ->first();

        if(!$interval){
            return 1;
        }

        $fixtures  = Fixture::select(['id'])->withTrashed()
                            ->where('start_date', '>' , $interval->date_from)
                            ->where('start_date', '<=', $interval->date_to)
                            ->get()
                            ->pluck('id');

        if(!sizeof($fixtures)){
            return 1;
        }

        $events = Event::select(['id'])
                         ->whereIn('fixture_id',$fixtures)
                         ->get()
                         ->pluck('id')
                         ->toArray();
        $events = array_unique($events);
        $total_events = sizeof($events);
        if(!$total_events){
            return 1;
        }

        Log::channel('commands')->info('Remove Interval - '.$interval->id );
        Log::channel('commands')->info('Total events - '.$total_events );
        $event_count = 0;
        foreach ($events as $event) {
            Log::channel('commands')->info('Events remains-'.($total_events - $event_count));
            Log::channel('commands')->info('Remove bets event-'.$event);
            $step = 0;
            $total_deleted = 0;
            do {
               $deleted_items =  Bet::where('event_id',$event)
                    ->limit(5000)
                    ->delete();

               if($step % 20 == 0){
                   Log::channel('commands')->info('Step -'.$step);
                   Log::channel('commands')->info('Deleted -'.(string)$deleted_items);
               }

                $step++;
                $total_deleted += (int)$deleted_items;
                sleep(2);
            } while($deleted_items >0);

            Log::channel('commands')->info('Bets event-'.$event.' removed!');
            Log::channel('commands')->info('Total deleted-'.$total_deleted);
            $event_count++;
        }
   /*     $log_bets = array_unique($log_bets);
        Log::channel('commands')->info('Total bets deleted - '.sizeof($log_bets));
        Log::channel('commands')->info('Min bet id - '.min($log_bets));
        Log::channel('commands')->info('Max bet id - '.max($log_bets));*/

        DB::table('history_date')
                ->where('id',$interval->id)
                ->update(['removed'=>1]);
        Log::channel('commands')->info('Remove Event Bets Finish!' );
        return 1;
    }
}
