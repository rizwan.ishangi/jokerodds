<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;
use Illuminated\Console\WithoutOverlapping;

class CheckLeaguesAndLocations extends Command
{
    use WithoutOverlapping;
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'check:leagues-and-locations';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command to check active leagues and locations';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle(): void
    {
        Log::channel('commands')->info('Command {check:leagues-and-locations}' );
        $this->info('Processing');
        $this->info('Check Leagues');
        $this->call('check:leagues');
        $this->info('Check Leagues done!');
        $this->info('Check Locations');
        $this->call('check:locations');
        $this->info('Check Location done!');
        $this->info('Job done!');
        Log::channel('commands')->info('Command {check:leagues-and-locations} END' );
    }
}
