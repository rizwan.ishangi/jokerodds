<?php

namespace App\Console\Commands;

use App\Models\League;
use App\Services\GoalServe\GoalServeService;
use App\Services\Lsports\PullOperations;
use GuzzleHttp\Exception\RequestException;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class GetLeagues extends Command
{
    /**
     * @var PullOperations $operations
     */
    private $operations;

    /**
     * @var GoalServeService $goalServeService
     */
    private $goalServeService;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'get:leagues';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command to get leagues from goal service.';

    /**
     * Create a new command instance.
     *
     * @param  PullOperations  $operations
     * @param  GoalServeService  $goalServeService
     */
    public function __construct(PullOperations $operations, GoalServeService $goalServeService)
    {
        $this->operations = $operations;
        $this->goalServeService = $goalServeService;
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle(): int
    {
        $leagues = $this->goalServeService->getLeaguesJSON();
        $leagues = json_decode($leagues);
        $leagues = $leagues->fixtures->mapping;
        $notInData = [];
        $inData = [];
//        foreach ($leagues as $league) {
//            DB::table('leagues-ga')->insert([
//                'country' => $league->{'@country'},
//                'name' => $league->{'@name'},
//                'season' => $league->{'@season'},
//                'ga_id' => $league->{'@id'}
//            ]);
//            if (isset($league->{'@id'})) {
//                $search = League::select(['id'])->where('goal_league_id', $league->{'@id'})->first();
//                if (is_null($search)) {
//                    $notInData[] = $league;
//                } else {
//                    $inData[] = $league;
//                }
//            }
//        }
//        return 0;
        $leagueStandings = [];
        $errorLeagues = [];
        $groupsWithoutTeam = [];
        $teams = [];
        $httpExceptions = [];
        $teamsWithId = [];
        $teamsWithEmptyId = [];
        $this->info('parsing inData');
        foreach ($inData as $league) {
            try {
                $standings = $this->goalServeService->getLeagueStandingsByLeagueIdJSON($league->{'@id'});
                $standings = json_decode($standings);
                $data = $standings->standings;
                $leagueStandings[$league->{'@id'}][] = $data;
                if (is_array($data->tournament)) {
                    foreach ($data->tournament as $group) {
                        if (isset($group->team)) {
                            foreach ($group->team as $team) {
                                $teams[] = $team;
                                if (!empty($team->{'@id'})) {
                                    $teamsWithId[] = $team;
                                    \DB::table('teams-ga')->insert([
                                        'name' => $team->{'@name'},
                                        'league' => $league->{'@name'},
                                        'goalserve_id' => $team->{'@id'},
                                    ]);
                                } else {
                                    $teamsWithEmptyId[] = $team;
                                }
                            }
                        } else {
                            dd(json_encode($data));
                            $groupsWithoutTeam[] = $group;
                        }
                    }
                } else {
                    foreach ($data->tournament->team as $team) {
                        $teams[] = $team;
                        if (!empty($team->{'@id'})) {
                            $teamsWithId[] = $team;
                            \DB::table('teams-ga')->insert([
                                'name' => $team->{'@name'},
                                'league' => $league->{'@name'},
                                'goalserve_id' => $team->{'@id'},
                            ]);
                        } else {
                            $teamsWithEmptyId[] = $team;
                        }
                    }
                }

            } catch (RequestException $e) {
                $httpExceptions[] = $e->getMessage();
            } catch (\Exception $e) {
                dd($e->getMessage(), $data);
                $errorLeagues[] = $league;
            }
            sleep(3);
        }

        $this->info('Errors: ' . count($errorLeagues));
        $this->info('League standings: ' . count($leagueStandings));
        $this->info('Group without teams: ' . count($groupsWithoutTeam));
        $this->info('Teams: ' . count($teams));
        $this->info('Teams with id: ' . count($teamsWithId));
        $this->info('Teams with empty id: ' . count($teamsWithEmptyId));
        $this->info('notInData: ' . count($notInData));
        $this->info('inData: ' . count($inData));
        $this->info('httpExceptions: ' . count($httpExceptions));

        Log::channel('leagues')->error('error', $errorLeagues);
        Log::channel('leagues')->info('league-standings', $leagueStandings);
        Log::channel('leagues')->info('group-without-teams', $groupsWithoutTeam);
        Log::channel('leagues')->info('teams', $teams);
        Log::channel('leagues')->info('teams with id', $teamsWithId);
        Log::channel('leagues')->info('teams with empty id', $teamsWithEmptyId);
        Log::channel('leagues')->info('notInData', $notInData);
        Log::channel('leagues')->info('inData', $inData);
        Log::channel('leagues')->info('httpException', $httpExceptions);
        return 0;
    }
}
