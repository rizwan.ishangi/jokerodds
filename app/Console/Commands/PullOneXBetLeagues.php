<?php

namespace App\Console\Commands;

use App\Facades\OneXBet;
use App\Models\OneXBetLeague;
use Illuminate\Console\Command;

class PullOneXBetLeagues extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = '1xbet:new-leagues';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $this->output->note('check new 1xbet leagues ...');

        $data = OneXBet::getLineFootballEvents();
        $currentLeagues = OneXBetLeague::pluck('id')->toArray();
        $data = collect($data);
        $dataInsert = $data->filter(function ($item) use($currentLeagues){
            return !in_array($item->CI ,$currentLeagues );
        })->map(function ($item) use($currentLeagues){
            return ['id'=>$item->CI, 'name'=>$item->C ];
        })->unique('id')->toArray();
        OneXBetLeague::insert($dataInsert);
        $this->output->success('Successfully pulled leagues: ' . sizeof($dataInsert));
        return 1;
    }
}
