<?php

namespace App\Console\Commands;

use App\Models\Location;
use App\Models\LocationSimpleModel;
use Illuminate\Console\Command;
use Illuminate\Database\Eloquent\Builder;

class CheckLocationForActiveLeagues extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'check:locations';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command to check locations for active leagues';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle(): void
    {
        $this->info('Processing');
        $locations = LocationSimpleModel::select(['id'])->withCount(['leagues' => function (Builder $q) {
            $q->where('has_fixtures', true);
            $q->where('active', true);
        }])->get();
        $locationsWithActiveLeagues = $locations->filter(function ($location) {
            return $location->leagues_count > 0;
        });
        $locationsWithoutActiveLeagues = $locations->filter(function ($location) {
            return $location->leagues_count === 0;
        });
        LocationSimpleModel::whereIn('id', $locationsWithActiveLeagues->pluck('id')->toArray())->update(['has_fixtures' => true]);
        LocationSimpleModel::whereIn('id', $locationsWithoutActiveLeagues->pluck('id')->toArray())->update(['has_fixtures' => false]);
        $this->info('Job done!');
    }
}
