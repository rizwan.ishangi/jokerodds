<?php

namespace App\Console\Commands;

use App\Facades\OneXBet;
use App\Models\OneXBetTeam;
use Illuminate\Console\Command;

class PullOneXBetTeams extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = '1xbet:new-teams';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Check and add new teams';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $this->output->note('check new 1xbet teams ...');
        $data = OneXBet::getLineFootballEvents();

        $currentLeagues = OneXBetTeam::all()->map(function ($team){
            $formattedName  = $team->name;
            if(!is_null($team->team_id)){
                $formattedName .=$team->team_id;
            }
            return $formattedName;
        })->toArray();

        $tmpBuf = [];
        $data = collect($data);

        foreach ($data as $event) {

            $formattedNameFirstTeam = $event->A;
            if(!is_null($event->B2)){
                $formattedNameFirstTeam .= $event->B2;
            }
            if(!in_array($formattedNameFirstTeam, $currentLeagues)){
                $tmpBuf[] = [
                    'name' => $event->A,
                    'team_id' => $event->B2,
                    'uniqueKey' =>$event->A.$event->B2
                ];

            }

            $formattedNameSecondTeam = $event->H;
            if(!is_null($event->B1)){
                $formattedNameSecondTeam .= $event->B1;
            }
            if(!in_array($formattedNameSecondTeam, $currentLeagues)){
                $tmpBuf[] = [
                    'name' => $event->H,
                    'team_id' => $event->B1,
                    'uniqueKey' =>$event->H.$event->B1
                ];

            }
        }

        $dataInsert = collect($tmpBuf)->unique('uniqueKey')->map(function ($team){
            return  [
                'name' => $team['name'],
                'team_id' => $team['team_id']
            ];
        })->toArray();
        OneXBetTeam::insert($dataInsert);
        $this->output->success('Successfully pulled teams: ' . sizeof($dataInsert));
        return 1;
    }
}
