<?php

namespace App\Console\Commands;

use App\Jobs\AddEventBetsToHistory;
use App\Models\Event;
use App\Models\Fixture;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class AddBetsToHistory extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'history:add-bets';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $hours_increment = 36;

        $date_from = DB::table('history_date')->max('date_to');

        if(!$date_from){
            $date_from   = Fixture::min('start_date');
        }

        $date_to = Carbon::parse($date_from)->addHours($hours_increment)->format('Y-m-d H:i:s');

        if(Carbon::parse($date_to)->gt(Carbon::now()->subHours($hours_increment))){
            return 1;
        }

        DB::table('history_date')->insert(['date_from'=>$date_from, 'date_to'=>$date_to]);

        $fixtures = Fixture::select(['id'])->withTrashed()
                    ->where('start_date', '>' , $date_from)
                    ->where('start_date', '<=', $date_to)
                    ->get()
                    ->pluck('id');

        if(!sizeof($fixtures)){
            return 1;
        }

         $events = Event::select(['id'])->whereIn('fixture_id',$fixtures)->get()->pluck('id');

        if(!sizeof($events)){
            return 1;
        }

        foreach ($events as $event) {
              AddEventBetsToHistory::dispatch($event);
        }

        return 1;
    }
}
