<?php

namespace App\Console\Commands;

use GuzzleHttp\Client;
use Illuminate\Console\Command;

class TestLsportApi extends Command
{
    /**
     * @var Client $client
     */
    private $client;

    /**
     * @var string$getEvents
     */
    private $getEvents = 'https://prematch.lsports.eu/OddService/GetEvents?Username=alexandr.rilskii@gmail.com&Password=dgsvt4e&Guid=d5898161-c33d-4cf0-996c-11ec7ed97280&lang=en&FromDate=1619071202&ToDate=1619157602&Sports=6046&Locations=14';

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'api:test-lsport';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command to test LSport api requests.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->client = new Client([
            'timeout' => 60 * 5
        ]);
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $response = $this->client->request('GET', $this->getEvents);

        dd(json_decode($response->getBody()->getContents()));
        return 0;
    }
}
