<?php

namespace App\Console\Commands;

use App\Models\Bet;
use App\Models\Event;
use App\Models\Fixture;
use App\Models\LocationSimpleModel;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\DB;

class TestCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'test:code';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command to test block of code.';

    protected const ONLY_MARKETS = [
        1,
        282,
        284,
        3,
        53,
        283,
        2,
        77,
        337,
        7,
        25,
        151,
        4,
    ];

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle(): void
    {
        $eventId = 178680;
        $this->info('Event id:                                      ' . $eventId);

        $betsTotal = Bet::where('event_id', $eventId)->count('id');
        $this->info('Total number of coefficients:                  ' . number_format($betsTotal, 0, '', ' '));

        $betsWeUse = Bet::whereIn('market_id', self::ONLY_MARKETS)->where('event_id', $eventId)->count('id');
        $this->info('Number of coefficients used:                   ' . number_format($betsWeUse, 0, '', ' '));

        $garbageCount = ($betsTotal - $betsWeUse);
        $this->error('Amount of trash:                               ' . number_format($garbageCount, 0, '', ' '));

        $event = Event::select(['id', 'fixture_id'])->where('id', $eventId)->first();
        $lastFixture = Fixture::select(['id', 'start_date'])->where('id', $event->fixture_id)->orderBy('start_date', 'desc')->first();
        $to = Carbon::parse($lastFixture->start_date)->setHours(23)->setMinutes(59)->setSeconds(59);
        $from = Carbon::parse($lastFixture->start_date)->subMonth()->setHours(0)->setMinutes(0)->setSeconds(0);

        $countEvents = Fixture::select(['id'])->where('start_date', '<=', $to->format('Y-m-d H:i:s'))
            ->where('start_date', '>=', $from->format('Y-m-d H:i:s'))
            ->count('id');

        $this->info('Average number of events per month:            ' . number_format($countEvents, 0, '', ' '));
        $this->error('Projected amount of trash generated per month: ' . number_format(($countEvents * $garbageCount), 0, '', ' '));

        $bet1 = Bet::select(['id'])->orderBy('id', 'desc')->limit(1)->first();
        sleep(60);
        $bet2 = Bet::select(['id'])->orderBy('id', 'desc')->limit(1)->first();
        $this->info('INSERT PER MINUTE:                             ' .  ($bet2->id - $bet1->id));
    }
}
