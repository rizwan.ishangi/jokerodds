<?php

namespace App\Console\Commands\GoalServe;

use App\Jobs\FetchTeamJob;
use App\Models\Participant;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;

class SyncTeams extends Command
{
    protected $signature = 'goalserve:sync-teams';

    protected $description = 'Sync existing teams with goalserve data';

    public function handle()
    {
        Log::channel('commands')->info('Command {goalserve:sync-teams}' );
        $teams = Participant::select(['id', 'goal_team_id', 'name'])
            ->whereNotNull('goal_team_id')
            ->cursor();

        $i = 0;

        foreach ($teams as $team) {
            $queue = $i % 2 == 0 ? '1' : '2';

            FetchTeamJob::dispatch($team->id, $team->goal_team_id)->onQueue('fetch_team_' . $queue);

            $i++;
        }

        $this->info($teams->count() . ' jobs successfully dispatched.');
        Log::channel('commands')->info('Command {goalserve:sync-teams} END' );
    }
}
