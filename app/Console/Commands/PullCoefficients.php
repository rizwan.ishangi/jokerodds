<?php

namespace App\Console\Commands;

use App\Events\BetsUpdated;
use App\Libraries\OddsConvector\OddsConvector;
use App\Models\Bet;
use App\Models\Bookmaker;
use App\Models\Event;
use App\Models\Fixture;
use App\Models\Incident;
use App\Models\Market;
use App\Models\Period;
use App\Models\Scoreboard;
use App\Services\Lsports\LsportMQ;
use App\Services\Lsports\PullOperations;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Redis;
use Illuminated\Console\WithoutOverlapping;

class PullCoefficients extends Command
{
    use WithoutOverlapping;

    private const INSTANT_GAMES_HOURS_RANGE = 36;
    private const NEAREST_GAMES_HOURS_RANGE = 24 * 7;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'pull:coefficients {mode} {--inplay}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Pull latest coefficients from ';

    private PullOperations $pullOperations;

    protected const ONLY_MARKETS = [
        1,
        282,
        284,
        3,
        53,
        283,
        2,
        77,
        337,
        7,
        25,
        151,
        4,
    ];

    /**
     * Create a new command instance.
     *
     * @param  PullOperations  $pullOperations
     */
    public function __construct(PullOperations $pullOperations)
    {
        $this->pullOperations = $pullOperations;

        parent::__construct();
    }

    /**
     * Instant update of cefs for nearest games
     * (!) Run process as a backend worker
     */
    private function pullInstantly()
    {
        $lspotMq = new LsportMQ();
        $lspotMq->connect($this->option('inplay') ? LsportMQ::MODE_INPLAY : LsportMQ::MODE_PREMATCH);

        $this->output->success('Successfully connected');

        $lspotMq->consume(function (array $events) {

            $this->output->note('New events at ' . now());

            if (!empty($events)) {
                $fixtures = Fixture::whereIn('id', collect($events)->pluck('FixtureId'))->get();

                $this->updateCoefficients($events, $fixtures, $this->option('inplay'));
            }
        });
    }

    /**
     * 30-minute interval for updating games within nearest range
     */
    private function pullNearest()
    {
        $fixtures = Fixture::where(
            'start_date',
            '>=',
            now()->subDay()
        )->where(
            'start_date',
            '<=',
            now()->addHours(self::NEAREST_GAMES_HOURS_RANGE)
        )->whereHas('event', function ($query) {
            $query->where('bets_last_update', '<', now()->subMinutes(30));
        })->limit(100)->get();

        $this->fetchAndUpdateCoefficients($fixtures);
    }

    /**
     * Daily update of cefs for far further games
     */
    private function pullAll()
    {
        $fixtures = Fixture::where(
            'start_date',
            '>=',
            now()->addHours(self::INSTANT_GAMES_HOURS_RANGE)
        )->where(
            'start_date',
            '<=',
            now()->addHours(self::NEAREST_GAMES_HOURS_RANGE)
        )->whereHas('event', function ($query) {
            $query->where('bets_last_update', '<', now()->subDay());
        })->limit(200)->get();

        $this->fetchAndUpdateCoefficients($fixtures);
    }

    /**
     * @param  array  $events
     * @param  Collection<Fixture>  $fixtures
     * @param  bool  $isInplay
     */
    private function updateCoefficients(array $events, Collection $fixtures, bool $isInplay = false): void
    {
        Event::whereIn('fixture_id', $fixtures->pluck('id'))->update([
            'bets_last_update' => now(),
        ]);

        $fixturesToEvents = $fixtures->pluck('event.id', 'id')->toArray();

        $cnt = count($events);
        $this->output->progressStart($cnt);

        Log::channel('commands')->info('TOTAL EVENTS: '.$cnt);

        $market_list = Market::pluck('id')->toArray();

        foreach ($events as $event) {
            try {
                DB::transaction(function () use ($event, $fixturesToEvents, $isInplay,$market_list) {
                    $this->output->progressAdvance(1);

                    if (empty($fixturesToEvents[$event->FixtureId])) {
                        $this->info(' ' . $event->FixtureId . ' = Skipped ');
                        return;
                    }

                    $eventId = $fixturesToEvents[$event->FixtureId];
                    //echo bets
                    $bets   = [];
                    //all event bets
                    $eventBets = [];

                    $dbBets = Bet::query();

                    $where_condition_step = 0;

                    $markets = collect($event->Markets)->pluck('Id')->unique()->toArray();
                    $bookmakers = [];

                    foreach ($event->Markets ?: [] as $market) {

                        foreach ($market->Providers ?: [] as $provider) {
                            if(isset($provider->Id)){
                                $bookmakers[] =$provider->Id;
                            }

                            Bet::whereIn('api_bet_id', collect($provider->Bets)->pluck('Id'))->where([
                                'bookmaker_id' => $provider->Id,
                                'market_id' => $market->Id,
                            ])->update([
                                'is_latest' => false,
                            ]);

                            foreach ($provider->Bets ?: [] as $bet) {
                                if(!$where_condition_step){
                                    $dbBets = $dbBets->whereRaw('(api_bet_id = ? AND  last_update= ? )',[ $bet->Id, Carbon::parse($bet->LastUpdate) ]);
                                } else{
                                    $dbBets = $dbBets->orWhereRaw('(api_bet_id = ? AND   last_update= ? )',[ $bet->Id, Carbon::parse($bet->LastUpdate)]);
                                }
                                /*
                                                            Bet::updateOrInsert([
                                                                'api_bet_id' => $bet->Id,
                                                                'last_update' => Carbon::parse($bet->LastUpdate),
                                                            ], [
                                                                'event_id' => $eventId,
                                                                'bookmaker_id' => $provider->Id,
                                                                'market_id' => $market->Id,
                                                                'status_id' => $bet->Status,
                                                                'settlement_id' => $bet->Settlement ?? null,
                                                                'name' => $bet->Name,
                                                                'line' => $bet->Line ?? null,
                                                                'base_line' => $bet->BaseLine ?? null,
                                                                'start_price' => $bet->StartPrice,
                                                                'price' => $bet->Price,
                                                                'is_inplay' => false,
                                                                'is_latest' => true,
                                                                'open_status_date' => $bet->Status == Bet::OPEN_BET ? Carbon::now() : null,
                                                                'suspended_status_date' => $bet->Status == Bet::SUSPENDED_BET ? Carbon::now() : null,
                                                                'settled_status_date' => $bet->Status == Bet::SETTLED_BET ? Carbon::now() : null,
                                                            ]);
                                */
                                if(in_array($market->Id,self::ONLY_MARKETS)){
                                    $bets[] = [
                                        'api_bet_id' => $bet->Id,
                                        'last_update' => Carbon::parse($bet->LastUpdate),
                                        'event_id' => $eventId,
                                        'bookmaker_id' => $provider->Id,
                                        'market_id' => $market->Id,
                                        'status_id' => $bet->Status,
                                        'settlement_id' => $bet->Settlement ?? null,
                                        'name' => $bet->Name,
                                        'line' => $bet->Line ?? null,
                                        'base_line' => $bet->BaseLine ?? null,
                                        'start_price' => $bet->StartPrice,
                                        'price' => $bet->Price,
                                        'is_inplay' => false,
                                        'is_latest' => true,
                                        'open_status_date' => $bet->Status == Bet::OPEN_BET ? Carbon::now() : null,
                                        'suspended_status_date' => $bet->Status == Bet::SUSPENDED_BET ? Carbon::now() : null,
                                        'settled_status_date' => $bet->Status == Bet::SETTLED_BET ? Carbon::now() : null,
                                        'bookmaker' => [],
                                        'market'    => []
                                    ];
                                }
                                // all event bets
                                if(in_array($market->Id, $market_list)){
                                    $eventBets[] = [
                                        'api_bet_id'            => $bet->Id,
                                        'last_update'           => Carbon::parse($bet->LastUpdate),
                                        'event_id'              => $eventId,
                                        'bookmaker_id'          => $provider->Id,
                                        'market_id'             => $market->Id,
                                        'status_id'             => $bet->Status,
                                        'settlement_id'         => $bet->Settlement ?? null,
                                        'name'                  => $bet->Name,
                                        'line'                  => $bet->Line ?? null,
                                        'base_line'             => $bet->BaseLine ?? null,
                                        'start_price'           => $bet->StartPrice,
                                        'price'                 => $bet->Price,
                                        'is_inplay'             => 0,
                                        'is_latest'             => 1,
                                        'open_status_date'      => $bet->Status == Bet::OPEN_BET ? Carbon::now() : null,
                                        'suspended_status_date' => $bet->Status == Bet::SUSPENDED_BET ? Carbon::now() : null,
                                        'settled_status_date'   => $bet->Status == Bet::SETTLED_BET ? Carbon::now() : null,
                                    ];
                                }
                                $where_condition_step++;
                            }
                        }
                    }

                    if($where_condition_step){
                        echo 'INSERT OR UPDATE'.PHP_EOL;

                        $dbBets         = $dbBets->get();

                        if($dbBets->count()){
                            $eventBets      = collect($eventBets);
                            $betsToUpdate   = $eventBets->filter(function ($bet) use ($dbBets) {
                                return $dbBets->filter(function ($item) use ($bet) {
                                    return ($item->api_bet_id ==$bet['api_bet_id']) && ($item->last_update ==$bet['last_update']);
                                })->count();
                            });
                            /*->map(function ($item) use ($dbBets){

                                $db_bet = $dbBets->filter(function ($db_item) use($item){
                                    return ($db_item->api_bet_id ==$item['api_bet_id']) && ($db_item->last_update ==$item['last_update']);
                                })->values()->toArray();
                                    echo  $db_bet[0]['id'];
                                if($db_bet && sizeof($db_bet)){
                                    return [
                                        'id'                    => $db_bet[0]['id'],
                                        'api_bet_id'            => $item['api_bet_id'],
                                        'last_update'           => Carbon::parse($item['last_update']),
                                        'event_id'              => $item['event_id'],
                                        'bookmaker_id'          => $item['bookmaker_id'],
                                        'market_id'             => $item['market_id'],
                                        'status_id'             => $item['status_id'],
                                        'settlement_id'         => $item['settlement_id'] ?? null,
                                        'name'                  => $item['name'],
                                        'line'                  => $item['line'] ?? null,
                                        'base_line'             => $item['base_line'] ?? null,
                                        'start_price'           => $item['start_price'],
                                        'price'                 => $item['price'],
                                        'is_inplay'             => false,
                                        'is_latest'             => true,
                                        'open_status_date'      => $item['open_status_date'],
                                        'suspended_status_date' => $item['suspended_status_date'],
                                        'settled_status_date'   => $item['settled_status_date'],
                                    ];
                                } else {
                                    return [];
                                }
                            })->toArray();*/
                            $tmp = [];
                            foreach ($betsToUpdate as $betToUpdate){
                                foreach ($dbBets as $dbBet) {
                                    if(($dbBet->api_bet_id ==$betToUpdate['api_bet_id'])
                                        && ($dbBet->last_update ==$betToUpdate['last_update'])
                                        && in_array($betToUpdate['market_id'],$market_list))
                                    {
                                        $tmp [] = [
                                            'id'                    => $dbBet->id,
                                         //   'api_bet_id'            => $betToUpdate['api_bet_id'],
                                         //   'last_update'           => Carbon::parse($betToUpdate['last_update']),
                                          //  'event_id'              => $betToUpdate['event_id'],
                                            'bookmaker_id'          => $betToUpdate['bookmaker_id'],
                                            'market_id'             => $betToUpdate['market_id'],
                                            'status_id'             => $betToUpdate['status_id'],
                                         //   'settlement_id'         => $betToUpdate['settlement_id'] ?? null,
                                            'name'                  => $betToUpdate['name'],
                                            'line'                  => $betToUpdate['line'] ?? null,
                                            'base_line'             => $betToUpdate['base_line'] ?? null,
                                            'start_price'           => $betToUpdate['start_price'],
                                            'price'                 => $betToUpdate['price'],
                                            'is_inplay'             => '0',
                                            'is_latest'             => '1',
                                            'open_status_date'      => $betToUpdate['open_status_date'],
                                            'suspended_status_date' => $betToUpdate['suspended_status_date'],
                                           'settled_status_date'   =>  $betToUpdate['settled_status_date'],
                                        ];
                                    }
                                }
                            }

                            $betsToUpdate = $tmp;

                            $betsToInsert   = $eventBets->filter(function ($bet) use ($dbBets, $market_list) {
                                return !($dbBets->filter(function ($item) use ($bet,$market_list) {
                                    return ($item->api_bet_id ==$bet['api_bet_id']) && ($item->last_update ==$bet['last_update']) && in_array($bet['market_id'],$market_list);
                                })->count());
                            });
                            //INSERT
                            echo 'BULK INSERT'.PHP_EOL;
                            foreach (  array_chunk($betsToInsert->toArray(), 250) as $data) {
                                Bet::insert($data);
                            }
                           // Bet::insert($betsToInsert->toArray());
                            //UPDATE
                            //echo 'To update--'.$this->updateBatch('bet',$betsToUpdate).PHP_EOL;
                            echo 'BULK UPDATE'.PHP_EOL;
                            foreach (  array_chunk($betsToUpdate, 100) as $data) {
                                $this->updateBatch('bet',$data);
                            }
                           // $this->updateBatch('bet',$betsToUpdate);
                        } else{
                            foreach (  array_chunk($eventBets, 250) as $data) {
                                Bet::insert($data);
                            }
                        }
                        unset($dbBets, $eventBets);

                        echo 'INSERT success'.PHP_EOL;
                        if ($event->Livescore) {
                            /** @var Carbon $fixtureDate */
                            $fixtureDate = Fixture::whereId($event->FixtureId)->value('start_date');

                            if ($fixtureDate->greaterThan(now())) {
                                $this->output->error('Livescore for future fixture: ' . $event->FixtureId);
                            } else {
                                Scoreboard::updateOrInsert([
                                    'event_id' => $eventId,
                                ], [
                                    'status_id' => $event->Livescore->Scoreboard->Status,
                                    'time' => $event->Livescore->Scoreboard->Time,
                                    'results' => collect($event->Livescore->Scoreboard->Results)->map(function ($result) {
                                        return $result->Value;
                                    })->join(':'),
                                ]);
                                foreach ($event->Livescore->Periods as $period) {
                                    Period::updateOrInsert([
                                        'type_id' => $period->Type,
                                        'event_id' => $eventId,
                                    ], [
                                        'is_finished' => $period->IsFinished,
                                        'is_confirmed' => $period->IsConfirmed,
                                        'results' => collect($period->Results)->map(function ($result) {
                                            return $result->Value;
                                        })->join(':'),
                                    ]);

                                    $periodId = Period::where([
                                        'type_id' => $period->Type,
                                        'event_id' => $eventId,
                                    ])->first()->id;

                                    foreach ($period->Incidents ?? [] as $incident) {
                                        Incident::updateOrInsert([
                                            'period_id' => $periodId,
                                            'seconds' => $incident->Seconds,
                                            'participant_position' => $incident->ParticipantPosition,
                                            'player_name' => $incident->PlayerName,
                                            'type_id' => $incident->IncidentType,
                                        ]);
                                    }
                                }
                            }
                        }
                        //get new average
                        if(sizeof($bets)){
                            $average_bets = Bet::select([DB::raw('avg(price) as average'),DB::raw('max(price) as highest'),'market_id', 'name','base_line'])
                                ->where('event_id',$eventId)
                                ->whereIn('market_id',array_values(array_intersect($markets,self::ONLY_MARKETS)))
                                ->where('is_latest',1)
                                ->groupBy(['market_id','base_line', 'name'])
                                ->get()
                                ->map(function ($item) use($eventId){
                                    return[
                                        'market_id' => $item->market_id,
                                        'average'   => (new OddsConvector($item->average))->handle(),
                                        'highest'   => $item->highest,
                                        'name'      => $item->name,
                                        'base_line' => $item->base_line,
                                        'event_id'  => $eventId
                                    ];
                                })->toArray();
                            //add to redis
                            $data = [];

                            foreach ($average_bets as $average_bet) {
                                $key = 'average#'.$average_bet['event_id'].'#'.$average_bet['market_id'].'#'.$average_bet['name'];
                                if(!is_null($average_bet['base_line'])) {
                                    $key .= $average_bet['base_line'];
                                }
                                $data[$key] = $average_bet['highest'].'#'.$average_bet['average'];
                            }

                            if(sizeof($data)){

                                $fixtureDate = Fixture::whereId($event->FixtureId)->value('start_date');

                                if($fixtureDate->greaterThan(now()->setTime(0,0,0))){
                                    $seconds = $fixtureDate->addHours(48)->diffInSeconds(now());
                                } else{
                                    $seconds = 3600;
                                }

                                Redis::pipeline(function ($pipe) use ($data,$seconds) {
                                    foreach ($data as $key => $value) {
                                        $pipe->setEX($key,$seconds,$value);
                                    }
                                });
                            }
                            //need to add bookmaker info, and market info to broadcast bets
                            $bookmakerList  = Bookmaker::whereIn('id', array_unique($bookmakers) )->get();
                            $marketList     = Market::whereIn('id', array_unique($markets))->get();

                            $bets = collect($bets)->map(function ($bet) use($bookmakerList, $marketList) {

                                $bet['market'] = $marketList->filter(function ($item) use ($bet) {
                                    return $item->id == $bet['market_id'];
                                })->first()->toArray();

                                $bet['bookmaker'] = $bookmakerList->filter(function ($item) use ($bet) {
                                    return $item->id == $bet['bookmaker_id'];
                                })->first()->toArray();

                                return $bet;

                            })->toArray();

                            $eventAverage = $average_bets;
                            event(new BetsUpdated($eventId, $bets, $eventAverage));
                        }
                    }
                });
            } catch (\Exception $e) {
                Log::channel('commands')->info('FIXTURE '.$event->FixtureId.' ERROR CONSUME: ' . $e->getMessage().$e->getLine().$e->getFile());
             //   $this->output->error('Unable to update cefs: ' . $e->getMessage());
            }
        }

        $this->output->progressFinish();
    }

    private function fetchAndUpdateCoefficients(Collection $fixtures)
    {
        $bookmakers = Bookmaker::select(['id'])->get();

        if ($fixtures->isEmpty()) {
            $this->output->warning('No fixtures to update');
            return;
        }

        $events = $this->pullOperations->getEvents([
            'Fixtures' => $fixtures->pluck('id')->join(','),
            'Bookmakers' => $bookmakers->pluck('id')->join(','),
        ]);

        $this->updateCoefficients($events, $fixtures);
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        Log::channel('commands')->info('Command {pull:coefficients}, mode {' . $this->argument('mode') . '}' );
        switch ($this->argument('mode')) {
            case 'instant':
                $this->pullInstantly();
                break;

            case 'near':
                $this->pullNearest();
                break;

            case 'all':
                $this->pullAll();
                break;

            default:
                $this->error('Unknown mode');
        }
        Log::channel('commands')->info('Command {pull:coefficients}, mode {' . $this->argument('mode') . '}'  . ' END!');
        return 1;
    }

    private function updateBatch($tableName = "", $multipleData = []){

        if( $tableName && !empty($multipleData) ) {

            // column or fields to update
            $updateColumn = array_keys($multipleData[0]);
            $referenceColumn = $updateColumn[0]; //e.g id
            unset($updateColumn[0]);
            $whereIn = "";

            $q = "UPDATE ".$tableName." SET ";
            foreach ( $updateColumn as $uColumn ) {
                $q .=  $uColumn." = CASE ";

                foreach( $multipleData as $data ) {

                    if($referenceColumn =='is_inplay'){
                        $q .= "WHEN ".$referenceColumn." = ".$data[$referenceColumn]." THEN 0 ";

                    }else if($referenceColumn =='is_latest'){
                        $q .= "WHEN ".$referenceColumn." = ".$data[$referenceColumn]." THEN 1 ";
                    }else if($referenceColumn =='name'){
                        $q .= "WHEN ".$referenceColumn." = ".$data[$referenceColumn]." THEN '".addslashes($data[$uColumn])."' ";
                    }  else {

                        if(!($data[$uColumn] == '') && !is_null($data[$uColumn])){
                            $q .= "WHEN ".$referenceColumn." = ".$data[$referenceColumn]." THEN '".$data[$uColumn]."' ";
                        } else{
                            $q .= "WHEN ".$referenceColumn." = ".$data[$referenceColumn]." THEN  NULL ";
                        }
                    }



                }
                $q .= "ELSE ".$uColumn." END, ";
            }
            foreach( $multipleData as $data ) {
                $whereIn .= "'".$data[$referenceColumn]."', ";
            }
            $q = rtrim($q, ", ")." WHERE ".$referenceColumn." IN (".  rtrim($whereIn, ', ').")";

            // Update
              return DB::update(DB::raw($q));
     /*       // to test
            return $q;*/

        } else {
            return false;
        }

    }
}
