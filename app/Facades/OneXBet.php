<?php


namespace App\Facades;

use Illuminate\Support\Facades\Facade;

class OneXBet extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'oneXBet';
    }
}
