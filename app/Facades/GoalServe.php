<?php


namespace App\Facades;


use Illuminate\Support\Facades\Facade;

class GoalServe extends Facade
{
    /**
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return 'goal-serve-client';
    }
}
