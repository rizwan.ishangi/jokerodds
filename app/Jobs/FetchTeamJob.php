<?php

namespace App\Jobs;

use App\Facades\GoalServe;
use App\Libraries\Base64ToStaticImage;
use App\Models\GoalServeTeam;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\Storage;

class FetchTeamJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * @var int
     */
    private $teamID;
    /**
     * @var int
     */
    private $goalTeamID;

    /**
     * FetchTeamJob constructor.
     * @param int $teamID
     * @param int $goalTeamID
     */
    public function __construct(int $teamID, int $goalTeamID)
    {
        $this->teamID = $teamID;
        $this->goalTeamID = $goalTeamID;
    }

    /**
     * Get team info from goal-serve
     * Fetch team logo
     * Convert base64 logo to static and save locally
     * Create db record with logo
     */
    public function handle()
    {
        $data = GoalServe::getTeam($this->goalTeamID);
        $team = $data->team->only('id', 'name', 'image');

        $model = GoalServeTeam::where('team_id', $team->id)->first() ?? (new GoalServeTeam);

        if (!is_null($team->image)) {
            if (!is_null($model->image) && Storage::disk('public')->exists($model->image)) {
                Storage::disk('public')->delete($model->image);
            }
            $model->fill([
                'participant_id' => $this->teamID,
                'team_id' => intval($team->id),
                'name' => $team->name,
                'image' => Base64ToStaticImage::convert($team->image, 'teams')
            ]);

            $model->save();
        }
    }
}
