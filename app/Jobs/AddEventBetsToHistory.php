<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\DB;

class AddEventBetsToHistory implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    private $event_id;
    protected const ONLY_MARKETS = [
        1,
        282,
        284,
        3,
        53,
        283,
        2,
        77,
        337,
        7,
        25,
        151,
        4,
    ];

    public function __construct(int $event_id)
    {
        //
        $this->event_id = $event_id;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        //
        $bets = DB::table('bet')
            ->whereIn('market_id', self::ONLY_MARKETS)
            ->where('event_id',$this->event_id)
            ->get();

        $bets = $bets->groupBy('event_id')->map(function ($bets){

            $bets = collect($bets);

            return $bets->groupBy('bookmaker_id')->map(function ($bets){

                $bets = collect($bets);

                return $bets->groupBy('market_id')->map(function ($bets,$key){

                    $bets = collect($bets);

                    return $bets->groupBy('name')->map(function ($bets){

                        $bets = collect($bets);
                        $count = $bets->count();

                        return $bets->filter(function ($bet, $key) use($count){
                            return ($key==0) || ($key== ($count-1));
                        });

                    });
                });
            });
        });

        $bets = $bets->flatten()->map(function ($bet){
                return[
                    "api_bet_id"            => $bet->api_bet_id,
                    "event_id"              => $bet->event_id,
                    "bookmaker_id"          => $bet->bookmaker_id,
                    "market_id"             => $bet->market_id,
                    "status_id"             => $bet->status_id,
                    "settlement_id"         => $bet->settlement_id,
                    "name"                  => $bet->name,
                    "line"                  => $bet->line,
                    "base_line"             => $bet->base_line,
                    "start_price"           => $bet->start_price,
                    "price"                 => $bet->price,
                    "last_update"           => $bet->last_update,
                    "is_latest"             => $bet->is_latest,
                    "is_inplay"             => $bet->is_inplay,
                    "open_status_date"      => $bet->open_status_date,
                    "suspended_status_date" => $bet->suspended_status_date,
                    "settled_status_date"   => $bet->settled_status_date,
                ];
        })->toArray();

        DB::table('bet_history')->insert($bets);
    }
}
