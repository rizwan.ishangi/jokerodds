<?php


namespace App\Helpers;
use Illuminate\Support\Carbon;
class TickToTimestamp
{
    private $tickDate;
    private $type;
    private $utc;

    public function __construct(string $tickDate, $type = 'datetime', $utc = 0)
    {
        $this->tickDate = $tickDate;
        $this->type = $type;
        $this->utc = $utc;
    }
    public static function transform(string $tickDate){
        return (new self($tickDate))->parse();
    }
    private function parse(){
      // Match the time stamp (microtime) and the timezone offset (may be + or -) and also negative Timestamps
      //  preg_match( '/\/Date\((-?\d+)([+-]\d{4})\)/', $this->tickDate, $matches);
        $matches[1] = str_replace(['(',')','/','Date'],'',$this->tickDate);
        $seconds  = $matches[1]/1000;                // microseconds to seconds

        if(isset($matches[2])){
            $UTCSec   = $matches[2]/100*60*60;           // utc timezone difference in seconds

            if($this->utc != 0){
                $seconds  = $seconds + $UTCSec; // add or divide the utc timezone difference
            }
        }
        $date       = Carbon::parse($seconds)->format('Y-m-d');       // only date
        $dateTime   = Carbon::parse($seconds)->format('Y-m-d H:i:s'); // date and time
        $time       = Carbon::parse($seconds)->format('H:i:s');       // only time
        $timestamp  = Carbon::parse($seconds)->timestamp;       // timestamp

        switch($this->type)
        {
            case 'date':
                return $date;
                break;

            case 'datetime':
                return $dateTime;
                break;

            case 'timestamp':
                return $timestamp;
                break;
            default: return $dateTime;
        }
    }
}
