<?php

namespace App\Traits;

use App\Models\Bet;
use App\Models\Bookmaker;
use App\Models\Event;
use App\Models\Fixture;
use App\Models\FixtureParticipant;
use App\Models\Incident;
use App\Models\League;
use App\Models\Location;
use App\Models\Market;
use App\Models\Participant;
use App\Models\Period;
use App\Models\Scoreboard;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Collection;

/**
 * Trait helps to load to system events from api for some range
 * @mixin Command
 */
trait EventsPullTrait
{
    private function pullEventsForRange(Carbon $fromDate, Carbon $toDate)
    {
        /**
         * @doc: https://lsports.api-docs.io/v4/pre-match/api-pre-match-get-events
         */

        $locations = Location::pluck('id')->toArray();
        $bookmakers = Bookmaker::pluck('id')->toArray();
        $leaguesChunked = League::pluck('id')->chunk(100);
        $markets = Market::pluck('id')->toArray();

        $eventsCount = 0;

        /** @var Collection $leagues */
        foreach ($leaguesChunked as $leagues) {
            $events = $this->pullOperations->getEvents([
                'FromDate' => $fromDate->timestamp,
                'ToDate' => $toDate->timestamp,
                'Sports' => implode(',', self::LIMIT_SPORTS_IDS),
                'Locations' => implode(',', $locations),
                'Leagues' => $leagues->join(','),
                'Bookmakers' => implode(',', $bookmakers),
            ]);

            $eventsCount += count($events);
            $errors = 0;

            $this->output->progressStart(count($events));

            foreach ($events as $event) {
                try {
                    \DB::transaction(function () use ($event, $markets) {
                        $this->processEvent($event, $markets);
                    });
                } catch (\Exception $e) {
                    $this->output->error('Event failed: ' . $e->getMessage());
                    $errors++;
                }

                $this->output->progressAdvance(1);
            }

            $this->output->progressFinish();

            if ($errors > 0) {
                $this->output->error('Found ' . $errors . ' errors');
            }
        }

        return $eventsCount;
    }

    private function pullLeagueEventsForRange(int $leagueIds, Carbon $fromDate, Carbon $toDate)
    {
        /**
         * @doc: https://lsports.api-docs.io/v4/pre-match/api-pre-match-get-events
         */

        $locations = Location::pluck('id')->toArray();
        $bookmakers = Bookmaker::pluck('id')->toArray();
        $markets = Market::pluck('id')->toArray();
        $eventsCount = 0;

        $events = $this->pullOperations->getEvents([
            'FromDate' => $fromDate->timestamp,
            'ToDate' => $toDate->timestamp,
            'Sports' => implode(',', self::LIMIT_SPORTS_IDS),
            'Locations' => implode(',', $locations),
            'Leagues' => $leagueIds,
            'Bookmakers' => implode(',', $bookmakers),
        ]);

        $eventsCount += count($events);
        $errors = 0;

        $this->output->progressStart(count($events));

        foreach ($events as $event) {
            try {
                \DB::transaction(function () use ($event, $markets) {
                    $this->processEvent($event, $markets);
                });
            } catch (\Exception $e) {
                $this->output->error('Event failed: ' . $e->getMessage());
                $errors++;
            }

            $this->output->progressAdvance(1);
        }

        $this->output->progressFinish();

        if ($errors > 0) {
            $this->output->error('Found ' . $errors . ' errors');
        }

        return $eventsCount;
    }

    private function processEvent($event, $markets)
    {
        $fixtureId = $event->FixtureId;

        Fixture::updateOrInsert([
            'id' => $event->FixtureId,
        ], [
            'location_id' => $event->Fixture->Location->Id,
            'sport_id' => $event->Fixture->Sport->Id,
            'league_id' => $event->Fixture->League->Id,
            'status_id' => $event->Fixture->Status,
            'start_date' => Carbon::parse($event->Fixture->StartDate),
            'last_update' => Carbon::parse($event->Fixture->LastUpdate),
        ]);

        foreach ($event->Fixture->Participants as $participant) {
            if (!Participant::whereId($participant->Id)->exists()) {
                Participant::insert([
                    'id' => $participant->Id,
                    'name' => $participant->Name,
                ]);
            }

            FixtureParticipant::updateOrInsert([
                'fixture_id' => $fixtureId,
                'participant_id' => $participant->Id,
            ], [
                'position' => $participant->Position,
            ]);
        }

        $eventId = Event::whereFixtureId($fixtureId)->value('id');

        if (empty($eventId)) {
            $eventId = Event::insertGetId([
                'fixture_id' => $fixtureId,
                'bets_last_update' => now(),
            ]);
        }

        foreach ($event->Markets ?: [] as $market) {
            if (!in_array($market->Id, $markets)) {
                continue;
            }
            foreach ($market->Providers ?: [] as $provider) {
                foreach ($provider->Bets ?: [] as $bet) {
                    Bet::updateOrInsert([
                        'api_bet_id' => $bet->Id,
                        'last_update' => Carbon::parse($bet->LastUpdate),
                    ], [
                        'event_id' => $eventId,
                        'bookmaker_id' => $provider->Id,
                        'market_id' => $market->Id,
                        'status_id' => $bet->Status,
                        'settlement_id' => $bet->Settlement ?? null,
                        'name' => $bet->Name,
                        'line' => $bet->Line ?? null,
                        'base_line' => $bet->BaseLine ?? null,
                        'start_price' => $bet->StartPrice,
                        'price' => $bet->Price,
                        'is_inplay' => false,
                        'is_latest' => true,
                    ]);
                }
            }
        }

        if ($event->Livescore) {
            /** @var Carbon $fixtureDate */
            $fixtureDate = Carbon::parse($event->Fixture->StartDate);

            if ($fixtureDate->greaterThan(now())) {
                $this->output->error('Livescore for future fixture: ' . $event->FixtureId);
            } else {
                Scoreboard::updateOrInsert([
                    'event_id' => $eventId,
                ], [
                    'status_id' => $event->Livescore->Scoreboard->Status,
                    'time' => $event->Livescore->Scoreboard->Time,
                    'results' => collect($event->Livescore->Scoreboard->Results)->map(
                        function ($result) {
                            return $result->Value;
                        }
                    )->join(':'),
                ]);

                foreach ($event->Livescore->Periods as $period) {
                    Period::updateOrInsert([
                        'type_id' => $period->Type,
                        'event_id' => $eventId,
                    ], [
                        'is_finished' => $period->IsFinished,
                        'is_confirmed' => $period->IsConfirmed,
                        'results' => collect($period->Results)->map(function ($result) {
                            return $result->Value;
                        })->join(':'),
                    ]);

                    $periodId = Period::where([
                        'type_id' => $period->Type,
                        'event_id' => $eventId,
                    ])->first()->id;

                    foreach ($period->Incidents ?? [] as $incident) {
                        Incident::updateOrInsert([
                            'period_id' => $periodId,
                            'seconds' => $incident->Seconds,
                            'participant_position' => $incident->ParticipantPosition,
                            'player_name' => $incident->PlayerName,
                            'type_id' => $incident->IncidentType,
                        ]);
                    }
                }
            }
        }
    }
}
